Version 4
SHEET 1 1696 680
WIRE 224 32 192 32
WIRE 336 32 304 32
WIRE 416 32 336 32
WIRE 528 32 416 32
WIRE 192 80 192 32
WIRE 528 80 528 32
WIRE 336 96 336 32
WIRE 416 96 416 32
WIRE -96 160 -208 160
WIRE -32 160 -96 160
WIRE 48 160 -32 160
WIRE 144 160 128 160
WIRE -208 192 -208 160
WIRE -96 192 -96 160
WIRE -32 192 -32 160
WIRE -208 304 -208 272
WIRE -96 304 -96 256
WIRE -32 304 -32 272
WIRE 192 304 192 176
WIRE 336 304 336 176
WIRE 416 304 416 160
WIRE 528 304 528 160
FLAG -32 304 0
FLAG -96 304 0
FLAG 192 304 0
FLAG 336 304 0
FLAG 416 304 0
FLAG -32 160 Vgs
FLAG 336 32 Vds
FLAG 192 32 D
FLAG 528 304 0
FLAG -208 304 0
SYMBOL nmos 144 80 R0
WINDOW 3 -151 14 Left 2
SYMATTR InstName M1
SYMATTR Value CD4007UBn
SYMBOL voltage -32 176 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
WINDOW 0 35 61 Left 2
SYMATTR InstName Vgs
SYMATTR Value 3.129
SYMBOL res 144 144 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R1
SYMATTR Value 836.22k
SYMBOL cap -112 192 R0
WINDOW 0 -51 26 Left 2
WINDOW 3 -79 65 Left 2
SYMATTR InstName C1
SYMATTR Value 0.111�
SYMBOL voltage 336 80 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
WINDOW 3 -785 376 Left 2
SYMATTR Value PWL(0 0  1 1.022 2 2.030  3 3.016  4 4.027  5 5.006  6 6.001  7 7.03  8 8.03  9 9.02  10 10 11 11)
SYMATTR InstName Vds
SYMBOL cap 400 96 R0
SYMATTR InstName C2
SYMATTR Value 0.113�
SYMBOL res 320 16 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName RA
SYMATTR Value 17
SYMBOL res 512 64 R0
SYMATTR InstName RV
SYMATTR Value 11.2Meg
SYMBOL res -224 176 R0
WINDOW 0 -66 25 Left 2
WINDOW 3 -112 66 Left 2
SYMATTR InstName RV1
SYMATTR Value 11.2Meg
TEXT -312 488 Left 2 !.tran 10
TEXT -312 424 Left 2 !.model CD4007UBn VDMOS (nchan Kp=0.551m Vto=1.163582 Lambda=0.00446747 mfg=unipd)
TEXT -344 40 Left 2 ;Qmis'd(Vgs=3.129, Id=1.0976m, Vds=6.019)
