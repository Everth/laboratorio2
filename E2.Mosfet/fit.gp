###CONFIG GENERALE###
unset multiplot
reset
set tics nomirror
set term post eps color "Verdana, 14"
set encoding iso_8859_1
set out "1_2_fit.eps"
set fit error
set macros
text = "NaN w dots linecolor rgb 'white' title sprintf"
set key samplen 2
set xlabel "I_D [A]"
set ylabel "V_{DS} [V]"

###FIT LAMBDA STORTO PESATO###
set multiplot layout 1,2
set key spacing 3.5
set key bottom right
f(x) = q + m*x
fit f(x) "1_1.csv" every ::4 using 3:1:2 yerror via q,m 
l = -1/real(q)
l_err = l * abs(q_err/q)
r0 = m
r0_err = m_err
set xtics rotate by 315
set rmargin 6
plot "1_1.csv" using 3:1:2 every ::4 w yerror t "Dati\nSperimentali" , f(x) t sprintf("%f \n%+f*x", q, m) dt 2 lc rgb "black", @text("{/Symbol l}=%g \n{/Symbol \261} %g", l,l_err), @text("r_0=%f \n{/Symbol \261} %f", r0, r0_err), @text("{/Symbol c}^2 = %g", FIT_STDFIT**2)
set xzeroaxis
set key top left
plot "1_1.csv" using 3:($1-f($3)):2 every ::4 w yerror t "Residui"
unset xzeroaxis
unset multiplot


###FIT LAMBDA STORTO NON PESATO###
set out "1_2_fit_unitw.eps"
set multiplot layout 1,2
set key top left Left reverse
#f(x) = q + m*x
fit f(x) "1_1.csv" every ::4 using 3:1 via q,m
l = -1/real(q)
l_err = l*abs(q_err/q)
r0 = m
r0_err = m_err
sigma1 = FIT_STDFIT
set rmargin 6
plot "1_1.csv" using 3:1:(FIT_STDFIT) every ::4 w yerror t "Dati\nSperimentali", f(x) t sprintf("%f \n%+f*x", q, m) dt 2 lc rgb "black", @text("{/Symbol l}=%g \n {/Symbol \261} %g", l ,l_err), @text("r_0=%f \n {/Symbol \261} %f", r0, r0_err), @text("{/Symbol s}_{fit} = %g", FIT_STDFIT)
set xzeroaxis
plot "1_1.csv" using 3:($1-f($3)):(FIT_STDFIT) every ::4 w yerror t "Residui"
unset xzeroaxis
unset multiplot

set print "MOSFET_params"
print sprintf("%s\t%.6g\t%.6g", "lambda", l, l_err)
print sprintf("%s\t%.6g\t%.6g", "r0", r0, r0_err)
unset print

###QPT###
VdsQpt=6.019
VdsQpt_err=0.017413682599228
IdQpt=1.0976e-3
IdQpt_err=0.000506959934775e-3
VgsQpt=3.129
VgsQpt_err=0.009106152224366

###FIT KN GM STORTO PESATO###
set out "1_3_fit.eps"
reset key
reset xtic
set multiplot layout 2,1
fit f(x) "1_3.csv" u 5:3:4 yerror via q,m
set key top left Left reverse
set key samplen 2
set xlabel "Y [A^{\275}]"
set ylabel "V_{GS} [V]"
k_n = 2/m**2
k_n_err = 2*k_n*m_err/m
g_m = 2*IdQpt/(VgsQpt-q)
g_m_err = g_m*sqrt( (IdQpt_err/IdQpt)**2 + (VgsQpt_err/(VgsQpt-q))**2 + ((-q_err)/(VgsQpt-q))**2)
set xzeroaxis
plot "1_3.csv" using 5:3:4 w yerror t "Dati Sperimentali", f(x) t sprintf("%f + %f*x", q, m), @text("K_n = %f {/Symbol \261} %f", k_n , k_n_err), @text("V_{TN} = %f {/Symbol \261} %f", q, q_err), @text("g_m = %f {/Symbol \261} %f", g_m, g_m_err), @text("{/Symbol c^2} = %f", FIT_STDFIT**2)
plot "1_3.csv" using 5:($3-f($5)):4 w yerror t "Residui"
unset multiplot

###FIT KN GM STORTO NON PESATO###
set out "1_3_fit_unitw.eps"
reset key
reset xtic
set multiplot layout 2,1
fit f(x) "1_3.csv" u 5:3 via q,m
set key top left Left reverse
set key samplen 2
set xlabel "Y [A^{\275}]"
set ylabel "V_{GS} [V]"
k_n = 2/m**2
k_n_err = 2*k_n*m_err/m
g_m = 2*IdQpt/(VgsQpt-q)
sigma2 = FIT_STDFIT
set xzeroaxis
g_m_err = g_m*sqrt( (IdQpt_err/IdQpt)**2 + (VgsQpt_err/(VgsQpt-q))**2 + ((-q_err)/(VgsQpt-q))**2)
plot "1_3.csv" using 5:3:(FIT_STDFIT) w yerror t "Dati Sperimentali", f(x) t sprintf("%f + %f*x", q, m), @text("K_n = %f {/Symbol \261} %f", k_n , k_n_err), @text("V_{TN} = %f {/Symbol \261} %f", q, q_err), @text("g_m = %f {/Symbol \261} %f", g_m, g_m_err), @text("{/Symbol s}_{fit} = %f", FIT_STDFIT)
plot "1_3.csv" using 5:($3-f($5)):(FIT_STDFIT) w yerror t "Residui"
unset multiplot

set print "MOSFET_params" append
print sprintf("%s\t%.6g\t%.6g", "kn", k_n, k_n_err)
print sprintf("%s\t%.6g\t%.6g", "gm", g_m, g_m_err)
print sprintf("%s\t%.6g\t%.6g", "Vtn", q, q_err)
unset print

set out "confronto1.eps"
set multiplot layout 2,1
set key bottom right
set format y "%.1s %c"
set ylabel "I_D [A]"
set xlabel "V_{DS} [V]"
plot [:] "1_1.csv" using 1:3:4 w yerror pt 2 lc rgb "#00CCCC" t "Dati Sperimentali", "MOS_1_Qpt.txt" every ::5 using 2:3 w l ls 1 dt 2 lc rgb "black" t "Simulazione Spice"

set ylabel "I_D [A]"
set xlabel "V_{GS} [V]"
plot "1_3.csv" using 3:1:2 w yerror pt 2 lc rgb "#00CCCC" t "Dati Sperimentali", "MOS_1_2Qptpar.txt" every ::2 using 2:3 w l ls 1 dt 2 lc rgb "black" t "Simulazione Spice"
unset multiplot

