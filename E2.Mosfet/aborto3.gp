reset
load "macros_2.gp"
@postcolor
set out "aborto3.eps"
set term post eps color "Verdana, 14" size 20cm,7cm
set samples 100000
f(x) = a*sin(b*2*pi*x)+e
g(x) = c*sin(d*2*pi*x)+f

set multiplot layout 1,2 margins 0.08, 0.95, 0.1, 0.98 spacing 0.08,0.08 

set format x "%.2s%c"

set offset 0,0,0.07,0
a = 0.1
c = -0.074
e=0
b=10000
d=10000
fit f(x) "MOS_3.5_CS_NObypass.txt" using 1:2  via a,b,e 
fit g(x) "MOS_3.5_CS_NObypass.txt" using 1:3  via c,d,f 
plot "MOS_3.5_CS_NObypass.txt" using 1:3, '' using 1:2 pt 1, g(x) t sprintf("%f sin(%f*2{/Symbol p}x)%+f",c,d,f), f(x) t sprintf("%f sin(%f*2{/Symbol p}x)%+f",a,b,e), NaN lc rgb "white" t sprintf("A^{sim'd} = %.3f %+f", real(a)/c, abs(real(a)/c)*sqrt((a_err/a)**2 + (c_err/c)**2))

set offset 0,0,0.05,0
a = 0.1
c = 0.1
e=0
b=10000
d=10000
fit f(x) "MOS_3.6_CDvero.txt" using 1:2 via a,b,e 
fit g(x) "MOS_3.6_CDvero.txt" using 1:3 via c,d,f 
set label 1 "Tensione [V]" at screen 0.02, 0.5 center rotate by 90
set label 2 "Tempo [s]" at screen 0.5,0.02 center
plot "MOS_3.6_CDvero.txt" using 1:3, '' using 1:2 pt 1, g(x) t sprintf("%f sin(%f*2{/Symbol p}x)%+f",c,d,f), f(x) t sprintf("%f sin(%f*2{/Symbol p}x)%+f",a,b,e), NaN lc rgb "white" t sprintf("A^{sim'd} = %.3f %+f", real(a)/c, abs(real(a)/c)*sqrt((a_err/a)**2 + (c_err/c)**2))
unset multiplot
unset label 1
unset label 2

set format x "%.1s%c"
set out "aborto_4_1.eps"
###AMP DIFFERENZIALE###
set multiplot layout 1,2 margins 0.08, 0.95, 0.1, 0.98 spacing 0.08,0.08 
set offset 0,0,0.3,0
a = -0.1
c = 0.4
e=0
b=10000
d=10000
set xlabel "V_1 [V]"
set ylabel "V_{out,1} - V_{out,2}"
fit f(x) "MOS_4.1_ampdifferenzialeV1=0.txt" using 1:2  via a,b,e 
fit g(x) "MOS_4.1_ampdifferenzialeV1=0.txt" using 1:3  via c,d,f
plot "MOS_4.1_ampdifferenzialeV1=0.txt" using 1:2, '' using 1:3, g(x) t sprintf("%f sin(%f*2{/Symbol p}x)%+f",c,d,f), f(x) t sprintf("%f sin(%f*2{/Symbol p}x)%+f",a,b,e), NaN lc rgb "white" t sprintf("A^{sim'd} = %.3f %+f", real(a)/c, abs(real(a)/c)*sqrt((a_err/a)**2 + (c_err/c)**2)) 


set offset 0,0,0.35,0
a = -0.4
c = 0.1
e=0
b=10000
d=10000
set xlabel "V_2 [V]"
set ylabel "V_{out,1} - V_{out,2}"
fit f(x) "MOS_4.1_ampdifferenzialeV2=0.txt" using 1:2  via a,b,e 
fit g(x) "MOS_4.1_ampdifferenzialeV2=0.txt" using 1:3  via c,d,f
plot "MOS_4.1_ampdifferenzialeV2=0.txt" using 1:2, '' using 1:3, g(x) t sprintf("%f sin(%f*2{/Symbol p}x)%+f",c,d,f), f(x) t sprintf("%f sin(%f*2{/Symbol p}x)%+f",a,b,e), NaN lc rgb "white" t sprintf("A^{sim'd} = %.3f %+f", real(a)/c, abs(real(a)/c)*sqrt((a_err/a)**2 + (c_err/c)**2))

unset multiplot
