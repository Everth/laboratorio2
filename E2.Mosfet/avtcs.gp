gm = 0.001126859123552
gm_err = 5.93842397724263E-06
kr = 1.0
kr_err = 0.0007/sqrt(3)
Rd = 5188.7
Rd_err = 2.14725233481458
Rs = 2935
Rs_err = 1.27291924986099
ro = 208647.448816873
ro_err = 103.109652798977
A0 = -198.0/148
A0_err = 0.06359555416464
A1 = -gm*kr*Rd/(1+gm*Rs)
A1_err = A1*sqrt( ((A1*gm_err)/(gm**2*kr*Rd))**2 + (kr_err*A1/(kr**2*gm*Rd))**2 + (Rd_err/Rd)**2 + (A1*Rs_err/Rd)**2 )
A2 = -gm /(1/ro + (1+gm*Rs)/Rd) # = -gm * Rd * (1+gm*Rs)*ro /( (1+ gm*Rs) * (Rd + (1+gm*Rs)*ro))
den = (gm*ro*kr*Rs + ro + kr*Rd)
A2_err = (gm*ro*Rd*kr)/den**2 * sqrt( (gm_err*(ro+kr*Rd)/gm)**2 + (ro_err*kr*Rd/ro)**2 + (kr_err*ro/kr)**2 + (Rd_err*ro*(gm*kr*Rs+1.0)/Rd)**2 + (Rs_err*gm*ro*kr)**2)
print sprintf("A_{vt}^{CS,exp} = %f %f", A0, A0_err )
print sprintf("A_{vt}^{CS,easy} = %f %f", A1 , A1_err )
print sprintf("A_{vt}^{CS,hard} = %f %f", A2, A2_err )
print sprintf("Compatibilità %f", abs(A0-A2)/sqrt(A0_err**2 + A2_err**2))

Rl = 554.2
Rl_err = 0.323272574566211

A4 = gm*Rs/(1.0+gm*Rs)
A4_err = abs(A4)/(1+gm*Rs)*sqrt( (Rs_err/Rs)**2 + (gm_err/gm)**2 )

A5 = 114.0/150.0
A5_err = 0.030509937664576
print sprintf("A_{vt}^{CD,th} = %f %f", A4, A4_err)
print sprintf("A_{vt}^{CD,exp} = %f %f", A5, A5_err)
print sprintf("Compatibilità %f", abs(A4-A5)/sqrt(A4_err**2 + A5_err**2))

RoutTh = Rs/(1.0+gm*Rs)
RoutTh_err = RoutTh/(1+gm*Rs) * sqrt( (Rs_err/Rs)**2 + (gm_err*Rs)**2 )

h = 0.442105263157895
h_err = 0.018579597998977

RoutExp = (1-h)/h * Rl
RoutExp_err = RoutExp * sqrt( (Rl_err/Rl)**2 + (1/h)**2*(h_err/(1-h))**2)
print sprintf("R_{out}^{th} = %f %f", RoutTh, RoutTh_err)
print sprintf("R_{out}^{exp} = %f %f", RoutExp, RoutExp_err)
print sprintf("Compatibilità %f", abs(RoutTh-RoutExp)/sqrt(RoutTh_err**2 + RoutExp_err**2))
