set macros
postcolor = "unset multiplot;\
reset;\
set tics nomirror ;\
set term post eps color 'Verdana, 14' ;\
set encoding iso_8859_1 ;\
set fit error;"
text = "NaN w dots linecolor rgb 'white' title sprintf"
textonly="set key center Left reverse;\
unset border;\
unset tics;\
unset xlabel;\
unset ylabel;\
set yrange [0:1]"
restoreplot = "set key top left;\
set bmargin;\
set tmargin;\
set lmargin;\
set rmargin;\
set yrange restore;\
set autoscale;\
set xlabel;\
set ylabel;\
set tics font ', 11';\
set border;"

