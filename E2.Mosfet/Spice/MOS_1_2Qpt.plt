[Transient Analysis]
{
   Npanes: 1
   {
      traces: 1 {524290,0,"sqrt(I(Ra)/(1+0.00452391*V(vds)))"}
      Parametric: "V(vgs)"
      X: (' ',1,2.7,0.1,3.8)
      Y[0]: ('m',0,0.024,0.002,0.044)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Units: "A�/V�" ('m',0,0,0,0.024,0.002,0.044)
      Log: 0 0 0
      LargePixels: 1
      GridStyle: 1
   }
}
