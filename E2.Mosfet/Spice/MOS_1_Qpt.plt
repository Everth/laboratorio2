[Transient Analysis]
{
   Npanes: 1
   {
      traces: 1 {34603010,0,"I(Ra)"}
      Parametric: "V(vds)"
      X: (' ',0,0,1,10)
      Y[0]: ('m',1,0,0.0001,0.0012)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Amps: ('m',0,0,1,0,0.0001,0.0012)
      Log: 0 0 0
      LargePixels: 1
      GridStyle: 1
   }
}
