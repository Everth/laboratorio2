[Transient Analysis]
{
   Npanes: 1
   {
      traces: 2 {524290,0,"V(out)"} {524291,0,"V(in)"}
      X: ('m',1,0,0.0001,0.001)
      Y[0]: ('m',0,-0.48,0.08,0.48)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: ('m',0,0,0,-0.48,0.08,0.48)
      Log: 0 0 0
      LargePixels: 1
      GridStyle: 1
   }
}
