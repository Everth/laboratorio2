[Transient Analysis]
{
   Npanes: 1
   {
      traces: 2 {524290,0,"V(n001)"} {524291,0,"V(out)"}
      X: ('m',0,0,0.001,0.01)
      Y[0]: ('m',0,-0.48,0.08,0.4)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: ('m',0,0,0,-0.48,0.08,0.4)
      Log: 0 0 0
      LargePixels: 1
      GridStyle: 1
   }
}
