kn = 0.000551032
kn_err = 5.3794e-06
Rd = 9.935e3
Rd_err = 6.12005093252227
Vgs = 5.0288
Vgs_err = 0.001
Vtn = 1.167146
Vtn_err = 0.009844
Vdd = 5.012
Vdd_err = 0.001
a = (kn*Rd)
b = (1 + kn*Rd*(Vgs-Vtn))
c = 2*Vdd
print "Vout con formula quadratica"
print (b - sqrt( b**2 - a*c ))/a


Ron= 1.0/(kn*(Vgs-Vtn))
Ron_err = Ron * sqrt( (kn_err/kn)**2 + (Vgs_err/(Vgs-Vtn))**2 + (Vtn_err/(Vgs-Vtn))**2 )
print sprintf("Ron = %f +- %f",Ron, Ron_err)

Vout = Vdd*Ron/(Rd+Ron)
Vout_err = Vout * sqrt( (Vdd_err/Vdd)**2 + (Rd_err/(Ron+Rd))**2 + ( (1.0/Ron)*(1.0/(1.0 + Ron/Rd))*Ron_err)**2)

print sprintf("Vout con Ron = %f +- %f ", Vout, Vout_err)
