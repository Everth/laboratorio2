reset
load "macros_2.gp"
@postcolor
set out "aborto_4_2.eps"
set term post eps color "Verdana, 14" size 20cm,7cm
set samples 100000
set key spacing 2.25

g_m = 1.127e-3
R_D = 5188.7
R_DMEZZI = 2195
A_RD = g_m*R_D
A_RDMEZZI = g_m*R_DMEZZI

ft_RD = 20*log10(A_RD/sqrt(2))
ft_RD_exp = 20*log10(5.6/sqrt(2))
ft_RDMEZZI = 20*log10(A_RDMEZZI/sqrt(2))
ft_RDMEZZI_exp = 20*log10(2.43/sqrt(2))

fL_RD= 9.1
fL_RD_simd= 8.91
fH_RD= 1.2e6
fH_RD_simd= 1.16e6
#fL_RDMEZZI= 2.84
fL_RDMEZZI_simd = 8.7
fH_RDMEZZI= 3e6
fH_RDMEZZI_simd = 2.84e6

set style line 3 lc rgb 'blue' lw 2 
set style line 4 lc rgb 'red' lw 2

set logscale x
set multiplot layout 1,2 margins 0.08, 0.95, 0.1, 0.98 spacing 0.08,0.08 
` awk '{gsub(/\(|\)|\,|\\u00b0/,"\t")}1' MOS_4.2_CS_bandapassantedossiRD.txt > test` 
set label 3 at fL_RD, ft_RD_exp left offset screen +0.01,screen  -0.2 gprintf("f_L^{exp}=\n%.2g\n\n", fL_RD).gprintf("f_L^{sim'd}=\n%.2g", fL_RD_simd) font ",20"
set label 4 at fH_RD, ft_RD_exp right offset screen -0.01,screen  -0.2 gprintf("f_H^{exp}=\n%.2s%c\n\n", fH_RD).gprintf("f_H^{sim'd}=\n%.2s%c", fH_RD_simd) font ",20"
set offset 0,0,6,0
set arrow 1 ls 4 from fL_RD, graph 0 to fL_RD, ft_RD_exp nohead
set arrow 2 ls 4 from fH_RD, graph 0 to fH_RD, ft_RD_exp nohead
set arrow 5 ls 3 from fL_RD_simd, graph 0 to fL_RD_simd, ft_RD nohead
set arrow 6 ls 3 from fH_RD_simd, graph 0 to fH_RD_simd, ft_RD nohead
plot  "test" every ::1 using 1:2 w l t gprintf("Amplificazione CS con R_D = %.4s%c", R_D), ft_RD_exp dt 1 t sprintf("A_{mid}^{exp}/{/Symbol @\326\140}2 = %.2f", ft_RD_exp), 20*log10(A_RD/sqrt(2)) dt 2 t sprintf("A_{mid}^{sim'd}/{/Symbol @\326\140}2 = %.2f", 20*log10(A_RD/sqrt(2)))
` awk '{gsub(/\(|\)|\,|\xc2\xb0/,"\t")}1' MOS_4.2_CS_bandapassantedossiRDMEZZI.txt > test`
set label 1 "Amplificazione [dB]" at screen 0.02, 0.5 center rotate by 90
set label 2 "Frequenza [Hz]" at screen 0.5,0.02 center
unset arrow 1
unset arrow 2
unset arrow 5
unset arrow 6
unset label 3
unset label 4
set label 5 at fL_RDMEZZI_simd, ft_RDMEZZI left offset screen +0.01, screen -0.2 gprintf("f_L^{sim'd}=\n%.2s%c\n", fL_RDMEZZI_simd) font ",20"
set label 6 at fH_RDMEZZI, ft_RDMEZZI_exp right offset screen -0.01, screen -0.2 gprintf("f_H^{exp}=\n%.2s%c\n\n", fH_RDMEZZI).gprintf("f_H^{sim'd}=\n%.2s%c", fH_RDMEZZI_simd) font ",20"
set offset 0,0,3,0
#set arrow 3 ls 4 from fL_RDMEZZI, graph 0 to fL_RDMEZZI, ft_RDMEZZI_exp nohead
set arrow 4 ls 4 from fH_RDMEZZI, graph 0 to fH_RDMEZZI, ft_RDMEZZI_exp nohead
set arrow 7 ls 3 from fL_RDMEZZI_simd, graph 0 to fL_RDMEZZI_simd, ft_RDMEZZI nohead
set arrow 8 ls 3 from fH_RDMEZZI_simd, graph 0 to fH_RDMEZZI_simd, ft_RDMEZZI nohead
plot  "test" every ::1 using 1:2 w l t gprintf("Amplificazione CS con R_D = %.4s%c", R_DMEZZI), ft_RDMEZZI_exp dt 1 t sprintf("A_{mid}^{exp}/{/Symbol @\326\140}2 = %.2f", ft_RDMEZZI_exp), 20*log10(A_RDMEZZI/sqrt(2)) dt 2 t sprintf("A_{mid}^{sim'd}/{/Symbol @\326\140}2 = %.2f", 20*log10(A_RDMEZZI/sqrt(2)))
`rm test`
unset multiplot
