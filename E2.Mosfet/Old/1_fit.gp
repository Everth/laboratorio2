###CONFIG GENERALE###
reset
set tics nomirror
set term post eps color "Verdana, 14"
set encoding iso_8859_1
set out "1_2_fit.eps"
set fit error
set macros
text = "NaN w dots linecolor rgb 'white' title sprintf"
set xlabel "V_{DS} [V]"
set ylabel "I_D [A]"
set offset 0.1, 0.1 

f(x) = q + m*x

###ERRORE A POSTERIORI###
set multiplot layout 2,1

#fit [4.2:] f(x) "1_1.csv" using 1:3:4 yerror via q,m 
fit [4.2:] f(x) "1_1.csv" using 1:3 via q,m 
l = m/q
l_err = l*sqrt((m_err/m)**2 + (q_err/q)**2)
r0 = 1/real(m)
set key bottom right
plot [:] [1e-3:1.2e-3] "1_1.csv" using 1:3:(FIT_STDFIT) w yerror t "Dati Sperimentali", "MOS_1_Qpt.txt" using 2:3 w p t "Simulazione Spice", f(x) t sprintf("%f + %f*x", q, m) dt 2 lc rgb "black", @text("{/Symbol l}=%g {/Symbol \261} %g", l,l_err), @text("r_0=%f", r0), @text("{/Symbol s}_{post}", FIT_STDFIT) 
VdsQ=6.019
set xzeroaxis
plot [4.2:] "1_1.csv" u 1:($3-f($1)):(FIT_STDFIT) w yerror ps 2
unset multiplot




###QUADRATIC###
set out "quadratic.eps"
set multiplot layout 2,1

g(x) = a + b*x + c*x**2
a = q
b = m
c = -0.1
fit [4:] g(x) "1_1.csv" using 1:3 via a,b,c
l2 = b/a
l2_err = l2*sqrt((b_err/b)**2 + (a_err/a)**2)
plot [:] [1e-3:1.2e-3] "1_1.csv" using 1:3:(FIT_STDFIT) w yerror t "Dati Sperimentali" ,"MOS_1_Qpt.txt" using 2:3 w p t "Simulazione Spice", "MOS_1_Qptlambdanew.txt" using 2:3 w p t "Simulazione Spice" , g(x) t sprintf("%g + %g*x + %g*x^2", a, b, c), @text("{/Symbol l} = %g {/Symbol \261} %g", l2, l2_err), @text("%g", FIT_WSSR)
plot [4:] "1_1.csv" u 1:($3-g($1)):(FIT_STDFIT) w yerror ps 2, "MOS_1_Qpt.txt" using 2:($3-f($2)) w p t "Simulazione Spice", "MOS_1_Qptlambdanew.txt" using 2:($3-f($2)) w p t "Simulazione Spice"
unset multiplot

y(x) = sqrt(x/(1+l*VdsQ))
fit [:] f(x) "1_3_fit.dat" using 3:(y($1)) via q,m 
set out "1_3_fit.eps"
set xlabel "V_{GS} [V]"
#set ylabel "{/Symbol \326} {/Symbol \140}{I_D/{(1+{/Symbol l}_n*V_{DS_Qpt})}} [A]"
set ylabel "Y [A^{\275}]"
set key top left Left reverse
IdQ=1.0976e-3
VgsQ=3.129
plot [:] "1_3_fit.dat" using 3:(y($1)) not , f(x) t sprintf("%f + %f*x", q, m), @text("K_n = %f", 2*m**2), @text("V_{TN} = %f", -q/real(m)), @text("g_m = %f", 2*IdQ/((VgsQ-(-q/real(m))))), @text("{/Symbol s}_{post} = %g", FIT_STDFIT)

#set table "ipsilon.txt"
#plot "1_3_fit.dat" using 3:(y($1))
#unset table
