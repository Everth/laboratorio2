###CONFIG GENERALE###
reset
set tics nomirror
set term post eps color "Verdana, 14"
set encoding iso_8859_1
set out "1_2_fit_storto.eps"
set key top left Left reverse
set fit error
set macros
text = "NaN w dots linecolor rgb 'white' title sprintf"
set xlabel "I_D [A]"
set ylabel "V_{DS} [V]"

set multiplot layout 1,2
f(x) = q + m*x
fit [:] f(x) "1_1.csv" every :::4 using 3:1:2 yerror via q,m 
#fit [1.08500E-03:] [4.5:] f(x) "1_1.csv" using 3:1 via q,m 
l = -1/real(q)
l_err = l * abs(q_err/q)
r0 = m
r0_err = m_err
set xtics rotate by 270
plot [1e-3:] [0:] "1_1.csv" using 3:1:2 w yerror t "Dati Sperimentali", "MOS_1_Qpt.txt" using 3:2 w p t "Simulazione Spice old" ,"MOS_1_Qptlambdanew.txt" using 3:2 w p t "Simulazione Spice", f(x) t sprintf("%f + %f*x", q, m) dt 2 lc rgb "black", @text("{/Symbol l}=%g {/Symbol \261} %g", l,l_err), @text("r_0=%f {/Symbol \261} %f", r0, r0_err), @text("{/Symbol c^2} = %g", FIT_STDFIT**2)
#plot [1e-3:] [0:] "1_1.csv" using 3:1:(FIT_STDFIT) w yerror t "Dati Sperimentali", "MOS_1_Qpt.txt" using 3:2 w p t "Simulazione Spice", f(x) t sprintf("%f + %f*x", q, m) dt 2 lc rgb "black", @text("{/Symbol l}=%g {/Symbol \261} %g", l,l_err), @text("r_0=%f {/Symbol \261} %f", r0, r0_err)
set xzeroaxis
plot [1.0900E-03:] "1_1.csv" u 3:($1-f($3)):2 w yerror , "MOS_1_Qpt.txt" using 3:($2-f($3)) w p t "Simulazione Spice old" ,"MOS_1_Qptlambdanew.txt" using 3:($2-f($3)) w p t "Simulazione Spice"
#plot [1.0900E-03:] "1_1.csv" u 3:($1-f($3)):(FIT_STDFIT) w yerror
#MOS_1_Qpt parametri da cambiare
unset xzeroaxis
unset multiplot

set out "1_2_fit_storto_nonpesato.eps"
set multiplot layout 1,2
f(x) = q + m*x
fit [:] [4.5:] f(x) "1_1.csv" using 3:1 via q,m 
#fit [1.08500E-03:] [4.5:] f(x) "1_1.csv" using 3:1 via q,m 
l = -1/real(q)
l_err = l * abs(q_err/q)
r0 = m
r0_err = m_err
plot [1e-3:] [0:] "1_1.csv" using 3:1:(FIT_STDFIT) w yerror t "Dati Sperimentali", "MOS_1_Qpt.txt" using 3:2 w p t "Simulazione Spice", f(x) t sprintf("%f + %f*x", q, m) dt 2 lc rgb "black", @text("{/Symbol l}=%g {/Symbol \261} %g", l,l_err), @text("r_0=%f {/Symbol \261} %f", r0, r0_err), @text("{/Symbol s}_{post} = %g", FIT_STDFIT)
set xzeroaxis
plot [1.0900E-03:] "1_1.csv" u 3:($1-f($3)):(FIT_STDFIT) w yerror


unset multiplot



fit [:] f(x) "1_3.csv" u 5:3:4 yerror via q,m
set out "1_3_fit_storto.eps"
set xlabel "Y [A^{\275}]"
set ylabel "V_{GS} [V]"
k_n = 2/m**2
k_n_err = 2*k_n*m_err/m
IdQ=1.0976e-3
VgsQ=3.129
plot [:] "1_3.csv" using 5:3:4 not w yerror,  "MOS_1_2Qpt.txt" using 3:2 w p t "Simulazione Spice", f(x) t sprintf("%f + %f*x", q, m), @text("K_n = %f {/Symbol \261} %f", k_n , k_n_err), @text("V_{TN} = %f {/Symbol \261} %f", q, q_err), @text("g_m = %f", 2*IdQ/(VgsQ-q))


fit [:] f(x) "1_3.csv" u 7:3:4 yerror via q,m
set out "1_3_fit_storto_y2.eps"
k_n = 2/m**2
k_n_err = 2*k_n*m_err/m
set key top left Left reverse
plot [:] "1_3.csv" using 7:3:4 not w yerror, "MOS_1_2Qptparab.txt" using 3:2 w p t "Simulazione Spice", f(x) t sprintf("%f + %f*x", q, m), @text("K_n = %f {/Symbol \261} %f", k_n , k_n_err), @text("V_{TN} = %f {/Symbol \261} %f", q, q_err), @text("g_m = %f", 2*IdQ/(VgsQ-q))
