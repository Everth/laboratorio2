\documentclass[fontsize=11pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[italian]{babel}
\usepackage[a4paper]{geometry}
\geometry{top=20mm, left=20mm, right=20mm}
\usepackage{array}
\usepackage[justification=centering]{caption}
\usepackage{amsmath}
\usepackage{gensymb}
\usepackage{mathrsfs}
\usepackage{graphicx}
\usepackage[T1]{fontenc}
\usepackage{caption}
\usepackage{tabularx}
\usepackage{catchfilebetweentags}
\DeclareGraphicsExtensions{.pdf,.png,.jpg}
\DeclareGraphicsRule{.png}{png}{.png.bb}{}
\fontencoding{T1}
\begin{document}
\title{\huge \textbf{Relazione di laboratorio} \\ \large {Esperienza con i transistor MOSFET nMOS enhancement mode} \\ \large Gruppo 28}
\date{8 Gennaio 2018}
\author{ Andrea Currò Dossi - 1122269 \quad Alessandro Lenoci - 1126038 \quad Samuele Albano - 1122661\\ }
\maketitle

\section{Obiettivo dell'esperienza}
L'obiettivo dell'esperienza di laboratorio è lo studio di un transistor nMOS  enhancement-mode e la realizzazione di amplificatori in tensione. L'esperienza si divide in diverse parti: \setlength{\itemsep}{0pt} 
\begin{enumerate}\setlength{\itemsep}{0pt}
\item Caratterizzazione del transistor nei pressi del punto di lavoro assegnato.
\item Studio di un inverter nMOS.
\item Studio di un amplificatore a singolo transistor in configurazione common-source e common-drain.
\item Studio di un amplificatore differenziale con due transistor.

\section{Analisi Dati}

\subsection{Determinazione sperimentale del Q-point}
\[ I_D ^\mathit{mis} = I_D \frac{R_\mathit{iD}}{R_\mathit{iD}+R_A} = I_D \frac{r_o}{r_o + R_A} \rightarrow I_D = \frac{r_o + R_A}{r_o} I_D ^\mathit{mis}\]
\subsection{Curva caratteristica $I_D$ vs. $V_\mathit{DS}$ e misura di $\lambda_n$ e $r_o$}
\[I_D = m V_\mathit{DS} + q = \frac{V_\mathit{DS}}{r_o} + \frac{1}{\lambda r_o} \rightarrow \lambda = m/q \: , \: r_o = 1/m \]
\subsection{Curva caratteristica $I_D$ vs. $V_\mathit{GS}$ e determinazione di $k_n$ e $V_\mathit{TN}$}
\[\sqrt{\frac{I_D}{1+\lambda_n V_\mathit{DS}}} = mV_\mathit{GS} +q \rightarrow k_n = 2m^2 \: , \: V_\mathit{TN} = - q/m\]
\[g_m = \frac{2I_D}{V_\mathit{GS}-V_\mathit{TN}}\]
\subsection{Utilizzo del MOSFET in zona di interdizione e conduzione: l'operatore logico NOR}
\[V_\mathit{GS} < V_\mathit{TN} \rightarrow V_\mathit{out} = V_\mathit{DD}\]
\[V_\mathit{GS} > V_\mathit{TN} \rightarrow V_\mathit{DS} = V_\mathit{DD} - R_D I_D \rightarrow I_D = \frac{V_\mathit{DD} - V_\mathit{DS}}{R_D} = k_n \left(V_\mathit{GS} - V_\mathit{TN} - \frac{1}{2} V_\mathit{DS} \right) V_\mathit{DS}\]
\[ k_n R_D V_\mathit{DS}^2 - 2V_\mathit{DS} \left(1+k_n R_D (V_\mathit{GS} - V_\mathit{TN})\right) + 2 V_\mathit{DD} = 0\]
\[V_\mathit{DS} = \frac{\left(1+k_n R_D (V_\mathit{GS} - V_\mathit{TN})\right) - \sqrt{\left(1+k_n R_D (V_\mathit{GS} - V_\mathit{TN})\right)^2 - 2 V_\mathit{DD} k_n R_d}}{k_nR_D}\]
\[V_\mathit{out} = \frac{\left(1+k_n R_D (V_\mathit{in} - V_\mathit{TN})\right) - \sqrt{\left(1+k_n R_D (V_\mathit{in} - V_\mathit{TN})\right)^2 - 2 V_\mathit{DD} k_n R_d}}{k_nR_D}\]
\subsection{Utilizzo del MOSFET in zona di saturazione: l'amplificatore in tensione}
\[V_\mathit{DS} > (V_\mathit{GS} - V_\mathit{TN}) \rightarrow V_\mathit{out} > (V_\mathit{in} - V_\mathit{TN})\]
\[g_m = \frac{2I_D}{V_\mathit{GS} - V_\mathit{TN}} = \frac{2(V_\mathit{DD} - V_\mathit{DS})}{R_D(V_\mathit{GS} - V_\mathit{TN})}\]
\[r_o = \left(\frac{1}{\lambda} +V_\mathit{DS}\right) \frac{1}{I_D} \simeq \frac{1}{\lambda I_D}\]
\[A_\mathit{vt} = \frac{V_\mathit{out}}{V_\mathit{in}} = -g_m r_o \parallel R_D = -g_m \frac{r_o R_D}{r_o + R_D} \simeq -g_m R_D\]
\subsection{Dimensionamento circuito}
\[g_m = \frac{2I_D}{V_\mathit{GS} - V_\mathit{TN}} \rightarrow A_\mathit{vt} = -g_m frac{r_o R_D}{r_o + R_D} \rightarrow R_D = \frac{r_o}{1 - g_m \frac{r_o}{A_\mathit{vt}}} = \frac{r_o}{1 - \frac{2I_D}{V_\mathit{GS} - V_\mathit{TN}} \frac{r_o}{A_\mathit{vt}}}\]
\[V_D = V_\mathit{DD} - R_D I_D \rightarrow V_\mathit{DS} = V_D - V_S \rightarrow V_S = V_\mathit{DD} - R_D I_D - V_\mathit{DS} = R_S I_D \rightarrow R_S = \frac{V_\mathit{DD} - V_\mathit{DS} - R_D I_D}{I_D} \]
\[V_G = V_\mathit{DD \frac{R_1}{R_1 + R_2}} \rightarrow R_2 = R_1 \frac{V_\mathit{DD} -V_G}{V_G}\]
\subsection{Misura del punto di lavoro}
\[I_D = \frac{V_S}{R_S} = \frac{V_\mathit{DD} - V_D}{R_D}\]
\subsection{Misura dell'amplificazione in tensione}
\[A_\mathit{vt} ^\mathit{CS} = -g_m \frac{R_D r_o}{R_D + r_o}\]
\subsection{Misura delle resistenze di ingresso e uscita}
\[A_\mathit{vt} ^\mathit{CS} = -g_m R_D \parallel r_o \simeq -g_m R_D\]
\[V_g = \frac{R_\mathit{in}}{R_\mathit{in} + R_I} V_\mathit{in} \rightarrow V_\mathit{out} = A_\mathit{vt} V_g = A_\mathit{vt} \frac{R_\mathit{in}}{R_\mathit{in} + R_I} V_\mathit{in} = A' V_\mathit{in} \rightarrow A'=A \frac{R_\mathit{in}}{R_\mathit{in} + R_I} \rightarrow h=\frac{A'}{A}\]
\[R_\mathit{in} \frac{h}{1-h} R_I\]
\[R_\mathit{out} = \frac{1-h}{h} R_L\]
\subsection{Configurazione senza capacità di bypass}
\[A_\mathit{vt} ^\mathit{CS} = -g_m \frac{R_D \parallel (1+g_m R_S)r_o}{1+g_m R_S} \simeq -g_m \frac{R_D}{1+g_m R_S}\]
\subsection{Configurazione Common-Drain (CD)}
\[A_\mathit{vt} ^\mathit{CD} = \frac{g_m R_S}{1+g_m R_S}\]
\[R_\mathit{out} ^\mathit{CD} = R_S \parallel \frac{1}{g_m} \simeq \frac{1}{g_m}\]
\subsection{Prototipo di amplificatore differenziale a MOSFET}
\[I_D = \frac{1}{2} k_n (V_\mathit{GS} - V_\mathit{TN})^2 (1+\lambda V_\mathit{DS})\]
\[g_m = \frac{2 I_D}{V_\mathit{GS} - V_\mathit{TN}}\]
\[V_2 = V_s + V_s ^{(2)}\]
\[V_1 = V_s + V_s ^{(1)}\]
\[V_s = g_m (V_s ^{(1)} + V_s ^{(1)}) R_s\]
\[V_\mathit{out} ^{(1)} = - g_m V_s ^{(1)} R_d ^{(1)}\]
\[V_\mathit{out} ^{(2)} = - g_m V_s ^{(2)} R_d ^{(2)}\]
\[V_1 = V_s ^{(1)} (g_m R_s +1) + g_m R_s V_s ^{(2)}\]
\[V_2 = V_s ^{(2)} (g_m R_s +1) + g_m R_s V_s ^{(1)}\]
\[V_2 - V_1 = V_s ^{(2)} - V_s ^{(1)}\]
\[V_2 + V_1 = (2 g_m R_s + 1)(V_s ^{(2)} + V_s ^{(1)})\]
\[V_s ^{(1)} = 1/2 \left[ V_1 - V_2 + \frac{V_2 + V_1}{2g_m R_s + 1}\right]\]
\[V_s ^{(2)} = 1/2 \left[ V_2 - V_1 + \frac{V_1 + V_2}{2g_m R_s + 1}\right]\]
\[V_\mathit{out} ^{(2)} - V_\mathit{out} ^{(1)} \simeq -g_m R_d (V_2 - V_1)\]
\[V_\mathit{out} ^{(1)} + V_\mathit{out} ^{(2)} = -g_m R_d \frac{V_1 + V_2}{2g_m R_s +1}\]
\[V_\mathit{out} ^{(1)} = - \frac{g_m R}{2} \left[V_2 - V_1 + \frac{V_2 + V_1}{2g_m R_S +1} \right]\]
\[V_\mathit{out} ^{(2)} = - \frac{g_m R}{2} \left[V_1 - V_2 + \frac{V_2 + V_1}{2g_m R_S +1}\right]\]
\[V_1 = V_2 = V \rightarrow V_\mathit{out} ^{(2)} - V_\mathit{out} ^{(1)} = 0 \rightarrow V_\mathit{out} ^{(2)} = -\frac{g_m R_d}{2g_mR_s+1}V=V_\mathit{out}^{(1)}\]
\[V_{1,2}=0 \rightarrow V_\mathit{out} ^{(2)} - V_\mathit{out} ^{(1)}\simeq g_m R_d V_1 \rightarrow V_\mathit{out} ^{(2)} = -\frac{g_m R_d}{2} \left[-V_2 + \frac{V_1}{2g_mR_s + 1}\right] \rightarrow V_\mathit{out} ^{(1)} = -\frac{g_m R_d}{2} \left[-V_2 + \frac{V_1}{2g_mR_s + 1} \right]\]
\subsection{Misura della banda passante}
\[R_o ' = r_o \parallel R_D \parallel R_L\]
\[f_H = \frac{1}{2\pi} \left[(R_I \parallel (R_1 \parallel R_2 ) (C_\mathit{GS} + C_\mathit{GD} (1 + g_m R_o ' ))\right]^{-1}\]
\[R_\mathit{SC} ^{(1)} = R_I + (R_1 \parallel R_2 ) \simeq R_1 \parallel R_2\]
\[R_\mathit{SC} ^{(2)} \simeq R_D + R_o\]
\[R_\mathit{SC} ^{(3)} = R_S \parallel \frac{1}{g_m}\]
\[s_1 = - \frac{1}{(R_I + R_1 \parallel R_2) C_1}\]
\[s_2 = - \frac{1}{(R_D + R_o ) C_2}\]
\[s_3 = \frac{1}{(R_S \parallel \frac{1}{g_m} ) C_3}\]
\[\omega _1 = \frac{1}{(R_1 \parallel R_2 ) C_1}\]
\[\omega _2 = \frac{1}{R_o C_2}\]
\[\omega _3 = \frac{1}{(R_S \parallel \frac{1}{g_m}) C_3}\]
\[f_L = [\omega _1+\omega _2+\omega _3]\frac{1}{2\pi}= \left[ \frac{1}{(R_1 \parallel R_2 ) C_1} + \frac{1}{R_o C_2} + \frac{1}{(R_S \parallel \frac{1}{g_m}) C_3}\right] \frac{1}{2\pi}\]
\end{enumerate}


\end{document}


TODO
1. 
	a) Spiegare il circuito con tb compomnenti, dove sono messi gli strumenti e cosa cambia coi disegni.
	b) determinazione sperimentale del Qpt
	c) curve caratteristiche Id(Vds) e trovare lambda e ro   FIT	 parte di Spice sovrapporre le curve, e stimare le differenze
	d) curve aratteristiche Id(Vgs) per trovare Vtn e kn	FIT  parte di Spice 
	e) calcolare il gm
	
2.
	a)Spiegare il circuito con tb compomnenti, dove sono messi gli strumenti e cosa cambia coi disegni.
	b) studiare l 'inverter in interdizione e conduzione, tabella logica --- SPICE
	c) studiare l'amplificatore in sat	--- SPICE
	
3.
	a) Costruire il circuito CS, spiegarlo
	b) misura del Qpt, spiegare dove abbiamo messo gli stumenti, e verificare che sia quello dato lo trovo anche in SPICE
	c) misura del guadagno, confronto con la teoria
	d) MIsura Rin Rout,confronto con teoria
	e) togliamo il bypass e misuriamo il guadagno
	f) Misura della banda passante, un minimo di teoria e via -- SPICE
	g) CD e misura del guadagno e Rout

4. Risoluzione teorica
	a) Costruzione del circuito 
	b) Varie configurazioni , in1 o in2 a terra, in è la differenza ecc
	