reset
load "macros_2.gp"
@postcolor
set out "fit_sin2_2.eps"
set samples 100000
f(x) = a*sin(b*2*pi*x)+e
g(x) = c*sin(d*2*pi*x)+f
a = -4e-1
c = 1e-1
b = 1000
d = 1000
e=-0.05
f = 0.001
fit f(x) "MOS_2.2_Amp.txt" using 1:2 via a,b,e
fit g(x) "MOS_2.2_Amp.txt" using 1:3 via c,d,f
set offset 0,0,0.2,0
plot "MOS_2.2_Amp.txt" using 1:2 t "V_{out}", '' using 1:3 pt 1 t "V_{in}", f(x) t sprintf("%f sin(%f*2{/Symbol p}x)%+f",a,b,e), g(x) t sprintf("%f sin(%f*2{/Symbol p}x)%+f",c,d,f), NaN lc rgb "white" t sprintf("A^{sim'd} = %.3f %+.3f", real(a)/c, abs(real(a)/c)*sqrt((a_err/a)**2 + (c_err/c)**2))


set out "fit_sin3_1.eps"
e=0
b=10000
d=10000
fit f(x) "MOS_3.1_CS_bypass.txt" using 1:3 via a,b,e
fit g(x) "MOS_3.1_CS_bypass.txt" using 1:2 via c,d,f
plot "MOS_3.1_CS_bypass.txt" using 1:3 t "V_{out}" , '' using 1:2 pt 1 t "V_{in}", f(x) t sprintf("%f sin(%f*2{/Symbol p}x)%+f",a,b,e), g(x) t sprintf("%f sin(%f*2{/Symbol p}x)%+f",c,d,f), NaN lc rgb "white" t sprintf("A^{sim'd} = %.3f %+.3f", real(a)/c, abs(real(a)/c)*sqrt((a_err/a)**2 + (c_err/c)**2))


set offset 0,0,0.05,0
set out "fit_sin3noby.eps"
a = 0.1
c = -0.074
e=0
b=10000
d=10000
fit f(x) "MOS_3.5_CS_NObypass.txt" using 1:2  via a,b,e 
fit g(x) "MOS_3.5_CS_NObypass.txt" using 1:3  via c,d,f 
plot "MOS_3.5_CS_NObypass.txt" using 1:3 t "V_{in}", '' using 1:2 pt 1 t "V_{out}", g(x) t sprintf("%f sin(%f*2{/Symbol p}x)%+f",c,d,f), f(x) t sprintf("%f sin(%f*2{/Symbol p}x)%+f",a,b,e), NaN lc rgb "white" t sprintf("A^{sim'd} = %.3f %+f", real(a)/c, abs(real(a)/c)*sqrt((a_err/a)**2 + (c_err/c)**2))

set offset 0,0,0.03,0
set out "fit_sin3_6.eps"
a = 0.1
c = 0.1
e=0
b=10000
d=10000
fit f(x) "MOS_3.6_CDvero.txt" using 1:2 via a,b,e 
fit g(x) "MOS_3.6_CDvero.txt" using 1:3 via c,d,f 
plot "MOS_3.6_CDvero.txt" using 1:3 t "V_{in}", '' using 1:2 pt 1 t "V_{out}", g(x) t sprintf("%f sin(%f*2{/Symbol p}x)%+f",c,d,f), f(x) t sprintf("%f sin(%f*2{/Symbol p}x)%+f",a,b,e), NaN lc rgb "white" t sprintf("A^{sim'd} = %.3f %+f", real(a)/c, abs(real(a)/c)*sqrt((a_err/a)**2 + (c_err/c)**2))

