###CONFIG GENERALE###
load "macros.gp"
unset multiplot
reset
set tics nomirror 
set term post eps color 'Verdana, 14' size 20cm, 7cm
set encoding iso_8859_1
set fit error

###PLOT1 INFOS###
set out "aborto1_2.eps"
set key samplen 2
#set xlabel "I_D [A]"
#set ylabel "V_{DS} [V]"
set multiplot layout 2,3 margins 0.1,0.9,0.1,0.9 spacing 0.08,0.05
set key spacing 3.5

###FIT LAMBDA STORTO PESATO###
f(x) = q + m*x
fit f(x) "1_1.csv" every ::4 using 3:1:2 yerror via q,m 
l = -1/real(q)
l_err = l * abs(q_err/q)
r0 = m
r0_err = m_err

@textonly
plot 2 t "Dati", 2 t sprintf("%f \n%+f*x", q, m), @text("{/Symbol l}=%g \n{/Symbol \261} %g", l,l_err), @text("r_0=%f \n{/Symbol \261} %f", r0, r0_err), @text("{/Symbol c^2} = %g", FIT_STDFIT**2)
@restoreplot
set format x ""
plot "1_1.csv" using 3:1:2 every ::4 w yerror not , f(x) not
set xzeroaxis
set key top left
plot "1_1.csv" using 3:($1-f($3)):2 every ::4 w yerror t "Residui"
unset xzeroaxis
#unset multiplot


###FIT LAMBDA STORTO NON PESATO###
#set out "1_2_fit_unitw.eps"
#set multiplot layout 1,2
set key top left Left reverse
#f(x) = q + m*x
fit f(x) "1_1.csv" every ::4 using 3:1 via q,m
l = -1/real(q)
l_err = l*abs(q_err/q)
r0 = m
r0_err = m_err
@textonly
plot 2 t "Dati", 2 t sprintf("%f \n%+f*x", q, m), @text("{/Symbol l}=%g \n {/Symbol \261} %g", l ,l_err), @text("r_0=%f \n {/Symbol \261} %f", r0, r0_err), @text("{/Symbol s}_{fit} = %g", FIT_STDFIT)
@restoreplot
#set xlabel "I_D [A]" offset 0,0.5	
#set ylabel "V_{DS} [V]" offset 2
set format x "%.3s%c"
set xtics rotate by 315
plot "1_1.csv" using 3:1:(FIT_STDFIT) every ::4 w yerror not, f(x) not
set xzeroaxis
set key top left 
set label 1 at screen 0.29,0.5 "V_{DS} [V]" rotate by 90
set label 2 at screen 0.65,0.02 "I_D [A]"
plot "1_1.csv" using 3:($1-f($3)):(FIT_STDFIT) every ::4 w yerror t "Residui"
unset xzeroaxis
unset multiplot

###QPT###
VdsQpt=6.019
VdsQpt_err=0.017413682599228
IdQpt=1.0976e-3
IdQpt_err=0.000506959934775e-3
VgsQpt=3.129
VgsQpt_err=0.009106152224366
