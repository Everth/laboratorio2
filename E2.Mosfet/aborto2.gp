###CONFIG GENERALE###
load "macros_2.gp"
unset multiplot;\
reset;\
set tics nomirror ;\
set term post eps color 'Verdana, 14' size 20cm,7cm;\
set encoding iso_8859_1 ;\
set fit error;

###PLOT1 INFOS###
set out "aborto2.eps"
set key samplen 2
set multiplot layout 2,2 margins 0.08,0.95,0.1,0.98 spacing 0.08,0.05 columnsfirst 

###QPT###
VdsQpt=6.019
VdsQpt_err=0.017413682599228
IdQpt=1.0976e-3
IdQpt_err=0.000506959934775e-3
VgsQpt=3.129
VgsQpt_err=0.009106152224366

###FIT KN GM STORTO PESATO###
f(x) = q + m*x
fit f(x) "1_3.csv" u 5:3:4 yerror via q,m
set key top left Left reverse
set key samplen 2
##PARAMS##
k_n = 2/m**2
k_n_err = 2*k_n*m_err/m
g_m = 2*IdQpt/(VgsQpt-q)
g_m_err = g_m*sqrt( (IdQpt_err/IdQpt)**2 + (VgsQpt_err/(VgsQpt-q))**2 + ((-q_err)/(VgsQpt-q))**2)
##PLOTS##
set format x ''
plot "1_3.csv" using 5:3:4 w yerror t "Dati Sperimentali", f(x) t sprintf("%f + %f*x", q, m), @text("K_n = %f {/Symbol \261} %f", k_n , k_n_err), @text("V_{TN} = %f {/Symbol \261} %f", q, q_err), @text("g_m = %f {/Symbol \261} %f", g_m, g_m_err), @text("{/Symbol c^2} = %f", FIT_STDFIT**2)
set xzeroaxis
set format x
plot "1_3.csv" using 5:($3-f($5)):4 w yerror t "Residui"
unset xzeroaxis

###FIT KN GM STORTO NON PESATO###
fit f(x) "1_3.csv" u 5:3 via q,m
k_n = 2/m**2
k_n_err = 2*k_n*m_err/m
g_m = 2*IdQpt/(VgsQpt-q)
g_m_err = g_m*sqrt( (IdQpt_err/IdQpt)**2 + (VgsQpt_err/(VgsQpt-q))**2 + ((-q_err)/(VgsQpt-q))**2)
sigma2 = FIT_STDFIT
set format x ''
plot "1_3.csv" using 5:3:(FIT_STDFIT) w yerror t "Dati Sperimentali", f(x) t sprintf("%f + %f*x", q, m), @text("K_n = %f {/Symbol \261} %f", k_n , k_n_err), @text("V_{TN} = %f {/Symbol \261} %f", q, q_err), @text("g_m = %f {/Symbol \261} %f", g_m, g_m_err), @text("{/Symbol s}_{fit} = %f", FIT_STDFIT)
set xzeroaxis
set label 1 "V_{GS} [V]" at screen 0.02, 0.5 center rotate by 90
set label 2 "Y [A^{\275}]" at screen 0.5,0.02 center
set format x
plot "1_3.csv" using 5:($3-f($5)):(FIT_STDFIT) w yerror t "Residui"
unset xzeroaxis
unset multiplot
