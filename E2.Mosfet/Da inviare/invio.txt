Mosfet:

Parte 1
Q-point: (V_DS = 6.019 V, I_D = 1.0976 mA, V_GS = 3.129 V)
Lambda_n = 0.004568
r_o = 204746.990404 Ohm
k_n = 0.000551 Ohm^-1
V_TN = 1.163385 V
g_m = 0.001117 Ohm^-1

Parte 2
VDD= 5.0120 / FS 10 V
V_in_low = 197.28 / FS 1000mV -> V_out_low = 5.0075 / FS 10V
V_in_high = 5.0288 / FS 10V -> V_out_high = 146.79 / FS 1000mV

Parte 3
R_D = 5.1887 / FS 10V
R_S = 2.9350 / FS 10V
R_1 = 327.43k / 1000k Ohm
R_2 = 446.79k / 1000k Ohm

Quantità, Valori calcolati,	Valori misurati
A_VT	-5.7	-5.6
R_IN	188.95k	183.781k Ohm
R_OUT	5.04k	5.112k Ohm
A_noBy	-1.35	-1.34
A_VT_CD	0.77	0.76
R_O_CD	699.32	679.7 Ohm


