#include <iostream>
#include <cmath>
#include <vector>
#include <string>
#include <fstream>
#include <iterator>
#include <iomanip>
#include <regex>
#include <sstream>
#include <typeinfo>


double valormedio(std::vector<double> a){
	double sum=0;
	int campione=a.size();
	for(double i:a){
	sum+=i;
		}
	return (sum+0.0)/campione;
};

double stdev(std::vector<double> a){
	double variance;
	double vv=valormedio(a);
	for(double i:a){
		variance+= pow((i-vv),2); }
		//Uso size-1 perché è la stima più precisa
		return sqrt(variance/(a.size()-1));
}

int findprecision (double a){
	char buffer[15] = "test";
	int n;
	std::string exp;
	sprintf(buffer, "%e", a);
	std::string rounded(buffer);
	rounded.shrink_to_fit();
	std::size_t found = rounded.find('e');
	exp = rounded.substr(found+1,3);
	n= -(std::stoi(exp, nullptr, 0));
	return (n);
}

template<typename T> void printElement(std::ostream &a,T t, const int& width, char sep){
	const char def_fill = a.fill();
	std::regex numeric("-?([0-9]+)\\.?[0-9]+");
	std::smatch match;
	std::stringstream ss;
	ss << t;
	std::string stringa = ss.str();
	std::regex_match(stringa,match,numeric);
	if(std::regex_search(stringa,match,numeric)){
		//std::cerr << match.length(1) << std::endl;
		a << std::fixed <<  std::left << std::setw(width+2+match.length(1)) << std::setprecision(width) << std::setfill(sep) << t;
	} else {
		a << std::left << t << sep;
	}
	
	
	/*if(!std::regex_match(stringa,numeric)) {
		a << std::left << t << sep;
	} 
	else if (width == 0){
		a  << std::left << t << sep;	
	} else {
		std::cerr << t << typeid(t).name() << std::endl;
		int d = 2;
		a << std::fixed <<  std::left << std::setw(width+2+2) << std::setprecision(width) << std::setfill(sep) << t;
	}*/
    a << std::setfill(def_fill);
}

double mean_stdev(std::vector<double> a){
	double variance;
	double vv=valormedio(a);
	for(double i:a){
		variance+= pow((i-vv),2.0); }
		return (sqrt(variance/(a.size()-1)))/sqrt(a.size());
};

std::vector<double> wmean_2(std::vector<std::vector<double>> dataerr){
	double num = 0;
	double den = 0;
	for (auto i : dataerr){
		std::cerr << "Data and error" << i[0] << "+-" << i[1] << std::endl;
		num += (i[0])/pow(i[1],2.0);
		den += 1.0/pow(i[1],2.0);
	}
	std::cerr << "Weighted data and error" << (num/den) <<  "+-" << static_cast<double>(sqrt(1.0/den)) << std::endl;
	std::vector<double> result{(num/den), static_cast<double>(sqrt(1.0/den))};
	return result;
}

main(){
	std::ofstream COUT("out2.txt");
	std::ifstream sferica("Aberrazione_sferica");
	for (unsigned int i = 0; i < 6; ++i){
		sferica.ignore(10000,'\n');
	}
	std::istream_iterator<double> start(sferica), end;
	std::vector<double> numbers(start, end);
	std::vector<std::vector<double>> temp(4);
	for (std::vector<double>::size_type i=0 ; i< numbers.size(); ++i){
			temp[i%4].push_back(numbers.at(i));
			//std::cerr << numbers.at(i) << std::endl;
	}
	std::vector<double> fp, fm, fp_mean, fm_mean, l_mean;
	for (std::vector<double>::size_type i=0 ; i< temp[3].size(); ++i){
		fp.push_back( (temp[1].at(i)+temp[0].at(i))/2.0 );
		fm.push_back( (temp[3].at(i)+temp[2].at(i))/2.0 );
	}
	fp_mean = {valormedio(fp), mean_stdev(fp)};
	fm_mean = {valormedio(fm), mean_stdev(fm)};
	l_mean = {fp_mean[0]-fm_mean[0], static_cast<double>(sqrt(pow(fp_mean[1],2.0)+pow(fm_mean[1],2.0)))};
	std::vector<double> F_para,F_marg,R,C,R_old,f_old;
	R = {1.4,0.05};
	R_old = {0.5,0};
	F_para = {fp_mean[0]-46-0.23/2-0.7/6, sqrt( 0.05*0.05 + pow(fp_mean[1],2.0))};
	F_marg = {fm_mean[0]-46-0.23/2-0.7/6, sqrt( 0.05*0.05 + pow(fm_mean[1],2.0))};
	f_old = {6.6362, 0.00218185};
	//compatibilità tra f_para e f_old
	double comp = fabs(F_para[0]-f_old[0])/sqrt(pow(f_old[1],2.0)+pow(F_para[1],2.0));
	double fpi_err ,fpf_err, fpm_err, fmi_err, fmf_err, fmm_err;
	fpi_err = stdev(temp[0]);
	fpf_err = stdev(temp[1]);
	fpm_err = stdev(fp);
	fmi_err = stdev(temp[2]);
	fmf_err = stdev(temp[3]);
	fmm_err = stdev(fm);
	C = {l_mean[0]*f_old[0]/pow(R[0],2.0), static_cast<double> (sqrt( pow(l_mean[0]/pow(R[0],2.0)*f_old[1],2.0) + pow(f_old[0]/pow(R[0],2.0)*l_mean[1],2.0) + pow(2*l_mean[0]*f_old[0]/(pow(R[0],3.0))*R[1],2.0) ) )};
	printElement(COUT,"F_p_i",0,'&');
	printElement(COUT,"F_p_f",0,'&');
	printElement(COUT,"F_p_m",0,'&');
	printElement(COUT,"F_m_i",0,'&');
	printElement(COUT,"F_m_f",0,'&');
	printElement(COUT,"F_m_m",0,'&');
	COUT << std::endl;
	COUT.flush();
	for(std::vector<double>::size_type i=0; i<fp.size() ; ++i){
		printElement(COUT,temp[0][i],3,'&');
		printElement(COUT,temp[1][i],3,'&');
		printElement(COUT,fp[i],findprecision(fpm_err),'&');
		printElement(COUT,temp[2][i],findprecision(fmi_err),'&');
		printElement(COUT,temp[3][i],findprecision(fmf_err),'&');
		printElement(COUT,fm[i],findprecision(fmm_err),'&');
		COUT<<std::endl;
	}
	COUT << "Compatibilità tra f_para ed f_old = " << comp << std::endl;
	COUT << "F parassiale medio = " << fp_mean[0] << "+-" << fp_mean[1] << std::endl;
	COUT << "F marginale medio = " << fm_mean[0] << "+-" << fm_mean[1] << std::endl;
	COUT << "Deviazioni standard sui campioni fpi fpf fpm fmi fmf fmm = " << fpi_err << '\t' << fpf_err << '\t' << fpm_err << '\t' << fmi_err << '\t' << fmf_err << '\t' << fmm_err << std::endl;
	COUT << "Aberrazione sferica longitudinale = " << l_mean[0] << "+-" << l_mean[1] << std::endl;
	COUT << "F parassiale = " << F_para[0] << "+-" << F_para[1] << std::endl;
	COUT << "C con f_old = " << C[0] << "+-" << C[1] << std::endl;
	COUT << std::endl;
	
	
	std::ifstream cromatica("Aberrazione_cromatica");
	for (unsigned int i = 0; i < 7; ++i){
		cromatica.ignore(10000,'\n');
	}
	std::istream_iterator<double> start2(cromatica), end2;
	std::vector<double> numbers2(start2,end2);
	std::vector<std::vector<double>> temp2(4);
	for (std::vector<double>::size_type i = 0; i < numbers2.size(); ++i){ 
		temp2[i%4].push_back(numbers2[i]);
	}
	std::vector<double> fF, fC, fF_mean, fC_mean, A_mean;
	for (std::vector<double>::size_type i=0 ; i< temp2[3].size(); ++i){
		fF.push_back( (temp2[1].at(i)+temp2[0].at(i))/2.0 );
		fC.push_back( (temp2[3].at(i)+temp2[2].at(i))/2.0 );
	}
	fF_mean = {valormedio(fF), mean_stdev(fF)};
	fC_mean = {valormedio(fC), mean_stdev(fC)};
	double fFi_err ,fFf_err, fFm_err, fCi_err, fCf_err, fCm_err;
	fFi_err = stdev(temp2[0]);
	fFf_err = stdev(temp2[1]);
	fFm_err = stdev(fF);
	fCi_err = stdev(temp2[2]);
	fCf_err = stdev(temp2[3]);
	fCm_err = stdev(fC);
	A_mean = {fabs(fF_mean[0]-fC_mean[0]), static_cast<double>(sqrt(pow(fF_mean[1],2.0)+pow(fC_mean[1],2.0)))};
	std::vector<double> V;
	V = {f_old[0]/(fC_mean[0]-fF_mean[0]), static_cast<double>( sqrt( pow( f_old[1]/(fC_mean[0]-fF_mean[0]),2.0) + pow( f_old[0]*fC_mean[1]/(pow(fC_mean[0]-fF_mean[0],2.0)),2.0) + pow(-f_old[0]*fF_mean[1]/pow(fC_mean[0]-fF_mean[0],2.0),2.0)))};
	printElement(COUT,"F_F_i",0,'&');
	printElement(COUT,"F_F_f",0,'&');
	printElement(COUT,"F_F_m",0,'&');
	printElement(COUT,"F_C_i",0,'&');
	printElement(COUT,"F_C_f",0,'&');
	printElement(COUT,"F_C_m",0,'&');
	COUT << std::endl;
	for(std::vector<double>::size_type i=0; i<fF.size() ; ++i){
		printElement(COUT,temp2[0][i],3,'&');
		printElement(COUT,temp2[1][i],3,'&');
		printElement(COUT,fF[i],findprecision(fFm_err),'&');
		printElement(COUT,temp2[2][i],findprecision(fCi_err),'&');
		printElement(COUT,temp2[3][i],findprecision(fCf_err),'&');
		printElement(COUT,fC[i],findprecision(fCm_err),'&');
		COUT<<std::endl;
	}
	COUT << "Deviazioni standard sui campioni fFi fFf fFm fCi fCf fCm = " << fFi_err << '\t' << fFf_err << '\t' << fFm_err << '\t' << fCi_err << '\t' << fCf_err << '\t' << fCm_err << std::endl;
	COUT << "F BLU medio = " << fF_mean[0] << "+-" << fF_mean[1] << std::endl;
	COUT << "F ROSSO medio = " << fC_mean[0] << "+-" << fC_mean[1] << std::endl;
	COUT << "fF, fD, fC marginali = " << fF[0]-46-0.23/2-0.7/6 << '\t' << fm[0]-46-0.23/2-0.7/6 << '\t' << fC[0]-46-0.23/2-0.7/6 << std::endl;
	// " +- " << V[0]*sqrt(pow(f_old[1]/f_old[0],2.0) + pow(A_mean[1]/A_mean[0],2.0)) 
	COUT << "Numero di abbe con f old = " << V[0] << "+-" << V[1] << std::endl;	
	COUT << "AAAAAAAAAAAA = " << A_mean[0] << "+-" << A_mean[1] << std::endl;
	return 0;
}
