reset
set term post eps enh color
set fit errorvariables
set sample 100000
set encoding iso_8859_1
p(x) = ( int(gprintf("%T",x)) > 0 ? 0 : -1*int(gprintf("%T",x)) )
f(x) = Vo*exp(-x/t)
g(x) = c+m*x
#rho print NaN lc 'white' t sprintf("{/Symbol r} = %f", -(X_mean/(sqrt(X_stddev**2+X_mean**2))) )
set zeroaxis
set key spacing 1.5
Vo = 10
t = 402.009
fit f(x) "scarica" u 1:2:( sqrt((0.03*$2)**2 + (0.1*2)**2)/3 )  via Vo, t
set xlabel "Tempo [{/Symbol m}s]"
set ylabel "Differenza di potenziale [V]" offset 1.5,0
set out "scarica.eps"
set auto fix; set offset  10,10,0.5,0.5
plot "scarica" u 1:2:( sqrt((0.03*$2)**2 + (0.1*2)**2)/3 ) lc 'black' w yerror t "Data", f(x) lc rgb "#9400D3" title "f(x) = V_0*e^{-t/{/Symbol t}}", NaN lt 1 lc rgb 'white' t sprintf("V_0 = %f+-%f", Vo, Vo_err) , NaN lt 1 lc rgb 'white' t sprintf("{/Symbol t} = %f+-%f", t, t_err)
set table "scarica_dati"
splot "scarica" u 1:2:( sqrt((0.03*$2)**2 + (0.1*2)**2)/3 )
unset table
set print "fit_out"
print "%<*scarica_ris>"
set print "test"
print sprintf("%f %f %d %s\n%f %f %d %s", Vo, Vo_err, p(Vo_err), "$V_0$", t, t_err, p(t_err), "$\\\\tau_{exp}$") 
system("(./printer.sh < test) >> fit_out; rm test")
set print "fit_out" append
print sprintf("NDF = %d\n$\\chi^2$ = %d", FIT_NDF, FIT_WSSR)
print "%</scarica_ris>"

set out "test3"
p '< echo "1 2\\n 2 3"' w impulse

#Senza reiezione
fit g(x) "scarica" u 1:(log($2)):( sqrt( ((0.03)**2)/3) + (0.1*2/sqrt(3))**2/($2)**2  )  via c,m
set out "scarica_lin.eps"
set auto fix; set offset  10,10,0.1,0.1
set ylabel "Logaritmo naturale della differenza di potenziale [V]" offset 1.5,0
plot "scarica" u 1:(log($2)):( sqrt( ((0.03)**2)/3) + (0.1*2/sqrt(3))**2/($2)**2 ) lc 'black' w yerror t "Data", g(x) lc rgb "#9400D3" title sprintf("f(x) = %f%+f*x", c, m) ,NaN lc rgb 'white' t sprintf("{/Symbol t} = %f+-%f", -1/m, m_err/m**2)
set table "scarica_lin_dati"
plot "scarica" u 1:(log($2)):( sqrt( (0.1*2/sqrt(3))**2/($2)**2 + ((0.03)**2)/3)) w table
unset table
set print "fit_out" append
print "%<*scarica_lin_ris>"
set print "test"
print sprintf("%f %f %d %s\n%f %f %d %s\n%f %f %d %s", c, c_err, p(c_err), "c", m, m_err, p(m_err), "m", -1/m, m_err/m**2, p(m_err/m**2), "$\\\\tau$") 
system("(./printer.sh < test) >> fit_out; rm test")
set print "fit_out" append
print sprintf("NDF = %d\n$\\chi^2$ = %d", FIT_NDF, FIT_WSSR)
print "%</scarica_lin_ris>"

set out "residui_scarica_lin.eps"
set offset
stats "scarica" using 1 prefix "X" noout
stats "scarica" using (log($2)) prefix "Y" noout
plot "scarica" u 1:(log($2)-g($1)):( sqrt( (0.1*2/sqrt(3))**2/($2)**2 + ((0.03)**2)/3 ) + c_err**2 + $1*m_err + -2*$1*(X_mean/(sqrt(X_stddev**2+X_mean**2))*c_err*m_err) ) lc 'black' w yerror t "Residui", NaN lc 'white' t sprintf("{/Symbol r} = %f", -(X_mean/(sqrt(X_stddev**2+X_mean**2))) )


#Reiezione
fit g(x) "scarica" u 1:(log($2)):( sqrt( (0.1*2/sqrt(3))**2/($2)**2 + ((0.03)**2)/3) ) every ::::14 via c,m
stats "scarica" using 1 every ::::14 prefix "X" noout
stats "scarica" using (log($2)) every ::::14 prefix "Y" noout
rho = -(X_mean/(sqrt(X_stddev**2+X_mean**2)))
set out "scarica_reiezione.eps"
set auto fix; set offset  10,10,0.1,0.1
plot "scarica" u 1:(log($2)):( sqrt( (0.1*2/sqrt(3))**2/($2)**2 + ((0.03)**2)/3)) every ::::14  lc 'black' w yerror t "Data", g(x) lc rgb "#9400D3" title sprintf("f(x) = %f%+f*x", c, m), NaN lc rgb 'white' t sprintf("{/Symbol t} = %f+-%f", -1/m, m_err/m**2)
set print "fit_out" append
print "%<*scarica_reiez_ris>"
set print "test"
print sprintf("%f %f %d %s\n%f %f %d %s\n%f %f %d %s", c, c_err, p(c_err), "c", m, m_err, p(m_err), "m", -1/m, m_err/m**2, p(m_err/m**2), "$\\\\tau$") 
system("(./printer.sh < test) >> fit_out; rm test")
set print "fit_out" append
print sprintf("NDF = %d\n$\\chi^2$ = %d\nrho(a,b) = %f", FIT_NDF, FIT_WSSR, rho)
print "%</scarica_reiez_ris>"


set out "residui_scarica_reiezione.eps"
set offset
plot "scarica" u 1:(log($2)-g($1)):( sqrt( (0.1*2/sqrt(3))**2/($2)**2 + ((0.03)**2)/3 ) + c_err**2 + $1*m_err + -2*$1*(X_mean/(sqrt(X_stddev**2+X_mean**2))*c_err*m_err) ) every ::::14 lc 'black' w yerrorbars t "Residui",  NaN lc 'white' t sprintf("{/Symbol r} = %f", -(X_mean/(sqrt(X_stddev**2+X_mean**2))) )

