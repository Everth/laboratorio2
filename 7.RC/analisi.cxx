#include <sstream>
#include <fstream>
#include <cmath>
#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <iterator>

//Restituisce la media sul campione
double valormedio(std::vector<double> a){
	double sum=0;
	int campione=a.size();
	for(double i:a){
	sum+=i;
		}
	return (sum+0.0)/campione;
};

//Restituisce il valore minimo
double smin(std::vector<double> a){
	double min=a[0];
	for(double i:a){
	if(i<min){
	min=i;
	} else {
		continue;
		}
	}
	return min;
}

//Restituisce il valore massimo
double smax(std::vector<double> a){
	double max=a[0];
	for(double i:a){
	if(i>max){
	max=i;
	} else {
		continue;
		}
	}
	return max;
}

//Scarto quadratico medio
double rms(std::vector<double> a){
	double variance;
	double vv=valormedio(a);
	for(double i:a){
		variance+= pow((i-vv),2); }
		return sqrt(variance/(a.size()));
}

//Deviazione standard
double stdev(std::vector<double> a){
	double variance;
	double vv=valormedio(a);
	for(double i:a){
		variance+= pow((i-vv),2); }
		//Uso size-1 perché è la stima più precisa
		return sqrt(variance/(a.size()-1));
}

//Errore della media
double mean_stdev(std::vector<double> a){
	double variance;
	double vv=valormedio(a);
	for(double i:a){
		variance+= pow((i-vv),2.0); }
		return (sqrt(variance/(a.size()-1)))/sqrt(a.size());
}

double covarianza_campionaria ( std::vector<double> a, std::vector<double> b){
	double sum1,sum2,sum3;
	for(std::vector<double>::size_type i = 0; i < a.size() ; ++i){
		sum1 += (a.at(i))*(b.at(i))/a.size();
	}
	for (auto i : a) sum2+= i / a.size();
	for (auto i : b) sum3+= i / b.size();
	return (sum1 - sum2*sum3);
}

double correlazione (std::vector<double> a, std::vector<double> b){
	return covarianza_campionaria(a,b)/(rms(a)*rms(b));
}

std::vector<double> reciproco(std::vector<double> a){
	std::vector<double> temp;
	for (double i : a){
		temp.push_back(1.0/i);
	}
	return temp;
}

std::vector<double> wmean(std::vector<double> data, std::vector<double> error){
		if(data.size() == error.size()){
		int N = data.size();
		double num=0;
		double den=0;
		for (int i = 0; i < N; ++i){
			num+=(data.at(i))/pow(error.at(i),2.0);
			den+=1.0/pow(error.at(i),2.0);
		}
		std::vector<double> result{(num/den), static_cast<double>(sqrt(1.0/den))};
		return result;
	} else exit(1);
}

int findprecision (double a){
	char buffer[15] = "test";
	int n;
	std::string exp;
	sprintf(buffer, "%e", a);
	std::string rounded(buffer);
	rounded.shrink_to_fit();
	std::size_t found = rounded.find('e');
	exp = rounded.substr(found+1,3);
	n= -(std::stoi(exp, nullptr, 0));
	if( n >= 0 ){ return (n);
	} else {
		std::cerr << "Controlla errore!!" << a << "\t" << (n)<< std::endl;
		return 0;
	}
}

std::string prec(double val, double err){
	std::stringstream ss;
	ss << std::fixed << std::setprecision(findprecision(err)) << val;
	return ss.str();
}

template<typename T> void printElement(std::ostream &a,T t, const int& width, char sep){
    a << std::fixed <<  std::left << std::setw(width+3) << std::setprecision(width) << std::setfill(sep) << t;
}

std::vector<double> interpolazione (std::vector<double> j, std::vector<double> k,std::vector<double> l, std::vector<double> m, std::ostream &out = std::cout, double yerror = 0){
	std::vector<double> *x = new std::vector<double> {0};
	std::vector<double> *y = new std::vector<double> {0};
	std::vector<double> *z = new std::vector<double> {1};
	bool chi = false;
	bool &chong = chi;
	x=&j;
	y=&k;
	int grandezza_campione = k.size();
	std::vector<double> errtemporaneo(grandezza_campione, 1.0);
	if(l.size() != 0) {
		z = &l;
		out<< "#Fit chi quadro (immesso errore singole misure)" <<std::endl;
		out.flush();
		chong = true;
	} else {
		out<< "#Fit minimi quadrati (error defaulted to 1)" <<std::endl;
		out.flush();
		z = &errtemporaneo;
	}
	if (x->size() == y->size()){
		//for(std::vector<double>::size_type i=0; i != x->size(); ++i){std::cout<< "x" << i << " = " << (*x).at(i) << " y" << i << " = " << (*y).at(i) << " z" << i << " = " << (*z).at(i) << std::endl;}
		double delta, a, b , sum1, sum2, sum3, sum4, sum5, sum6, vary, sigmay, sigmaa, sigmab, corr, chisq;
		delta = 0; sum1 = 0; sum2 = 0; sum3 = 0; sum4 = 0; sum5 = 0; vary=0;
		//sum1= (x), sum2=(x)^2, sum3= (x^2), sum4=(y), sum5=(x*y) , sum6 = 1/s^2 ; il fit cercato Ã¨ a+bx
		for(std::vector<double>::size_type i=0; i != x->size(); ++i){
			sum1 += ((*x).at(i))/(pow(((*z).at(i)),2.0));
		}
		sum2 = pow(sum1, 2.0);
		for(std::vector<double>::size_type i=0; i != x->size(); ++i){
			sum3 += pow((*x).at(i),2.0)/pow((*z).at(i),2.0);
		}
		
		for(std::vector<double>::size_type i=0; i != x->size(); ++i){
			sum4 += ((*y).at(i))/pow(((*z).at(i)),2.0);
		}
		
		for(std::vector<double>::size_type i=0; i != x->size(); ++i){
			sum5+=(((*x).at(i))*((*y).at(i)))/pow(((*z).at(i)),2.0);
		}
		
		for(std::vector<double>::size_type i=0; i!= x->size(); ++i){
			sum6+= 1.0/pow((*z).at(i),2.0);
		}
		if (chi == false){
			delta = x->size()*sum3-sum2;
			a = (1.0/delta)*(sum3*sum4-sum1*sum5);
			b = (1.0/delta)*(x->size()*sum5-sum1*sum4);
			if (yerror == 0){
				for (int i = 0; i<x->size(); ++i){
					vary += pow(((a+b*((*x).at(i))-(*y).at(i))),2.0)/(x->size()-2.0);
				}
				sigmay = pow(vary,0.5);
			} else {
			sigmay = yerror;
			}
			sigmaa = sigmay*sqrt(sum3/delta);
			sigmab = sigmay*sqrt(x->size()/delta);
			corr = correlazione(*x,*y);
			double cov_a_b = (-1.0/delta*sum1*sigmay);
			out << "#" << std::left << std::setw(24) << "Intercetta x = " << -a/b << std::left << std::setw(25) << " Errore intercetta x = " << static_cast<double>(sqrt(pow(a/(b*b)*sigmab,2.0)+pow(-1.0/b*sigmaa,2.0))+2*cov_a_b*(-a/(b*b*b))) << std::endl;
			out << "#" << std::left << std::setw(24) << "Intercetta y = " << a << std::left << std::setw(25) << " Errore intercetta y = " << sigmaa << std::left << std::setw(25) << "\n#Coefficiente angolare = " << b << std::left << std::setw(25) << " Errore coefficiente angolare = " << sigmab << std::endl;
			out << "#" << std::left << std::setw(24) << "Errore a posteriori = " << std::left << std::setw(25) << sigmay << std::endl;
			out << "#" << std::left << std::setw(24) << "Correlazione lineare = " << std::left << std::setw(25) << corr << std::endl;
			out << "#" << std::left << "Ascissa" << std::left << "," << "Ordinata" << std::endl;
			for(int i = 0; i < x->size(); ++i){
				printElement(out,x->at(i),findprecision(m.at(i)),',');
				printElement(out,y->at(i),findprecision(sigmay),',');
				//out << std::left << std::setw(25) << std::fixed << x->at(i) << std::left << std::setw(25)  << (*y).at(i) << std::endl;
			}
			return std::vector<double>{a,b,delta,sigmay,sigmaa,sigmab,corr};
			
			
		} else {
			delta = sum6*sum3-sum2;
			a = (1.0/delta)*(sum3*sum4-sum1*sum5);
			b = (1.0/delta)*(sum6*sum5-sum1*sum4);
			if (yerror == 0){
				for (int i = 0; i<x->size(); ++i){
					vary += pow(((a+b*((*x).at(i))-(*y).at(i))),2.0)/(x->size()-2.0);
				}
				sigmay = pow(vary,0.5);
			} else {
			sigmay = yerror;
			}
			sigmaa = sqrt((1.0/delta)*sum3);
			sigmab = sqrt((1.0/delta)*sum6);
			corr = correlazione(*x,*y);
			for(std::vector<double>::size_type i = 0; i < x->size(); ++i){
				chisq += pow(((*y).at(i)-(a+b*(*x).at(i)))/(*z).at(i),2.0);
			}
			double cov_a_b = (-1.0/delta)*sum1*sigmay;
			std::vector<double> intercette{a,-a/b};
			std::vector<double> intercette_err = {sigmaa, sqrt(pow(a/(b*b)*sigmab,2.0)+pow(-1.0/b*sigmaa,2.0)+2*cov_a_b*(-a/(b*b*b)))};
			std::vector<double> intercette_wmean = wmean(intercette, intercette_err);
			out << "#" << std::left << std::setw(24) << "Intercetta x = " << intercette[1] << std::left << std::setw(25) << " Errore intercetta x = " << intercette_err[1] << std::endl;
			out << "#" << std::left << std::setw(24) << "Intercetta y = " << intercette[0] << std::left << std::setw(25) << " Errore intercetta y = " << intercette_err[0] << std::left << std::setw(25) << "\n#Coefficiente angolare = " << b << std::left << std::setw(25) << " Errore coefficiente angolare = " << sigmab << std::endl;
			out << "#" << std::left << std::setw(24) << "Media pesata distanza focale = " << 1.0/intercette_wmean[0] << " +- " <<  intercette_wmean[1]/pow(intercette_wmean[0],2.0) << std::endl;
			out << "#" << std::left << std::setw(24) << "Compatibilità tra intercette = " << fabs(a+a/b)/(sqrt( pow(sigmaa,2.0)+ pow(a/(b*b)*sigmab,2.0)+pow(-1.0/b*sigmaa,2.0) ))<< std::endl;
			out << "#" << std::left << std::setw(24) << "Correlazione lineare = " << std::left << std::setw(25) << corr << std::endl;
			out << "#Chiquadro = " << chisq << "\t sigmaypost = " << sigmay << std::endl;
			out << "#" << std::left <<  "Ascissa " << ',' << "Errore ascissa" << ',' << "Ordinata" << ',' << "Errore ordinata" << std::endl;
			for(int i = 0; i < x->size(); ++i){
				//out << std::left << std::setw(25) << std::fixed << x->at(i) << std::left << std::setw(25) << m.at(i) << std::left << std::setw(25) << (*y).at(i) << std::left << std::setw(25) << (*z).at(i) << std::endl;
				printElement(out,x->at(i),findprecision(m.at(i)),',');
				printElement(out,m.at(i),findprecision(m.at(i)),',');
				printElement(out,y->at(i),findprecision(z->at(i)),',');
				printElement(out,z->at(i),findprecision(z->at(i)),',');
				out << std::endl;
			}
			errtemporaneo.clear();
			return std::vector<double>{a,b,delta,sigmay,sigmaa,sigmab, corr};
		} 
	} else {
		std::cout<<"I dati non sono stati forniti in coppia"<<std::endl;
		out.flush();
		exit(1);
	}
}

int dgtoi(char a){
	if( (a - '0') < 10 ) {
		return (a - '0');
	} else {
		std::cerr << "Forse non era una digit" << std::endl;
	}
}

std::vector<double> instr_err(const char* spec, double measure){
	// char[0] strumento, char[1] unità, char[2] fs
	std::vector<double> result;
	//prima valore, poi sigma tutto, poi sigma fs, poi sigma digit, poi valore fondo scala
	enum instrument{FLUKE=1, AGILENT=2, ICE=3};
	std::vector<std::vector<std::vector<double>>> fluke;
	enum units{ACVOLT=0,DCVOLT=1,OHM=2,CAPACITANCE=3,ACAMP=4,DCAMP=5};
	enum oscilloscopio{VOLT16=0, VOLT16off=1, DELTAV=2, T=3};
	fluke.push_back(std::vector<std::vector<double>>{ {6,60,600},{0.001,0.01,0.1},{0.01,0.01,0.01},{3,3,3} });
	fluke.push_back(std::vector<std::vector<double>>{ {6,60,600},{0.001,0.01,0.1},{0.007,0.007,0.007},{2,2,2} });
	fluke.push_back(std::vector<std::vector<double>>{ {600,6000,60000,600000,6000000,40000000},{0.1,1,10,100,1000,10000},{0.009,0.009,0.009,0.009,0.009,0.015},{2,1,1,1,1,3} });
	fluke.push_back(std::vector<std::vector<double>>{ {0.000001,0.000001,0.00001,0.001},{pow(10.0,-9),pow(10.0,-8),pow(10.0,-8),pow(10.0,-7),pow(10.0,-6)},{0.019,0.019,0.019,0.019},{2,2,2,2} });
	fluke.push_back(std::vector<std::vector<double>>{ {10},{0.01},{0.015},{3} });
	fluke.push_back(std::vector<std::vector<double>>{ {6,10},{0.001,0.01},{0.01,0.01},{3,3} });
	std::vector<std::vector<std::vector<double>>> agilent;
	agilent.push_back(std::vector<std::vector<double>>{ {0.6,6,60,600,600},{0.0001,0.001,0.01,0.1,0.1},{0.01,0.01,0.01,0.01,0.02},{3,3,3,3,3} });
	agilent.push_back(std::vector<std::vector<double>>{ {0.6,6,60,600,600},{0.0001,0.001,0.01,0.1,0.1},{0.005,0.005,0.005,0.005,0.02},{2,2,2,2,3} });
	agilent.push_back(std::vector<std::vector<double>>{ {600,6000,60000,600000,6000000,60000000},{0.1,1,10,100,1000,10000},{0.009,0.009,0.009,0.009,0.009,0.015},{3,3,3,3,3,3}, {0.00057,0.000057,0.0000057,0.000000570,0.00000100,0.000000100} });
	agilent.push_back(std::vector<std::vector<double>>{ {0.000001,0.00001,0.0001,0.001,0.01},{0.000000001,0.000000001,0.0000001,0.000001,0.00001},{0.019,0.019,0.019,0.019,0.019},{2,2,2,2,2} });
	agilent.push_back(std::vector<std::vector<double>>{ {0.00006,0.0006,6,10},{0.00000001,0.0000001,0.001,0.01},{0.015,0.015,0.015,0.015},{3,3,3,3} });
	agilent.push_back(std::vector<std::vector<double>>{ {0.00006,0.0006,6,10},{0.00000001,0.0000001,0.001,0.01},{0.01,0.01,0.01,0.01},{2,2,2,3} });
	std::vector<double> ice{1.5,0.05};
	
	switch( dgtoi(spec[0]) ){
		case FLUKE: 
		if( dgtoi(spec[1]) <= fluke.size() ){
			if( fabs(measure) < fluke[dgtoi(spec[1])][0][dgtoi(spec[2])]){
				double sigma_dgt = fluke[dgtoi(spec[1])][3][dgtoi(spec[2])]/sqrt(3)*fluke[dgtoi(spec[1])][1][dgtoi(spec[2])];
				double sigma_fs = fluke[dgtoi(spec[1])][2][dgtoi(spec[2])]/sqrt(3)*measure;
				double sigma_kr = fluke[dgtoi(spec[1])][2][dgtoi(spec[2])]/sqrt(3);
				double val_fs = fluke[dgtoi(spec[1])][0][dgtoi(spec[2])];
				//std::cerr << "SIGMA DGT     SIGMA KR " << sigma_dgt << '\t' << sigma_kr << std::endl;
				result = {measure, sqrt( pow(sigma_dgt,2.0) + pow(sigma_fs,2.0) ), sigma_fs, sigma_dgt, sigma_kr, val_fs };
			} else {
				std::cerr << "Valore fuori scala" << std::endl;
			}
		}
		break;
		case AGILENT:
			if( fabs(measure) < agilent[dgtoi(spec[1])][0][dgtoi(spec[2])]){
				double sigma_dgt = agilent[dgtoi(spec[1])][3][dgtoi(spec[2])]/sqrt(3)*agilent[dgtoi(spec[1])][1][dgtoi(spec[2])];
				double sigma_fs = agilent[dgtoi(spec[1])][2][dgtoi(spec[2])]/sqrt(3)*measure;
				double sigma_kr = agilent[dgtoi(spec[1])][2][dgtoi(spec[2])]/sqrt(3);
				double val_fs = agilent[dgtoi(spec[1])][0][dgtoi(spec[2])];
				std::cerr << "SIGMA DGT     SIGMA KR " << sigma_dgt << '\t' << sigma_kr << std::endl;
				result = {measure, sqrt( pow(sigma_dgt,2.0) + pow(sigma_fs,2.0) ), sigma_fs, sigma_dgt, sigma_kr, val_fs };
			} else {
				std::cerr << "Valore fuori scala" << std::endl;
			}
		break;
		case ICE:
			result = {measure, ice[0]*0.05/sqrt(3)*0.01, 0, 0, 0, 0.05};
		break;
		default:
		std::cerr << "Non è stato inserito correttamente lo strumento di misura" << std::endl;
		break;
	}
	return result;
}

void printInTable(std::ostream &a, std::vector<double> b, std::string c){
	//Devo assumere di sapere gia' nel vettore dove sono gli errori. quindi..
	//Nota : b[4] è sigma_kr
	a << c + "&" << std::fixed << std::setprecision(findprecision(b[1])) << b.at(0) << "&" << b[1] << "&" << std::setprecision(findprecision(b[2])) << b[2] << '&' << std::setprecision(findprecision(b[3])) << b[3] << '&' << std::defaultfloat << std::setprecision(6) << b[5] << "\\\\" << std::endl;
	std::cerr << c << findprecision(b[1]) << std::endl;
}

double compatibility(std::vector<double> dato1, std::vector<double> dato2){
	if((dato1.size() >= 2) && (dato2.size() >= 2)){
		return fabs(dato1[0]-dato2[0])/sqrt(pow(dato1[1],2.0)+pow(dato2[1],2.0));
	} else {
		std::cerr << "Forse non mi hai dato l'errore, o i dati, o entrambi" << std::endl;
		return -1;
	}
}

int main(){
	double pi = 3.14159265359;
	std::ofstream ultime("ultimecose");
	std::vector<double> R, C, Tns;
	double Ro = 1000000;
	//double Rg = 45.4;
	double Rg = 0;
	double Ce = 120*pow(10.0,-12);
	R = instr_err("121",3903);
	C = instr_err("130",103*pow(10.0,-9));
	//Tns = {R[0]*C[0], sqrt( pow(R[1]/R[0],2.0) + pow(C[1]/C[0],2.0) )*R[0]*C[0]};
	Tns = {407*pow(10.0,-6),3*pow(10.0,-6)};
	ultime << "R4 = " << R[0] << "+-" << R[1] << std::endl;
	ultime << "C2 = " << C[0] << "+-" << C[1] << std::endl;
	ultime << "R4 * C2 = " << Tns[0] << " +- " << Tns[1] << std::endl;
	ultime << "Corretto con Ro e Ce" << Tns[0]*(R[0]+Rg+Ro)/Ro - (R[0]+Rg)*Ce << " +- " <<  sqrt( pow((R[0]+Rg+Ro)*Tns[1]/Ro,2.0) + pow(Tns[0]/(Ro-Ce)*R[1],2.0) + pow(Tns[0]*(R[0]+Rg)/(Ro*Ro)*0,2.0)+pow((R[0]+Rg)*0,2.0)) << std::endl;
	std::vector<double> T1, T2, T3, F1, F2, F3, C1, C2, C3;
	T1 = {8.24/10.0,8.24/10.0*sqrt(pow(0.1*2/sqrt(3)/8.24,2.0) + pow(0.1*2/sqrt(3)/10.0,2.0))};
	T2 = {7.80/10.0,7.80/10.0*sqrt(pow(0.1*2/sqrt(3)/7.80,2.0) + pow(0.1*2/sqrt(3)/10.0,2.0))};
	T3 = {7.12/10.0,7.12/10.0*sqrt(pow(0.1*2/sqrt(3)/7.12,2.0) + pow(0.1*2/sqrt(3)/10.0,2.0))};
	F1 = {10*sqrt(1-pow(T1[0],2.0))/T1[0], 10*T1[1]/(pow(T1[0],2.0)*sqrt(1-pow(T1[0],2.0)))};
	F2 = {8*sqrt(1-pow(T2[0],2.0))/T2[0], 8*T2[1]/(pow(T2[0],2.0)*sqrt(1-pow(T2[0],2.0)))};
	F3 = {7*sqrt(1-pow(T3[0],2.0))/T3[0], 7*T3[1]/(pow(T3[0],2.0)*sqrt(1-pow(T3[0],2.0)))};
	C1 = {1.0/(2*pi*Ro*F1[0]), sqrt(pow(F1[1]/(2*pi*Ro*pow(F1[0],2.0)),2.0))};
	C2 = {1.0/(2*pi*Ro*F2[0]), sqrt(pow(F2[1]/(2*pi*Ro*pow(F2[0],2.0)),2.0))};
	C3 = {1.0/(2*pi*Ro*F3[0]), sqrt(pow(F3[1]/(2*pi*Ro*pow(F3[0],2.0)),2.0))};
	std::cerr << std::scientific << C1[0] << '\t' << C2[0] << '\t' << C3[0] << std::endl;
	ultime << "F1 = " << F1[0] << '\t' << F1[1] << std::endl;
	ultime << "F2 = " << F2[0] << '\t' << F2[1] << std::endl;
	ultime << "F3 = " << F3[0] << '\t' << F3[1] << std::endl;
	ultime << "C1 = " << C1[0] << '\t' << C1[1] << std::endl;
	ultime << "C2 = " << C2[0] << '\t' << C2[1] << std::endl;
	ultime << "C3 = " << C3[0] << '\t' << C3[1] << std::endl;
	ultime << "Compatibilità tra le capacità ac 12 13 23 = " << compatibility(C1,C2) << '\t' << compatibility(C1,C3) << '\t' << compatibility(C2,C3) << std::endl;
	ultime << "Stima finale = " << wmean({C1[0],C2[0],C3[0]}, {C1[1],C2[1],C3[1]})[0] << '\t' << wmean({C1[0],C2[0],C3[0]}, {C1[1],C2[1],C3[1]})[1] << std::endl;
	ultime << "CENTOTRENTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" << std::endl;
	
	return 0;
}
	
