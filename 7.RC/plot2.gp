reset
set term post eps enhanced color
set fit errorvariables
set sample 100000
set angles radians
FIT_LIMIT = 1e-15
set encoding iso_8859_1
p(x) = ( int(gprintf("%T",x)) > 0 ? 0 : -1*int(gprintf("%T",x)) )
#rho print NaN lc 'white' t sprintf("{/Symbol r} = %f", -(X_mean/(sqrt(X_stddev**2+X_mean**2))) )
#sigmav(x) =  sqrt((0.03*x)**2 + (0.1*2)**2)/3  #noi non usiamo questo totale peché kVout/Kvin=T e si cancella


f(x) = a + b*x
set out "lin_f2A-2.eps"
set key top left Left reverse
set xlabel "Frequenza al quadrato [Hz^2]"
set ylabel "Rapporto fra tensione d'uscita e tensione d'ingresso al quadrato" offset 1,0
fit f(x) "ftaglio1-10" using ($1**2):(($4/10.0)**(-2)):((2*(10.0/$4)**2*sqrt( (0.1*2/sqrt(3)/10.0)**2 + (0.1*$5/sqrt(3)/$4)**2))) every ::3::10 via a,b
set arrow from sqrt(1.0/b)**2, graph 0 to sqrt(1.0/b)**2, graph 1 nohead ls 2 lc 'grey50' dt 3 #ft = 1/2*pi*tau
plot "ftaglio1-10" using ($1**2):(($4/10.0)**(-2)):( (2*(10.0/$4)**2*sqrt( (0.1*2/sqrt(3)/10.0)**2 + (0.1*$5/sqrt(3)/$4)**2))) every ::3::10 w yerror lc 'black' t "Data", f(x) title sprintf("%f%+f*x",a,b) lc rgb 'violet', NaN lc rgb 'white' t sprintf("f_t = %f +- %f", sqrt(1.0/b), b_err/(2*b**(3/2))), NaN lc rgb 'white' t sprintf("f(f_t)=%f", f(sqrt(1.0/b)**2))
set table "lin_f2A-2_data"
plot "ftaglio1-10" using ($1**2):(($4/10.0)**(-2)):((2*(10.0/$4)**2*sqrt( (0.1*2/sqrt(3)/10.0)**2 + (0.1*$5/sqrt(3)/$4)**2))) every ::3::10 w table
unset table
set print "fit_out2"
print "%<*lin_f2A-2_ris>"
set print "test"
print sprintf("%f %f %d %s\n%f %.10f %d %s\n%f %f %d %s", a, a_err, p(a_err), "$a$", b, b_err, p(b_err), "$b$", sqrt(1.0/b), b_err/(2*b**(3/2)), p(b_err/(2*b**(3/2))), "$f_t$") 
system("(./printer.sh < test) >> fit_out2; rm test")
set print "fit_out2" append
print sprintf("Compatibilità intercetta con 1 = %.2f", abs(1-a)/a_err)
stats "ftaglio1-10" using ($1**2)  every ::3::10 prefix "X"
print sprintf("NDF = %d\n$\\chi^2$ = %.2f\nrho(a,b) = %f", FIT_NDF, FIT_WSSR, -(X_mean/(sqrt(X_stddev**2+X_mean**2))) )
print "%</lin_f2A-2_ris>" 

set out "residui_lin_f2A-2.eps"
set key top right
set zeroaxis
plot "ftaglio1-10" using ($1**2):(($4/10.0)**-2 - f($1**2)):( sqrt( (2*(10.0/$4)**2*sqrt( (0.1*2/sqrt(3)/10.0)**2 + (0.1*$5/sqrt(3)/$4)**2))**2 + a_err**2 + (($1**2)*b_err)**2 -2*($1**2)*(X_mean/(sqrt(X_stddev**2+X_mean**2)))*a_err*b_err ) ) every ::3::10 w yerror lc 'black' t "Residui", NaN lc 'white' t sprintf("{/Symbol r} = %f",  -(X_mean/(sqrt(X_stddev**2+X_mean**2))))
unset zeroaxis


set out "lin_tandphi.eps"
h(x) = c+d*x
set xlabel "Frequenza [Hz]"
set ylabel "Tangente differenza di fase tra onda d'ingresso e onda d'uscita"
fit h(x) "ftaglio1-10" u 1:(tan(2*pi*$1*$6*10**-6)):((2*pi*$1*0.1*$7*10**-6/(sqrt(3)*cos(2*pi*$1*$6*10**-6)**2))) every ::3::8 via c, d 
fit h(x) "ftaglio1-10" u 1:( (abs(tan(2*pi*$1*$6*10**-6) - h($1))) <= ((2*pi*$1*0.1*$7*10**-6/(sqrt(3)*cos(2*pi*$1*$6*10**-6)**2))) ? (tan(2*pi*$1*$6*10**-6)) : NaN):((2*pi*$1*0.1*$7*10**-6/(sqrt(3)*cos(2*pi*$1*$6*10**-6)**2))) every ::3::8  via c, d
set arrow from 1.0/d, graph 0 to 1.0/d, graph 1 nohead ls 2
plot "ftaglio1-10" u 1:( abs(tan(2*pi*$1*$6*10**-6) - h($1)) <= ((2*pi*$1*0.1*$7*10**-6/(sqrt(3)*cos(2*pi*$1*$6*10**-6)**2))) ? (tan(2*pi*$1*$6*10**-6)) : NaN):((2*pi*$1*0.1*$7*10**-6/(sqrt(3)*cos(2*pi*$1*$6*10**-6)**2))) every ::3::8 with yerrorbars lc 'black' t "Data", h(x)  title sprintf("%f%+f*x",c,d) lc rgb 'violet', NaN lc 'white' t sprintf("f_t = %f +- %f", 1.0/d , d_err/(d**2))
#unset arrow
set table "lin_tandphi_data"
plot "ftaglio1-10" u ( (abs(tan(2*pi*$1*$6*10**-6) - h($1))) <= (2*pi*$1*0.1*$7*10**-6/(sqrt(3)*cos(2*pi*$1*$6*10**-6)**2)) ? ($1) : NaN ):( (abs(tan(2*pi*$1*$6*10**-6) - h($1))) <= (2*pi*$1*0.1*$7*10**-6/(sqrt(3)*cos(2*pi*$1*$6*10**-6)**2)) ? (tan(2*pi*$1*$6*10**-6)) : NaN ):( (abs(tan(2*pi*$1*$6*10**-6) - h($1))) <= (2*pi*$1*0.1*$7*10**-6/(sqrt(3)*cos(2*pi*$1*$6*10**-6)**2)) ? (2*pi*$1*0.1*$7*10**-6/(sqrt(3)*cos(2*pi*$1*$6*10**-6)**2)) : NaN ) every ::3::8 w table
unset table
stats "lin_tandphi_data" using 1 prefix "X"
set print "fit_out2" append
print "%<*lin_tandphi_ris>"
set print "test"
print sprintf("%f %.10f %d %s\n%f %.10f %d %s\n%f %.10f %d %s", c, c_err, p(c_err), "$c$", d, d_err, p(d_err), "$d$", 1.0/d , d_err/(d**2), p(d_err/(d**2)), "$f_t$") 
system("(./printer.sh < test) >> fit_out2; rm test")
set print "fit_out2" append
print sprintf("Compatibilità intercetta con 0 = %.2f", abs(0-c)/c_err)
print sprintf("NDF = %d\n$\\chi^2$ = %.1f\nrho(a,b) = %f", FIT_NDF, FIT_WSSR, -(X_mean/(sqrt(X_stddev**2+X_mean**2))))
print "%</lin_tandphi_ris>"


set out "residui_lin_tandphi.eps"
set zeroaxis
#plot "lin_tandphi_data" u 1:($2-h($1)):(sqrt($3**2 + c_err**2 + $1*d_err**2 -2*$1*(X_mean/(sqrt(X_stddev**2+X_mean**2)))*c_err*d_err)) w yerror lc 'black' t "Residui", "lin_tandphi_data" u 1:($2-h($1)):(sprintf("%.10f",(sqrt($3**2 + c_err**2 + $1*d_err**2 -2*$1*(X_mean/(sqrt(X_stddev**2+X_mean**2)))*c_err*d_err)))) w labels lc 'black' , "lin_tandphi_data" u 1:($2-h($1)):(stringcolumn(3)) w labels offset 0,1 lc 'black',  NaN lc 'white' t sprintf("{/Symbol r} = %f", -(X_mean/(sqrt(X_stddev**2+X_mean**2))) )
plot "lin_tandphi_data" u 1:($2-h($1)):(sqrt($3**2 + c_err**2 + $1*d_err**2 -2*$1*(X_mean/(sqrt(X_stddev**2+X_mean**2)))*c_err*d_err)) w yerror lc 'black' t "Residui", NaN lc 'white' t sprintf("{/Symbol r} = %f", -(X_mean/(sqrt(X_stddev**2+X_mean**2))) )
unset zeroaxis


set out "amplificazione.eps"
set logscale x
set auto fix
set offset 0.1,0.1,0.1,0.1
set xlabel "Frequenza [Hz]"
set ylabel "Modulo funzione di tramsissione"
set grid mxtics lc 'grey70' lw 0.5
unset arrow
#set arrow from 1, graph 0 to 1, graph 1 nohead
f(x)=k/sqrt(1+((x)**2)/(f)**2)
f=390
k=0.99
fit f(x) "ftaglio1-500" using 1:($4/10.0):( $5 == 2 ? ($4/10.0)*sqrt( (0.1*2/sqrt(3)/10.0)**2 + (0.1*$5/sqrt(3)/$4)**2) : (($4/10.0)*sqrt( (0.1*2/sqrt(3)/10.0)**2 + (0.1*$5/sqrt(3)/$4)**2 + 2*(0.03/sqrt(3))**2 ) ) )  via f, k
plot "ftaglio1-500" using 1:($4/10.0):( $5 == 2 ? ($4/10.0)*sqrt( (0.1*2/sqrt(3)/10.0)**2 + (0.1*$5/sqrt(3)/$4)**2) : (($4/10.0)*sqrt( (0.1*2/sqrt(3)/10.0)**2 + (0.1*$5/sqrt(3)/$4)**2 + 2*(0.03/sqrt(3))**2 ) ) ) w error not, f(x) not, NaN lc 'white' title sprintf("f_t = %f%+f", f, f_err), NaN lc 'white' title sprintf("k = %f%+f", k, k_err)
set table "amplificazione_dati"
plot "ftaglio1-500" using 1:($4/10.0):( $5 == 2 ? ($4/10.0)*sqrt( (0.1*2/sqrt(3)/10.0)**2 + (0.1*$5/sqrt(3)/$4)**2) : (($4/10.0)*sqrt( (0.1*2/sqrt(3)/10.0)**2 + (0.1*$5/sqrt(3)/$4)**2 + 2*(0.03/sqrt(3))**2 ) ) ) w table
unset table
set print "fit_out2" append
print "%<*amplificazione_ris>"
set print "test"
print sprintf("%f %.10f %d %s\n%f %.10f %d %s", k, k_err, p(k_err), "$k$", f, f_err, p(f_err), "$f_t$") 
system("(./printer.sh < test) >> fit_out2; rm test")
set print "fit_out2" append
print sprintf("NDF = %d\n$\\chi^2$ = %.1f", FIT_NDF, FIT_WSSR)
print "%</amplificazione_ris>"

set out "bode.eps"
set logscale x
unset logscale y
set key top right Right
set ylabel "20log_{10}(|T|)"
set xlabel "f/f_t"
f = 394
s = -20 # FIXED
set tics nomirror
o(x) = r + s*log10(x)
set offset 0,0.1, 2, 2
fit [10:] o(x) "ftaglio1-500" using ($1/f):(20*log10($4/10.0)):( $5 == 2 ? (20*log10(exp(1))*sqrt( (0.1*2/sqrt(3)/10.0)**2 + (0.1*$5/sqrt(3)/$4)**2) ) : (20*log10(exp(1))*sqrt( (0.1*2/sqrt(3)/10.0)**2 + (0.1*$5/sqrt(3)/$4)**2) + 2*(0.03/sqrt(3))**2) ) via r,s
plot "ftaglio1-500" using ($1/f):(20*log10($4/10.0)):( $5 == 2 ? (20*log10(exp(1))*sqrt( (0.1*2/sqrt(3)/10.0)**2 + (0.1*$5/sqrt(3)/$4)**2) ) : (20*log10(exp(1))*sqrt( (0.1*2/sqrt(3)/10.0)**2 + (0.1*$5/sqrt(3)/$4)**2) + 2*(0.03/sqrt(3))**2) ) w yerror lc 'black' title "Data", [log10(5):] o(x) lc rgb 'violet' title sprintf("%f%+f*log_{10}(f/f_t)",r,s)
set table "bode_dati"
unset logscale x
plot "ftaglio1-500" using (log10($1/f)):(20*log10($4/10.0)):( $5 == 2 ? (20*log10(exp(1))*sqrt( (0.1*2/sqrt(3)/10.0)**2 + (0.1*$5/sqrt(3)/$4)**2) ) : (20*log10(exp(1))*sqrt( (0.1*2/sqrt(3)/10.0)**2 + (0.1*$5/sqrt(3)/$4)**2) + 2*(0.03/sqrt(3))**2) ) w table
unset table
stats [log10(10):] "bode_dati" using 1 prefix "X"
set print "fit_out2" append
print "%<*bode_ris>"
set print "test"
print sprintf("%f %.10f %d %s", r, r_err, p(r_err), "$r$")
print sprintf("%f %.10f %d %s", s, s_err, p(s_err), "$s$") 
system("(./printer.sh < test) >> fit_out2; rm test")
set print "fit_out2" append
print sprintf("NDF = %d\n$\\chi^2$ = %.1f\nrho(a,b) = %f", FIT_NDF, FIT_WSSR, -(X_mean/(sqrt(X_stddev**2+X_mean**2))))
print "%</bode_ris>"

set out "residui_bode.eps"
set zeroaxis
set xlabel "log_{10}(f/f_t)"
set autoscale yfix
set offset 0.1,0.1
plot [log10(50):] [-1:1] "bode_dati" using 1:($2-(r+s*$1)):( sqrt( $3**2 + r_err**2 + ($1*s_err)**2 -2*$1*(X_mean/(sqrt(X_stddev**2+X_mean**2)))*r_err*s_err) ) w  yerror lc "black" t "Residui", NaN lc 'white' t sprintf("{/Symbol r} = %f", -(X_mean/(sqrt(X_stddev**2+X_mean**2))) )

set out "sfasamento.eps"
set format y "%.2P {/Symbol p}"
set offset 0, 0, 0.1, 0
set ylabel "Modulo sfasamento [rad]"
set xlabel "Frequenza [Hz]" offset 0,1
set ytics (0, pi/8.0 ,pi/4.0, 3*pi/8.0, pi/2.0)
set logscale x
l(x) = atan(x/ft)
set grid mxtics lc 'grey70' lw 0.5
set key top left Left reverse
fit [:] l(x) "ftaglio1-500" using 1:(2*pi*$1*$6*10**-6):(2*pi*$1*0.1*$7*10**-6) via ft #:(2*pi*$1*0.1*$7*10**-6)
set arrow from ft, graph 0 to ft, graph 1 nohead lc 'grey50' dt 3
plot [:] [0:pi/2] "ftaglio1-500" using 1:(2*pi*$1*$6*10**-6):(2*pi*$1*0.1*$7*10**-6) w yerror t "Data", l(x) lc rgb 'violet' t sprintf("f_t = %f+-%f", ft, ft_err)
set table "sfasamento_dati"
plot [:] [0:pi/2] "ftaglio1-500" using 1:(2*pi*$1*$6*10**-6):(2*pi*$1*0.1*$7*10**-6) w table
unset table
set print "fit_out2" append
print "%<*sfasamento_ris>"
set print "test"
print sprintf("%f %.10f %d %s", ft, ft_err, p(ft_err), "$f_t$") 
system("(./printer.sh < test) >> fit_out2; rm test")
set print "fit_out2" append
print sprintf("NDF = %d\n$\\chi^2$ = %f", FIT_NDF, FIT_WSSR)
print "%</sfasamento_ris>"
