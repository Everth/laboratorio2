reset
set term post eps enh color
set datafile sep '&'
set sample 100000
set fit errorvariables
set encoding iso_8859_1
set zeroaxis
set output "graphR1.eps"
set xlabel "Corrente [mA]"
set ylabel "Differenza di potenziale [V]" offset 1,0
set key top left Left reverse
f(x) = a + b*x
fit f(x) "graph.txt" using 1:2:4 yerror via a,b 
plot "graph.txt" using 1:2:4 w yerror t "data" lc 'black', f(x) lc rgb 'violet' t sprintf("%f%+f*x",a,b)
stats "graph.txt" using 1 prefix "X" noout
stats "graph.txt" using 2 prefix "Y" noout
set output "residui1.eps"
plot "graph.txt" using 1:($2-f($1)):(sqrt($4**2+a_err**2+$1**2*b_err**2-2*$1*(X_mean/(sqrt(X_stddev**2+X_mean**2))*a_err*b_err))) w yerror t sprintf("Residui"), NaN lc 'white' t sprintf("{/Symbol r} = %f", -(X_mean/(sqrt(X_stddev**2+X_mean**2))) )
rho_pre = -(X_mean/(sqrt(X_stddev**2+X_mean**2)))
# errore fit (sqrt($4**2+a_err**2+$1**2*b_err**2-2*$1*(X_mean/(sqrt(X_stddev**2+X_mean**2))*a_err*b_err)))

#Reiezione
stats "graph.txt" using ( ($2-f($1)) <= (sqrt($4**2+a_err**2+$1**2*b_err**2-2*$1*(X_mean/(sqrt(X_stddev**2+X_mean**2))*a_err*b_err))) ? $1 : 1/0) prefix "X" noout
stats "graph.txt" using ( ($2-f($1)) <= (sqrt($4**2+a_err**2+$1**2*b_err**2-2*$1*(X_mean/(sqrt(X_stddev**2+X_mean**2))*a_err*b_err))) ? $2 : 1/0) prefix "Y" noout
g(x) = c+R*x
set out "graphR2.eps"
fit g(x) "graph.txt" using 1:( ($2-f($1)) <= (sqrt($4**2+a_err**2+$1**2*b_err**2-2*$1*(X_mean/(sqrt(X_stddev**2+X_mean**2))*a_err*b_err))) ? $2 : 1/0):4 yerror via c,R
plot "graph.txt" using 1:( ($2-f($1)) <= (sqrt($4**2+a_err**2+$1**2*b_err**2-2*$1*(X_mean/(sqrt(X_stddev**2+X_mean**2))*a_err*b_err))) ? $2 : 1/0):4 w yerror t "data" lc 'black' , g(x) lc rgb 'violet' t sprintf("%f%+f*x",c,R)
set out "residui2.eps"
plot "graph.txt" using 1:( ($2-f($1)) <= (sqrt($4**2+a_err**2+$1**2*b_err**2-2*$1*(X_mean/(sqrt(X_stddev**2+X_mean**2))*a_err*b_err))) ? ($2-g($1)) : 1/0):(sqrt($4**2+c_err**2+$1**2*R_err**2-2*$1*(X_mean/(sqrt(X_stddev**2+X_mean**2))*c_err*R_err))) w yerror t "residuals", NaN lc 'white' t sprintf("{/Symbol r} = %f", -(X_mean/(sqrt(X_stddev**2+X_mean**2))) )
rho_post = -(X_mean/(sqrt(X_stddev**2+X_mean**2)))

p(x) = ( int(gprintf("%T",x)) > 0 ? 0 : -1*int(gprintf("%T",x)) )
set print "test"
print sprintf("%f %f %d %s\n%f %f %d %s", c, c_err, p(c_err), "c", R, sqrt(R_err**2+R**2*(0.009/sqrt(3))**2), p(R_err), "R") 
system("(./printer.sh < test) > fit_out; rm test")
set print "fit_out" append
print sprintf("Valore sigmaR non corretto = %f \nValore chi quadro per g = %f \nndf = %d\n rho pre reiez = %f", R_err, FIT_WSSR, FIT_NDF, rho_pre)
print sprintf("Valore R + sigmaR post reiezione,corretta con più cifre = %f\t%f\n rho post reiez = %f", R, sqrt(R_err**2+R**2*(0.009/sqrt(3))**2), rho_post)
