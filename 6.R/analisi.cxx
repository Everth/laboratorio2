#include <sstream>
#include <fstream>
#include <cmath>
#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <iterator>

//Restituisce la media sul campione
double valormedio(std::vector<double> a){
	double sum=0;
	int campione=a.size();
	for(double i:a){
	sum+=i;
		}
	return (sum+0.0)/campione;
};

//Restituisce il valore minimo
double smin(std::vector<double> a){
	double min=a[0];
	for(double i:a){
	if(i<min){
	min=i;
	} else {
		continue;
		}
	}
	return min;
}

//Restituisce il valore massimo
double smax(std::vector<double> a){
	double max=a[0];
	for(double i:a){
	if(i>max){
	max=i;
	} else {
		continue;
		}
	}
	return max;
}

//Scarto quadratico medio
double rms(std::vector<double> a){
	double variance;
	double vv=valormedio(a);
	for(double i:a){
		variance+= pow((i-vv),2); }
		return sqrt(variance/(a.size()));
}

//Deviazione standard
double stdev(std::vector<double> a){
	double variance;
	double vv=valormedio(a);
	for(double i:a){
		variance+= pow((i-vv),2); }
		//Uso size-1 perché è la stima più precisa
		return sqrt(variance/(a.size()-1));
}

//Errore della media
double mean_stdev(std::vector<double> a){
	double variance;
	double vv=valormedio(a);
	for(double i:a){
		variance+= pow((i-vv),2.0); }
		return (sqrt(variance/(a.size()-1)))/sqrt(a.size());
}

double covarianza_campionaria ( std::vector<double> a, std::vector<double> b){
	double sum1,sum2,sum3;
	for(std::vector<double>::size_type i = 0; i < a.size() ; ++i){
		sum1 += (a.at(i))*(b.at(i))/a.size();
	}
	for (auto i : a) sum2+= i / a.size();
	for (auto i : b) sum3+= i / b.size();
	return (sum1 - sum2*sum3);
}

double correlazione (std::vector<double> a, std::vector<double> b){
	return covarianza_campionaria(a,b)/(rms(a)*rms(b));
}

std::vector<double> reciproco(std::vector<double> a){
	std::vector<double> temp;
	for (double i : a){
		temp.push_back(1.0/i);
	}
	return temp;
}

std::vector<double> wmean(std::vector<double> data, std::vector<double> error){
		if(data.size() == error.size()){
		int N = data.size();
		double num=0;
		double den=0;
		for (int i = 0; i < N; ++i){
			num+=(data.at(i))/pow(error.at(i),2.0);
			den+=1.0/pow(error.at(i),2.0);
		}
		std::vector<double> result{(num/den), static_cast<double>(sqrt(1.0/den))};
		return result;
	} else exit(1);
}

int findprecision (double a){
	char buffer[15] = "test";
	int n;
	std::string exp;
	sprintf(buffer, "%e", a);
	std::string rounded(buffer);
	rounded.shrink_to_fit();
	std::size_t found = rounded.find('e');
	exp = rounded.substr(found+1,3);
	n= -(std::stoi(exp, nullptr, 0));
	if( n >= 0 ){ return (n);
	} else {
		std::cerr << "Controlla errore!!" << a << "\t" << (n)<< std::endl;
		return (n);
	}
}

std::string prec(double val, double err){
	std::stringstream ss;
	if(findprecision(err) > 0){
		ss << ( abs(findprecision(val)) <= 6 ? std::fixed : std::scientific) << std::setprecision(findprecision(err)) << val;
	} else {
		val = int(val/pow(10.0,-findprecision(err))+0.5);
		val *= pow(10.0,-findprecision(err));
		ss << ( abs(findprecision(val)) <= 6 ? std::fixed : std::scientific) << std::setprecision(0) << val;
		std::cerr << val << '\t' << ss.str() << std::endl; 
	}
	return ss.str();
}

template<typename T> void printElement(std::ostream &a,T t, const int& width, char sep){
    a << std::fixed <<  std::left << std::setw(width+3) << std::setprecision(width) << std::setfill(sep) << t;
}

std::vector<double> interpolazione (std::vector<double> j, std::vector<double> k,std::vector<double> l, std::vector<double> m, std::ostream &out = std::cout, double yerror = 0){
	std::vector<double> *x = new std::vector<double> {0};
	std::vector<double> *y = new std::vector<double> {0};
	std::vector<double> *z = new std::vector<double> {1};
	bool chi = false;
	bool &chong = chi;
	x=&j;
	y=&k;
	int grandezza_campione = k.size();
	std::vector<double> errtemporaneo(grandezza_campione, 1.0);
	if(l.size() != 0) {
		z = &l;
		out<< "#Fit chi quadro (immesso errore singole misure)" <<std::endl;
		out.flush();
		chong = true;
	} else {
		out<< "#Fit minimi quadrati (error defaulted to 1)" <<std::endl;
		out.flush();
		z = &errtemporaneo;
	}
	if (x->size() == y->size()){
		//for(std::vector<double>::size_type i=0; i != x->size(); ++i){std::cout<< "x" << i << " = " << (*x).at(i) << " y" << i << " = " << (*y).at(i) << " z" << i << " = " << (*z).at(i) << std::endl;}
		double delta, a, b , sum1, sum2, sum3, sum4, sum5, sum6, vary, sigmay, sigmaa, sigmab, corr, chisq;
		delta = 0; sum1 = 0; sum2 = 0; sum3 = 0; sum4 = 0; sum5 = 0; vary=0;
		//sum1= (x), sum2=(x)^2, sum3= (x^2), sum4=(y), sum5=(x*y) , sum6 = 1/s^2 ; il fit cercato Ã¨ a+bx
		for(std::vector<double>::size_type i=0; i != x->size(); ++i){
			sum1 += ((*x).at(i))/(pow(((*z).at(i)),2.0));
		}
		sum2 = pow(sum1, 2.0);
		for(std::vector<double>::size_type i=0; i != x->size(); ++i){
			sum3 += pow((*x).at(i),2.0)/pow((*z).at(i),2.0);
		}
		
		for(std::vector<double>::size_type i=0; i != x->size(); ++i){
			sum4 += ((*y).at(i))/pow(((*z).at(i)),2.0);
		}
		
		for(std::vector<double>::size_type i=0; i != x->size(); ++i){
			sum5+=(((*x).at(i))*((*y).at(i)))/pow(((*z).at(i)),2.0);
		}
		
		for(std::vector<double>::size_type i=0; i!= x->size(); ++i){
			sum6+= 1.0/pow((*z).at(i),2.0);
		}
		if (chi == false){
			delta = x->size()*sum3-sum2;
			a = (1.0/delta)*(sum3*sum4-sum1*sum5);
			b = (1.0/delta)*(x->size()*sum5-sum1*sum4);
			if (yerror == 0){
				for (int i = 0; i<x->size(); ++i){
					vary += pow(((a+b*((*x).at(i))-(*y).at(i))),2.0)/(x->size()-2.0);
				}
				sigmay = pow(vary,0.5);
			} else {
			sigmay = yerror;
			}
			sigmaa = sigmay*sqrt(sum3/delta);
			sigmab = sigmay*sqrt(x->size()/delta);
			corr = correlazione(*x,*y);
			double cov_a_b = (-1.0/delta*sum1*sigmay);
			out << "#" << std::left << std::setw(24) << "Intercetta x = " << -a/b << std::left << std::setw(25) << " Errore intercetta x = " << static_cast<double>(sqrt(pow(a/(b*b)*sigmab,2.0)+pow(-1.0/b*sigmaa,2.0))+2*cov_a_b*(-a/(b*b*b))) << std::endl;
			out << "#" << std::left << std::setw(24) << "Intercetta y = " << a << std::left << std::setw(25) << " Errore intercetta y = " << sigmaa << std::left << std::setw(25) << "\n#Coefficiente angolare = " << b << std::left << std::setw(25) << " Errore coefficiente angolare = " << sigmab << std::endl;
			out << "#" << std::left << std::setw(24) << "Errore a posteriori = " << std::left << std::setw(25) << sigmay << std::endl;
			out << "#" << std::left << std::setw(24) << "Correlazione lineare = " << std::left << std::setw(25) << corr << std::endl;
			out << "#" << std::left << "Ascissa" << std::left << "," << "Ordinata" << std::endl;
			for(int i = 0; i < x->size(); ++i){
				printElement(out,x->at(i),findprecision(m.at(i)),',');
				printElement(out,y->at(i),findprecision(sigmay),',');
				//out << std::left << std::setw(25) << std::fixed << x->at(i) << std::left << std::setw(25)  << (*y).at(i) << std::endl;
			}
			return std::vector<double>{a,b,delta,sigmay,sigmaa,sigmab,corr};
			
			
		} else {
			delta = sum6*sum3-sum2;
			a = (1.0/delta)*(sum3*sum4-sum1*sum5);
			b = (1.0/delta)*(sum6*sum5-sum1*sum4);
			if (yerror == 0){
				for (int i = 0; i<x->size(); ++i){
					vary += pow(((a+b*((*x).at(i))-(*y).at(i))),2.0)/(x->size()-2.0);
				}
				sigmay = pow(vary,0.5);
			} else {
			sigmay = yerror;
			}
			sigmaa = sqrt((1.0/delta)*sum3);
			sigmab = sqrt((1.0/delta)*sum6);
			corr = correlazione(*x,*y);
			for(std::vector<double>::size_type i = 0; i < x->size(); ++i){
				chisq += pow(((*y).at(i)-(a+b*(*x).at(i)))/(*z).at(i),2.0);
			}
			double cov_a_b = (-1.0/delta)*sum1*sigmay;
			std::vector<double> intercette{a,-a/b};
			std::vector<double> intercette_err = {sigmaa, sqrt(pow(a/(b*b)*sigmab,2.0)+pow(-1.0/b*sigmaa,2.0)+2*cov_a_b*(-a/(b*b*b)))};
			std::vector<double> intercette_wmean = wmean(intercette, intercette_err);
			out << "#" << std::left << std::setw(24) << "Intercetta x = " << intercette[1] << std::left << std::setw(25) << " Errore intercetta x = " << intercette_err[1] << std::endl;
			out << "#" << std::left << std::setw(24) << "Intercetta y = " << intercette[0] << std::left << std::setw(25) << " Errore intercetta y = " << intercette_err[0] << std::left << std::setw(25) << "\n#Coefficiente angolare = " << b << std::left << std::setw(25) << " Errore coefficiente angolare = " << sigmab << std::endl;
			out << "#" << std::left << std::setw(24) << "Media pesata distanza focale = " << 1.0/intercette_wmean[0] << " +- " <<  intercette_wmean[1]/pow(intercette_wmean[0],2.0) << std::endl;
			out << "#" << std::left << std::setw(24) << "Compatibilità tra intercette = " << fabs(a+a/b)/(sqrt( pow(sigmaa,2.0)+ pow(a/(b*b)*sigmab,2.0)+pow(-1.0/b*sigmaa,2.0) ))<< std::endl;
			out << "#" << std::left << std::setw(24) << "Correlazione lineare = " << std::left << std::setw(25) << corr << std::endl;
			out << "#Chiquadro = " << chisq << "\t sigmaypost = " << sigmay << std::endl;
			out << "#" << std::left <<  "Ascissa " << ',' << "Errore ascissa" << ',' << "Ordinata" << ',' << "Errore ordinata" << std::endl;
			for(int i = 0; i < x->size(); ++i){
				//out << std::left << std::setw(25) << std::fixed << x->at(i) << std::left << std::setw(25) << m.at(i) << std::left << std::setw(25) << (*y).at(i) << std::left << std::setw(25) << (*z).at(i) << std::endl;
				printElement(out,x->at(i),findprecision(m.at(i)),',');
				printElement(out,m.at(i),findprecision(m.at(i)),',');
				printElement(out,y->at(i),findprecision(z->at(i)),',');
				printElement(out,z->at(i),findprecision(z->at(i)),',');
				out << std::endl;
			}
			errtemporaneo.clear();
			return std::vector<double>{a,b,delta,sigmay,sigmaa,sigmab, corr};
		} 
	} else {
		std::cout<<"I dati non sono stati forniti in coppia"<<std::endl;
		out.flush();
		exit(1);
	}
}

int dgtoi(char a){
	if( (a - '0') < 10 ) {
		return (a - '0');
	} else {
		std::cerr << "Forse non era una digit" << std::endl;
	}
}

std::vector<double> instr_err(const char* spec, double measure){
	// char[0] strumento, char[1] unità, char[2] fs
	std::vector<double> result;
	//prima valore, poi sigma tutto, poi sigma fs, poi sigma digit, poi valore fondo scala
	enum instrument{FLUKE=1, AGILENT=2, ICE=3};
	std::vector<std::vector<std::vector<double>>> fluke;
	enum units{ACVOLT=0,DCVOLT=1,OHM=2,CAPACITANCE=3,ACAMP=4,DCAMP=5};
	fluke.push_back(std::vector<std::vector<double>>{ {6,60,600},{0.001,0.01,0.1},{0.01,0.01,0.01},{3,3,3} });
	fluke.push_back(std::vector<std::vector<double>>{ {6,60,600},{0.001,0.01,0.1},{0.007,0.007,0.007},{2,2,2} });
	fluke.push_back(std::vector<std::vector<double>>{ {600,6000,60000,600000,6000000,40000000},{0.1,1,10,100,1000,10000},{0.009,0.009,0.009,0.009,0.009,0.015},{2,1,1,1,1,3} });
	fluke.push_back(std::vector<std::vector<double>>{ {0.000001,0.000001,0.00001,0.001},{pow(10.0,-9),pow(10.0,-8),pow(10.0,-8),pow(10.0,-7),pow(10.0,-6)},{0.019,0.019,0.019,0.019},{2,2,2,2} });
	fluke.push_back(std::vector<std::vector<double>>{ {10},{0.01},{0.015},{3} });
	fluke.push_back(std::vector<std::vector<double>>{ {6,10},{0.001,0.01},{0.01,0.01},{3,3} });
	std::vector<std::vector<std::vector<double>>> agilent;
	agilent.push_back(std::vector<std::vector<double>>{ {0.6,6,60,600,600},{0.0001,0.001,0.01,0.1,0.1},{0.01,0.01,0.01,0.01,0.02},{3,3,3,3,3} });
	agilent.push_back(std::vector<std::vector<double>>{ {0.6,6,60,600,600},{0.0001,0.001,0.01,0.1,0.1},{0.005,0.005,0.005,0.005,0.02},{2,2,2,2,3} });
	agilent.push_back(std::vector<std::vector<double>>{ {600,6000,60000,600000,6000000,60000000},{0.1,1,10,100,1000,10000},{0.009,0.009,0.009,0.009,0.009,0.015},{3,3,3,3,3,3}, {0.00057,0.000057,0.0000057,0.000000570,0.00000100,0.000000100} });
	agilent.push_back(std::vector<std::vector<double>>{ {0.000001,0.00001,0.0001,0.001,0.01},{0.000000001,0.000000001,0.0000001,0.000001,0.00001},{0.019,0.019,0.019,0.019,0.019},{2,2,2,2,2} });
	agilent.push_back(std::vector<std::vector<double>>{ {0.00006,0.0006,6,10},{0.00000001,0.0000001,0.001,0.01},{0.015,0.015,0.015,0.015},{3,3,3,3} });
	agilent.push_back(std::vector<std::vector<double>>{ {0.00006,0.0006,6,10},{0.00000001,0.0000001,0.001,0.01},{0.01,0.01,0.01,0.01},{2,2,2,3} });
	std::vector<double> ice{1.5,0.05};
	
	switch( dgtoi(spec[0]) ){
		case FLUKE: 
		if( dgtoi(spec[1]) <= fluke.size() ){
			if( fabs(measure) < fluke[dgtoi(spec[1])][0][dgtoi(spec[2])]){
				double sigma_dgt = fluke[dgtoi(spec[1])][3][dgtoi(spec[2])]/sqrt(3)*fluke[dgtoi(spec[1])][1][dgtoi(spec[2])];
				double sigma_fs = fluke[dgtoi(spec[1])][2][dgtoi(spec[2])]/sqrt(3)*measure;
				double sigma_kr = fluke[dgtoi(spec[1])][2][dgtoi(spec[2])]/sqrt(3);
				double val_fs = fluke[dgtoi(spec[1])][0][dgtoi(spec[2])];
				//std::cerr << "SIGMA DGT     SIGMA KR " << sigma_dgt << '\t' << sigma_kr << std::endl;
				result = {measure, sqrt( pow(sigma_dgt,2.0) + pow(sigma_fs,2.0) ), sigma_fs, sigma_dgt, sigma_kr, val_fs };
			} else {
				std::cerr << "Valore fuori scala" << std::endl;
			}
		}
		break;
		case AGILENT:
			if( fabs(measure) < agilent[dgtoi(spec[1])][0][dgtoi(spec[2])]){
				double sigma_dgt = agilent[dgtoi(spec[1])][3][dgtoi(spec[2])]/sqrt(3)*agilent[dgtoi(spec[1])][1][dgtoi(spec[2])];
				double sigma_fs = agilent[dgtoi(spec[1])][2][dgtoi(spec[2])]/sqrt(3)*measure;
				double sigma_kr = agilent[dgtoi(spec[1])][2][dgtoi(spec[2])]/sqrt(3);
				double val_fs = agilent[dgtoi(spec[1])][0][dgtoi(spec[2])];
				std::cerr << "SIGMA DGT     SIGMA KR " << sigma_dgt << '\t' << sigma_kr << std::endl;
				result = {measure, sqrt( pow(sigma_dgt,2.0) + pow(sigma_fs,2.0) ), sigma_fs, sigma_dgt, sigma_kr, val_fs };
			} else {
				std::cerr << "Valore fuori scala" << std::endl;
			}
		break;
		case ICE:
			result = {measure, ice[0]*0.05/sqrt(3)*0.01, 0, 0, 0, 0.05};
		break;
		default:
		std::cerr << "Non è stato inserito correttamente lo strumento di misura" << std::endl;
		break;
	}
	return result;
}

void printInTable(std::ostream &a, std::vector<double> b, std::string c){
	//Devo assumere di sapere gia' nel vettore dove sono gli errori. quindi..
	//Nota : b[4] è sigma_kr
	//	ultimo << "\\[R_V = (" << prec(Rfluke[0],Rfluke[1]) << "\\pm" << prec(Rfluke[1],Rfluke[1]) << ")\\: \\Omega\\]" << std::endl;
	a << c + "&" << prec(b[0],b[1]) << "&" << prec(b[1],b[1]) << "&" << prec(b[2],b[2]) << "&" << prec(b[3],b[3]) << "&"  << std::defaultfloat << std::setprecision(6) << b[5] << "\\\\" << std::endl;
	std::cerr << c << findprecision(b[1]) << std::endl;
}

double compatibility(std::vector<double> dato1, std::vector<double> dato2){
	if((dato1.size() >= 2) && (dato2.size() >= 2)){
		return fabs(dato1[0]-dato2[0])/sqrt(pow(dato1[1],2.0)+pow(dato2[1],2.0));
	} else {
		std::cerr << "Forse non mi hai dato l'errore, o i dati, o entrambi" << std::endl;
		return -1;
	}
}

int main(){
	std::vector<double> R1_err,R2_err,R3_err,R4_err,R5_err,R6_err,serie_err,parallelo_err,serie_ind_err,parallelo_ind_err;
	//R1 = 32.2; R2 = 32.2; R3 = 320.8; R4 = 3903; R5 = 150.3; R6 = 250.6;serie=400.8, parallelo=94;
	//nota potrei definire direttamente misura + errore in ogni vettore ma poi dovrei trattarla come struttura di dati a parte o adattare le funzioni. forse lo faccio uguale
	R1_err = instr_err("120",32.2);
	R2_err = instr_err("120",32.2);
	R3_err = instr_err("120",320.8);
	R4_err = instr_err("121",3903);
	R5_err = instr_err("120",150.3);
	R6_err = instr_err("120",250.6);
	serie_err = instr_err("120",400.8);
	parallelo_err = instr_err("120",94);
	std::cerr << std::endl << R5_err[0] << "+-" << R5_err[1] << '\t' << R6_err[0] << "+-" << R6_err[1] << '\t' << R5_err[4] << R6_err[4] << std::endl << std::endl;
	serie_ind_err = {R5_err[0]+R6_err[0],sqrt( pow(R5_err[1],2.0) + pow(R6_err[1],2.0) + pow((R5_err[0]+R6_err[0])*R5_err[4],2.0) ) };
	parallelo_ind_err = {(R5_err[0]*R6_err[0])/(R5_err[0]+R6_err[0]), sqrt( pow(R6_err[0]/(R5_err[0]+R6_err[0]),4.0)*pow(R5_err[1],2.0) + pow(R5_err[0]/(R5_err[0]+R6_err[0]),4.0)*pow(R6_err[1],2.0) + pow((R5_err[0]*R6_err[0])/(R5_err[0]+R6_err[0])*R5_err[4],2.0) )};
	
	std::vector<double> deltaserie, deltaparallelo;
	deltaserie = {(serie_ind_err[0]-serie_err[0]), sqrt( pow(R5_err[1],2.0) + pow(R6_err[1],2.0) + pow(serie_err[1],2.0) + pow((serie_ind_err[0]-serie_err[0])*R5_err[4],2.0) )};
	deltaparallelo = {(parallelo_ind_err[0]-parallelo_err[0]), sqrt( pow(R6_err[0]/(R5_err[0]+R6_err[0]),4.0)*pow(R5_err[1],2.0) + pow(R5_err[0]/(R5_err[0]+R6_err[0]),4.0)*pow(R6_err[1],2.0) + pow( (parallelo_ind_err[0]-parallelo_err[0])*R5_err[4],2.0) )};
	double compserie,compparallelo;
	compserie = fabs(deltaserie[0]/deltaserie[1]);
	compparallelo = fabs(deltaparallelo[0]/deltaparallelo[1]);
	std::cerr << R5_err[0] << '\t' << R5_err[1] << "\t\t" << R6_err[0] << '\t' << R6_err[1] << std::endl << serie_err[0] << '\t' << serie_err[1] << "\t\t" << parallelo_err[0] << '\t' << parallelo_err[1] << std::endl << serie_ind_err[0] << '\t' << serie_ind_err[1] << "\t\t" << parallelo_ind_err[0] << '\t' << parallelo_ind_err[1] << std::endl;
	std::cerr << "compserieepparallelo" << compserie << '\t' << compparallelo << std::endl;
	std::vector<double> Rx_dir, R_car, Vgen, V_Rx, I1, Rcalc, Rfit;
	Rx_dir = instr_err("120",3.3);
	R_car = instr_err("120",489.9);
	Vgen = instr_err("110",3.011);
	V_Rx = instr_err("110",0.019);
	I1 = instr_err("3xx",0.005);
	Rcalc = {V_Rx[0]/I1[0],(V_Rx[0]/I1[0])*sqrt( pow(V_Rx[1]/V_Rx[0],2.0) + pow(I1[1]/I1[0],2.0) ) };
	Rfit = {3.173039, 0.036130};
	//std::cerr << Rcalc[0] << '\t' << Rcalc[1] << std::endl; 
	std::vector<std::vector<double>> V_R, I_R, R_C;
	std::vector<double> Rvalues, Vvalues,Ivalues;
	Rvalues = {30.2, 25.0, 39.9, 45.0,50.0,55.1,60.0,65.0,70.0,75.1};
	Vvalues = {0.148,0.139,0.128,0.120,0.112,0.106,0.100,0.096,0.090,0.086};
	Ivalues = {0.046, 0.0435, 0.040, 0.037, 0.035, 0.033, 0.031, 0.029, 0.028, 0.0265};
	for(auto i : Rvalues) R_C.push_back( instr_err("120",i) );
	for(auto i : Vvalues) V_R.push_back( instr_err("110",i) );
	for(auto i : Ivalues) I_R.push_back( instr_err("3xx",i) );
	std::ofstream graph("graph.txt");
	std::ofstream datifit("datifit");
	datifit << "&$R_C$ $[\\Omega]$ & $V_\\mathit{mis}$ $[V]$ & $\\sigma_{V_\\mathit{mis}}$ $[V]$ & $\\frac{\\sigma_V}{V}$ & $I_\\mathit{mis}$ $[A]$ & $\\sigma_{I_\\mathit{mis}}$ $[A]$ & $\\frac{\\sigma_I}{I}$ \\\\ \\hline" << std::endl;
	//& $\\sigma_{R_C}$ $[\\Omega]$ 
	if ( V_R.size() == I_R.size() && (I_R.size()!= 0) ){
		for (std::vector<std::vector<double>>::size_type i = 0; i < I_R.size(); ++i){
			graph << I_R[i][0] << '&' << V_R[i][0] << '&' << I_R[i][1] << '&' << V_R[i][1] << std::endl;
			datifit << i+1 << '&' << prec(R_C[i][0],R_C[i][1]) << '&' << prec(V_R[i][0],V_R[i][1]) << '&' << prec(V_R[i][1],V_R[i][1]) << '&' << V_R[i][1]/V_R[i][0] << '&' << prec(I_R[i][0],I_R[i][1]) << '&' << prec(I_R[i][1],I_R[i][1]) << '&' << I_R[i][1]/I_R[i][0]  << "\\\\" << std::endl;
		}
	}
	
	
	//unpodioutput
	std::ofstream outfile;
	//outfile.open("fit_out", std::ios_base::app); //append
	//outfile << "R5&\\sigmaR5&R6&\\sigmaR6&R_{serie,mis}&\\sigma_{Rserie,mis}&R_{parallelo,mis}&\\sigma_{Rparallelo,mis}&R_{serie,ind}&\\sigma{Rserie,ind}&R_{parallelo,ind}&\\sigma_{Rparallelo,ind} \\\\" << std::endl;
	//outfile << R5_err[0] << '&' << R5_err[1] << '&' << R6_err[0] << '&' << R6_err[1] << '&' << serie_err[0] << '&' << serie_err[1] << '&' << parallelo_err[0] << '&' << parallelo_err[1] << '&' << serie_ind_err[0] << '&' << serie_ind_err[1] << '&' << parallelo_ind_err[0] << '&' << parallelo_ind_err[1] << std::endl;
	outfile.open("datiRdir");
	outfile << "&$R_\\mathit{mis}$[$\\Omega$] & $\\sigma_R $[$\\Omega$] & $\\sigma_{k_R} $[$\\Omega$]& $\\sigma_\\mathit{dgt}$[$\\Omega$]& $R_{FS}$[$\\Omega$]\\\\ \\hline" << std::endl;
	printInTable(outfile, R1_err, "$R_1$");
	printInTable(outfile, R2_err, "$R_2$");
	printInTable(outfile, R3_err, "$R_3$");
	printInTable(outfile, R4_err, "$R_4$");	
	outfile.close();
	
	std::ofstream outfile2("datiRdir2");
	outfile2 << "&$R_\\mathit{mis}$[$\\Omega$] & $\\sigma_R $[$\\Omega$] & $\\sigma_{k_R} $[$\\Omega$]& $\\sigma_\\mathit{dgt}$[$\\Omega$]& $R_{FS}$[$\\Omega$]\\\\ \\hline" << std::endl;
	printInTable(outfile2, R5_err, "$R_5$");
	printInTable(outfile2, R6_err, "$R_6$");
	printInTable(outfile2, serie_err, "$R_+$");
	printInTable(outfile2, parallelo_err, "$R_{||}$");
	outfile2.close();
	
	std::ofstream outfile3("Rind");
	outfile3 << "\\[R_+ ^\\mathit{ind} = (" << prec(serie_ind_err[0], serie_ind_err[1]) << "\\pm" << prec(serie_ind_err[1], serie_ind_err[1]) << ")\\: \\Omega\\]" << std::endl;
	outfile3 << "\\[R_{||} ^\\mathit{ind} = ("<< prec(parallelo_ind_err[0],parallelo_ind_err[1]) << "\\pm" << prec(parallelo_ind_err[1],parallelo_ind_err[1]) << ")\\: \\Omega\\]" << std::endl;
	outfile3.close();
	
	std::ofstream tempout("voltamperometrica");
	tempout << "Compatibilità serie = " << compserie << std::endl << "Compatibilità parallelo = " << compparallelo << std::endl;
	tempout << "\\[R_x^\\mathit{dir}= (" << prec(Rx_dir[0],Rx_dir[1]) <<"\\pm" << prec(Rx_dir[1],Rx_dir[1]) << ")\\: \\Omega\\]" << std::endl;
	tempout << "\\[V_\\mathit{mis}= (" << prec(V_Rx[0], V_Rx[1]) << "\\pm" << prec(V_Rx[1], V_Rx[1])  << ")\\: V \\]" << std::endl;
	tempout << "\\[I_\\mathit{mis}= (" << prec(I1[0],I1[1]) << "\\pm" << prec(I1[1],I1[1]) << ")\\: A \\]" << std::endl;
	tempout << "\\[R_x^\\mathit{ind1}= (" << prec(Rcalc[0],Rcalc[1]) << "\\pm" << prec(Rcalc[1],Rcalc[1]) << ")\\: \\Omega\\]" << std::endl;
	tempout << "\\[R_x^\\mathit{ind2} = (" << prec(Rfit[0],Rfit[1]) << "\\pm" << prec(Rfit[1],Rfit[1]) << ")\\: \\Omega\\]" << std::endl;
	tempout << "Compatibilità Rdir, Rind1 = " << compatibility(Rx_dir,Rcalc) << std::endl;
	tempout << "Compatibilità Rdir, Rind2 = " << compatibility(Rx_dir,Rfit) << std::endl;
	tempout << "Compatibilità Rind1, Rind2 = " << compatibility(Rfit,Rcalc) << std::endl;
	tempout.close();
	
	
	std::vector<double> V0, ij, R0, Vj, Rgen, Iv, Vv, Rfluke, Ragilent;
	V0 = instr_err("110", 3.042);
	ij = instr_err("3xx", 0.05);
	R0 = instr_err("120", 29.0);
	Vj = instr_err("110", 1.802);
	std::cerr << " SOFIASUPERATTENZIONEQUI " << std::endl;
	for(auto i : V0){ std::cerr << i << '\t';}; std::cerr << std::endl;
	for(auto i : Vj){ std::cerr << i << '\t';}; std::cerr << std::endl;
	Rgen = { (V0[0]-Vj[0])/ij[0] , sqrt( pow(V0[1]/ij[0],2.0) + (Vj[1]/ij[0],2.0) + pow((V0[0]-Vj[0])*ij[1]/pow(ij[0],2.0),2.0) + pow( (V0[0] - Vj[0])*Vj[4]/ij[0],2.0) )};
	Iv = instr_err("250", 0.27*pow(10.0,-6));
	Vv = instr_err("110", 3.042);
	Rfluke = {Vv[0]/Iv[0], (Vv[0]/Iv[0])*sqrt( pow(Vv[1]/Vv[0],2.0) + pow(Iv[1]/Iv[0],2.0) ) };
	Ragilent = instr_err("120", 6.1);
	
	std::ofstream outfile4("Rintgen");
	outfile4 << "&\\emph{Valore}&\\emph{Incertezza totale}&\\emph{$\\sigma_k$}&\\emph{$\\sigma_{dgt}$}&\\emph{Fondo scala} \\\\\\hline" << std::endl;
	printInTable(outfile4, R0, "R [$\\Omega$]");
	printInTable(outfile4, V0, "$V_0 [V]$");
	printInTable(outfile4, Vj, "$V_{\\jmath} [V]$");
	printInTable(outfile4, ij, "$I_{\\jmath} [A]$");
	outfile4.close();
	/*
	std::ofstream outfile5("Rintfluke");
	outfile5 << "&\\emph{Valore}&\\emph{Incertezza totale}&\\emph{$\\sigma_k$}&\\emph{$\\sigma_{dgt}$}&\\emph{Fondo scala} \\\\\\hline" << std::endl;
	//printInTable(outfile5, Iv, "$I_{\\jmath} [A]$"); GRAVI problemi con misure troppo piccine
	outfile5 << "$I_V$&" << std::scientific << Iv[0] << '&' << Iv[1] << '&' << Iv[2] << '&' << Iv[3] << '&' << Iv[5] << "\\\\" << std::endl;
	printInTable(outfile5, Vv, "$V_V [V]$");
	outfile5.close();
	*/
	std::ofstream ultimo("ultimo");
	ultimo << "\\[R_G = (" << prec(Rgen[0],Rgen[1]) << "\\pm" << prec(Rgen[1],Rgen[1]) << ")\\: \\Omega\\]" << std::endl;
	ultimo << "\\[R_V = (" << prec(Rfluke[0],Rfluke[1]) << "\\pm" << prec(Rfluke[1],Rfluke[1]) << ")\\: \\Omega\\]" << std::endl;
	ultimo << "\\[R_A = (" << prec(Ragilent[0],Ragilent[1]) << "\\pm" << prec(Ragilent[1],Ragilent[1]) << ")\\: \\Omega\\]" << std::endl;
	ultimo.close();
	
	return 0;
}

