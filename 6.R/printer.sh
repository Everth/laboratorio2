#!/bin/bash
LC_NUMERIC="en_US.UTF-8"
while read p;
do
num=$(echo $p |awk '{print $1}')
err=$(echo $p |awk '{print $2}')
prec=$(echo $p |awk '{print $3}')
name=$(echo $p |awk '{print $4}')
printf "%s = %.*f \pm %.*f\n" $name $prec $num $prec $err; 
done;
