#include <Riostream.h>
#include <stdlib.h>
#include <TROOT.h>
#include <TSystem.h>
#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TH1D.h"
#include "TF1.h"
#include "TRint.h"
#include "TLegend.h"
#include "TStyle.h"
#include <iostream>
#include <fstream>
#include <string>
#include <memory>
#include <iterator>
#include <algorithm>

//compile g++ -Wall -I. -I `root-config --cflags` -o Part1.out Part1.cxx `root-config --libs`
//run ./Part1.out HgCd.csv HgCd.csv HgCd_bkg.csv HgCd_bkg.csv 


std::shared_ptr<TH1D> ReadHistoFromTextFile(const char *fname, const char *histname=nullptr, bool draw=true){
	std::ifstream infile;
	//std::string line;
	constexpr int maxbins = 4096*4;
	
	infile.open(fname, std::ios::in);
	if(!infile.is_open()){
		std::cerr << fname << " not opened!" << std::endl;
		return nullptr;
	}
	
	std::vector<double> binc;
	binc.reserve(maxbins);
	std::istream_iterator<double> start(infile), end;
	std::copy(start, end, std::back_inserter(binc)); //Confido nell'intelligenza superiore dello standard
	
	infile.close();
	
	
	int fakeargc=0; char* fakeargv[fakeargc];
	TRint *theApp = new TRint("App", &fakeargc, fakeargv,0,0,1);
	
	unsigned nbins = binc.size();
	
	std::cerr << "creating new histo with " << nbins << " bins..." << std::endl;
	TCanvas* canvas = new TCanvas("canvas");
	
	char hname[100];
  if (histname) strcpy(hname,histname);
  else strcpy(hname,"histo");
	
	if (gROOT->FindObject("App")) gROOT->FindObject("App")->Delete();
  if (gROOT->FindObject(hname)) gROOT->FindObject(hname)->Delete(); //Utile se non vuoi che utilizzi successivi creino leak vari (quando va bene root se ne accorge e fa la stessa cosa)
	
	auto h = std::make_shared<TH1D>(hname,hname,nbins,0,nbins);
	for (unsigned i=1; i<=nbins; i++) {
    h->SetBinContent(i,binc[i-1]);
  }

  h->SetEntries(h->Integral());

  if (draw)	h->Draw();
  
  canvas->Update();
  //canvas->WaitPrimitive();
  
  theApp->Run(kTRUE);
  
  return h;
  
}


int main(int argc, char** argv){
	//parser per l'input, da migliorare
	
	std::cerr << argv[0] << '\t' << argv[1] << std::endl;
	auto hsource = ReadHistoFromTextFile(argv[1],argv[2],true);	
	auto hbkg = ReadHistoFromTextFile(argv[3], argv[4], true);
	
	hbkg->SetLineColor(2);

  TCanvas *outfile1=new TCanvas("outfile1"); // apre una prima canvas "c1"
  gStyle->SetOptStat(0); //ma visto che non funziona
  hsource->SetStats(0);
  hbkg->SetStats(0);
  
  
  hsource->Draw();
  hbkg->Draw("same"); // disegna il bkg sovrapposto al primo spettro
  
  char srcmean[30], bgmean[30];
  sprintf(srcmean,"Media segnale : %.2f",hsource->GetMean());
  sprintf(bgmean,"Media fondo : %.2f",hbkg->GetMean());
  
  auto legend = new TLegend(0.6,0.7,0.9,0.9);
  legend->SetHeader("Acquisizione","C"); // option "C" allows to center the header
  legend->AddEntry(hsource.get(),"Acquisizione sorgente","f");
  legend->AddEntry(hbkg.get(),"Acquisizione fondo","l");
  legend->AddEntry((TObject*)0,srcmean); //(TObject*)0 fa molto nullptr
  legend->AddEntry((TObject*)0,bgmean);
  legend->Draw();
  
  outfile1->Print("prisma1_1.root"); // salva la canvas su root file
  
  
  //TH1D *hnet = (TH1D*) hsource->Clone("hnet"); // crea una copia di hsource
  TCanvas *outfile2=new TCanvas("outfile2");
  auto clone = static_cast<TH1D*>(hsource->Clone("hnet"));
  clone->SetStats(10000001);
  std::shared_ptr<TH1D> hnet(clone); // crea una copia di hsource
  hnet->SetLineColor(kBlue);
  hnet->SetTitle("Spettro al netto della sottrazione del fondo");
  hnet->Sumw2(1); // setta il calcolo automatico degli errori statistici
  hnet->Add(hbkg.get(),-1.); // sottrae il background
  
  hnet->Draw(); 
  outfile2->Print("prisma1_2.root"); // salva la canvas su root file
  
  
	return 0;
	
	
	
	
	
	
	
	
	
	
}
