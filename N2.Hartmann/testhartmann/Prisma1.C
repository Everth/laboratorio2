// con ROOT 6 occorre caricare prima la macro ReadHistoFromTextFile.C con .L ...
#include <iostream>
#include "TH1F.h"
#include "TCanvas.h"

TH1F * ReadHistoFromTextFile(const char *fname,
                             const char *histname=nullptr, bool draw=true);

void Prisma1(const char *hname, const char *hnamebkg) {

  // carica gli istogrammi, senza disegnarli
  TH1F *hsource = ReadHistoFromTextFile(hname,"hsource",false); 
  if (!hsource) {
      std::cerr << "*E* Cannot read istrogram from file\n";
      return;
  }
  TH1F *hbkg = ReadHistoFromTextFile(hnamebkg,"hbkg",false);
  if (!hbkg) {
      std::cerr << "*E* Cannot read istrogram from file\n";
      return;
  }
  hbkg->SetLineColor(2);

  TCanvas *c1=new TCanvas("c1"); // apre una prima canvas "c1"
  hsource->Draw();
  hbkg->Draw("same"); // disegna il bkg sovrapposto al primo spettro
  c1->Print("prisma1_1.root"); // salva la canvas su root file

  TH1F *hnet = (TH1F*) hsource->Clone("hnet"); // crea una copia di hsource
  hnet->SetLineColor(1);
  hnet->SetTitle("net spectrum");
  hnet->Sumw2(1); // setta il calcolo automatico degli errori statistici
  hnet->Add(hbkg,-1.); // sottrae il background

  TCanvas *c2=new TCanvas("c2");
  hnet->Draw(); 
  c2->Print("prisma1_2.root"); // salva la canvas su root file
}
