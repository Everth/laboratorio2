#include <Riostream.h>
#include <stdlib.h>
#include <TROOT.h>
#include <TSystem.h>
#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TH1D.h"
#include "TF1.h"
#include "TFitResult.h"
#include "TRint.h"
#include "TLegend.h"
#include "TStyle.h"
#include <iostream>
#include <fstream>
#include <string>
#include <memory>
#include <iterator>
#include <algorithm>
//1(prisma), 0(retta)
//compile g++ -Wall -I. -I `root-config --cflags` -o Part2.out Part2.cxx `root-config --libs` 
//run ./Part2.out prisma1_2.root HgCd_ranges peak 1 1


//penso alla struttura dati. Come voglio che sia strutturato il mio file?
// viewfrom, viewto, TEST, fitfrom, fitto per ogni fit
	
struct FitInfo{
	int viewfrom;
	int viewto;
	int nbins;	
	int fitfrom;
	int fitto;
	
	void print(std::ostream& out=std::cerr){ out << "\t" << viewfrom << "\t" << viewto << "\t" << nbins << "\t" << fitfrom << "\t" << fitto << std::endl;}
	//friend std::ostream& operator<<(std::ostream &os, FitInfo &fitinfo);
	friend std::istream& operator>>(std::istream &is, FitInfo &fitinfo);
};

std::istream& operator>>(std::istream &is, FitInfo &fitinfo)
{
	is >> fitinfo.viewfrom >> fitinfo.viewto >> fitinfo.nbins >> fitinfo.fitfrom >> fitinfo.fitto;
	return is;
}

TFitResultPtr fitmany(TH1D* histogram, FitInfo limits,std::string outputbase = "peak", bool draw=false){
		static unsigned int call_count = 0;
		call_count++; //mi servirà per il file name
		std::string histo(std::string("histo") + std::to_string(call_count)), 
								canvas(std::string("canvas") + std::to_string(call_count)),
								fitfunc(std::string("fitfunc") + std::to_string(call_count));
		std::string outfilename = outputbase + std::to_string(call_count) + ".root";
		std::string outimgname = outputbase + std::to_string(call_count) + ".png";
		auto outfile = std::make_shared<TFile>(outfilename.c_str(),"RECREATE");
		TCanvas* fitcanvas = new TCanvas(canvas.c_str());		
		TH1D* fithisto = (TH1D*)histogram->Clone();
		fithisto->SetName(histo.c_str());
		fithisto->SetTitle(histo.c_str());
		fithisto->GetXaxis()->SetRangeUser(limits.viewfrom, limits.viewto);
		TF1 *func = new TF1(fitfunc.c_str(),"gaus", limits.fitfrom, limits.fitto); //non sto usando bins e in base ad un parametro di fitinfo devo scegliere la f di fit!!
		TFitResultPtr res = fithisto->Fit(func,"S");
		if (draw) fithisto->Draw();
		fitcanvas->Write();
		fithisto->Write();
		func->Write();
		res->Print("V");
		res->Write();
		fitcanvas->SaveAs(outimgname.c_str());
		outfile->Close();
		return res;
	}


int main(int argc, char** argv){
	std::string filedata(argv[1]), fileranges(argv[2]), outputbase(argv[3]);
	bool wannacalibrate = std::stoi(argv[4]);
	bool fitflag = std::stoi(argv[5]);
	
	
	std::ifstream fitranges;
	fitranges.open(fileranges,std::ios::in); //dovrei fare if argc>1 ecc. Questo è il file dove scriveremo i parametri del fit
	if(!fitranges.is_open()){
		std::cerr << fileranges << " not opened!" << std::endl;
		return -1;
	}
	
	std::vector<FitInfo> ranges;
	std::istream_iterator<FitInfo> start(fitranges), end; //devo fare l'operatore >> amico
	std::copy(start, end, std::back_inserter(ranges));
	for (auto i : ranges) i.print();
	
	
	/*std::ifstream datatofit;
	datatofit.open(filedata,std::ios::in); // stessa cosa ma col due. Questo è l'output di prima, cioè tipo prisma1_2.root
	if(!datatofit.is_open()){
		std::cerr << filedata << " not opened!" << std::endl;
		return -2;
	}*/
	
	int fakeargc=1; char* fakeargv[fakeargc];
	TRint *theApp = new TRint("App", &fakeargc, fakeargv,0,0,1);
	gStyle->SetOptStat(kTRUE);
	gStyle->SetOptFit(1111);
	TFile* datatofit = TFile::Open(filedata.c_str());	
	TH1D* histo = static_cast<TH1D*>(static_cast<TCanvas*>(datatofit->Get("outfile2"))->FindObject("hnet"));
	histo->SetLineColor(kBlue);
	TCanvas* bg = new TCanvas("bg");
	histo->SetLineStyle(1);
	histo->SetTitle(filedata.c_str());
	histo->Draw(">>bg");
	bg->Modified();
	bg->Update();
	
	std::string dataimg = filedata;
	dataimg.replace(dataimg.find(".root"), std::string(".root").size(), ".png");
	bg->SaveAs(dataimg.c_str());
	
	std::vector<TFitResultPtr> results;
	for (auto i : ranges) { results.push_back(fitmany(histo, i)); }
	
	
	if(wannacalibrate){
	
		std::vector<double> knownlambdas{	//404.656,
																			435.835,
																			467.816,
																			479.992,
																			508.582, //uno di questi non si vede manco a pagarlo!
																			546.074,
																			576.959,
																			579.065,
																			643.847,
																			};
		
		
		std::ofstream retta("calib_line");
		for(std::vector<TFitResultPtr>::size_type i=0; i < results.size(); ++i){
			TMatrixDSym cov = results[i]->GetCovarianceMatrix(); //non so quanto sia pesante questa richiesta ma non ho un profiler
			retta << results[i]->GetParams()[1] <<  '\t' << knownlambdas[i] << '\t' << sqrt(cov(1,1))<< '\t' << 0 <<  std::endl;
		}
		
		if(fitflag){
			
			TFile *fcalib=new TFile("calib_line.root","RECREATE"); // output file
			TCanvas* canvascalib = new TCanvas;
			gStyle->SetStatX(0.5);
			gStyle->SetStatY(0.9);                
			TGraphErrors *plot2 = new TGraphErrors("calib_line"); 
			plot2->SetTitle("Calibrazione");
			TF1* calibfunc = new TF1("calibfunc","[0] + [1]/((x-[2]))", 500, 5000);
			calibfunc->SetParNames("l0","B","x0");
		
			//scelgo tre punti di riferimento 0 4 7 per trovare dei valori iniziali per i parametri
			double R = (knownlambdas[0]-knownlambdas[4])/(knownlambdas[4]-knownlambdas[7])*(results[4]->GetParams()[1]-results[7]->GetParams()[1])/(results[0]->GetParams()[1]-results[4]->GetParams()[1]);
			double x0 = (R*results[0]->GetParams()[1]-results[7]->GetParams()[1])/(R-1);
			double B = -(knownlambdas[0]-knownlambdas[4])*(results[0]->GetParams()[1]-x0)*(results[4]->GetParams()[1]-x0)/(results[0]->GetParams()[1]-results[4]->GetParams()[1]);
			double l0 = knownlambdas[0]+ B/(x0-results[0]->GetParams()[1]);
			calibfunc->SetParameters(l0,B,x0);
			TFitResultPtr hartmann = plot2->Fit("calibfunc","RS");
			plot2->Draw("AL*");
			plot2->Write();
			calibfunc->Write();
			hartmann->Write();
			canvascalib->SaveAs("calib_line.png");
			fcalib->Write();
		
		
			// GRAFICO DEI RESIDUI
			TCanvas *residuals = new TCanvas("Residui");
			TGraphErrors *gr = new TGraphErrors("calib_line"); // parte da una copia del grafico originale
			for (int i=0; i<plot2->GetN(); i++) {
				double res = plot2->GetY()[i] - calibfunc->Eval(plot2->GetX()[i]); // residuo
				gr->SetPoint(i,plot2->GetX()[i],res);
				double eresy = plot2->GetEY()[i]; // contributo delle Yi
				double eresx = calibfunc->Derivative(plot2->GetX()[i])*plot2->GetEX()[i]; // contrib. Xi (approx. 1 ordine)
				double eres = TMath::Sqrt(eresy*eresy+eresx*eresx);
				gr->SetPointError(i,0,eres);
			}
			gr->SetName("Grafico dei residui");
			gr->SetTitle("Grafico dei residui");
			gr->Draw("Ap");
			gr->Write();
			residuals->Write();
			residuals->SaveAs("calib_line_residuals.png");
		
			std::ofstream resolution("resolution");
			double hw = sqrt(2*log(2));
			for (std::vector<TFitResultPtr>::size_type i = 0; i < results.size(); ++i){
				//TMatrixDSym cov = results[i]->GetCovarianceMatrix();
				double lambda = calibfunc->Eval(results[i]->GetParams()[1]);
				double deltalambda = calibfunc->Eval(results[i]->GetParams()[1] + hw*results[i]->GetParams()[2]) - calibfunc->Eval(results[i]->GetParams()[1] - hw*results[i]->GetParams()[2]);
				resolution << lambda << '\t' << deltalambda << '\t' << deltalambda/lambda << std::endl;
			}
		}
		
		if(!fitflag){
			
			TFile *fcalib=new TFile("calib_line.root","RECREATE"); // output file
			TCanvas* canvascalib = new TCanvas;
			gStyle->SetStatX(0.5);
			gStyle->SetStatY(0.9);                
			TGraphErrors *plot2 = new TGraphErrors("calib_line"); 
			plot2->SetTitle("Calibrazione");
			TF1* calibfunc = new TF1("calibfunc","[0] + [1]*x", 500, 5000);
			calibfunc->SetParNames("A","B");
		/*
			//scelgo tre punti di riferimento 0 4 7 per trovare dei valori iniziali per i parametri
			double R = (knownlambdas[0]-knownlambdas[4])/(knownlambdas[4]-knownlambdas[7])*(results[4]->GetParams()[1]-results[7]->GetParams()[1])/(results[0]->GetParams()[1]-results[4]->GetParams()[1]);
			double x0 = (R*results[0]->GetParams()[1]-results[7]->GetParams()[1])/(R-1);
			double B = -(knownlambdas[0]-knownlambdas[4])*(results[0]->GetParams()[1]-x0)*(results[4]->GetParams()[1]-x0)/(results[0]->GetParams()[1]-results[4]->GetParams()[1]);
			double l0 = knownlambdas[0]+ B/(x0-results[0]->GetParams()[1]);
			calibfunc->SetParameters(l0,B,x0);*/
			TFitResultPtr hartmann = plot2->Fit("calibfunc","RS");
			plot2->Draw("AL*");
			plot2->Write();
			calibfunc->Write();
			hartmann->Write();
			canvascalib->SaveAs("calib_line.png");
			fcalib->Write();
		
		
			// GRAFICO DEI RESIDUI
			TCanvas *residuals = new TCanvas("Residui");
			TGraphErrors *gr = new TGraphErrors("calib_line"); // parte da una copia del grafico originale
			for (int i=0; i<plot2->GetN(); i++) {
				double res = plot2->GetY()[i] - calibfunc->Eval(plot2->GetX()[i]); // residuo
				gr->SetPoint(i,plot2->GetX()[i],res);
				double eresy = plot2->GetEY()[i]; // contributo delle Yi
				double eresx = calibfunc->Derivative(plot2->GetX()[i])*plot2->GetEX()[i]; // contrib. Xi (approx. 1 ordine)
				double eres = TMath::Sqrt(eresy*eresy+eresx*eresx);
				gr->SetPointError(i,0,eres);
			}
			gr->SetName("Grafico dei residui");
			gr->SetTitle("Grafico dei residui");
			gr->Draw("Ap");
			gr->Write();
			residuals->Write();
			residuals->SaveAs("calib_line_residuals.png");
		
			std::ofstream resolution("resolution");
			double hw = sqrt(2*log(2));
			for (std::vector<TFitResultPtr>::size_type i = 0; i < results.size(); ++i){
				//TMatrixDSym cov = results[i]->GetCovarianceMatrix();
				double lambda = calibfunc->Eval(results[i]->GetParams()[1]);
				double deltalambda = calibfunc->Eval(results[i]->GetParams()[1] + hw*results[i]->GetParams()[2]) - calibfunc->Eval(results[i]->GetParams()[1] - hw*results[i]->GetParams()[2]);
				resolution << lambda << '\t' << deltalambda << '\t' << deltalambda/lambda << std::endl;
			}
		}
	}
	
	theApp->Run(kTRUE);
}

