// Macro di esempio che fitta delle misure di assorbimento di raggi gamma
// a differenti spessori di assorbitore, con una funzione esponenziale 
// decrescente piu' un fondo costante

// per le opzioni di fit guardare anche:
// 
// https://root.cern.ch/doc/master/classTH1.html#TH1:Fit
//
#include <iostream>
#include "TROOT.h"
#include "TMath.h"
#include "TGraphErrors.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TF1.h"
#include "TAxis.h"
#include "TFitResult.h"
#include "TStyle.h"

double fitf(double *x, double *par) {
  double fitval = par[0] + par[1]*TMath::Exp(-par[2]*x[0]);
  return fitval;
}

void FitAssorb() {

  // crea l'oggetto GRAFICO CON ERRORI leggendo da file di testo
  TGraphErrors *g = new TGraphErrors("assorb.dat");
  g->SetName("assorb");
  // formattazione del grafico con comandi testuali
  g->SetMarkerColor(4);
  g->SetMarkerStyle(20);
  g->SetTitle("Gamma absorption");
  g->GetXaxis()->SetTitle("Thickness [#mum]");
  g->GetXaxis()->SetTitleSize(0.04);
  g->GetXaxis()->CenterTitle();
  g->GetYaxis()->SetTitle("Counts");
  g->GetYaxis()->SetTitleSize(0.04);
  g->GetYaxis()->CenterTitle();

  TCanvas * c1 = new TCanvas("c1"); // apre la finestra grafica
  
  // Un top Pad per il fit esponenziale
  TPad *top_pad = new TPad("top_pad", "Upper TPad",
		            0.005, 0.505, 0.995, 0.995);
  top_pad->SetFillColor(0);
  top_pad->SetTopMargin(0.02);
  top_pad->SetLeftMargin(0.075);
  top_pad->SetRightMargin(0.075);
  top_pad->SetBottomMargin(0.1);
  top_pad->SetGrid();
  top_pad->SetLogy();
  top_pad->Draw();

  // Un bottom Pad per i residui
  TPad *bottom_pad = new TPad("bottom_pad", "Lower TPad",
                              0.005, 0.005, 0.995, 0.445);
  bottom_pad->SetFillColor(0);
  bottom_pad->SetTopMargin(0.02);
  bottom_pad->SetLeftMargin(0.075);
  bottom_pad->SetRightMargin(0.075);
  bottom_pad->SetBottomMargin(0.1);
  bottom_pad->SetGrid();
  bottom_pad->Draw();

  top_pad->cd();
  g->Draw("ap"); // disegna il grafico a punti (p) nel Top Frame
  g->GetYaxis()->SetRangeUser(2000,50000); // setta il range dell'asse Y

  // due modi per definire la funzione di fit:
  // 1) invocando il costruttore della classe:
  //  TF1 *f1 = new TF1("f1","[0]+[1]*exp(-x*[2])",0,60);
  // 2) tramite una funzione
  TF1 *f1 = new TF1("f1", fitf, 0, 60, 3);

  // imposta i valori iniziali dei parametri
  f1->SetParameters(0, 30000, 0.1);
  f1->SetParNames("bkg", "constant", "absorption corfficient");

  TFitResultPtr r = g->Fit(f1, "RS"); // R = Region   S = StoreResults
  
  gStyle->SetOptFit();
  // gStyle->SetoptFit(1111); // stampa tutto il necessario

  double mu = f1->GetParameter(2);
  double emu = f1->GetParError(2);
  
  std::cout << "\nGamma absorption coefficient mu "
            << mu << " +- " << emu << std::endl;

  // RECUPERA LA MATRICE DI COVARIANZA
  // to access the covariance matrix
  TMatrixDSym cov = r->GetCovarianceMatrix();
  double chi2   = r->Chi2(); // to retrieve the fit chi2

  std::cout << "\nCOVARIANCE MATRIX :\n ";
  cov.Print();

  std::cout << "chi2 : " << chi2 << std::endl;
  std::cout << "sigma_mu = " << TMath::Sqrt(cov(2,2)) << std::endl;


  // GRAFICO DEI RESIDUI
  TGraphErrors *gr = new TGraphErrors("assorb.dat"); // parte da una copia del grafico originale
  for (int i=0; i<g->GetN(); i++) {
    double res = g->GetY()[i] - f1->Eval(g->GetX()[i]); // residuo
    gr->SetPoint(i,g->GetX()[i],res);
    double eresy = g->GetEY()[i]; // contributo delle Yi
    double eresx = f1->Derivative(g->GetX()[i])*g->GetEX()[i]; // contrib. Xi (approx. 1 ordine)
    double eres = TMath::Sqrt(eresy*eresy+eresx*eresx);
    gr->SetPointError(i,0,eres);
  }
  gr->SetName("gr");
  // formattazione del grafico con comandi testuali
  gr->SetMarkerColor(4);
  gr->SetMarkerStyle(20);
  gr->SetTitle("Gamma absorption - residuals");
  gr->GetXaxis()->SetTitle("Thickness [#mum]");
  gr->GetXaxis()->SetTitleSize(0.04);
  gr->GetXaxis()->CenterTitle();
  gr->GetYaxis()->SetTitle("Counts");
  gr->GetYaxis()->SetTitleSize(0.04);
  gr->GetYaxis()->CenterTitle();

  //TCanvas *c2 = new TCanvas("c2");
  //c2->SetGrid();
  bottom_pad->cd();
  gr->Draw("ap");
  gr->GetYaxis()->SetRangeUser(-100,100); // setta il range dell'asse Y

  // Salva l'output in un file
  c1->Print("FitAssorb.png"); // salva la canvas in formato grafico
  c1->Print("FitAssorb.root"); // salva la canvas in formato root: e' possibile modificare la figura in un secondo momento...

}
