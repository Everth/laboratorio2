#include <Riostream.h>
#include <stdlib.h>
#include <TROOT.h>
#include <TSystem.h>
#include <numeric>
#include <vector>
#include <string>
#include "TNtuple.h"
#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TF1.h"

int Plot(TNtuple *tnt, const char *outfilename, int nbin=50, int minint=0, int maxint = 6000, int nbinv=50, int minvmax=0, int maxvmax = 300){
  char out[200],plot[200];
	strcpy(out,outfilename);
	strcpy(plot,outfilename);
	strcat(plot,".root");
  
  TFile *fplot=new TFile(plot,"RECREATE"); // output file
	
	
	/*int PlotIntegral(TNtuple *nt, int nbin=50, int minint=0, int maxint = 6000){
		TH1F *integral = new TH1F("integral","integral counts", nbin, minint, maxint);
		nt->Print();
		nt->Draw("integral>>integral");
		return 0;
		}

	int PlotVmax(TNtuple *nt, int nbinv=50, int minvmax=0, int maxvmax = 300){
		TH1F *vmax = new TH1F("vmax","vmax counts", nbinv, minvmax, maxvmax);
		nt->Draw("vmax>>vmax");	
		return 0;
	}

	int PlotIntegralEv(TNtuple *nt, int nbin=50, int minint=0, int maxint = 6000){
		TH2F *integralev = new TH2F("integralev","integral vs ev", 3000, 0, 3000, nbin, minint, maxint);
		nt->Draw("integral:ev>>integralev");
		return 0;
	}

	int PlotVmaxEv(TNtuple *nt, int nbinv=50, int minvmax=0, int maxvmax = 300){
		TH2F *vmaxev = new TH2F("vmaxev","vmax vs ev", 3000, 0, 3000, nbinv, minvmax, maxvmax);
		nt->Draw("vmax:ev>>vmaxev");
		return 0;
	} */
	//1 Canvas 4 Histos
	TCanvas *Br1 = new TCanvas("Br1","Canvas Integrali e Vmax",200,10,800,640);
	TPad *pad1 = new TPad("padintegral","Integrale",0.02,0.52,0.48,0.98,21);
  TPad *pad2 = new TPad("padvmax","Vmax",0.52,0.52,0.98,0.98,21);
  TPad *pad3 = new TPad("padintegralev","Integrale vs Evento",0.02,0.02,0.48,0.48,21);
  TPad *pad4 = new TPad("padvmaxev","Vmax vs Evento",0.52,0.02,0.98,0.48,21);
  
  pad1->Draw();
  pad2->Draw();
  pad3->Draw();
  pad4->Draw();
	
	// Change default style for the statistics box
  gStyle->SetStatW(0.25);
  gStyle->SetStatH(0.15);
	gStyle->SetStatColor(23);
	gStyle->SetStatX(0.35);
	gStyle->SetStatY(0.9);
	gStyle->SetOptStat("em");
	gStyle->SetFillStyle(0);
	
	//Pad1
	pad1->cd();
	pad1->GetFrame()->SetFillColor(33);
	tnt->SetLineColor(1);
	tnt->SetFillStyle(1001);
  tnt->SetFillColor(kRed);
  TH1F *integral = new TH1F("hintegral","Energy Counts", nbin, minint, maxint);
  tnt->Draw("integral>>hintegral");
	Br1->Update();
	
	//Pad2
	pad2->cd();
  //pad2->SetGrid();
  pad2->GetFrame()->SetFillColor(33);
  tnt->SetFillColor(38);
  TH1F *vmax = new TH1F("hvmax","Vmax counts", nbinv, minvmax, maxvmax);
  tnt->Draw("vmax>>hvmax");
  Br1->Update();
  
  //Pad3
  pad3->cd();
  pad3->GetFrame()->SetFillColor(33);
  //pad3->GetFrame()->SetBorderSize(8);
	TH2F *integralev = new TH2F("hintegralev","Energy vs ev", 3000, 0, 3000, nbin, minint, maxint);
	tnt->Draw("integral:ev>>hintegralev");
	Br1->Update();
	
	//Pad4
	pad4->cd();
	pad4->GetFrame()->SetFillColor(33);
  TH2F *vmaxev = new TH2F("hvmaxev","Vmax vs ev", 3000, 0, 3000, nbinv, minvmax, maxvmax);
	tnt->Draw("vmax:ev>>hvmaxev");
	
	Br1->Update();
	
	strcat(out,".pdf");
	Br1->SaveAs(out);
	
	TNtuple *tClone = (TNtuple *)tnt->CloneTree();
  tClone->Write();
	
	fplot->Write();
	
	
	return 0;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
