#include <Riostream.h>
#include <stdlib.h>
#include <TROOT.h>
#include <TSystem.h>
#include <numeric>
#include <vector>
#include <string>
#include "TNtuple.h"
#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TF1.h"

int Fits(TNtuple *nt, int x1from, int x1to, int x2from, int x2to, int x3from, int x3to){
	
	TCanvas *fits = new TCanvas;
	
	TH1F *integral = new TH1F("integral","Alpha emissions peaks", 100, 3800, 5200);
	nt->SetLineColor(1);
	gStyle->SetOptStat(0);
	nt->Draw("integral>>integral");
	TF1 *pu = new TF1("plutonium","gaus", x1from, x1to);
	integral->Fit("plutonium","R");
	Double_t pucentroid = integral->GetFunction("plutonium")->GetParameter(1);
	gStyle->SetOptFit(0);
	pu->SetLineColor(kBlue);
	pu->Draw("SAME");
	
	
	TF1 *am = new TF1("americium","gaus", x2from, x2to);
	integral->Fit("americium","R");
	am->SetLineColor(kRed);
	am->Draw("SAME");
	Double_t amcentroid = integral->GetFunction("americium")->GetParameter(1);
	
	TF1 *cu = new TF1("curium","gaus", x3from, x3to);
	integral->Fit("curium","R");
	cu->SetLineColor(kAzure);
	cu->Draw("SAME");
	fits->Update();
	Double_t cucentroid = integral->GetFunction("curium")->GetParameter(1);
	
	
	TLegend *legend = new TLegend(.6,0.75,.9,.9);
	legend->SetTextFont(12);
  legend->SetTextSize(0.04);
  
  char spu[30], sam[30], scu[30];
  sprintf(spu,"Plutonium : %.2f",pucentroid);
  sprintf(sam,"Americium : %.2f",amcentroid);
  sprintf(scu,"Curium    : %.2f",cucentroid);
  
	legend->AddEntry(pu, spu);
	legend->AddEntry(am, sam);
	legend->AddEntry(cu, scu);
	legend->SetFillStyle(0);
	legend->Draw();
	
	fits->SaveAs("test.pdf");
	
	
	std::cerr << "Plutonium : " << pucentroid << std::endl
						<< "Americium : " << amcentroid << std::endl
						<< "Curium    : " << cucentroid << std::endl;
						
	
	return 0;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
