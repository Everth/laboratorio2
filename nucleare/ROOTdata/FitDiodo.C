/*
file 1N4148.txt : caratteristica I-V di un diodo 1N4148. Creare una macro
che carica il grafico, lo disegna con asse Y logaritmica, esegue il fit
esponenziale (verificare la regione di fit...) e stampa sullo schermo i
risultati. Potete anche usare i vostri dati raccolti in laboratorio (meglio!)

*/

#include <iostream>
#include "TROOT.h"
#include "TMath.h"
#include "TGraphErrors.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TF1.h"
#include "TAxis.h"
#include "TFitResult.h"
#include "TStyle.h"

double fit (double *x, double *par)
{
	double fitval=par[0]+par[1]*TMath::Exp(par[2]*x[0]);
	return fitval;
}

void FitDiodo()
{
	//cra l'oggetto GRAFICO CON ERRORI leggendo da file di testo
	TGraphErrors *g=new TGraphErrors("1N4148.txt");
	g->SetName("diodo");
	g->SetMarkerColor(4);
	g->SetMarkerStyle(20);
	g->SetTitle("Caratteristica I-V diodo");
	g->GetXaxis()->SetTitle("Differenza di potenziale [V]");
        g->GetXaxis()->SetTitleSize(0.04);
        g->GetXaxis()->CenterTitle();
        g->GetYaxis()->SetTitle("Corrente [A]");
        g->GetYaxis()->SetTitleSize(0.04);
	g->GetYaxis()->CenterTitle();
	

	TCanvas * c1 = new TCanvas("c1"); // apre la finestra grafica

  // Un top Pad per il fit esponenziale
  TPad *top_pad = new TPad("top_pad", "Upper TPad",
                            0.005, 0.505, 0.995, 0.995);
  top_pad->SetFillColor(0);
  top_pad->SetTopMargin(0.02);
  top_pad->SetLeftMargin(0.075);
  top_pad->SetRightMargin(0.075);
  top_pad->SetBottomMargin(0.1);
  top_pad->SetGrid();
  top_pad->SetLogy();
  top_pad->Draw();

  // Un bottom Pad per i residui
  TPad *bottom_pad = new TPad("bottom_pad", "Lower TPad",
                              0.005, 0.005, 0.995, 0.445);
  bottom_pad->SetFillColor(0);
  bottom_pad->SetTopMargin(0.02);
  bottom_pad->SetLeftMargin(0.075);
  bottom_pad->SetRightMargin(0.075);
  bottom_pad->SetBottomMargin(0.1);
  bottom_pad->SetGrid();
  bottom_pad->Draw();

  top_pad->cd();
 
	g->Draw("ap"); // disegna il grafico a punti (p) nel Top Frame
	g->GetYaxis()->SetRangeUser(0, 20e-3); // setta il range dell'asse Y

	TF1 *f1 = new TF1("f1", fit , 0.,1., 3);
	// imposta i valori iniziali dei parametri
	f1->SetParameters(2.0e-9, 20., 2.0e-9);
	f1->SetParNames("fondo", "pendenza", "roba");
	TFitResultPtr r = g->Fit(f1, "RS"); // R = Region   S = StoreResults

	gStyle->SetOptFit(1111); // stampa tutto il necessario
	
	double b = f1->GetParameter(2);
 	double eb = f1->GetParError(2);


	std::cout << "\npendenza b ="
            << b << " +- " << eb << std::endl;

	// RECUPERA LA MATRICE DI COVARIANZA
	  // to access the covariance matrix
	 TMatrixDSym cov = r->GetCovarianceMatrix();
 	 double chi2   = r->Chi2(); // to retrieve the fit chi2

 	 std::cout << "\nCOVARIANCE MATRIX :\n ";
	 cov.Print();

	 std::cout << "chi2 : " << chi2 << std::endl;
	 std::cout << "sigma_b = " << TMath::Sqrt(cov(2,2)) << std::endl;

	// GRAFICO DEI RESIDUI
	 TGraphErrors *gr = new TGraphErrors("1N4148.txt"); // parte da una copia del grafico originale
	
	 for (int i=0; i<g->GetN(); i++)
 {
	 
	 double res = g->GetY()[i] - f1->Eval(g->GetX()[i]); // residuo
     	 gr->SetPoint(i,g->GetX()[i],res);
    	 double eresy = g->GetEY()[i]; // contributo delle Yi
         double eresx = f1->Derivative(g->GetX()[i])*g->GetEX()[i]; // contrib. Xi (approx. 1 ordine)
         double eres = TMath::Sqrt(eresy*eresy+eresx*eresx);
   	 gr->SetPointError(i,0,eres);
  }
 
  gr->SetName("gr");
  // formattazione del grafico con comandi testuali
  gr->SetMarkerColor(4);
  gr->SetMarkerStyle(20);
  gr->SetTitle("Caratteristica I V diodo - residui");
  gr->GetXaxis()->SetTitle("Differenza di Potenziale [V]");
  gr->GetXaxis()->SetTitleSize(0.04);
  gr->GetXaxis()->CenterTitle();
  gr->GetYaxis()->SetTitle("Corrente I [A]");
  gr->GetYaxis()->SetTitleSize(0.04);
  gr->GetYaxis()->CenterTitle();

  //TCanvas *c2 = new TCanvas("c2");
  //c2->SetGrid();
  bottom_pad->cd();
  gr->Draw("ap");
  gr->GetYaxis()->SetRangeUser(-1,1); // setta il range dell'asse Y

  // Salva l'output in un file
  c1->Print("FitDiodo.png"); // salva la canvas in formato grafico
  c1->Print("FitDiodo.root"); // salva la canvas in formato root: e' possibile modificare la figura in un secondo momento...

}
