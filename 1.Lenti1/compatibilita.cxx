#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <iostream>
#include <cmath>
#include <iomanip>
#include <algorithm>
#include <iterator>

//sì ma è dispari?
bool IsOdd (int i) { return (i%2)==1; }

//Restituisce la media sul campione
double valormedio(std::vector<double> a){
	double sum=0;
	int campione=a.size();
	for(double i:a){
	sum+=i;
		}
	return (sum+0.0)/campione;
};

//Restituisce il valore minimo
double smin(std::vector<double> a){
	double min=a[0];
	for(double i:a){
	if(i<min){
	min=i;
	} else {
		continue;
		}
	}
	return min;
}

//Restituisce il valore massimo
double smax(std::vector<double> a){
	double max=a[0];
	for(double i:a){
	if(i>max){
	max=i;
	} else {
		continue;
		}
	}
	return max;
}

//Scarto quadratico medio
double rms(std::vector<double> a){
	double variance;
	double vv=valormedio(a);
	for(double i:a){
		variance+= pow((i-vv),2); }
		return sqrt(variance/(a.size()));
}

//Deviazione standard
double stdev(std::vector<double> a){
	double variance;
	double vv=valormedio(a);
	for(double i:a){
		variance+= pow((i-vv),2); }
		//Uso size-1 perché è la stima più precisa
		return sqrt(variance/(a.size()-1));
}

//Errore della media
double mean_stdev(std::vector<double> a){
	double variance;
	double vv=valormedio(a);
	for(double i:a){
		variance+= pow((i-vv),2.0); }
		return (sqrt(variance/(a.size()-1)))/sqrt(a.size());
}

double covarianza_campionaria ( std::vector<double> a, std::vector<double> b){
	double sum1,sum2,sum3;
	for(std::vector<double>::size_type i = 0; i < a.size() ; ++i){
		sum1 += (a.at(i))*(b.at(i))/a.size();
	}
	for (auto i : a) sum2+= i / a.size();
	for (auto i : b) sum3+= i / b.size();
	return (sum1 - sum2*sum3);
}

double correlazione (std::vector<double> a, std::vector<double> b){
	return covarianza_campionaria(a,b)/(rms(a)*rms(b));
}

std::vector<double> wmean(std::vector<double> data, std::vector<double> error){
		if(data.size() == error.size()){
		int N = data.size();
		double num=0;
		double den=0;
		for (int i = 0; i < N; ++i){
			num+=(data.at(i))/pow(error.at(i),2.0);
			den+=1.0/pow(error.at(i),2.0);
		}
		std::vector<double> result{(num/den), static_cast<double>(sqrt(1.0/den))};
		return result;
	} else exit(1);
}

std::vector<double> wmean_2(std::vector<std::vector<double>> dataerr){
	double num = 0;
	double den = 0;
	for (auto i : dataerr){
		num += (i[0])/pow(i[1],2.0);
		den += 1.0/pow(i[1],2.0);
	}
	std::vector<double> result{(num/den), static_cast<double>(sqrt(1.0/den))};
	return result;
}

double compatibility(std::vector<double> a, std::vector<double> b){
	return fabs(a[0]-b[0])/sqrt(pow(a[1],2.0)+pow(b[1],2.0));
}

int main() {
	std::vector<double> f_es1, f_es2_lin_iato, f_es2_h, f_es2_h_iato, f_es3_B, f_es3_2;
	f_es1 = {6.8731299,0.0711876};
	f_es2_lin_iato = {6.62425, 0.00765451};
	f_es2_h = {6.63667189015636, 0.00230490846705528};
	f_es2_h_iato = {6.66243915978774, 0.0285857053029774};
	f_es3_B = {6.400998,0.010988};
	f_es3_2 = {6.659498,0.016806};
	std::ofstream output("compatibility.txt");
	output << "Compatibilità tra es2_lin_iato ed es2_h\t" << compatibility(f_es2_lin_iato,f_es2_h) << std::endl; 
	output << "Compatibilità tra es2_lin_iato ed es2_h_iato\t" << compatibility(f_es2_lin_iato,f_es2_h_iato) << std::endl; 
	output << "Compatibilità tra es2_h ed es2_h_iato\t" << compatibility(f_es2_h,f_es2_h_iato) << std::endl;
	std::vector<std::vector<double>> es2_mean_data = {f_es2_lin_iato,f_es2_h,f_es2_h_iato};
	std::vector<double> mean_es2 = wmean_2(es2_mean_data);
	output << "Media pesata distanze focali esperienza 2\t" << mean_es2[0] << " +- " << mean_es2[1] << std::endl;
	output << std::endl;
	output << "Compatibilità tra es3_bessel ed es3_2\t" << compatibility(f_es3_B,f_es3_2) << std::endl;
	output << std::endl;
	output << "Compatibilità tra stima es 1 e stima es2\t" << compatibility(f_es1,wmean_2(es2_mean_data)) << std::endl;
	output << "Compatibilità tra stima es 2 e stima es3\t" << compatibility(f_es3_2,wmean_2(es2_mean_data)) << std::endl;
	output << "Compatibilità tra stima es 1 e stima es3\t" << compatibility(f_es1,f_es3_2) << std::endl;
	output << std::endl;
	std::vector<std::vector<double>> es23_mean_data = {mean_es2, f_es3_2};
	output << "Media pesata metodo 2 e metodo 3 = " << wmean_2(es23_mean_data)[0] << " +- " << wmean_2(es23_mean_data)[1] << std::endl;
}
