#include <iostream>
#include <dirent.h>
#include <cmath>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include <locale>
#include <iomanip>
#include <algorithm>
#include <iterator>
#include <regex>

//Legge i nomi dei file in una cartella
std::vector<std::string> read_directory( const std::string& path, std::string reg = ".*") {
	std::vector <std::string> result;
	std::regex reg1(reg.c_str());
	struct dirent* de;
	DIR* dp;
	dp = opendir( path.empty() ? "." : path.c_str() );
	if (dp) {
		while ((de = readdir(dp))) {
			if (de == nullptr) break;
			if (de->d_name[0]=='.' )  {
				std::cerr << de->d_name << " skipped" << std::endl;
				continue;
				} else {
					if(std::regex_match(de->d_name,reg1)){
						result.push_back( std::string(de->d_name) );
					} else {
						std::cerr << de->d_name << " not maching"<< std::endl;
					}
			}
		}
	closedir( dp );
	std::sort( result.begin(), result.end() );
	} else {
	std::cerr<< "Directory non aperta" << std::endl;
	}
	return result;
}

struct decpoint : std::numpunct<char> {
    char do_decimal_point()   const { return '.'; }
};

//Restituisce la media sul campione
double valormedio(std::vector<double> a){
	double sum=0;
	int campione=a.size();
	for(double i:a){
	sum+=i;
		}
	return (sum+0.0)/campione;
};

//Restituisce il valore minimo
double smin(std::vector<double> a){
	double min=a[0];
	for(double i:a){
	if(i<min){
	min=i;
	} else {
		continue;
		}
	}
	return min;
}

//Restituisce il valore massimo
double smax(std::vector<double> a){
	double max=a[0];
	for(double i:a){
	if(i>max){
	max=i;
	} else {
		continue;
		}
	}
	return max;
}

//Scarto quadratico medio
double rms(std::vector<double> a){
	double variance;
	double vv=valormedio(a);
	for(double i:a){
		variance+= pow((i-vv),2); }
		return sqrt(variance/(a.size()));
}

//Deviazione standard
double stdev(std::vector<double> a){
	double variance;
	double vv=valormedio(a);
	for(double i:a){
		variance+= pow((i-vv),2.0); }
		return sqrt(variance/(a.size()-1));
}

//Errore della media
double mean_stdev(std::vector<double> a){
	double variance;
	double vv=valormedio(a);
	for(double i:a){
		variance+= pow((i-vv),2.0); }
		return (sqrt(variance/(a.size()-1)))/sqrt(a.size());
}

double covarianza_campionaria ( std::vector<double> a, std::vector<double> b){
	double sum1,sum2,sum3;
	for(std::vector<double>::size_type i = 0; i < a.size() ; ++i){
		sum1 += (a.at(i))*(b.at(i))/a.size();
	}
	for (auto i : a) sum2+= i / a.size();
	for (auto i : b) sum3+= i / b.size();
	return (sum1 - sum2*sum3);
}

int findprecision (double a){
	char buffer[15] = "test";
	int n;
	std::string exp;
	sprintf(buffer, "%e", a);
	std::string rounded(buffer);
	rounded.shrink_to_fit();
	std::size_t found = rounded.find('e');
	exp = rounded.substr(found+1,3);
	n= -(std::stoi(exp, nullptr, 0));
	return (n);
}

template<typename T> void printElement(std::ostream &a,T t, const int& width, char sep){
	const char def_fill = a.fill();
	const unsigned int def_prec = a.precision();
    a << std::fixed <<  std::left << std::setw(width+3) << std::setprecision(width) << std::setfill(sep) << t;
    a << std::setfill(def_fill);
    a << std::setprecision(def_prec);
}

std::string make_output_filename(const char* name, size_t index) {
   std::ostringstream ss;
   ss << name << index+1;
   return ss.str();
}

double covarianza_campionaria ( std::vector<std::vector<double> > a, std::vector<std::vector<double> > b){
	return covarianza_campionaria(a[0],b[0]);
}

double correlazione (std::vector<double> a, std::vector<double> b){
	return covarianza_campionaria(a,b)/(rms(a)*rms(b));
}

std::vector<double> wmean(std::vector<double> data, std::vector<double> error){
		if(data.size() == error.size()){
		int N = data.size();
		double num=0;
		double den=0;
		for (int i = 0; i < N; ++i){
			num+=(data.at(i))/pow(error.at(i),2.0);
			den+=1.0/pow(error.at(i),2.0);
		}
		std::vector<double> result{(num/den), static_cast<double>(sqrt(1.0/den))};
		return result;
	} else exit(1);
}

std::vector<double> wmean_2(std::vector<std::vector<double>> dataerr){
	double num = 0;
	double den = 0;
	for (auto i : dataerr){
		num += (i[0])/pow(i[1],2.0);
		den += 1.0/pow(i[1],2.0);
	}
	std::vector<double> result{(num/den), static_cast<double>(sqrt(1.0/den))};
	return result;
}

struct campione {
	std::string nome;
	std::vector<double> data_p1;
	std::vector<double> data_p2;
	std::vector<double> mean_p1;
	std::vector<double> mean_p2;
	std::vector<double> singoli_f;
	std::vector<double> singoli_f_err;
	std::vector<double> mean_f_B;
	std::vector<double> mean_f;
	//std::vector<double> mean_f_h;
	std::vector<double> L;
	std::vector<double> L_mean;
	std::vector<double> S;
};

int main() {
	std::string namefmt= "Es3_[0-9]?";
	std::vector<std::string> nome_campione;
	std::vector<campione> sets;
	std::string _DIR_ = "./";
	nome_campione= read_directory(_DIR_,namefmt);
	for(int i = 0; i < nome_campione.size(); ++i){
		std::ifstream input(_DIR_ + nome_campione.at(i));
		if(!input.is_open()) {
			std::cerr << "Impossibile aprire il file " << nome_campione.at(i) << std::endl;
			return 1;
		}
		campione temp1;
		temp1.nome = nome_campione.at(i);
		std::vector<std::vector<double>> temp2,temp3;
		temp2.resize(2);
		temp3.resize(2);
		
		std::string first;
		std::getline(input,first);
	    std::regex rgx("^[^0-9]+([0-9]+\\.*[0-9]*)[^0-9]+([0-9]+\\.*[0-9]*).*$");
		std::smatch match;

		if (std::regex_search(first, match, rgx))
		{
		std::cerr << "Ps e P0" << match[2] << '\t' << match[1] << std::endl; 
        temp1.L = {std::stod(match[2]) - std::stod(match[1])-0.7/3,sqrt(2)*0.05};

		} else std::cerr << "Houston, abbiamo un problema" << std::endl;
		
		// non devo saltare righe !input.ignore(10000,'\n');
		std::istream_iterator<double> start(input), end;
		std::vector<double> numbers(start, end);
		for (std::vector<double>::size_type i=0 ; i < numbers.size(); ++i){
			// p = P - P0 + dr/2 - Vv'/6 
			temp2[i%2].push_back(numbers[i]-20.0+0.23/2-0.7/6);
			temp3[i%2].push_back(numbers[i]);
		}
		temp1.data_p1 = temp2[0];
		temp1.data_p2 = temp2[1];
		temp1.mean_p1 = {valormedio(temp1.data_p1),sqrt(pow(mean_stdev(temp3[0]),2.0) + 2*pow(0.05,2.0))};
		temp1.mean_p2 = {valormedio(temp1.data_p2),sqrt(pow(mean_stdev(temp3[1]),2.0) + 2*pow(0.05,2.0))};
		temp1.S = {(temp1.mean_p2[0] - temp1.mean_p1[0]), sqrt( pow(mean_stdev(temp3[0]),2.0) + pow(mean_stdev(temp3[1]),2.0) )};
		temp1.L_mean = {(temp1.mean_p2[0] + temp1.mean_p1[0]), sqrt( pow(temp1.mean_p1[1],2.0) + pow(temp1.mean_p2[1],2.0))};
		std::cerr << temp1.L[0] << " Errori " << temp1.L_mean[1] << '\t' << temp1.L[1] << std::endl;
		double So2L = temp1.S[0]/(2*temp1.L[0]);
		double So2L_m = temp1.S[0]/(2*temp1.L_mean[0]);
		//std::cerr << temp1.mean_p1[0] << '\t' << temp1.mean_p2[0] << std::endl;
		/*for(std::vector<double>::size_type i = 0; i <temp1.data_p1.size(); ++i){
			
			double temp_l, temp_s, temp_f,sigmal,sigmas;
			//tolgo lo spessore ottico da l che si conta mentre su s si toglie
			temp_l = temp1.data_p1[i]+temp1.data_p2[i]-0.7/3;
			//temp_l = temp1.L;
			temp_s = temp1.data_p1[i]-temp1.data_p2[i];
			sigmal = sqrt(2)*sqrt(2)*0.05; //ammano
			sigmas = sigmal;
			temp_f = (pow(temp_l,2.0) - pow(temp_s,2.0))/(4*temp_l);
			temp1.singoli_f.push_back(temp_f);
			temp1.singoli_f_err.push_back( sqrt( pow((temp_l - 2*temp_f)/(2*temp_l)*sigmal,2.0) + pow(-temp_s/(2*temp_l)*sigmas,2.0) ) );
		}*/
		std::cerr << " AAAAAA " << temp1.L[0] << " AAAAAA " << temp1.L_mean[0] << std::endl;
		temp1.mean_f_B = { (pow(temp1.L[0],2.0) - pow(temp1.S[0],2.0))/(4*(temp1.L[0])), sqrt( pow(So2L*temp1.S[1],2.0) + pow( (0.25+ pow(So2L,2.0))*temp1.L[1],2.0) )};
		temp1.mean_f = { (pow(temp1.L_mean[0],2.0) - pow(temp1.S[0],2.0))/(4*(temp1.L_mean[0])), sqrt( pow(So2L_m*temp1.S[1],2.0) + pow( (0.25+ pow(So2L_m,2.0))*temp1.L_mean[1],2.0) - 2*(So2L_m)*(0.25+pow(So2L_m,2.0))*(temp1.mean_p2[1]-temp1.mean_p1[1]))}; //cie la kovarianza xd
		//temp1.mean_f_h = { (temp1.mean_p1[0]*temp1.mean_p2[0])/(temp1.mean_p1[0]+temp1.mean_p2[0]) , sqrt( pow(temp1.mean_p2[0]/(temp1.mean_p1[0]+temp1.mean_p2[0])*temp1.mean_p1[1],2.0) + pow(temp1.mean_p1[0]/(temp1.mean_p1[0]+temp1.mean_p2[0])*temp1.mean_p2[1],2.0))};
		sets.push_back(temp1);
	}
	std::ofstream out("Es3_out");
	for(campione c : sets){
		out << "Campione " << c.nome << std::endl;
		//out << "Grandezza campione " << c.singoli_f.size() << std::endl;
		//out << std::left << "Distanza focale" << "," << "Errore" << std::endl;
		//out.flush();
		/*for(std::vector<double>::size_type i = 0; i < c.singoli_f.size(); ++i){
			//out << std::left << std::setw(25) << std::fixed << c.singoli_f[i] << std::left << std::setw(24) << c.singoli_f_err[i] << std::endl;
			printElement(out,c.singoli_f[i], findprecision(c.singoli_f_err[i]),',');
			printElement(out,c.singoli_f_err[i], findprecision(c.singoli_f_err[i]),',');
			out << std::endl;
		}*/
		//out << std::endl;
		std::cerr << std::left << std::setw(25) << std::fixed << c.mean_f_B[0] << std::left << std::setw(24) << c.mean_f_B[1] << "  <-- METODO 1 " << std::endl;
		std::cerr << std::left << std::setw(25) << std::fixed << c.mean_f[0] << std::left << std::setw(24) << c.mean_f[1] << "  <-- METODO 2 " << std::endl;
		//std::cerr << std::left << std::setw(25) << std::fixed << c.mean_f_h[0] << std::left << std::setw(24) << c.mean_f_h[1] << "  <-- METODO 3 " << std::endl;
		//std::cerr << std::left << std::setw(25) << std::fixed << wmean(c.singoli_f,c.singoli_f_err)[0] << std::left << std::setw(24) << wmean(c.singoli_f,c.singoli_f_err)[1] << "  <-- media pesata f con singoli p1 p2 " << std::endl;
		out << "Grandezza campione = " << c.data_p1.size() << std::endl;
		out << "Distanza focale METODO 1" << std::endl;
		printElement(out,c.mean_f_B[0],findprecision(c.mean_f_B[1]),',');
		printElement(out,c.mean_f_B[1],findprecision(c.mean_f_B[1]),',');
		out << std::endl;
		
		out << "Distanza focale METODO 2" << std::endl;
		printElement(out,c.mean_f[0],findprecision(c.mean_f[1]),',');
		printElement(out,c.mean_f[1],findprecision(c.mean_f[1]),',');
		out << std::endl;
		out << std::endl;
		out << "L Bessel = " << std::fixed << std::setprecision(findprecision(c.L[1])) << c.L[0] << " +- " << c.L[1] << std::endl;
		out << "L somma = " << std::fixed << std::setprecision(findprecision(c.L_mean[1])) << c.L_mean[0] << " +- " << c.L_mean[1] << std::endl;
		out << std::endl;
		
		
		
		/*out << "Distanza focale METODO 3 (media armonica)" << std::endl;
		printElement(out,c.mean_f_h[0],findprecision(c.mean_f_h[1]),',');
		printElement(out,c.mean_f_h[1],findprecision(c.mean_f_h[1]),',');
		out << std::endl;*/
		
		/*out << "Distanza focale calcolata come media pesata delle distanze focali per ogni coppia p1 p2" << std::endl;
		printElement(out,wmean(c.singoli_f,c.singoli_f_err)[0], findprecision(wmean(c.singoli_f,c.singoli_f_err)[1]), ',');
		printElement(out,wmean(c.singoli_f,c.singoli_f_err)[1], findprecision(wmean(c.singoli_f,c.singoli_f_err)[1]), ',');
		out << std::endl;
		out << std::endl;*/
		
		
	}
	out.precision(6);
	std::vector<std::vector<double>> metodoB = {sets[0].mean_f_B, sets[1].mean_f_B, sets[2].mean_f_B};
	std::vector<std::vector<double>> metodo2 = {sets[0].mean_f, sets[1].mean_f, sets[2].mean_f};
	out << "Media pesata metodo Bessel = " << wmean_2(metodoB)[0] << " +- " << wmean_2(metodoB)[1] << std::endl;
	out << "Media pesata metodo 2 = " << wmean_2(metodo2)[0] << " +- " << wmean_2(metodo2)[1] << std::endl;
			
	
}
