reset
set term postscript eps enhanced color "Verdana,12"
set fit errorvariables
set encoding iso_8859_1 
set sample 5000
set xlabel "Posizione lente [mm]"
set ylabel "Diametro fascio [cm]"
set key center right
set title "Grafico 1 - Interpolazione misure dei diametri" center
set xtics format "%.2f"
set ytics format "%.2f"
Uo=8.5
Vv=7
dr=2.3
Dv=40
Dl=90
Po=20
Pl=27
stats "Es1" using 1 prefix "Mu"
set out "es1.eps"
f(x) = a + b*x
g(x) = c + d*x
a = 1.0
b = 0.1
c = 1.0
d = 0.5
fit f(x) "Es1" using (-$1):2 via a,b
fit g(x) "Es1" using (-$1):3 via c,d
x_int = (c-a)/(b-d)
dfda = 1.0/(b-d)
dfdb = (a-c)/(b-d)**2
dfdc = -1.0/(b-d)
dfdd = (c-a)/(b-d)**2
x_int_err = 0.0822639
f_nocorr = Pl-Po+(Uo-x_int)/10
f_corr = Pl-Po+(Uo-x_int+dr/2-Vv/6)/10
save variables "Es1.dat"
plot [-Mu_max - 0.1: -Mu_min + 0.1] "Es1" using (-$1):2:(0.01) w error lt rgb "blue" title "Diametro vicino", "Es1" using (-$1):3:(0.01) w error lt rgb "red" title "Diametro lontano", f(x) lt rgb "blue" title sprintf("%f + %f*{/Symbol m}",a,b), g(x) lt rgb "red" title sprintf("%f + %f*{/Symbol m}",c,d), NaN lc rgb "white" title sprintf("\nIntersezione = \n%.4f {\261} %.4f",x_int, x_int_err)
system("evince es1.eps&")



