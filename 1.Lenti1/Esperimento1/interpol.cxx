#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <iostream>
#include <cmath>
#include <iomanip>
#include <algorithm>
#include <iterator>

//sì ma è dispari?
bool IsOdd (int i) { return (i%2)==1; }

//Restituisce la media sul campione
double valormedio(std::vector<double> a){
	double sum=0;
	int campione=a.size();
	for(double i:a){
	sum+=i;
		}
	return (sum+0.0)/campione;
};

//Restituisce il valore minimo
double smin(std::vector<double> a){
	double min=a[0];
	for(double i:a){
	if(i<min){
	min=i;
	} else {
		continue;
		}
	}
	return min;
}

//Restituisce il valore massimo
double smax(std::vector<double> a){
	double max=a[0];
	for(double i:a){
	if(i>max){
	max=i;
	} else {
		continue;
		}
	}
	return max;
}

//Scarto quadratico medio
double rms(std::vector<double> a){
	double variance;
	double vv=valormedio(a);
	for(double i:a){
		variance+= pow((i-vv),2); }
		return sqrt(variance/(a.size()));
}

//Deviazione standard
double stdev(std::vector<double> a){
	double variance;
	double vv=valormedio(a);
	for(double i:a){
		variance+= pow((i-vv),2); }
		//Uso size-1 perché è la stima più precisa
		return sqrt(variance/(a.size()-1));
}

//Errore della media
double mean_stdev(std::vector<double> a){
	double variance;
	double vv=valormedio(a);
	for(double i:a){
		variance+= pow((i-vv),2.0); }
		return (sqrt(variance/(a.size()-1)))/sqrt(a.size());
}

double covarianza_campionaria ( std::vector<double> a, std::vector<double> b){
	double sum1,sum2,sum3;
	for(std::vector<double>::size_type i = 0; i < a.size() ; ++i){
		sum1 += (a.at(i))*(b.at(i))/a.size();
	}
	for (auto i : a) sum2+= i / a.size();
	for (auto i : b) sum3+= i / b.size();
	return (sum1 - sum2*sum3);
}

double correlazione (std::vector<double> a, std::vector<double> b){
	return covarianza_campionaria(a,b)/(rms(a)*rms(b));
}

struct rette {
	std::vector<double> ascissa1;
	std::vector<double> ascissa2;
	std::vector<double> ordinata1;
	std::vector<double> ordinata2;
	std::vector<double> interpolazioneprimo;
	std::vector<double> interpolazionesecondo;
	double xint;
	double cov_a_b;
	double cov_c_d;
	double errore_xint_nocov;
	double errore_xint;
};

void interpolazione (rette &e, std::ostream& output = std::cout){
	std::vector<double> *x = new std::vector<double> {0,0,0,0,0,0};
	std::vector<double> *y = new std::vector<double> {0,0,0,0,0,0};
	
	//Interpolazione retta 1
	
	x=&e.ascissa1;
	y=&e.ordinata1;

	if (x->size() == y->size()){
		double delta, a, b , sum1, sum2, sum3, sum4, sum5, vary, sigmay, sigmaa, sigmab, corr, chisq;
		delta = 0; sum1 = 0; sum2 = 0; sum3 = 0; sum4 = 0; sum5 = 0; vary=0;
		//sum1= (x), sum2=(x)^2, sum3= (x^2), sum4=(y), sum5=(x*y) ; il fit cercato è a+bx
		for(double i : *x){
			sum1 += i;
		}
		sum2 = pow(sum1, 2.0);
		for(double i : *x){
			sum3 += pow(i,2.0);
		}

		for(std::vector<double>::size_type i=0; i != x->size(); ++i){
			sum4 += (*y).at(i);
		}

		for(std::vector<double>::size_type i=0; i != x->size(); ++i){
			sum5+=((*x).at(i))*((*y).at(i));
		}
		chisq = 0;
		delta = x->size()*sum3-sum2;
		a = (1.0/delta)*(sum3*sum4-sum1*sum5);
		b = (1.0/delta)*(x->size()*sum5-sum1*sum4);
		for (int i = 0; i< x->size(); ++i){
			vary += pow(((a+b*((*x).at(i))-(*y).at(i))),2.0)/(x->size()-2.0);
		}
		sigmay = pow(vary,0.5);
		sigmaa = sigmay*sqrt(sum3/delta);
		sigmab = sigmay*sqrt(x->size()/delta);
		corr = correlazione(*x,*y);
		e.interpolazioneprimo = {a,b,delta,sigmay,sigmaa,sigmab};
		double cov_a_b = (-1.0/e.interpolazioneprimo.at(2))*sum1*e.interpolazioneprimo.at(3);
		e.cov_a_b = cov_a_b;
		output<<"#Retta 1"<< std::endl;
		output <<  "#Sx = "<< sum1 << "\t(Sx)^2 = " << sum2 <<  "\t Sx^2 = " << sum3 <<  "\t Sy =" << sum4 <<  "\t Sxy =" << sum5 <<  "\t N = " << x->size() << std::endl;
		output << "#" << std::left << std::setw(24) << "Intercetta = " << a << std::left << std::setw(25) << " Errore intercetta = " << sigmaa << std::left << std::setw(25) << "\n#Coefficiente angolare = " << b << std::left << std::setw(25) << " Errore coefficiente angolare = " << sigmab << std::endl;
		output << "#" << std::left << std::setw(24) << "Errore a posteriori = " << std::left << std::setw(25) << sigmay << std::endl;
		output << "#" << std::left << std::setw(24) << "Correlazione lineare = " << std::left << std::setw(25) << corr << std::endl;
		output << "#" << std::left << std::setw(24) << "Ascissa " << std::left << std::setw(25) << "Ordinata" << std::endl;
		for(int i = 0; i < x->size(); ++i){
		output << std::left << std::setw(25) << std::fixed << x->at(i) << std::left << std::setw(25)  << (*y).at(i) << std::endl;
		}
		for(std::vector<double>::size_type i = 0; i < x->size(); ++i){
			chisq += pow(((*y).at(i)-(a+b*(*x).at(i)))/0.01,2.0);
		}
		output << "Chiquadro con errore a priori sul diametro 0.1mm = " << chisq << std::endl;
	} else {std::cerr<<"Il numero delle misure delle ordinate differisce da quello delle ascisse"<<std::endl;
	}



	//Interpolazione retta 2
	output << std::endl;
	x=&e.ascissa2;
	y=&e.ordinata2;
	
	if(x->size() == y->size() ){
		double delta, a, b , sum1, sum2, sum3, sum4, sum5, vary, sigmay, sigmaa, sigmab, corr, chisq;
		delta = 0; sum1 = 0; sum2 = 0; sum3 = 0; sum4 = 0; sum5 = 0; vary=0;
		//sum1= (x), sum2=(x)^2, sum3= (x^2), sum4=(y), sum5=(x*y) ; il fit cercato è c+dx
		for(double i : *x){
			sum1 += i;
		}
		sum2 = pow(sum1, 2.0);
		for(double i : *x){
			sum3 += pow(i,2.0);
		}

		for(std::vector<double>::size_type i=0; i != x->size(); ++i){
			sum4 += (*y).at(i);
		}
		
		for(std::vector<double>::size_type i = 0; i != x->size();++i){
			sum5+=((*x).at(i))*((*y).at(i));
		}
		delta = x->size()*sum3-sum2;
		a = (1.0/delta)*(sum3*sum4-sum1*sum5);
		b = (1.0/delta)*(x->size()*sum5-sum1*sum4);
		for (int i = 0; i< x->size(); ++i){
			vary += pow(((a+b*((*x).at(i))-(*y).at(i))),2.0)/(x->size()-2.0);
		}
		chisq = 0;
		sigmay = sqrt(vary);
		sigmaa = sigmay*sqrt(sum3/delta);
		sigmab = sigmay*sqrt(x->size()/delta);
		corr = correlazione(*x,*y);
		e.interpolazionesecondo = {a,b,delta,sigmay,sigmaa,sigmab};
		double cov_c_d = (-1.0/e.interpolazionesecondo.at(2))*sum1*e.interpolazionesecondo.at(3);
		e.cov_c_d = cov_c_d;		
		output << "#Retta 2"<<std::endl;
		output << "#Sx = "<< sum1 << "\t(Sx)^2 = " << sum2 <<  "\t Sx^2 = " << sum3 <<  "\t Sy =" << sum4 <<  "\t Sxy =" << sum5 <<  "\t N = " << x->size() << std::endl;
		output << "#" << std::left << std::setw(24) << "Intercetta = " << a << std::left << std::setw(25) << " Errore intercetta = " << sigmaa << std::left << std::setw(25) << "\n#Coefficiente angolare = " << b << std::left << std::setw(25) << " Errore coefficiente angolare = " << sigmab << std::endl;
		output << "#" << std::left << std::setw(24) << "Errore a posteriori = " << std::left << std::setw(25) << sigmay << std::endl;
		output << "#" << std::left << std::setw(24) << "Correlazione lineare = " << std::left << std::setw(25) << corr << std::endl;
		output << "#" << std::left << std::setw(24) << "Ascissa " << std::left << std::setw(25) << "Ordinata" << std::endl;
		for(int i = 0; i < x->size(); ++i){
		output << std::left << std::setw(25) << std::fixed << x->at(i) << std::left << std::setw(25)  << (*y).at(i) << std::endl;
		}
		for(std::vector<double>::size_type i = 0; i < x->size(); ++i){
			chisq += pow(((*y).at(i)-(a+b*(*x).at(i)))/0.01,2.0);
		}
		output << "Chiquadro con errore a priori sul diametro 0.1mm = " << chisq << std::endl;
		
	} else {std::cerr<<"Il numero delle misure dell'altezza del peso è diverso dalle misure dei periodi"<<std::endl;}
	{
		double A = e.interpolazioneprimo.at(0);
		double B = e.interpolazioneprimo.at(1);
		double C = e.interpolazionesecondo.at(0);
		double D = e.interpolazionesecondo.at(1);
		double e_A = e.interpolazioneprimo.at(4);
		double e_B = e.interpolazioneprimo.at(5);
		double e_C = e.interpolazionesecondo.at(4);
		double e_D = e.interpolazionesecondo.at(5);
		double dfda = 1.0/(D-B);
		double dfdb = (A-C)/pow((D-B),2.0);
		double dfdc = 1.0/(B-D);
		double dfdd = (C-A)/pow((D-B),2.0);		
		e.xint = (A-C)/(D-B);
		std::cerr << "dfda = " << dfda << "\t dfdb = " << dfdb << "\t dfdc = " << dfdc << "\t dfdd = " << dfdd << std::endl;
		std::cerr << "Covarianze ab cd = " << 2*e.xint/pow((D-B),2.0)*valormedio(*x)*(pow(e_B,2.0)+pow(e_D,2.0)) << std::endl;
		e.errore_xint = sqrt( pow(dfda*e_A,2.0) + pow(dfdb*e_B,2.0) + pow(dfdc*e_C,2.0) + pow(dfdd*e_D,2.0) -2*e.xint/pow((D-B),2.0)*valormedio(*x)*(pow(e_B,2.0)+pow(e_D,2.0)));
		e.errore_xint_nocov = sqrt( pow((1.0/(D-B)),2.0)*pow(e_A,2.0) + pow(((A-C)/pow((D-B),2.0)),2.0)*pow(e_B,2.0) + pow(((-1.0)/(D-B)),2.0)*pow(e_C,2.0) + pow(((C-A)/pow((D-B),2.0)),2.0)*pow(e_D,2.0));
		}
	}


void getData (std::ifstream &input , rette &e, int columns = 3, int skip = 0){
	if(!input.is_open()) {
			std::cerr << "Impossibile aprire il file "  << std::endl;
		}
	for (unsigned int i = 0; i < skip; ++i){
		input.ignore(10000,'\n');
	}
	std::istream_iterator<double> start(input), end;
	std::vector<double> numbers(start, end);
	std::vector<std::vector<double>> temporaneo(columns);
	for (std::vector<double>::size_type i=0 ; i< numbers.size(); ++i){
			temporaneo[i%columns].push_back(numbers.at(i));
			std::cerr << numbers.at(i) << std::endl;
		}
	//ridondante ma non c'ho voglia di fare altro
	e.ascissa1 = temporaneo[0];
	e.ascissa2 = e.ascissa1;
	e.ordinata1 = temporaneo[1];
	e.ordinata2 = temporaneo[2];	
}

int main() {
	rette Esperienza;
	std::ifstream primaes("Es1");
	getData(primaes, Esperienza, 3, 1);
	std::ofstream out1("Es1_out");
	interpolazione(Esperienza,out1);
	out1 << std::endl;
	out1 << "Intersezione " << Esperienza.xint << "\nErrore intersezione (+cov) " << Esperienza.errore_xint << "\nErrore intersezione (nocov) " << Esperienza.errore_xint_nocov << std::endl;
	out1 << "Covarianza a_b = " << Esperienza.cov_a_b << "\nCovarianza c_d = " << Esperienza.cov_c_d << std::endl;
	out1 << "Stima distanza focale = " << 70+8.5+Esperienza.xint << std::endl;
	out1 << "Stima distanza focale con correzione = " << 70+8.5+Esperienza.xint+2.33/2.0-7.0/6 << std::endl;
	std::cerr << Esperienza.errore_xint << std::endl;
	out1 << "Errore distanza focale = " << sqrt( 2*0.5*0.5 + pow(Esperienza.errore_xint,2.0)) << std::endl;
	return 0;
}
