reset
set term postscript eps enhanced color "Verdana,12"
set fit errorvariables
set sample 50000
set encoding iso_8859_1
set title "Grafico 4 - Fit iperbolico con correzione lente spessa da teoria"
set xlabel "Distanza oggetto [cm]"
set ylabel "Distanza immagine [cm]"
set output "Es2_h.eps"
set tics nomirror norotate
FIT_LIMIT = 1e-30
dr=2.3
Vv=7.0
g(x) = (f*x)/(x-f)
f = 6.6
fit g(x) "Es2" using ($1-20+dr/20-Vv/60):($2-$1-dr/20-Vv/60) via f
plot "Es2" using ($1-20+dr/20-Vv/60):($2-$1-dr/20-Vv/60):(sqrt(2)*0.05) w errorbars title "Data, (p;q)", g(x) lt rgb "red" title sprintf("g(x) = %.4f*x/(x-%.4f)",f,f) ,NaN lc rgb "white" title sprintf("\nf = %.4f {\261} %.4f",f,f_err)
system("LANG=C evince Es2_h.eps&")
save variables "Es2_h.dat"

