#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <iostream>
#include <cmath>
#include <iomanip>
#include <algorithm>
#include <iterator>

//sì ma è dispari?
bool IsOdd (int i) { return (i%2)==1; }

//Restituisce la media sul campione
double valormedio(std::vector<double> a){
	double sum=0;
	int campione=a.size();
	for(double i:a){
	sum+=i;
		}
	return (sum+0.0)/campione;
};

//Restituisce il valore minimo
double smin(std::vector<double> a){
	double min=a[0];
	for(double i:a){
	if(i<min){
	min=i;
	} else {
		continue;
		}
	}
	return min;
}

//Restituisce il valore massimo
double smax(std::vector<double> a){
	double max=a[0];
	for(double i:a){
	if(i>max){
	max=i;
	} else {
		continue;
		}
	}
	return max;
}

//Scarto quadratico medio
double rms(std::vector<double> a){
	double variance;
	double vv=valormedio(a);
	for(double i:a){
		variance+= pow((i-vv),2); }
		return sqrt(variance/(a.size()));
}

//Deviazione standard
double stdev(std::vector<double> a){
	double variance;
	double vv=valormedio(a);
	for(double i:a){
		variance+= pow((i-vv),2); }
		//Uso size-1 perché è la stima più precisa
		return sqrt(variance/(a.size()-1));
}

//Errore della media
double mean_stdev(std::vector<double> a){
	double variance;
	double vv=valormedio(a);
	for(double i:a){
		variance+= pow((i-vv),2.0); }
		return (sqrt(variance/(a.size()-1)))/sqrt(a.size());
}

double covarianza_campionaria ( std::vector<double> a, std::vector<double> b){
	double sum1,sum2,sum3;
	for(std::vector<double>::size_type i = 0; i < a.size() ; ++i){
		sum1 += (a.at(i))*(b.at(i))/a.size();
	}
	for (auto i : a) sum2+= i / a.size();
	for (auto i : b) sum3+= i / b.size();
	return (sum1 - sum2*sum3);
}

double correlazione (std::vector<double> a, std::vector<double> b){
	return covarianza_campionaria(a,b)/(rms(a)*rms(b));
}

std::vector<double> reciproco(std::vector<double> a){
	std::vector<double> temp;
	for (double i : a){
		temp.push_back(1.0/i);
	}
	return temp;
}

struct lente {
	std::vector<double> ascissa;
	std::vector<double> errore_x;
	std::vector<double> ordinata;
	std::vector<double> errore_y;
	std::vector<double> interpolazione;
	double correzione;
};

std::vector<double> wmean(std::vector<double> data, std::vector<double> error){
		if(data.size() == error.size()){
		int N = data.size();
		double num=0;
		double den=0;
		for (int i = 0; i < N; ++i){
			num+=(data.at(i))/pow(error.at(i),2.0);
			den+=1.0/pow(error.at(i),2.0);
		}
		std::vector<double> result{(num/den), static_cast<double>(sqrt(1.0/den))};
		return result;
	} else exit(1);
}

int findprecision (double a){
	char buffer[15] = "test";
	int n;
	std::string exp;
	sprintf(buffer, "%e", a);
	std::string rounded(buffer);
	rounded.shrink_to_fit();
	std::size_t found = rounded.find('e');
	exp = rounded.substr(found+1,3);
	n= -(std::stoi(exp, nullptr, 0));
	return (n);
}

template<typename T> void printElement(std::ostream &a,T t, const int& width, char sep){
	const char def_fill = a.fill();
    a << std::fixed <<  std::left << std::setw(width+3) << std::setprecision(width) << std::setfill(sep) << t;
    a << std::setfill(def_fill);
}

std::vector<double> interpolazione (std::vector<double> j, std::vector<double> k,std::vector<double> l, std::vector<double> m, std::ostream &out = std::cout, double yerror = 0){
	std::vector<double> *x = new std::vector<double> {0};
	std::vector<double> *y = new std::vector<double> {0};
	std::vector<double> *z = new std::vector<double> {1};
	//Interpolazione per accelerazione
	bool chi = false;
	x=&j;
	y=&k;
	int grandezza_campione = k.size();
	std::vector<double> errtemporaneo(grandezza_campione, 1.0);
	if(l.size() != 0) {
		z = &l;
		out<< "#Fit chi quadro (immesso errore singole misure)" <<std::endl;
		chi = true;
	} else {
		out<< "#Fit minimi quadrati (error defaulted to 1)" <<std::endl;
		std::cout.flush();
		z = &errtemporaneo;
	}
	if (x->size() == y->size()){
		//for(std::vector<double>::size_type i=0; i != x->size(); ++i){std::cout<< "x" << i << " = " << (*x).at(i) << " y" << i << " = " << (*y).at(i) << " z" << i << " = " << (*z).at(i) << std::endl;}
		double delta, a, b , sum1, sum2, sum3, sum4, sum5, sum6, vary, sigmay, sigmaa, sigmab, corr, chisq;
		delta = 0; sum1 = 0; sum2 = 0; sum3 = 0; sum4 = 0; sum5 = 0; vary=0;
		//sum1= (x), sum2=(x)^2, sum3= (x^2), sum4=(y), sum5=(x*y) , sum6 = 1/s^2 ; il fit cercato Ã¨ a+bx
		for(std::vector<double>::size_type i=0; i != x->size(); ++i){
			sum1 += ((*x).at(i))/(pow(((*z).at(i)),2.0));
		}
		sum2 = pow(sum1, 2.0);
		for(std::vector<double>::size_type i=0; i != x->size(); ++i){
			sum3 += pow((*x).at(i),2.0)/pow((*z).at(i),2.0);
		}
		
		for(std::vector<double>::size_type i=0; i != x->size(); ++i){
			sum4 += ((*y).at(i))/pow(((*z).at(i)),2.0);
		}
		
		for(std::vector<double>::size_type i=0; i != x->size(); ++i){
			sum5+=(((*x).at(i))*((*y).at(i)))/pow(((*z).at(i)),2.0);
		}
		
		for(std::vector<double>::size_type i=0; i!= x->size(); ++i){
			sum6+= 1.0/pow((*z).at(i),2.0);
		}
		if (chi == false){
			delta = x->size()*sum3-sum2;
			a = (1.0/delta)*(sum3*sum4-sum1*sum5);
			b = (1.0/delta)*(x->size()*sum5-sum1*sum4);
			if (yerror == 0){
				for (int i = 0; i<x->size(); ++i){
					vary += pow(((a+b*((*x).at(i))-(*y).at(i))),2.0)/(x->size()-2.0);
				}
				sigmay = pow(vary,0.5);
			} else {
			sigmay = yerror;
			}
			sigmaa = sigmay*sqrt(sum3/delta);
			sigmab = sigmay*sqrt(x->size()/delta);
			corr = correlazione(*x,*y);
			double cov_a_b = (-1.0/delta*sum1*sigmay);
			out << "#" << std::left << std::setw(24) << "Intercetta x = " << -a/b << std::left << std::setw(25) << " Errore intercetta x = " << static_cast<double>(sqrt(pow(a/(b*b)*sigmab,2.0)+pow(-1.0/b*sigmaa,2.0))+2*cov_a_b*(-a/(b*b*b))) << std::endl;
			out << "#" << std::left << std::setw(24) << "Intercetta y = " << a << std::left << std::setw(25) << " Errore intercetta y = " << sigmaa << std::left << std::setw(25) << "\n#Coefficiente angolare = " << b << std::left << std::setw(25) << " Errore coefficiente angolare = " << sigmab << std::endl;
			out << "#" << std::left << std::setw(24) << "Errore a posteriori = " << std::left << std::setw(25) << sigmay << std::endl;
			out << "#" << std::left << std::setw(24) << "Correlazione lineare = " << std::left << std::setw(25) << corr << std::endl;
			out << "#" << std::left << "Ascissa" << std::left << "," << "Ordinata" << std::endl;
			for(int i = 0; i < x->size(); ++i){
				printElement(out,x->at(i),findprecision(m.at(i)),',');
				printElement(out,y->at(i),findprecision(sigmay),',');
				//out << std::left << std::setw(25) << std::fixed << x->at(i) << std::left << std::setw(25)  << (*y).at(i) << std::endl;
			}
			return std::vector<double>{a,b,delta,sigmay,sigmaa,sigmab,corr};
			
			
		} else {
			delta = sum6*sum3-sum2;
			a = (1.0/delta)*(sum3*sum4-sum1*sum5);
			b = (1.0/delta)*(sum6*sum5-sum1*sum4);
			if (yerror == 0){
				for (int i = 0; i<x->size(); ++i){
					vary += pow(((a+b*((*x).at(i))-(*y).at(i))),2.0)/(x->size()-2.0);
				}
				sigmay = pow(vary,0.5);
			} else {
			sigmay = yerror;
			}
			sigmaa = sqrt((1.0/delta)*sum3);
			sigmab = sqrt((1.0/delta)*sum6);
			corr = correlazione(*x,*y);
			for(std::vector<double>::size_type i = 0; i < x->size(); ++i){
				chisq += pow(((*y).at(i)-(a+b*(*x).at(i)))/(*z).at(i),2.0);
			}
			double cov_a_b = (-1.0/delta)*sum1*sigmay;
			std::vector<double> intercette{a,-a/b};
			std::vector<double> intercette_err = {sigmaa, sqrt(pow(a/(b*b)*sigmab,2.0)+pow(-1.0/b*sigmaa,2.0)+2*cov_a_b*(-a/(b*b*b)))};
			std::vector<double> intercette_wmean = wmean(intercette, intercette_err);
			out << "#" << std::left << std::setw(24) << "Intercetta x = " << intercette[1] << std::left << std::setw(25) << " Errore intercetta x = " << intercette_err[1] << std::endl;
			out << "#" << std::left << std::setw(24) << "Intercetta y = " << intercette[0] << std::left << std::setw(25) << " Errore intercetta y = " << intercette_err[0] << std::left << std::setw(25) << "\n#Coefficiente angolare = " << b << std::left << std::setw(25) << " Errore coefficiente angolare = " << sigmab << std::endl;
			out << "#" << std::left << std::setw(24) << "Media pesata distanza focale = " << 1.0/intercette_wmean[0] << " +- " <<  intercette_wmean[1]/pow(intercette_wmean[0],2.0) << std::endl;
			out << "#" << std::left << std::setw(24) << "Compatibilità tra intercette = " << fabs(a+a/b)/(sqrt( pow(sigmaa,2.0)+ pow(a/(b*b)*sigmab,2.0)+pow(-1.0/b*sigmaa,2.0) ))<< std::endl;
			out << "#" << std::left << std::setw(24) << "Correlazione lineare = " << std::left << std::setw(25) << corr << std::endl;
			out << "#Chiquadro = " << chisq << "\t sigmaypost = " << sigmay << std::endl;
			out << "#" << std::left <<  "Ascissa " << ',' << "Errore ascissa" << ',' << "Ordinata" << ',' << "Errore ordinata" << std::endl;
			for(int i = 0; i < x->size(); ++i){
				//out << std::left << std::setw(25) << std::fixed << x->at(i) << std::left << std::setw(25) << m.at(i) << std::left << std::setw(25) << (*y).at(i) << std::left << std::setw(25) << (*z).at(i) << std::endl;
				printElement(out,x->at(i),findprecision(m.at(i)),',');
				printElement(out,m.at(i),findprecision(m.at(i)),',');
				printElement(out,y->at(i),findprecision(z->at(i)),',');
				printElement(out,z->at(i),findprecision(z->at(i)),',');
				out << std::endl;
			}
			return std::vector<double>{a,b,delta,sigmay,sigmaa,sigmab, corr};
		} 
	} else {
		std::cout<<"I dati non sono stati forniti in coppia"<<std::endl;
		exit(1);
	}
}

void getData (std::ifstream &input , lente &e, int columns = 2, int skip = 0){
	if(!input.is_open()) {
			std::cerr << "Impossibile aprire il file "  << std::endl;
		}
	for (unsigned int i = 0; i < skip; ++i){
		input.ignore(10000,'\n');
	}
	std::istream_iterator<double> start(input), end;
	std::vector<double> numbers(start, end);
	std::vector<std::vector<double>> temporaneo(columns);
	for (std::vector<double>::size_type i=0 ; i< numbers.size(); ++i){
			temporaneo[i%columns].push_back(numbers.at(i));
			//std::cerr << numbers.at(i) << std::endl;
		}
	for(std::vector<double>::size_type i = 0; i < temporaneo[0].size(); ++i){
		//è tutto in centimetri
		e.ascissa.push_back(temporaneo[0].at(i)-20+0.23/2);
		e.ordinata.push_back(temporaneo[1].at(i)-temporaneo[0].at(i)-0.23/2);
	}
	e.errore_x = std::vector<double>(e.ascissa.size(),0.05*sqrt(2));
	e.errore_y = std::vector<double>(e.ordinata.size(),0.05*sqrt(2));
}

int main() {
	//senza correzione iato
	lente Esperienza;
	std::ifstream secondaes("Es2");
	getData(secondaes, Esperienza, 2, 0);
	lente Esperienza2 = Esperienza;
	std::ofstream out2("Es2_out");
	for(std::vector<double>::size_type i = 0; i < Esperienza.ordinata.size(); ++i){
		Esperienza.errore_x.at(i)*= (1.0/pow(Esperienza.ascissa.at(i),2.0));
		Esperienza.errore_y.at(i)*= (1.0/pow(Esperienza.ordinata.at(i),2.0));
	}
	Esperienza.interpolazione = interpolazione(reciproco(Esperienza.ascissa),reciproco(Esperienza.ordinata), Esperienza.errore_y, Esperienza.errore_x, out2);
	out2 << "\n#Compatibilità tra b e -1 = " << fabs((Esperienza.interpolazione.at(1)+1)/Esperienza.interpolazione.at(5)) << std::endl;
	
	//con correzione iato
	for(std::vector<double>::size_type i = 0; i < Esperienza2.ordinata.size(); ++i){
		Esperienza2.ascissa.at(i)-= 0.7/6;
		Esperienza2.ordinata.at(i)-= 0.7/6;
	}
	for(std::vector<double>::size_type i = 0; i < Esperienza2.ordinata.size(); ++i){
		Esperienza2.errore_x.at(i)*= (1.0/pow(Esperienza2.ascissa.at(i),2.0));
		Esperienza2.errore_y.at(i)*= (1.0/pow(Esperienza2.ordinata.at(i),2.0));
	}
	
	std::ofstream out2_iato("Es2_out_iato");
	Esperienza2.interpolazione = interpolazione(reciproco(Esperienza2.ascissa),reciproco(Esperienza2.ordinata), Esperienza2.errore_y, Esperienza2.errore_x , out2_iato);
	out2_iato << "\n#Compatibilità tra b e -1 = " << fabs((Esperienza2.interpolazione.at(1)+1)/Esperienza2.interpolazione.at(5)) << std::endl;
	
	
	/*
	lente Esperienza_reiezione;
	std::cerr << Esperienza2.interpolazione[0] << '\t' << Esperienza2.interpolazione[1] << '\t' << Esperienza2.interpolazione[3] << std::endl;
	for(std::vector<double>::size_type i = 0; i < Esperienza2.ordinata.size(); ++i){
		if ( fabs(1.0/Esperienza2.ordinata[i]-(Esperienza2.interpolazione[0]+Esperienza2.interpolazione[1]*1.0/Esperienza2.ascissa[i]))/Esperienza2.errore_y[i] < 3){
			Esperienza_reiezione.ascissa.push_back( Esperienza2.ascissa[i] );
			Esperienza_reiezione.ordinata.push_back( Esperienza2.ordinata[i] );
			Esperienza_reiezione.errore_y.push_back( Esperienza2.errore_y[i] );
			std::cerr << fabs(1.0/Esperienza2.ordinata[i]-(Esperienza2.interpolazione[0]+Esperienza2.interpolazione[1]*1.0/Esperienza2.ascissa[i]))/Esperienza2.errore_y[i] << std::endl;
		} else {
			std::cerr << "Entrata numero " << i << " sputata via" << std::endl;
			std::cerr << fabs(1.0/Esperienza2.ordinata[i]-(Esperienza2.interpolazione[0]+Esperienza2.interpolazione[1]*1.0/Esperienza2.ascissa[i]))/Esperienza2.errore_y[i] << std::endl;
		}
	}
	
	
	std::ofstream out2_reiez("Es2_reiez");
	Esperienza_reiezione.interpolazione = interpolazione(reciproco(Esperienza_reiezione.ascissa),reciproco(Esperienza_reiezione.ordinata), Esperienza_reiezione.errore_y, out2_reiez);
	out2_reiez << "\n#Compatibilità tra b e -1 = " << fabs((Esperienza_reiezione.interpolazione.at(1)+1)/Esperienza_reiezione.interpolazione.at(5)) << std::endl;
	*/
			
			
}
