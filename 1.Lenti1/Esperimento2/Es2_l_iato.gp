reset
set term postscript eps enhanced "Verdana,12"
set encoding iso_8859_1
set datafile separator ','
set title "Grafico 3 - Interpolazione reciproci p e q con correzione iato"
f(x) = a + b * x
set tics nomirror norotate
set xlabel "Reciproco distanza oggetto [cm^{-1}]"
set ylabel "Reciproco distanza immagine [cm^{-1}]"
a = system("cat 'Es2_out_iato' | awk 'NR==3 {print}' | egrep -o '(\\+|-)?([0-9])+\\.([0-9])+' | head -1 ") +0
b = system("cat 'Es2_out_iato' | awk 'NR==4 {print}' | egrep -o '(\\+|-)?([0-9])+\\.([0-9])+' | head -1 ") +0
set output "Es2_l_iato.eps"
plot "Es2_out_iato" using 1:3:4 w error title "(1/p, 1/q)", f(x) title sprintf("%.4f + %.4f * x", a, b)
system("LANG=C evince Es2_l_iato.eps&")
