reset
set term postscript eps enhanced color "Verdana,12"
set fit errorvariables
set sample 50000
set xlabel "Reciproco distanza oggetto p [cm^{-1}]"
set ylabel "Reciproco distanza immagine q [cm^{-1}]"
FIT_LIMIT = 1e-15
dr=2.3
Vv=7.0
f = 7.0
set output "Es2_lin.eps"
unset label
a = 1.0/f
b = -1.0
l(x) = a + b*x

fit l(x) "Es2" using (1.0/($1-20)):(1.0/($2-$1)):(sqrt(2)/(2*($2-$1)**2)) via a,b
set label sprintf("Stima di f = %.5f cm \n  b= %.5f \n intx = %.4f \n inty = %.4f \n comp = %.4f ", 1.0/a, b, -a/(b+0.0), a, (b+1.0)/b_err) at graph 0.9,0.9 right
plot "Es2" using (1.0/($1-20)):(1.0/($2-$1)):(sqrt(2)/(2*($2-$1)**2)) title "Data, (1/p;1/q)" , l(x)
system("evince Es2_lin.eps&")
save variables "Es2_lin.dat"
