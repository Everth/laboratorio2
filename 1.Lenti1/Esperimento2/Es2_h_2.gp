reset
set term postscript eps enhanced color "Verdana,12"
set fit errorvariables
set sample 50000
set encoding iso_8859_1
set title "Grafico 5 - Fit iperbolico con correzione stimata dal fit"
set xlabel "Distanza oggetto [cm]"
set ylabel "Distanza immagine [cm]"
set output "Es2_h_2.eps"
set tics nomirror norotate
FIT_LIMIT = 1e-30
dr=2.3
Vv=7
h = Vv/60
g(x) = (h**2+2*f*h-x*(h+f))/(f+h-x) 
f = 7
h = 0.7/6
fit g(x) "Es2" using ($1-20+dr/20):($2-$1-dr/20) via f,h
plot "Es2" using ($1-20+dr/20):($2-$1-dr/20):(sqrt(2)*0.05) w errorbars title "Data, (p;q)", g(x) lt rgb "red" title sprintf("(%.4f^2+2*%.4f*%.4f-x*(%.4f))/(%.4f-x)",h,f,h,h+f,f+h) ,NaN lc rgb "white" title sprintf("\nf = %.4f {\261} %.4f\nsemispessore ottico = %.4f {\261} %.4f",f,f_err,h,h_err) 
system("evince Es2_h_2.eps&")
save variables "Es2_h_2.dat"

