#include <Riostream.h>
#include <stdlib.h>
#include <TROOT.h>
#include <TSystem.h>
#include <numeric>
#include <vector>
#include <string>
#include "TStyle.h"
#include "TLegend.h"
#include "TH1F.h"
#include "TNtuple.h"
#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TF1.h"
#include "TFitResult.h"


int Fits(TNtuple *nt,const char* outfilename, int x1from, int x1to, int x2from, int x2to, int x3from, int x3to,int nbins = 100,int intlow = 3000,int intup = 5000){
	
	
	TNtuple *tClone = (TNtuple *)nt->CloneTree();
  tClone->Write();
	
  char out[200],plot[200],calibout[200];
	strcpy(out,outfilename);
	strcpy(calibout,outfilename);
	strcpy(plot,outfilename);
	strcat(out,"fit.pdf");
	strcat(calibout,"calib.pdf");
	strcat(plot,"fit.root");
	
	

	TFile *fplot=new TFile(plot,"RECREATE"); // output file


	TCanvas *fits = new TCanvas;
	
	TH1F *integral = new TH1F("integral","Alpha emissions peaks", nbins, intlow, intup);
	nt->SetLineColor(1);
	gStyle->SetOptStat(0); //don't show the panel
	nt->Draw("integral>>integral");
	TF1 *pu = new TF1("plutonium","gaus", x1from, x1to);
	TFitResultPtr pur = integral->Fit("plutonium","RS");
	Double_t pucentroid = integral->GetFunction("plutonium")->GetParameter(1);
	TMatrixDSym pucov = pur->GetCovarianceMatrix();
	Double_t purms = TMath::Sqrt(pucov(1,1)); 
	gStyle->SetStatX(0.5);
	//	gStyle->SetOptFit(1111);
	gStyle->SetOptFit(0);
	pu->SetLineColor(kBlue);
	pu->Draw("SAME");
	
	
	TF1 *am = new TF1("americium","gaus", x2from, x2to);
	TFitResultPtr amr = integral->Fit("americium","RS");
	Double_t amcentroid = integral->GetFunction("americium")->GetParameter(1);
	TMatrixDSym amcov = amr->GetCovarianceMatrix();
	Double_t amrms = TMath::Sqrt(amcov(1,1));
	am->SetLineColor(kRed);
	am->Draw("SAME");
	
	TF1 *cu = new TF1("curium","gaus", x3from, x3to);
	TFitResultPtr cur = integral->Fit("curium","RS");
	Double_t cucentroid = integral->GetFunction("curium")->GetParameter(1);
	TMatrixDSym cucov = cur->GetCovarianceMatrix();
	Double_t curms = TMath::Sqrt(cucov(1,1));
	cu->SetLineColor(kAzure);
	cu->Draw("SAME");
	
	fits->Update();
	
	TLegend *legend = new TLegend(.4,0.75,.9,.9);
	legend->SetTextFont(12);
	legend->SetTextSize(0.04);
  
  char spu[30], sam[30], scu[30];
  sprintf(spu,"Plutonium : %.2f +- %.2f",pucentroid,purms);
  sprintf(sam,"Americium : %.2f +- %.2f",amcentroid,amrms);
  sprintf(scu,"Curium    : %.2f +- %.2f",cucentroid,curms);
  
	legend->AddEntry(pu, spu);
	legend->AddEntry(am, sam);
	legend->AddEntry(cu, scu);
	legend->SetFillStyle(0);
	legend->Draw();
	
	fits->SaveAs(out);
	
	TCanvas *calib = new TCanvas("calib");
	double puth = 5156.59, amth = 5485.56, cuth = 5804.77;
	Double_t ax[] = {pucentroid, amcentroid, cucentroid};
	Double_t ax_err[] = {purms, amrms, curms};
	Double_t ay[] = {puth, amth, cuth};
	TGraph *threepoints = new TGraph(3,ax,ay);
	threepoints->SetTitle("Retta di Calibrazione");
	threepoints->Fit("pol1");
	gStyle->SetOptFit(1111);
	threepoints->Draw("A*");
	calib->SaveAs(calibout);
	
	
	std::cerr << spu << std::endl << sam << std::endl << scu << std::endl;
	
	fplot->Write();

	return 0;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
