#include <Riostream.h>
#include <stdlib.h>
#include <TROOT.h>
#include <TSystem.h>
#include <numeric>
#include <vector>
#include <string>
#include "TNtuple.h"
#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TF1.h"
#include "TH1F.h"
#include "TLegend.h"
#include "TStyle.h"

int FitVmax(TNtuple *nt,const char* outfilename, int x1from, int x1to,int vmaxfrom=0,int vmaxto=300,int nbins = 50,int intlow = 3000,int intup = 5000){
	
  char out[200],plot[200];
	strcpy(out,outfilename);
	strcpy(plot,outfilename);
	strcat(plot,"fitVmax.root");
	strcat(out,"fitVmax.pdf");

	TFile *fplot=new TFile(plot,"RECREATE"); // output file


	TCanvas *fits = new TCanvas;
	
	TH1F *vmax = new TH1F("vmax","Maximum voltage signal", nbins, vmaxfrom, vmaxto);
	nt->SetLineColor(1);
	gStyle->SetOptStat(0);
	std::cerr<<intlow<<" "<<intup<<" "<<std::endl;
	char condizione[50];
	sprintf(condizione,"integral > %d && integral < %d", intlow, intup);
	nt->Draw("vmax>>vmax",condizione);
	//nt->Draw("vmax>>vmax");
	TF1 *fitvmax = new TF1("fvmax","gaus", x1from, x1to);
	vmax->Fit("fvmax","R");
	Double_t vmaxcentroid = vmax->GetFunction("fvmax")->GetParameter(1);
	Double_t vmaxRMS = vmax->GetFunction("fvmax")->GetParameter(2);
	gStyle->SetStatX(0.5);
	gStyle->SetOptFit(1111);
	fitvmax->SetLineColor(kBlue);
	fitvmax->Draw("SAME");
	
	
	
	
	TLegend *legend = new TLegend(.6,0.75,.9,.9);
	legend->SetTextFont(12);
	legend->SetTextSize(0.04);
  
  char svmax[30],svmaxRMS[30];
  sprintf(svmax,"Vmax centroid : %.2f",vmaxcentroid);
  sprintf(svmaxRMS,"Vmax RMS : %.2f",vmaxRMS);
  
	legend->AddEntry(fitvmax, svmax);
	legend->SetFillStyle(0);
	legend->Draw();
	
	fits->SaveAs(out);
	
	
	std::cerr << "Vmax Centroid : " << vmaxcentroid << std::endl;
	std::cerr << "Vmax RMS : " << vmaxRMS << std::endl;
	
	TNtuple *tClone = (TNtuple *)nt->CloneTree();
	tClone->Write();
	
	fplot->Write();

	return 0;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
