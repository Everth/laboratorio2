reset
set term post eps color
set datafile sep "&"
set sample 10000
Av = 1.25e-6
Av_err = 1.25e-8
D = 0.0655
D_err = 0.0005
l = 670e-9
l_err = 5e-9
FIT_LIMIT = e-20
Th(n) = real(n*Av)/D
stats "./Fits/1f3000_1/1f3000_1min" using 6
f(x) = k*exp(-0.5*(x/s)**2)
g(x) = h*(sin(pi*a*sin(atan(x*Av/D))/l)**2)/(pi*a*sin(atan(x*Av/D))/l)**2
h(x) = h*(sin(pi*a*sin(atan(x*Av/D))/l)**2)/(pi*a*sin(atan(x*Av/D))/l)**2*(sin(pi*d*4/l*sin(atan(x*Av/D)))**2)/(sin(pi*d/l*sin(atan(x*Av/D)))**2)
k = 100000
h=k
s = 360
a = 0.93e-5
d = 1.98e-5
fit f(x) "./Fits/1f3000_1/1f3000_1min" using 2:6 via s,k
fit g(x) "test1f" using 1:($2-f($1)) via h, a
set out "test1.eps"
a = 0.000045
#plot "test1f" using 1:($2-f($1)), "./Fits/1f3000_1/1f3000_1min" using 2:6, f(x), g(x) 
plot "test1f" using 1:($2-f($1)), g(x) title sprintf("a = %f",a)
set out "test4.eps"
a = 4*a
stats "./Sits/4f3000_1/4f3000_1max" using 6
fit f(x) "./Fits/4f3000_1/4f3000_1min" using 2:6 via s,k
fit h(x) "test4f" using (($1)+20):($2-f($1)) via a, d, h
d = 2*d
h = 1.3*h
#plot "test4f" using 1:(($2)-f($1)) w l, "./Fits/4f3000_1/4f3000_1min" using 2:6 pt 2, f(x), h(x+20)
plot "test4f" using 1:(($2)-f($1)) w l,  h(x+20) w l 


