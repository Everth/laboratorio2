reset
set term postscript eps enhanced
set sample 10000
set xlabel "Passi"
set ylabel "Conteggi"
list1 = system("ls Plots/ | grep -v 'eps' | grep -E '[[:digit:]]f[[:digit:]]+(_[[:digit:]]+)?'")
do for [file in list1]{
stats "Plots/".file using 1
set output "Plots/".file.".eps"
plot [STATS_min-50:STATS_max+50] "Plots/".file w l lw 1 title ( (file[0:1] eq "1") ? "Grafico conteggi ".file[0:1]." fenditura" : "Grafico conteggi ".file[0:1]." fenditure" )
}
list2 = system("ls Plots/ | grep -v 'eps' | grep -E 'rumore' ")
do for [rumore in list2]{
stats "Plots/".rumore using 1
set output "Plots/".rumore.".eps"
plot [STATS_min-50:STATS_max+50] "Plots/".rumore w l lw 1 title "Rumore"
}
set term postscript eps color
set output "tagli.eps"
set tmargin at screen 0.95
set bmargin at screen 0.15
set rmargin at screen 0.95
set lmargin at screen 0.15
set key at graph 0.95, graph 0.5 Right
set key font ",12"
tagli = "170010 176385 182760 189136 195511"
colori = "#C71585 #6495ED #03A89E #FFD700 #8A360F"
nomicolori = "medioviolarosso fiordaliso blumanganese oro terradisienabruciata"
plot [-800:800] [120000:] "./Plots/1f3000_2" pt 1 ps 1 lt 2 lc rgb "black" title "Picco centrale", for [i=1:words(nomicolori)] (real(word(tagli,i))) w l ls 2 lc rgb word(colori,i) title "Taglio altezza ".word(tagli,i)
