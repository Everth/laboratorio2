reset
set term postscript eps enhanced
set datafile separator '&'
set fit errorvariables
set tics nomirror
FIT_LIMIT = 1e-15
set sample 10000
Av = 1.25e-6
Av_err = 1.25e-8
D = 0.0655
D_err = 0.0005
l = 670e-9
l_err = 5e-9
Th(n) = real(n*Av)/D
Err(n,n_err) = abs(Th(n)*cos(atan(Th(n)))/(1+Th(n)**2))*sqrt((Av_err/Av)**2+(n_err/n)**2+(D_err/D)**2)
list1 = system("ls Sits/")
do for [folder in list1]{
relpath = "./Sits/".folder."/"
set xtic
unset grid
set print "temppath"
print relpath
print folder
system("VAR=$(head -n 1 temppath); VAR2=$(awk 'NR==2' temppath); echo -n '' > $VAR$VAR2'max'")
listpos = system("VAR=$(head -n 1 temppath); ls $VAR | grep -E '^pos_([[:digit:]])+$' ")
do for [posfile in listpos]{
set xlabel "Passi"
set ylabel "Conteggi"
set out relpath.posfile.".eps"
set print "tempfile"
print relpath.posfile
print posfile
f(x) = a*x**2 + b*x + c
stats relpath.posfile using 2
a = real( system("VAR=$(head -n 1 tempfile); head -n 1 $VAR | grep -E -o 'a = -?[[:digit:]]+(.[[:digit:]]+)?' | sed 's/a = //' ") )
b = real( system("VAR=$(head -n 1 tempfile); head -n 1 $VAR | grep -E -o 'b = -?[[:digit:]]+(.[[:digit:]]+)?' | sed 's/b = //' ") )
c = STATS_min
unset key
fit f(x) relpath.posfile via a,b,c
plot relpath.posfile, f(x)
xmax = -1*(b)/(2.0*a) #IN PASSI
ymax = (4*a*c-b**2)/(4*a)
xmax_err_post = sqrt( ( (b*real(a_err))/(2*a**2) )**2 + ( (-1*real(b_err))/(2*a) )**2 )
mpos = int( system("awk 'NR==2' tempfile | sed 's/pos_//' ")) #no +1 per interferenza, non si salta nessun ordine
set print relpath."tempdata"
print sprintf("%d&%f&%f&%f&%f&%f", mpos, xmax, xmax_err_post, (sin(atan(xmax*Av/D))), Err(xmax,xmax_err_post), ymax)
system("VAR=$(head -n 1 temppath); VAR2=$(awk 'NR==2' temppath); cat $VAR'tempdata' >> $VAR$VAR2'max'; rm $VAR'tempdata'; rm tempfile")
}
listneg = system("VAR=$(head -n 1 temppath); ls $VAR | grep -E '^neg_([[:digit:]])+$' ")
do for [negfile in listneg]{
set out relpath.negfile.".eps"
set print "tempfile2"
print relpath.negfile
print negfile
f(x) = a*x**2+b*x+c
stats relpath.negfile using 2
a = real( system("VAR=$(head -n 1 tempfile2); head -n 1 $VAR | grep -E -o 'a = -?[[:digit:]]+(.[[:digit:]]+)?' | sed 's/a = //' ") )
b = real( system("VAR=$(head -n 1 tempfile2); head -n 1 $VAR | grep -E -o 'b = -?[[:digit:]]+(.[[:digit:]]+)?' | sed 's/b = //' ") )
c = STATS_min
unset key
fit f(x) relpath.negfile via a,b,c
plot relpath.negfile, f(x)
xmax = -1*(b)/(2.0*a) #IN PASSI
ymax = (4*a*c-b**2)/(4*a)
xmax_err_post = sqrt( ( (b*real(a_err))/(2*a**2) )**2 + ( (-1*real(b_err))/(2*a) )**2 )
mneg =  -1*int( system(" awk 'NR==2' tempfile2 | sed 's/neg_//' ")) #anche qua +0
set print relpath."tempdata"
print sprintf("%d&%f&%f&%f&%f&%f", mneg , xmax, xmax_err_post, (sin(atan(xmax*Av/D))), Err(xmax,xmax_err_post),ymax)
system("VAR=$(head -n 1 temppath); VAR2=$(awk 'NR==2' temppath); cat $VAR'tempdata' >> $VAR$VAR2'max'; rm $VAR'tempdata'; rm tempfile2")
}
set xlabel "Ordine"
set ylabel "sin {/Symbol q}"
f(x) = B+C*x
g(x) = c+d*x
h(x) = e+f*x
stats relpath.folder."max" using 1
fit [0:] g(x) relpath.folder."max" using 1:(sin(atan(($2)*Av/D))):(Err($2,$3)) via c,d
fit [:0] h(x) relpath.folder."max" using 1:(sin(atan(($2)*Av/D))):(Err($2,$3)) via e,f
fit [-8:8] f(x) relpath.folder."max" using 1:(sin(atan(($2)*Av/D))):(Err($2,$3)) via B,C
set output relpath."retta.eps"
set key top left Left reverse
set xtics 1
set zeroaxis
plot [-8-0.5:8+0.5] relpath.folder."max" using 1:(sin(atan(($2)*Av/D))):(Err($2,$3)) w yerror title ( (folder[0:1] eq "1") ? "Fit massimi ".folder[0:1]." fenditura" : "Fit massimi ".folder[0:1]." fenditure" ) ,f(x) title sprintf("Retta interpolante %f%+f*x",B,C), g(x) title sprintf("Retta interpolante ordini positivi %f%+f*x",c,d), h(x) title sprintf("Retta interpolante ordini negativi %f%+f*x",e,f)
fenditura = real(l)/C
fenditura_err = abs(fenditura)*sqrt((l_err/l)**2+(C_err/C)**2)
compatibilita = abs(B/B_err)
set print relpath.folder."fitdata"
print sprintf("d +- sigma_d = %f +- %f\nIntercetta = %f +- %f\nPendenza = %f +- %f\nCompatibilita' dell'intercetta con l'origine %f\nWSSR = %f\nndf = %f", fenditura, fenditura_err, B, B_err, C, C_err, compatibilita,FIT_WSSR, FIT_NDF)
unset zeroaxis
system("rm temppath")
}

























