#include <iostream>
#include <dirent.h>
#include <cmath>
#include <vector>
#include <string>
#include <fstream>
#include <iterator>
#include <iomanip>
#include <regex>
#include <sstream>
#include <typeinfo>

double valormedio(std::vector<double> a){
	double sum=0;
	int campione=a.size();
	for(double i:a){
	sum+=i;
		}
	return (sum+0.0)/campione;
};

double stdev(std::vector<double> a){
	double variance;
	double vv=valormedio(a);
	for(double i:a){
		variance+= pow((i-vv),2); }
		//Uso size-1 perché è la stima più precisa
		return sqrt(variance/(a.size()-1));
}

std::vector<std::string> read_directory( const std::string& path, std::string reg = ".*") {
	std::vector <std::string> result;
	std::regex reg1(reg.c_str());
	struct dirent* de;
	DIR* dp;
	dp = opendir( path.empty() ? "." : path.c_str() );
	if (dp) {
		while ((de = readdir(dp))) {
			if (de == nullptr) break;
			if (de->d_name[0]=='.' )  {
				std::cerr << de->d_name << " skipped" << std::endl;
				continue;
				} else {
					if(std::regex_match(de->d_name,reg1)){
						result.push_back( std::string(de->d_name) );
					} else {
						std::cerr << de->d_name << " not maching"<< std::endl;
					}
			}
		}
	closedir( dp );
	std::sort( result.begin(), result.end() );
	} else {
	std::cerr<< "Directory non aperta" << std::endl;
	}
	return result;
}

struct decpoint : std::numpunct<char> {
    char do_decimal_point()   const { return '.'; }
};

int findprecision (double a){
	char buffer[15] = "test";
	int n;
	std::string exp;
	sprintf(buffer, "%e", a);
	std::string rounded(buffer);
	rounded.shrink_to_fit();
	std::size_t found = rounded.find('e');
	exp = rounded.substr(found+1,3);
	n= -(std::stoi(exp, nullptr, 0));
	return (n);
}

template<typename T> void printElement(std::ostream &a,T t, const int& width, char sep){
	const char def_fill = a.fill();
	std::regex numeric("-?([0-9]+)\\.?[0-9]+");
	std::smatch match;
	std::stringstream ss;
	ss << t;
	std::string stringa = ss.str();
	std::regex_match(stringa,match,numeric);
	if(std::regex_search(stringa,match,numeric)){
		//std::cerr << match.length(1) << std::endl;
		a << std::fixed <<  std::left << std::setw(width+2+match.length(1)) << std::setprecision(width) << std::setfill(sep) << t;
	} else {
		a << std::left << t << sep;
	}
    a << std::setfill(def_fill);
}

std::string make_output_filename(const char* name, size_t index) {
   std::ostringstream ss;
   ss << name << index+1;
   return ss.str();
}

std::string make_output_filename(std::string name, size_t index) {
   std::ostringstream ss;
   ss << name << index+1;
   return ss.str();
}

double mean_stdev(std::vector<double> a){
	double variance;
	double vv=valormedio(a);
	for(double i:a){
		variance+= pow((i-vv),2.0); }
		return (sqrt(variance/(a.size()-1)))/sqrt(a.size());
};

std::vector<double> wmean_2(std::vector<std::vector<double>> dataerr){
	double num = 0;
	double den = 0;
	for (auto i : dataerr){
		std::cerr << "Data and error" << i[0] << "+-" << i[1] << std::endl;
		num += (i[0])/pow(i[1],2.0);
		den += 1.0/pow(i[1],2.0);
	}
	std::cerr << "Weighted data and error" << (num/den) <<  "+-" << static_cast<double>(sqrt(1.0/den)) << std::endl;
	std::vector<double> result{(num/den), static_cast<double>(sqrt(1.0/den))};
	return result;
}

struct misura{
	std::string name;
	int nfend;
	std::vector<double> x,y, posmax, negmax, posmin, negmin;
	double xmin,xmax,ymin,ymax;
};

struct background{
	std::vector<double> x,y;
	double mean, stdev, mean_stdev;
};

bool isPos(double c){
	if ( c >= 0) {
		return true;
	}
	else {
		return false;
	}
}

double centerwithcuts(int numero_tagli, double steptaglio, int t_max, std::vector<double> x, std::vector<double> y, int npunti){
	std::vector<double> d;
	std::vector<double> tempx12,tempx34;
	std::vector<double> tempxcenter;
	std::vector<double> a = x;
	double altezzamax = y[t_max];
for(int indextaglio = 0; indextaglio < numero_tagli ; ++indextaglio){
	std::vector<double> b = y;
	for(std::vector<int>::size_type t = 0; t < a.size()-1; ++t){
		d.push_back( b[t] - ((0.80+steptaglio*indextaglio)*altezzamax));
		if( (t > (t_max-npunti+1) ) && (t < t_max+npunti-1 ) ){
			if( isPos(d[t]) ){ // se dt è più in alto del taglio
				if( !isPos(d[t-1]) ){ // ed il precedente è più in basso
					tempx12.push_back(t); //primo punto di intersezione sta tra dt e dt-1
				}
			} else { // se dt è più in basso
				if ( isPos(d[t-1]) ){ //ma quello prima è più in alto
					tempx34.push_back(t); //secondo punto di intersezione
				}
			}
		}
	}
	// ora trovo la x del centro per questo taglio
	std::cerr << tempx12.size() << '\t' << tempx34.size() << std::endl;
	if( (tempx12.size()==tempx34.size()) && (tempx12.size()!= 0)){
		std::vector<int>::size_type id1,id2,id3,id4;
		id2 = tempx12[indextaglio];
		id1 = id2-1;
		id4 = tempx34[indextaglio];
		id3 = id4-1;
		tempxcenter.push_back( ( (a[id3]+a[id2])/2.0 + (a[id4]+a[id1])/2.0)/2.0 );
		std::cerr << ( (a[id3]+a[id2])/2.0 + (a[id4]+a[id1])/2.0)/2.0 << std::endl;
	} //else dovrei cercare di eliminare le rientranze con qualche criterio
	d.clear();
}
return valormedio(tempxcenter); //non arrotondato
std::cerr << valormedio(tempxcenter) << std::endl;
}

main(){
	std::locale::global( std::locale( "it_IT.UTF-8" ) );
	std::string namefmt= "([0-9])f[0-9]+_?[0-9]";
	std::regex rgx(namefmt);
	std::vector<std::string> nome_files, nome_rumori;
	std::vector<misura> tuttidati;
	std::vector<background> tuttisfondi;
	std::string _DIR_ = "./Data/";
	std::string _DIR2_ = "./Background/";
	nome_files = read_directory(_DIR_,namefmt);
	nome_rumori = read_directory(_DIR2_,std::string("[aA-zZ0-9]+"));
	
	//Dovrei fare qui i rumori 
	for(int i = 0; i < nome_rumori.size(); ++i){
		std::ifstream input(_DIR2_ + nome_rumori.at(i));
		if(!input.is_open()) {
			std::cerr << "Impossibile aprire il file" << nome_rumori.at(i) << std::endl;
			return 1;
		}
		background temporanea;
		std::istream_iterator<double> start(input), end;
		std::vector<double> numbers(start, end);
		std::vector<std::vector<double>> temp(2);
		for (std::vector<double>::size_type i=0 ; i< numbers.size(); ++i){
			temp[i%2].push_back(numbers.at(i));
		}
		temporanea.x = temp[0];
		temporanea.y = temp[1];
		temporanea.mean = valormedio(temp[1]);
		temporanea.stdev = stdev(temp[1]);
		temporanea.mean_stdev = mean_stdev(temp[1]);
		tuttisfondi.push_back(temporanea);
	}		
		
	//Questo per i dati
	for(int i = 0; i < nome_files.size(); ++i){
		std::ifstream input(_DIR_ + nome_files.at(i));
		if(!input.is_open()) {
			std::cerr << "Impossibile aprire il file " << nome_files.at(i) << std::endl;
			return 1;
		}
		//Creo la struttura temporanea che poi conterrà i dati relativi alla misura
		misura temporanea;
		//Prendo il numero delle fenditure
		std::smatch match;
		int numerofenditure;
		int&N = numerofenditure;
		temporanea.name = nome_files.at(i);
		temporanea.nfend = numerofenditure;
		if(std::regex_search(nome_files.at(i), match, rgx)){
			std::regex_match(nome_files.at(i), match, rgx);
			N = std::stoi(match[1]);
			std::cerr << "NUMERO FENDITURE = " << N << std::endl;
		}
		//Riempio i vettori con i dati
		std::istream_iterator<double> start(input), end;
		std::vector<double> numbers(start, end);
		std::vector<std::vector<double>> temp(2);
		for (std::vector<double>::size_type i=0 ; i< numbers.size(); ++i){
			temp[i%2].push_back(numbers.at(i));
			//std::cerr << numbers.at(i) << std::endl;
		}
		auto xrange = minmax_element(temp[0].begin(),temp[0].end());
		auto yrange = minmax_element(temp[1].begin(),temp[1].end());
		std::cerr << "Xrange [" << *xrange.first << ":" << *xrange.second << "] \tYrange [" << *yrange.first << ":" << *yrange.second << "] " << std::endl;
		//mi conviene fare ora il taglio così poi gli indici sono tutti rispetto allo "0"
		std::vector<double> a = temp[0];
		//std::vector<double> b = temp[1];
		std::vector<double> d;
		std::vector<double> tempx12,tempx34;
		std::vector<double> tempxcenter;
		//faccio diversi tagli
		for(int indextaglio = 0; indextaglio <5; ++indextaglio){
			std::vector<double> b = temp[1];
			//std::cerr << "Indextaglio = " << indextaglio << "\t nome file = " << nome_files.at(i) << std::endl;		
			for(std::vector<int>::size_type t = 0; t < a.size()-1; ++t){ //dovrei stare sicuro col picco principale
				double altezzamax = *yrange.second;
				// piccola modifica ad altezzamax = minmax_element(a.begin()+(t_max-npunti),a.end()-(t_max-npunti)-1).second;
				d.push_back( b[t] - ((0.80+0.03*indextaglio)*altezzamax));
				if( (t > 300) && (t < a.size()-300) ){
					if( isPos(d[t]) ){ // se dt è più in alto del taglio
						if( !isPos(d[t-1]) ){ // ed il precedente è più in basso
							//std::cerr << "bt = " << b[t] << "  Differenza = " << d[t]-d[t-1] << "INDICE = " << t << std::endl << "Altezzataglio " << (0.80+0.03*indextaglio)*altezzamax << std::endl;
							tempx12.push_back(t); //primo punto di intersezione sta tra dt e dt-1
						}
					} else { // se dt è più in basso
						if ( isPos(d[t-1]) ){ //ma quello prima è più in alto
							tempx34.push_back(t); //secondo punto di intersezione
						}
					}
				}
			}
			// ora trovo la x del centro per questo taglio
			if(tempx12.size()==tempx34.size()){
				//std::cerr << "tempx12size (ovvero quanti punti ha preso a sx) = " << tempx12.size() << std::endl;
				std::vector<int>::size_type id1,id2,id3,id4;
				id2 = tempx12[indextaglio];
				id1 = id2-1;
				id4 = tempx34[indextaglio];
				id3 = id4-1;
				std::cerr << a[id3] << '\t' << a[id2] << '\t' << a[id4] << '\t' << a[id1] << std::endl;
				tempxcenter.push_back( (a[id1]*(b[id2]-b[id1])/static_cast<double>(a[id2]-b[id1]) - b[id1] + b[id3] - a[id3]*(b[id4]-b[id3])/static_cast<double>(a[id4]-a[id3]) ) / ((b[id2]-b[id1])/static_cast<double>(a[id2]-a[id1]) - (b[id4]-b[id3])/static_cast<double>(a[id4]-a[id3]) ) );
				tempxcenter.push_back( ( (a[id3]+a[id2])/2.0 + (a[id4]+a[id1])/2.0)/2.0 );
				std::cerr << "Centro temporaneo" << ( (a[id3]+a[id2])/2.0 + (a[id4]+a[id1])/2.0)/2.0 << std::endl;
			}
			d.clear();
			std::cerr << (0.80+0.03*indextaglio)**yrange.second << std::endl;
		}
		double xcenter = valormedio(tempxcenter); //non arrotondato
		
		//Ora posso rispostare tutto al centro
		for(std::vector<int>::size_type i = 0; i < a.size(); ++i){
			a[i] -= xcenter;
		}
		xrange = std::minmax_element(a.begin(),a.end());
		//if( !(nome_files.at(i) == "1f3000_2") ){
		if( *(a.begin())<0 ){
			temporanea.x = a;
			temporanea.y = temp[1];
		} else { // Se il primo passo è positivo giro tutto
			std::reverse(a.begin(), a.end());
			std::reverse(temp[1].begin(), temp[1].end());
			temporanea.x = a;
			temporanea.y = temp[1];
		}
			
		temporanea.xmin = *xrange.first;
		temporanea.xmax = *xrange.second;
		temporanea.ymin = *yrange.first;
		temporanea.ymax = *yrange.second;
		
		//SE SCARTO I VALORI ESTREMI LI SCARTO QUI per non avere bordelli con gli indici dopo

		//double centerwithcuts(int numero_tagli, double steptaglio, int t_max, std::vector<double> x, std::vector<double> y, int npunti)
		std::vector<double> b = temp[1];
		auto yminmaxptr = std::minmax_element(b.begin(),b.end());
		std::vector<double> c;
		std::vector<double> postempmaxid, posA, posB, negtempmaxid, negA, negB, postempminid, posa, posb, negtempminid, nega, negb;
		int consecutive_index = 15;
		for(std::vector<double>::size_type t=250; t<b.size()-250; t++) { //MASSIMI INTENSITÀ
			int poscnt=1;
			int negcnt=1;
			while( b[t]>=b[t-poscnt] && b[t]>=b[t+poscnt] && poscnt <= consecutive_index) {
				++poscnt;
			}
			if(poscnt>=consecutive_index) {
				if( t > ( (std::distance(b.begin(),yminmaxptr.second)) + 30) ){ //positivi ignorando i 100 al centro
					//postempmaxid.push_back(centerwithcuts(5,0.02,t,a,b,20));
					postempmaxid.push_back(t);
					posA.push_back( ( temporanea.y.at(t+1)-temporanea.y.at(t) ) / (temporanea.x.at(t+1) - temporanea.x.at(t) )+ 0.7 );
					posB.push_back( -(2*temporanea.x.at(t)*( ( temporanea.y.at(t+1)-temporanea.y.at(t) ) / (temporanea.x.at(t+1) - temporanea.x.at(t) ) +0.7) ) );
				} else if( t < ( (std::distance(b.begin(),yminmaxptr.second)) - 30) ) {
					negtempmaxid.push_back(t);
					negA.push_back( ( temporanea.y.at(t+1)-temporanea.y.at(t) ) / (temporanea.x.at(t+1) - temporanea.x.at(t) ) +0.7);
					negB.push_back( -(2*temporanea.x.at(t)*( ( temporanea.y.at(t+1)-temporanea.y.at(t) ) / (temporanea.x.at(t+1) - temporanea.x.at(t) ) +0.7 ) ) );
				}
			}
			while (b[t]<=b[t-negcnt] && b[t] <= b[t+negcnt] && negcnt <= consecutive_index) {
				++negcnt;
			}
			if(negcnt>=consecutive_index) {
				
				// Qua non ho capito che ho fatto con i segni perché non mi torna esattamente il minore per i pos etc
				if( t > ( (std::distance(b.begin(),yminmaxptr.second)) + 30) ){ //negativi ignorando i 100 al centro
					postempminid.push_back(t);
					posa.push_back( ( temporanea.y.at(t+1)-temporanea.y.at(t) ) / (temporanea.x.at(t+1) - temporanea.x.at(t) )+ 0.7 );
					posb.push_back( -(2*temporanea.x.at(t)*( ( temporanea.y.at(t+1)-temporanea.y.at(t) ) / (temporanea.x.at(t+1) - temporanea.x.at(t) ) +0.7) ) );
				} else if( t < ( (std::distance(b.begin(),yminmaxptr.second)) - 30) ) {
					negtempminid.push_back(t);
					nega.push_back( ( temporanea.y.at(t+1)-temporanea.y.at(t) ) / (temporanea.x.at(t+1) - temporanea.x.at(t) ) +0.7);
					negb.push_back( -(2*temporanea.x.at(t)*( ( temporanea.y.at(t+1)-temporanea.y.at(t) ) / (temporanea.x.at(t+1) - temporanea.x.at(t) ) +0.7 ) ) );
				}
			}
		}
		temporanea.posmax.clear();
		temporanea.negmax.clear();
		
		//Minimi
		
		for(std::vector<int>::size_type j = postempminid.size(); j-- > 0;){
			std::string nomeout = std::string("Fits/") + nome_files.at(i) + "/pos_";
			//std::ofstream out(make_output_filename(nomeout,(postempminid.size()-j-1)));
			//std::cerr << "accipicchia" << "/pos_" << (postempminid.size())-j-1 << std::endl;
			std::ofstream out(make_output_filename(nomeout,j));
			out.imbue(std::locale(out.getloc(), new decpoint));
			out << "# a = " << posa.at(j) << "  &b = " << posb.at(j) << std::endl;
			for(std::vector<int>::size_type t = 0; t<7; t++){
				out << temporanea.x.at(postempminid.at(j)+3-t) << '&' << temporanea.y.at(postempminid.at(j)+3-t) << std::endl;
			}
		}
		
		std::cerr << negtempminid.size() << std::endl;
		for(std::vector<int>::size_type j = negtempminid.size(); j-- > 0;){
			std::string nomeout = std::string("Fits/") + nome_files.at(i) + "/neg_";
			//std::cerr << "broccoletti" << "/neg_" << (negtempminid.size())-j-1 << std::endl;
			std::ofstream out(make_output_filename(nomeout,(negtempminid.size()-j-1)));
			out.imbue(std::locale(out.getloc(), new decpoint));
			out << "# a = " << nega.at(j) << "  &b = " << negb.at(j) << std::endl;
			for(std::vector<int>::size_type t = 0; t < 7; t++){
				out << temporanea.x.at(negtempminid.at(j)+3-t) << '&' << temporanea.y.at(negtempminid.at(j)+3-t) << std::endl;
				//printElement(out, temporanea.x.at(negtempminid.at(j)+5-t), findprecision(temporanea.x.at(negtempminid.at(j)+5-t)), ',');
				//printElement(out, temporanea.y.at(negtempminid.at(j)+5-t), findprecision(temporanea.y.at(negtempminid.at(j)+5-t)), ',');
			}
		}
		
		for(std::vector<int>::size_type j = postempmaxid.size(); j-- > 0;){
			std::string nomeout = std::string("Sits/") + nome_files.at(i) + "/pos_";
			//std::ofstream out(make_output_filename(nomeout,(postempmaxid.size()-j-1)));
			std::ofstream out(make_output_filename(nomeout,j));
			out.imbue(std::locale(out.getloc(), new decpoint));
			out << "# a = " << posA.at(j) << "  &b = " << posB.at(j) << std::endl;
			for(std::vector<int>::size_type t = 0; t<7; t++){
				out << temporanea.x.at(postempmaxid.at(j)+3-t) << '&' << temporanea.y.at(postempmaxid.at(j)+3-t) << std::endl;
			}
		}
		
		std::cerr << negtempmaxid.size() << std::endl;
		for(std::vector<int>::size_type j = negtempmaxid.size(); j-- > 0;){
			std::string nomeout = std::string("Sits/") + nome_files.at(i) + "/neg_";
			std::ofstream out(make_output_filename(nomeout,(negtempmaxid.size()-j-1)));
			out.imbue(std::locale(out.getloc(), new decpoint));
			out << "# a = " << negA.at(j) << "  &b = " << negB.at(j) << std::endl;
			for(std::vector<int>::size_type t = 0; t < 7; t++){
				out << temporanea.x.at(negtempmaxid.at(j)+3-t) << '&' << temporanea.y.at(negtempmaxid.at(j)+3-t) << std::endl;
			}
		}
		
		//Massimi, i secondari dovrebbero essere eliminati dall'algoritmo
		
		/*for(std::vector<double>::size_type i = 0; i < postempmaxid.size(); ++i){
//			if( (N <= 2) || ((i+1)%N > temporanea.posmax.size()) ){
				temporanea.posmax.push_back( a[postempmaxid.at(i)] );
//			}
		}
		
		for(std::vector<double>::size_type i = negtempmaxid.size(); i-- > 0;){
//			if( (N<= 2) || ((negtempmaxid.size()-i)%N > temporanea.negmax.size()) ){
				temporanea.negmax.push_back( a[negtempmaxid.at(i)] );
//			}
		}
		/*
		/*std::ofstream out(std::string("Sits/") + nome_files.at(i) + "max");
		std::vector<int> negposmaxid;
		//negposmaxid.reserve(postempmaxid.size()+negtempmaxid.size());
		//std::merge(negtempmaxid.begin(), negtempmaxid.end(), postempmaxid.begin(), postempmaxid.end(), std::back_inserter(negposmaxid));
		out.imbue(std::locale(out.getloc(), new decpoint));
		int N1 = negtempmaxid.size();
		int N2 = postempmaxid.size();
		for(int t = -negtempmaxid.size(); t < postempmaxid.size() ; ++t){
		std::cerr << "DENTRO" << std::endl;
			std::cerr << negtempmaxid.at(negtempmaxid.size()+t) << negtempmaxid.size()+t << std::endl;
			if(t < 0){
				std::cerr << t-1 << '&' << temporanea.x.at(negtempmaxid.at(N1+t)) << std::endl;
			} else {
				std::cerr << t+1 << '&' << temporanea.x.at(postempmaxid.at(t-1)) << std::endl;
			}
		}*/
		
		// it -negsize to +possize if < 0 cnt-- if >0 cnt++
		
		
		std::ofstream centered(std::string("Plots/")+nome_files.at(i));
		centered.imbue(std::locale(centered.getloc(), new decpoint));
		for(std::vector<double>::size_type i = temporanea.x.size(); i-- > 0;){
			centered << temporanea.x[i] << '\t' << temporanea.y[i] << std::endl;
		}
		std::cerr << std::endl;
		std::cerr << postempmaxid.size() << '\t' << negtempmaxid.size() << std::endl;
		std::cerr << postempminid.size() << '\t' << negtempminid.size() << std::endl;
		tuttidati.push_back(temporanea);
	}
	std::cerr << "Che bella la vita" << std::endl;
	std::ofstream out("Rumore1");
	out.imbue(std::locale(out.getloc(), new decpoint));
	out << "Numero dati& Media& Deviazione standard& Errore della media" << std::endl;
	out << tuttisfondi[0].mean << '&' <<  tuttisfondi[0].stdev << '&' << tuttisfondi[0].mean_stdev << std::endl;
	out.close();
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
