\documentclass[12pt,a4paper]{article}	%tipo file
\usepackage[utf8]{inputenc}	%per mettere accenti senza parentesi
\usepackage[italian]{babel}	%sintassi italiana
\usepackage[a4paper]{geometry}	%foglio e margini più piccoli
\usepackage{amsmath}	%linguaggio matematico per formule
\usepackage{amssymb} %interi, reali..
\usepackage{graphicx} %o era graphics? we'll never know
\usepackage{epstopdf}
\usepackage[justification=centering]{caption}
\DeclareGraphicsExtensions{.pdf,.png,.jpg}
\DeclareGraphicsRule{.png}{png}{.png.bb}{}
\fontencoding{T1}	%font
\begin{document}
\title{Relazione di laboratorio \\ \large {Esperienza su interferenza e diffrazione}}
\author{\begin{tabular*}{\textwidth}{cc @{\extracolsep{\fill}} r} & &Currò Dossi Andrea \\ & &Lenoci Alessandro \\ & &Albano Samuele
\end{tabular*}}
\date{}
\maketitle
%CAMBIA COL MATHITTTTTONE

\section{Obiettivi dell'esperienza}
Gli obiettivi dell'esperienza di laboratorio sono la stima della larghezza $a$ delle fenditure e della loro reciproca distanza $d$ relative al reticolo di diffrazione fornito.
\section{Caratteristiche sperimentali}
L'apparato sperimentale consente lo studio dei fenomeni di diffrazione e interferenza che si verificano col passaggio della luce attraverso sistemi di fenditure. Un laser produce un fascio di luce di lunghezza d'onda pari a 
\[\lambda = (670 \pm 5) nm\]
a bassa dispersione angolare e quindi in buona approssimazione parallelo e sufficientemente intenso. Il fascio è diretto contro un reticolo di diffrazione il quale presenta a scelta 1, 2, 3 o 4 fenditure di spessore $a$ e distanza reciproca $d$ da determinare. Per l'analisi spaziale della figura di diffrazione e interferenza si utilizza un rivelatore costituito da un fotodiodo con diaframma di $0.6 mm$ in grado di misurare l'intensità luminosa. Il rivelatore è posto al termine di un braccio rotante di lunghezza $L= 700 mm$ imperniato ad un'estremità sull'asse della fenditura. Quindi il rivelatore è in grado di intercettare un angolo solido $\Omega = 5.7 \cdot 10^{-7} sterad $. Il braccio è posto in movimento tramite una vite senza fine posta alla distanza dal reticolo pari a 
\[D=(65.5\pm 0.5)mm\]
Poiché la vite che muove il braccio ha un passo di $0.5mm/giro$ con errore del centesimo e il motore fa $400 passi/giro$, l'avanzamento $A$ del motore per ogni passo sarà
\[A = 500\mu m/400passi = 1.25 \mu m/passo\]
Ciò corrisponde ad una rotazione del braccio pari a $\delta \theta\simeq\frac{A}{D}= 1.9 \cdot 10^{-5} rad$ per unità di passo che fornisce all'apparato una risoluzione ottima della figura di diffrazione e interferenza che si viene a creare. Il segnale luminoso è amplificato linearmente e letto da un software per l'acquisizione dati. La presenza del rumore di fondo nel rivelatore limita l'ampiezza angolare massima su cui si possono effettuare misure. 
\section{Cenni teorici}
Lo studio dei fenomeni di interferenza è possibile solo se le sorgenti delle onde sono \emph{coerenti}, ovvero se l'emissione avviene con una differenza di fase costante e, idealmente, se le sorgenti sono puntiformi. Quindi ai fini delle esperienze di laboratorio è conveniente considerare particolari condizioni sperimentali dette di \emph{Fraunhofer}. Si considera una sorgente di onde luminose e, a distanza $l$ da essa, un reticolo formato da $N$ fenditure di larghezza $a$ e di distanza reciproca $d$; se la sorgente è posta ad una distanza tale che valga la relazione
\[\frac{l}{a}>\frac{a}{\lambda}\]
allora si ha che sulle fenditure giunge un fronte di onda piano e che in buona approssimazione le fenditure, trascurando la loro larghezza, per il principio di Huygens, possono essere considerate sorgenti puntiformi di onde sferiche monocromatiche, coerenti e in fase. Inoltre, se si vanno a misurare le intensità delle onde in un punto a distanza $L$ dalle fenditure tale che sussista la relazione
\[\frac{L}{d}>\frac{d}{\lambda}\]
si ha che, detto $\theta$ l'angolo da cui è visto il punto rispetto all'asse di simmetria del reticolo, la differenza di fase $\delta$ dell'onda risultante generata da due fenditure successive nel punto sarà data dalla differenza tra i cammini ottici consecutivi (perché le sorgenti sono in fase):
\[\delta=d\sin\theta\]
Quindi l'ampiezza dell'onda risultante nel punto considerato è, per il principio di sovrapposizione
\begin{gather*}
E_{P}=E_{0}e^{-i \omega t}\sum_{n=0}^{N-1}e^{i\vec{k}(\vec{r_{n}}-\vec{r_{0}})}=E_{0}e^{-i \omega t}\sum_{n=0}^{N-1}e^{\frac{2\pi}{\lambda}d\sin\theta}
\end{gather*}
Con $E_{0}$ l'ampiezza dell'onda uscente dalla prima fenditura, $\omega$ la frequenza angolare del fascio luminoso, $\vec{k}$ il numero d'onda, $\vec{r_{n}}$ il cammino n-esimo, $\vec{r_{0}}$ il cammino del primo raggio uscente dalla prima fenditura e $N$ il numero totale delle fenditure.
Dopo alcuni passaggi algebrici si dimostra che l'intensità nel punto considerato è pari a
\begin{gather}\label{pene:interf}
I_{P}=I_{0}\left(\frac{\sin\left(\frac{N \pi d \sin\theta }{\lambda}\right)}{\sin\left(\frac{\pi d 
\sin\theta }{\lambda}\right)}\right)^{2}
\end{gather}
Da questa formula si ricavano le posizioni dei punti di massimo e di minimo dell'intensità per i valori dell'apertura angolare $\theta$.
\begin{align*}
\text{Massimi principali} &: \sin\theta=m\frac{\lambda}{d} \\
\text{Minimi} &: \sin\theta=m_{1}\frac{\lambda}{Nd} \\
\text{Massimi secondari} &: \sin\theta=\frac{(2m_{2}+1)}{2}\frac{\lambda}{Nd}
\end{align*}
dove i numeri $m,m_{1},m_{2} \in \mathbb{Z}$ sono detti ordini. Si dimostra che tra due massimi principali esistono $N-1$ minimi e $N-2$ massimi secondari.
Dalle relazioni sopra ottenute è evidente che, individuando la posizione di minimi e massimi, si può ricavare il rapporto $\frac{\lambda}{d}$ e quindi, nota una di queste ultime due grandezze, ricavare l'altra. Si può ottenere questo rapporto in particolare dalla posizione dei massimi principali, più facilmente individuabili rispetto ai minimi (influenzati da imperfezioni o da rumore di fondo), e per i quali non si ha dipendenza dal numero di fenditure $N$.
Ora si consideri invece il caso reale e fisico in cui le fenditure hanno larghezza finita $a$: quando l'onda generata dal laser, considerata piana, vi incide si verifica il fenomeno della diffrazione. Si può studiare la diffrazione di Fraunhofer riconducendosi all'interferenza. Ogni punto interno alla fenditura si può infatti considerare, nuovamente per il principio di Huygens, come sorgente puntiforme di onde armoniche monocromatiche, coerenti e in fase con l'onda principale. Quindi, partendo dal caso di una singola fenditura presente, per il principio di sovrapposizione, l'ampiezza dell'onda risultante in un punto dello schermo visto con l'angolo $\theta$ rispetto all'orizzontale  
à data da
\begin{gather*}
E_{P}=E_{0}e^{-i\omega t}\frac{1}{a}\int_{-a/2}^{a/2}e^{\frac{2\pi ix\sin\theta}{\lambda}}dx
\end{gather*}
Si ottiene quindi la formula per l'intensità dell'onda nel punto considerato
\begin{gather}\label{pene:diffr}
I_{P}=I_{0}\left(\frac{\sin\left(\frac{\pi a \sin\theta }{\lambda}\right)}{\frac{\pi a \sin\theta }{\lambda}}\right)^{2}
\end{gather}
Da questa formula si ricavano i valori di massimo e di minimo dell'intensità per $\theta$.
\begin{align*}
\text{Minimi} &: \sin\theta=m\frac{\lambda}{a} \\
\text{Massimi} &: \sin\theta=\frac{(2m_{2}+1)}{2}\frac{\lambda}{a}
\end{align*}
con $m,m_{1} \in \mathbb{Z}$. Naturalmente il massimo centrale si ottiene per un valore di $\theta=0$. Anche in questo caso individuando la posizione dei minimi è immediato ricavare il rapporto $\frac{\lambda}{a}$ e quindi nota una delle due grandezze, ricavare l'altra. Infine, poiché nel reticolo di diffrazione adoperato in laboratorio sono presenti entrambi gli effetti, ovvero interferenza tra onde diffratte in $N$ fenditure, si possono combinare le due relazioni \eqref{pene:interf} e \eqref{pene:diffr} per ottenere l'intensità nel punto considerato

\begin{gather}\label{pene:retic}
I_{P}=I_{0}\left(\frac{\sin\left(\frac{N \pi d \sin\theta }{\lambda}\right)}{\sin\left(\frac{\pi d \sin\theta }{\lambda}\right)}\right)^{2}\left(\frac{\sin\left(\frac{\pi a \sin\theta }{\lambda}\right)}{\frac{\pi a \sin\theta }{\lambda}}\right)^{2}
\end{gather}
La formula descrive dal punto di vista teorico la figura che si ottiene sperimentalmente andando a misurare ad una certa distanza $L$ dal reticolo l'intensità luminosa in funzione della posizione angolare rispetto all'asse di simmetria del sistema. Il risultato sarà una figura di interferenza che presenterà, nel caso di un numero di fenditure maggiore di due, massimi principali e massimi secondari modulati dal termine derivato dalla diffrazione che funge da fattore di soppressione.
\section{Procedura sperimentale}
Poiché gli obiettivi dell' esperienza sono la stima dei parametri fisici che descrivono il reticolo, nel caso di una singola fenditura fino a quattro fenditure, sperimentalmente si è misurata tramite il rivelatore a fotodiodo l'intensità luminosa ad una distanza data dalla lunghezza del braccio rotante per un'escursione angolare impostatabile dagli sperimentatori. Particolare cura è stata posta alla scelta del numero di misure da acquisire, al range di presa dati e infine al numero di "passi" da far compiere al motore per l'acquisizione di ogni misura. Si è scelto quindi per tutte le misure effettuate un intervallo di presa dati compreso tra le posizioni di $-3000$ e $+3000$ passi centrato approssimativamente attorno al massimo principale di ordine zero dell'intensità rivelate. Al fine di garantire un'adeguata copertura di questo intervallo si è scelto di acquisire circa $1500$ misure in totale, una ogni $4$ passi compiuti dalla vite. L'acquisizione della presa dati è stata effettuata automaticamente grazie ad un software in grado di acquisire coppie di posizione ed intensità $(x,I)$ nelle unità di misura relative adimensionali rispettivamente di \emph{passi} e \emph{conteggi}. Il software permetteva l'immediata visualizzazione dei dati su un grafico intensità -posizione. Sono stati acquisiti quattro campioni di dati ciascuno relativo al caso di una, due, tre o quattro fenditure nel reticolo. Inoltre, data la presenza di un rumore di fondo nel rivelatore, che si suppone spazialmente e temporalmente omogeneo, è stato acquisito un campione di dati a laser spento nell' intervallo tra $-500$ e $+500$ con una frequenza di 2 passi per dato per un totale di 500 dati, che desse appunto la possibilità di analizzare il fondo.


\section{Analisi dati}
Come detto nel paragrafo precedente, sono stati acquisiti dei campioni di misure di posizione e intensità $(x,I)$ relativi ai diversi reticoli e un campione di rumore di fondo. Riguardo a quest'ultimo, si può notare che il rumore di fondo, come previsto, è sostanzialmente omogeneo nel tempo e nello spazio, a parte oscillazioni casuali dell'intensità. Nel grafico \ref{im:rumore} è possibile visualizzare i dati ottenuti.

\begin{figure}[htpb]
\includegraphics[width=\textwidth]{rumore_1.eps}\caption{\label{im:rumore} Grafico delle misure di rumore}
\end{figure}
Data l'omogeneità dei valori di fondo, si è deciso di adoperare come stima del rumore $R$ la media del campione, a cui si associa l'errore della media.
\[R= (1870.5 \pm 0.3)\mathit{conteggi}\]
Inoltre si è deciso di adoperare il valore della deviazione standard $\sigma_{R}$ del campione del rumore come stima dell'errore casuale sull'intensità 
\[\sigma_{I}=8\mathit{conteggi}\]
I valori di $x$ forniti dal software per la presa dati sono considerati equispaziati e servono solo come riferimento di posizione rispetto allo zero scelto e di suddivisione dell'intervallo di misura. I "passi" del motore nei file di dati si possono cosiderare degli indici di riferimento e quindi non sono affetti da errore casuale.

Per analizzare i dati ed individuare i massimi e i minimi degli spettri delle figure di interferenza e diffrazione ottenuti è necessario \emph{centrare} il campione. Dopo aver individuato il picco centrale, corrispondente al massimo principale di ordine 0 dello spettro, bisogna far coincidere lo 0 dell'indice di riferimento dei passi del motore con l'ascissa di tale massimo. Per far ciò si è utilizzato un algoritmo. Innanzitutto si trova il massimo dell'intensità del campione considerato del quale si prende un valore di riferimento $k_{i}I$ con $k_{i}=0.8+0.03i$ e $i=0,1,2,3,4$. Ciò vuol dire che si effettuano dei tagli orizzontali all'$80 \%$, $83 \%$ ecc. dell'intensità di picco come mostrato nel grafico \ref{im:tagli}
\begin{figure}[!ht]
\centering
\includegraphics[width=\textwidth]{tagli.eps}\caption{\label{im:tagli} Grafico algoritmo dei tagli}
\end{figure}
Si individuano i primi due punti superiori $x_{sup1,2}$ e inferiori $x_{inf1,2}$ alla retta orizzontale: il valore di centro sarà la media dei centri dei punti superiori e di quelli inferiori
\[x_{centro}=\frac{\left(\frac{x_{sup1}+x_{sup2}}{2}\right)+\left(\frac{x_{inf1}+x_{inf2}}{2}\right)}{2} \] 
Il valore di centro finale sarà la media dei valori di centro per ciascun taglio. Si è posta cura a  scegliere un valore di taglio abbastanza alto in modo da considerare solo il picco centrale ma comunque tale che venissero esclusi i valori troppo prossimi al massimo dell'intensità. Questa procedura è cruciale nel caso della presenza di una singola fenditura: infatti il campione presenta in un intorno del massimo centrale uno smussamento non compatibile con la forma della curva e quindi non attribuibile al fenomeno della diffrazione bensì probabilmente alla disomogeneità della fenditura. Inoltre come risulta visibile in figura \ref{im:tagli} i massimi di ordine 1 sono praticamente indistinguibili dal picco centrale: anche per questo si è scelto un valore di partenza per i tagli sufficientemente alto. Nei grafici \ref{im:gr1a}, \ref{im:gr1b}, \ref{im:gr2}, \ref{im:gr3} e \ref{im:gr4}
si mostrano i dati dopo aver centrato i massimi principali di ordine 0. 
\begin{figure}
\vspace{-20px}
\includegraphics[width=0.95\textwidth]{1f3000_1.eps}\caption{\label{im:gr1a} Grafico dei dati una fenditura primo campione}
\vspace{-20px}
\end{figure}
\begin{figure}
\vspace{-20px}
\includegraphics[width=0.95\textwidth]{1f3000_2.eps}\caption{\label{im:gr1b} Grafico dei dati una fenditura secondo campione}
\vspace{-20px}
\end{figure}
\begin{figure}
\vspace{-20px}
\includegraphics[width=0.95\textwidth]{2f3000_1.eps}\caption{\label{im:gr2} Grafico dei dati due fenditure }
\vspace{-20px}
\end{figure}
\begin{figure}
\vspace{-20px}
\includegraphics[width=0.95\textwidth]{3f3000_1.eps}\caption{\label{im:gr3} Grafico dei dati tre fenditure }
\vspace{-20px}
\end{figure}
\begin{figure}
\vspace{-20px}
\includegraphics[width=0.95\textwidth]{4f3000_1.eps}\caption{\label{im:gr4} Grafico dei dati quattro fenditure }
\end{figure}


In tutti i grafici dei dati si nota un aumento di intensità nella zona centrale alla base del grafico: ciò causa un'evidente asimmetria degli spettri, sia per quanto riguarda la posizione dei punti di massimo e di minimo sia per i valori di intensità ad essi corrispondenti. Inoltre in linea teorica i punti di minimo dovrebbero corrispondere a valori nulli dell'intensità, tenendo conto anche del rumore. L'effetto riscontrato in particolare sposta i punti di minimo dal centro di simmetria dello spettro della diffrazione. Si è dapprima ipotizzato che l'asimmetria fosse dovuta ad un'errata ortogonalizzazione del reticolo rispetto alla luce incidente del laser ma analizzando i dati e ragionando dal punto di vista teorico si è vista non essere questa la causa di tale effetto: ciò sarà trattato nel paragrafo successivo. Inoltre non si sono analizzati i valori troppo distanti dal centro dello spettro: per essi infatti l'intensità era ridotta al punto tale da confondersi con il rumore.

Si è quindi proceduto all'individuazione dei punti di massimo e di minimo al fine di stimare i parametri fisici del reticolo. 

\newpage
\subsection{Diffrazione}
Al fine di eseguire un fit lineare tra questi punti e gli ordini delle frange di diffrazione si sono cercati i punti di minimo dei due campioni. Dopo aver individuato questi punti come ascisse dei valori minimi in intorni di 41 punti, si sono selezionati tre valori $(x,I)$ a destra e a sinistra per ogni minimo. Per ognuno di questi sottointervalli è stato effettuato un fit parabolico tramite il programma GNUPLOT come si vede in figura \ref{im:parab}: a scopo esemplificativo è raffigurato il fit del minimo di ordine $+2$ del primo campione.
\begin{figure}[!ht]
\includegraphics[width=\textwidth]{pos_1.eps}\caption{\label{im:parab} Fit parabolico intorno al minimo}
\end{figure}
Si è quindi preso come  punto di minimo $x_{min}$ effettivo l'ascissa del vertice della parabola corrispondente. Trovati tali valori si è proceduto a convertirli dall'unità di \emph{passo} a valori di angolo in \emph{radianti}. Per la geometria del sistema, come descritto al paragrafo 2, l'angolo $\theta$ corrispondente al numero di passi $x$, è dato da
\[\theta= \arctan \frac{nA}{D}\]
con $A=(1.25 \pm 0.01)\mu m$ e $D=(65.5\pm 0.5)mm$ rispettivamente l'avanzamento del motore e il braccio rotante.
Al fine di usare la formula già citata
\[\sin\theta=m\frac{\lambda}{a}\] 
per un fit lineare in un grafico di posizione angolare dei minimi in funzione dell'ordine degli stessi $m$, è necessario calcolare il seno degli angoli di minimo di diffrazione. 
Si può associare alla grandezza $\sin \theta$ l'errore dato dalla formula di propagazione
\begin{gather*}
\sigma_{\sin \theta}\simeq \left|\frac{xA}{D}\frac{\cos (\arctan \left(\frac{xA}{D}\right) )}{1+\left(\frac{xA}{D}\right)^2}\right|\sqrt{\left(\frac{\sigma_{A}}{A}\right)^2+\left(\frac{\sigma_{x}}{x}\right)^2+\left(\frac{\sigma_{D}}{D}\right)^2}
\end{gather*}
Si nota che l'errore sul seno della posizione angolare aumenta all'aumentare del numero di passi.
Si riportano nelle tabelle \ref{tab:difmin1} e \ref{tab:difmin2} i punti di minimo ottenuti e i valori convertiti con rispettivi errori ed ordini corrispondenti.

\begin{table}[!ht]
\parbox{\linewidth}{
\centering
\caption{\label{tab:difmin1} Valori del fit una fenditura campione 1}
\begin{tabular}{ccccc}
$m$ & $x_{min}[passi]$ & $\sigma_{x_{min}}[passi]$& $\sin \theta$ &$\sigma_{\sin \theta}$\\ \hline
2&720&20&0.0137&0.0004\\
3&1160&90&0.022&0.002\\
4&1600&300&0.030&0.006\\
5&2000&1000&0.04&0.03\\
-2&-740&40&-0.0141&0.0007\\
-3&-1200&200&-0.023&0.004\\
-4&-1600&200&-0.032&0.003\\
\end{tabular} } 

\parbox{\linewidth}{
\centering
\caption{\label{tab:difmin2} Valori del fit una fenditura campione 2}
\begin{tabular}{ccccc} 
$m$ & $x_{min}[passi]$ & $\sigma_{x_{min}}[passi]$& $\sin \theta$ &$\sigma_{\sin \theta}$\\ \hline
2&700&90&0.013&0.002\\
3&1100&200&0.022&0.003\\
4&1600&300&0.030&0.005\\
5&2000&1000&0.04&0.02\\
-1&-800&100&-0.014&0.002\\
-2&-1200&100&-0.023&0.002\\
-3&-1600&200&-0.031&0.005\\
-4&-2100&100&-0.039&0.003\\
\end{tabular} } 
\end{table}

Si osserva che non si sono potuti ottenere i valori corrispondenti ai minimi di ordine $\pm 1$: ciò, come detto in precedenza è dovuto ad imperfezioni della fenditura, per cui non si riescono a distinguere a loro volta i massimi di ordine $\pm 1$ dal massimo centrale.
La procedura di analisi dati considerata presenta due vantaggi: innanzitutto in questo caso l'ipotesi del fit lineare di errore casuale nullo per le grandezze poste in ascissa è pienamente verificata; inoltre, la scelta di eseguire il fit sui minimi e non sui massimi consente di evitare di dover estrarre il parametro fisico di interesse sia dalla pendenza che dall'intercetta della retta interpolante. Si sono eseguiti quindi due fit lineari di equazione $y=B+Cx$ dei dati (uno per ogni campione) e dalla pendenza $C$ si è ricavata la larghezza $a$ della fenditura del reticolo
\[a=\frac{\lambda}{C}\]
a cui si è associato l'errore dato dalla propagazione
\begin{gather*}
\sigma_a \simeq a\sqrt{\left(\frac{\sigma_{\lambda}}{\lambda}\right)^2+\left(\frac{\sigma_{C}}{C}\right)^2}
\end{gather*}
Si è deciso, al fine di dare una spiegazione fisica per l'effetto di spostamento dei punti di minimo dal centro dello spettro, di eseguire dei fit lineari che considerassero separatamente solo gli ordini positivi e solo gli ordini negativi. Ne risultano due rette che racchiudono la retta precedentemente discussa e che mostrano come associato al fenomeno di aumento di intensità alla base dei grafici si abbia anche una traslazione sostanzialmente simmetrica a destra e a sinistra dei punti di minimo. Possibile causa di ciò sono imperfezioni della fenditura. Infatti la buona compatibilità tra le intercette $B$ e il valore dello zero, che indica corrispondenza tra l'ordine 0 e il centro della figura di diffrazione, è principalmente dovuta ad un errore consistente sull'intercetta della retta piuttosto che ad un'effettiva simmetria del grafico. D'altra parte il fenomeno di spostamento simmetrico dei punti di minimo non può nemmeno essere dovuto ad un errore di ortogonalità del reticolo che avrebbe come effetto una traslazione rigida della figura. Infatti, se si ipotizza che il laser formi l'angolo non nullo $\jmath$ con la normale al reticolo, il termine di sfasamento nel reticolo a $N$ fenditure, o equivalentemente della figura di diffrazione con $a$ in luogo di $d$, sarà dato da
\begin{gather*}
\delta=d\sin\jmath+d\sin\theta
\end{gather*}
con $\theta$ l'angolo che il raggio forma con la normale al supporto delle fenditure. Si nota quindi che il risultato sarebbe sì una rottura della simmetria, ma solo per una traslazione di riferimento angolare pari a $\jmath$. Ciò si tradurrebbe in una traslazione della retta interpolante nel grafico $(m,\sin\theta)$ 
\begin{gather*}
\sin\theta=m\frac{\lambda}{d}-\sin\jmath
\end{gather*}
e non nella separazione tra le rette interpolanti degli ordini positivi e negativi che in realtà si osserva. Si nota inoltre che le distanze tra gli ordini successivi al secondo, nell'ipotesi di equispaziatura dei minimi, prevederebbero la presenza di minimi di ordine $\pm 1$ tra lo 0 e i primi minimi rilevati sperimentalmente: ciò non è verificato in quanto, in corrispondenza di questi, non si riscontra che una minima deflessione della figura. La separazione tra le rette interpolanti per ordini positivi e negativi si può quindi attribuire totalmente a questo problema che si è in grado di spiegare solamente attribuendolo ad imperfezioni dell'apparato.
\begin{figure}
\vspace{-50px}
\includegraphics[width=0.95\textwidth]{retta.eps}\caption{\label{im:retta} Interpolazione lineare una fenditura primo campione}
\vspace{-20px}
\end{figure}
\begin{figure}
\vspace{-20px}
\includegraphics[width=0.95\textwidth]{retta2.eps}\caption{\label{im:retta1} Interpolazione lineare una fenditura secondo campione}
\end{figure}

Si riportano i risultati dei fit nelle tabelle \ref{tab:fitmin1} e \ref{tab:fitmin2}.

\begin{table}[!ht]
\parbox{0.5\linewidth}{
\centering
\caption{\label{tab:fitmin1} Risultati fit lineare una fenditura campione 1}
\begin{tabular}{c|c}
$B$ &-0.0003\\
$\sigma_{B}$ &0.0002\\
$C$ &0.0070\\
$\sigma_{C}$ &0.0001\\
$\chi ^2$&1.48\\
\emph{NDF}&5\\
\emph{Compatibilità} $B$ \emph{e} 0&1.5\\
$a_{1}[\mu m]$&95\\
$\sigma_{a_{1}}[\mu m]$&2\\
\end{tabular} } 
\hfill
\parbox{0.5\linewidth}{
\centering
\caption{\label{tab:fitmin2} Risultati fit lineare una fenditura campione 2}
\begin{tabular}{c|c}
$B$ &-0.0010\\
$\sigma_{B}$ &0.0004\\
$C$ &0.0074\\
$\sigma_{C}$ &0.0001\\
$\chi ^2$&0.92\\
\emph{NDF}&6\\
\emph{Compatibilità} $B$ \emph{e} 0&2.5\\
$a_{2}[\mu m]$&91\\
$\sigma_{a_{2}}[\mu m]$&2\\
\end{tabular} }
\end{table}
Come già detto si ottengono buoni valori di compatibilità tra le intercette e l'origine degli assi.
Poiché si ottengono dei valori del $\chi ^2$ sufficientemente vicini al numero di gradi di libertà, è verificata l'ipotesi di linearità del fit ed è confermata l'adeguatezza dell'errore attribuito alle grandezze in ordinata.
Sono stati quindi ottenuti due valori per la larghezza $a$ della fenditura. Essendo perfettamente compatibili, si è deciso di prendere la media pesata delle due
\[a=(93 \pm 2) \mu m\]

\newpage
\subsection{Interferenza}
Successivamente sono state analizzate le figure di interferenza relative ai reticoli con 2, 3 e 4 fenditure. Questi spettri, come preannunciato, presentano oltre a frange di interferenza, un'evidente modulazione di ampiezza dovuta alla diffrazione analizzata nel paragrafo precedente.
Per la stima della distanza tra le fenditure $d$ si è scelto di effettuare un fit lineare in un grafico $(m, \sin\theta)$ che presenti in ascissa l'ordine dei massimi principali d'interferenza e in ordinata il seno della loro posizione angolare $\theta$. Per far ciò è necessario innanzitutto individuare i massimi principali; a questo scopo è stato adoperato l'algoritmo già usato per trovare i minimi della diffrazione adattato all'uopo: si sono individuati dei punti che fossero dei massimi di intensità in intorni di almeno 31 punti. Poi si sono selezionati tre valori $(x,I)$ a destra e a sinistra per ogni massimo e per ognuno di questi sottointervalli è stato effettuato un fit parabolico tramite il programma GNUPLOT: l'ascissa del vertice della parabola dà il punto di massimo $x_{max}$. Si è scelto di considerare unicamente i massimi principali dell'interferenza per due ragioni: innanzitutto i massimi secondari (nei grafici con numero di fenditure maggiore di due) sono poco distinguibili e risultano in parte "assorbiti" dai massimi principali più evidenti, come già accadeva per i massimi di ordine 1 nella diffrazione; inoltre i minimi, per la stessa ragione, sono sia spostati a causa del fenomeno di aumento d'intensità nella zona centrale sia del tutto inapprezabili quando separano un massimo principale da uno secondario.

Dopo l'opportuna conversione dei punti di minimo in seno di distanze angolari, riportati nelle tabelle \ref{tab:intermax1}, \ref{tab:intermax2} e \ref{tab:intermax3}, sono stati quindi effettuati dei fit lineari in grafici $(m,\sin\theta)$ tramite rette di equazione $y=E+Fx$ da cui si ricava il valore della distanza tra le fenditure come
\[d=\frac{\lambda}{F}\]
a cui si associa l'errore casuale dato dalla propagazione.
\begin{gather*}
\sigma_d \simeq d\sqrt{\left(\frac{\sigma_{\lambda}}{\lambda}\right)^2+\left(\frac{\sigma_{F}}{F}\right)^2}
\end{gather*}
\begin{table}[!ht]
\parbox{\linewidth}{
\centering
\caption{\label{tab:intermax1} Valori del fit due fenditure}
\begin{tabular}{ccccc}
$m$ & $x_{min}[passi]$ & $\sigma_{x_{min}}[passi]$& $\sin \theta$ &$\sigma_{\sin \theta}$\\ \hline
1&154&10&0.0029&0.0002\\
2&340&10&0.0065&0.0003\\
3&510&30&0.0097&0.0005\\
4&710&40&0.0135&0.0008\\
5&870&40&0.0167&0.0009\\
6&1060&100&0.020&0.002\\
7&1250&40&0.0238&0.0008\\
8&1420&60&0.027&0.001\\
9&1600&100&0.031&0.002\\
10&1770&80&0.034&0.002\\
11&1920&60&0.037&0.001\\
-1&-180&10&-0.0034&0.0002\\
-2&-370&10&-0.0070&0.0002\\
-3&-530&40&-0.0100&0.0008\\
-4&-730&40&-0.0140&0.0007\\
-5&-900&30&-0.0172&0.0006\\
-6&-1070&20&-0.0205&0.0005\\
-7&-1280&80&-0.024&0.001\\
-8&-1440&40&-0.0275&0.0008\\
-9&-1660&70&-0.032&0.001\\
-10&-1800&200&-0.034&0.004\\
-11&-1960&90&-0.037&0.002\\
\end{tabular} } 
\end{table}
\begin{table}[!ht]
\parbox{\linewidth}{
\centering
\caption{\label{tab:intermax2} Valori del fit tre fenditure}
\begin{tabular}{ccccc}
$m$ & $x_{min}[passi]$ & $\sigma_{x_{min}}[passi]$& $\sin \theta$ &$\sigma_{\sin \theta}$\\ \hline
1&159&9&0.0030&0.0002\\
2&344&2&0.00656&0.00010\\
3&510&10&0.0098&0.0003\\
4&710&20&0.0135&0.0005\\
5&880&20&0.0167&0.0005\\
6&1070&60&0.020&0.001\\
7&1250&50&0.0238&0.0009\\
8&1430&70&0.027&0.001\\
9&1600&200&0.031&0.004\\
10&1780&70&0.034&0.001\\
11&1940&40&0.0371&0.0009\\
-1&-189&3&-0.00360&0.00008\\
-2&-368&9&-0.0070&0.0002\\
-3&-530&50&-0.0101&0.0009\\
-4&-730&10&-0.0140&0.0003\\
-5&-900&60&-0.017&0.001\\
-6&-1080&30&-0.0206&0.0006\\
-7&-1300&100&-0.024&0.003\\
-8&-1440&50&-0.027&0.001\\
-9&-1630&50&-0.031&0.001\\
-10&-1800&200&-0.034&0.004\\
-11&-2000&100&-0.037&0.002\\
\end{tabular} } 
\end{table}

\begin{table}[ht]
\parbox{\linewidth}{
\centering
\caption{\label{tab:intermax3} Valori del fit quattro fenditure}
\begin{tabular}{ccccc} 
$m$ & $x_{min}[passi]$ & $\sigma_{x_{min}}[passi]$& $\sin \theta$ &$\sigma_{\sin \theta}$\\ \hline
1&148&9&0.0028&0.0002\\
2&329&10&0.0063&0.0002\\
3&507&9&0.0097&0.0002\\
4&690&70&0.013&0.001\\
5&860&20&0.0165&0.0005\\
6&1060&70&0.020&0.001\\
7&1220&40&0.0234&0.0009\\
8&1430&90&0.027&0.002\\
9&1600&100&0.030&0.002\\
10&1800&200&0.034&0.003\\
11&1900&200&0.037&0.003\\
-1&-189&9&-0.0036&0.0002\\
-2&-380&10&-0.0073&0.0003\\
-3&-550&40&-0.0104&0.0008\\
-4&-750&30&-0.0142&0.0006\\
-5&-900&100&-0.018&0.002\\
-6&-1100&40&-0.0209&0.0008\\
-7&-1280&30&-0.0244&0.0007\\
-8&-1460&70&-0.028&0.001\\
-9&-1800&70&-0.034&0.001\\
-10&-1980&50&-0.038&0.001\\
\end{tabular} } 
\end{table}

Come per la diffrazione sono state graficate le rette interpolanti per ordini positivi e per ordini negativi: nel caso di più fenditure e con l'aumentare degli ordini l'effetto di separazione scompare. Si deduce quindi che il problema riscontrato è presente solo nella modulazione di ampiezza dovuta alla diffrazione e non si ripercuote sui massimi d'interferenza.

\begin{figure}
\vspace{-20px}
\includegraphics[width=0.95\textwidth]{retta3.eps}\caption{\label{im:retta3} Interpolazione lineare due fenditure}
\vspace{-20px}
\end{figure}
\begin{figure}
\vspace{-20px}
\includegraphics[width=0.95\textwidth]{retta4.eps}\caption{\label{im:retta4} Interpolazione lineare tre fenditure}
\vspace{-20px}
\end{figure}
\begin{figure}
\vspace{-20px}
\includegraphics[width=0.95\textwidth]{retta5.eps}\caption{\label{im:retta5} Interpolazione lineare quattro fenditure}
\vspace{-20px}
\end{figure}

Si riportano i risultati dei fit nelle tabelle \ref{tab:fitmax1}, \ref{tab:fitmax2} e \ref{tab:fitmax3}.

\begin{table}[ht]
\parbox{0.5\linewidth}{
\centering
\caption{\label{tab:fitmax1} Risultati fit lineare due fenditure}
\begin{tabular}{c|c}
$E$ &-0.00025\\
$\sigma_{E}$ &0.00005\\
$F$ &0.0043\\
$\sigma_{F}$ &0.00002\\
$\chi ^2$&3.19\\
\emph{NDF}&14\\
\emph{Compatibilità} $E$ \emph{e} 0&6\\
$d_{1}[\mu m]$&198\\
$\sigma_{d_{1}}[\mu m]$&2\\
\end{tabular} } 
\hfill
\parbox{0.5\linewidth}{
\centering
\caption{\label{tab:fitmax2} Risultati fit lineare tre fenditure}
\begin{tabular}{c|c}
$E$ &0.00024\\
$\sigma_{E}$ &0.00002\\
$F$ &0.003396\\
$\sigma_{F}$ &0.000009\\
$\chi ^2$&1.72\\
\emph{NDF}&14\\
\emph{Compatibilità} $E$ \emph{e} 0&13\\
$d_{2}[\mu m]$&197\\
$\sigma_{d_{2}}[\mu m]$&2\\
\end{tabular} }
\end{table}
\begin{table}[ht]
\parbox{\linewidth}{
\centering
\caption{\label{tab:fitmax3} Risultati fit lineare quattro fenditure}
\begin{tabular}{c|c}
$E$ &0.00044\\
$\sigma_{E}$ &0.00004\\
$F$ &0.00338\\
$\sigma_{F}$ &0.00002\\
$\chi ^2$&2.83\\
\emph{NDF}&14\\
\emph{Compatibilità} $E$ \emph{e} 0&11\\
$d_{3}[\mu m]$&198\\
$\sigma_{d_{3}}[\mu m]$&2\\
\end{tabular} } 
\end{table}
Analizzando i fit si può notare che a differenza della diffrazione, emerge un'incompatibilità tra le intercette a causa dell'errore molto minore su queste ultime (dovuto ad un fit più accurato del precedente)  queste e l'origine degli assi e quindi un errore sulla procedura di scelta del centro dei campioni, resa più difficile e soggetta ad imprecisioni a causa della ristretta larghezza del massimo centrale. Anche in questo caso i valori del $\chi ^2$, inferiori ai gradi di libertà mostrano che è verificata l'ipotesi della linearità sulla base degli errori attribuiti alle grandezze in ordinata. Sono stati quindi ottenuti tre valori della distanza tra le fenditure $d$ e, essendo compatibili si è deciso di prendere una media pesata

\[d =(198 \pm 3) \mu m \]

\clearpage
\section{Conclusioni}
In conclusione, riguardo alla stima della larghezza delle fenditure $a$, nei limiti delle imperfezioni riscontrate si è ottenuto il valore di
\[a=(93 \pm 2) \mu m\]
Per quanto riguarda invece la distanza tra le fenditure, si è ottenuta una stima più attendibile, sostanzialmente priva dei problemi riguardanti la figura di diffrazione
\[d =(198 \pm 3) \mu m \]

%давай кричи но тебя могут не понять..
\end{document}

