#include <sstream>
#include <fstream>
#include <cmath>
#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <iterator>

//Restituisce la media sul campione
double valormedio(std::vector<double> a){
	double sum=0;
	int campione=a.size();
	for(double i:a){
	sum+=i;
		}
	return (sum+0.0)/campione;
};

//Restituisce il valore minimo
double smin(std::vector<double> a){
	double min=a[0];
	for(double i:a){
	if(i<min){
	min=i;
	} else {
		continue;
		}
	}
	return min;
}

//Restituisce il valore massimo
double smax(std::vector<double> a){
	double max=a[0];
	for(double i:a){
	if(i>max){
	max=i;
	} else {
		continue;
		}
	}
	return max;
}

//Scarto quadratico medio
double rms(std::vector<double> a){
	double variance;
	double vv=valormedio(a);
	for(double i:a){
		variance+= pow((i-vv),2); }
		return sqrt(variance/(a.size()));
}

//Deviazione standard
double stdev(std::vector<double> a){
	double variance;
	double vv=valormedio(a);
	for(double i:a){
		variance+= pow((i-vv),2); }
		//Uso size-1 perché è la stima più precisa
		return sqrt(variance/(a.size()-1));
}

//Errore della media
double mean_stdev(std::vector<double> a){
	double variance;
	double vv=valormedio(a);
	for(double i:a){
		variance+= pow((i-vv),2.0); }
		return (sqrt(variance/(a.size()-1)))/sqrt(a.size());
}

double covarianza_campionaria ( std::vector<double> a, std::vector<double> b){
	double sum1,sum2,sum3;
	for(std::vector<double>::size_type i = 0; i < a.size() ; ++i){
		sum1 += (a.at(i))*(b.at(i))/a.size();
	}
	for (auto i : a) sum2+= i / a.size();
	for (auto i : b) sum3+= i / b.size();
	return (sum1 - sum2*sum3);
}

double correlazione (std::vector<double> a, std::vector<double> b){
	return covarianza_campionaria(a,b)/(rms(a)*rms(b));
}

std::vector<double> reciproco(std::vector<double> a){
	std::vector<double> temp;
	for (double i : a){
		temp.push_back(1.0/i);
	}
	return temp;
}

std::vector<double> wmean(std::vector<double> data, std::vector<double> error){
		if(data.size() == error.size()){
		int N = data.size();
		double num=0;
		double den=0;
		for (int i = 0; i < N; ++i){
			num+=(data.at(i))/pow(error.at(i),2.0);
			den+=1.0/pow(error.at(i),2.0);
		}
		std::vector<double> result{(num/den), static_cast<double>(sqrt(1.0/den))};
		return result;
	} else exit(1);
}

int findprecision (double a){
	char buffer[15] = "test";
	int n;
	std::string exp;
	sprintf(buffer, "%e", a);
	std::string rounded(buffer);
	rounded.shrink_to_fit();
	std::size_t found = rounded.find('e');
	exp = rounded.substr(found+1,3);
	n= -(std::stoi(exp, nullptr, 0));
	return (n);
}

template<typename T> void printElement(std::ostream &a,T t, const int& width, char sep){
    a << std::fixed <<  std::left << std::setw(width+3) << std::setprecision(width) << std::setfill(sep) << t;
}

std::vector<double> interpolazione (std::vector<double> j, std::vector<double> k,std::vector<double> l, std::vector<double> m, std::ostream &out = std::cout, double yerror = 0){
	std::vector<double> *x = new std::vector<double> {0};
	std::vector<double> *y = new std::vector<double> {0};
	std::vector<double> *z = new std::vector<double> {1};
	bool chi = false;
	bool &chong = chi;
	x=&j;
	y=&k;
	int grandezza_campione = k.size();
	std::vector<double> errtemporaneo(grandezza_campione, 1.0);
	if(l.size() != 0) {
		z = &l;
		out<< "#Fit chi quadro (immesso errore singole misure)" <<std::endl;
		out.flush();
		chong = true;
	} else {
		out<< "#Fit minimi quadrati (error defaulted to 1)" <<std::endl;
		out.flush();
		z = &errtemporaneo;
	}
	if (x->size() == y->size()){
		//for(std::vector<double>::size_type i=0; i != x->size(); ++i){std::cout<< "x" << i << " = " << (*x).at(i) << " y" << i << " = " << (*y).at(i) << " z" << i << " = " << (*z).at(i) << std::endl;}
		double delta, a, b , sum1, sum2, sum3, sum4, sum5, sum6, vary, sigmay, sigmaa, sigmab, corr, chisq;
		delta = 0; sum1 = 0; sum2 = 0; sum3 = 0; sum4 = 0; sum5 = 0; vary=0;
		//sum1= (x), sum2=(x)^2, sum3= (x^2), sum4=(y), sum5=(x*y) , sum6 = 1/s^2 ; il fit cercato Ã¨ a+bx
		for(std::vector<double>::size_type i=0; i != x->size(); ++i){
			sum1 += ((*x).at(i))/(pow(((*z).at(i)),2.0));
		}
		sum2 = pow(sum1, 2.0);
		for(std::vector<double>::size_type i=0; i != x->size(); ++i){
			sum3 += pow((*x).at(i),2.0)/pow((*z).at(i),2.0);
		}
		
		for(std::vector<double>::size_type i=0; i != x->size(); ++i){
			sum4 += ((*y).at(i))/pow(((*z).at(i)),2.0);
		}
		
		for(std::vector<double>::size_type i=0; i != x->size(); ++i){
			sum5+=(((*x).at(i))*((*y).at(i)))/pow(((*z).at(i)),2.0);
		}
		
		for(std::vector<double>::size_type i=0; i!= x->size(); ++i){
			sum6+= 1.0/pow((*z).at(i),2.0);
		}
		if (chi == false){
			delta = x->size()*sum3-sum2;
			a = (1.0/delta)*(sum3*sum4-sum1*sum5);
			b = (1.0/delta)*(x->size()*sum5-sum1*sum4);
			if (yerror == 0){
				for (int i = 0; i<x->size(); ++i){
					vary += pow(((a+b*((*x).at(i))-(*y).at(i))),2.0)/(x->size()-2.0);
				}
				sigmay = pow(vary,0.5);
			} else {
			sigmay = yerror;
			}
			sigmaa = sigmay*sqrt(sum3/delta);
			sigmab = sigmay*sqrt(x->size()/delta);
			corr = correlazione(*x,*y);
			double cov_a_b = (-1.0/delta*sum1*sigmay);
			out << "#" << std::left << std::setw(24) << "Intercetta x = " << -a/b << std::left << std::setw(25) << " Errore intercetta x = " << static_cast<double>(sqrt(pow(a/(b*b)*sigmab,2.0)+pow(-1.0/b*sigmaa,2.0))+2*cov_a_b*(-a/(b*b*b))) << std::endl;
			out << "#" << std::left << std::setw(24) << "Intercetta y = " << a << std::left << std::setw(25) << " Errore intercetta y = " << sigmaa << std::left << std::setw(25) << "\n#Coefficiente angolare = " << b << std::left << std::setw(25) << " Errore coefficiente angolare = " << sigmab << std::endl;
			out << "#" << std::left << std::setw(24) << "Errore a posteriori = " << std::left << std::setw(25) << sigmay << std::endl;
			out << "#" << std::left << std::setw(24) << "Correlazione lineare = " << std::left << std::setw(25) << corr << std::endl;
			out << "#" << std::left << "Ascissa" << std::left << "," << "Ordinata" << std::endl;
			for(int i = 0; i < x->size(); ++i){
				printElement(out,x->at(i),findprecision(m.at(i)),',');
				printElement(out,y->at(i),findprecision(sigmay),',');
				//out << std::left << std::setw(25) << std::fixed << x->at(i) << std::left << std::setw(25)  << (*y).at(i) << std::endl;
			}
			return std::vector<double>{a,b,delta,sigmay,sigmaa,sigmab,corr};
			
			
		} else {
			delta = sum6*sum3-sum2;
			a = (1.0/delta)*(sum3*sum4-sum1*sum5);
			b = (1.0/delta)*(sum6*sum5-sum1*sum4);
			if (yerror == 0){
				for (int i = 0; i<x->size(); ++i){
					vary += pow(((a+b*((*x).at(i))-(*y).at(i))),2.0)/(x->size()-2.0);
				}
				sigmay = pow(vary,0.5);
			} else {
			sigmay = yerror;
			}
			sigmaa = sqrt((1.0/delta)*sum3);
			sigmab = sqrt((1.0/delta)*sum6);
			corr = correlazione(*x,*y);
			for(std::vector<double>::size_type i = 0; i < x->size(); ++i){
				chisq += pow(((*y).at(i)-(a+b*(*x).at(i)))/(*z).at(i),2.0);
			}
			double cov_a_b = (-1.0/delta)*sum1*sigmay;
			std::vector<double> intercette{a,-a/b};
			std::vector<double> intercette_err = {sigmaa, sqrt(pow(a/(b*b)*sigmab,2.0)+pow(-1.0/b*sigmaa,2.0)+2*cov_a_b*(-a/(b*b*b)))};
			std::vector<double> intercette_wmean = wmean(intercette, intercette_err);
			out << "#" << std::left << std::setw(24) << "Intercetta x = " << intercette[1] << std::left << std::setw(25) << " Errore intercetta x = " << intercette_err[1] << std::endl;
			out << "#" << std::left << std::setw(24) << "Intercetta y = " << intercette[0] << std::left << std::setw(25) << " Errore intercetta y = " << intercette_err[0] << std::left << std::setw(25) << "\n#Coefficiente angolare = " << b << std::left << std::setw(25) << " Errore coefficiente angolare = " << sigmab << std::endl;
			out << "#" << std::left << std::setw(24) << "Media pesata distanza focale = " << 1.0/intercette_wmean[0] << " +- " <<  intercette_wmean[1]/pow(intercette_wmean[0],2.0) << std::endl;
			out << "#" << std::left << std::setw(24) << "Compatibilità tra intercette = " << fabs(a+a/b)/(sqrt( pow(sigmaa,2.0)+ pow(a/(b*b)*sigmab,2.0)+pow(-1.0/b*sigmaa,2.0) ))<< std::endl;
			out << "#" << std::left << std::setw(24) << "Correlazione lineare = " << std::left << std::setw(25) << corr << std::endl;
			out << "#Chiquadro = " << chisq << "\t sigmaypost = " << sigmay << std::endl;
			out << "#" << std::left <<  "Ascissa " << ',' << "Errore ascissa" << ',' << "Ordinata" << ',' << "Errore ordinata" << std::endl;
			for(int i = 0; i < x->size(); ++i){
				//out << std::left << std::setw(25) << std::fixed << x->at(i) << std::left << std::setw(25) << m.at(i) << std::left << std::setw(25) << (*y).at(i) << std::left << std::setw(25) << (*z).at(i) << std::endl;
				printElement(out,x->at(i),findprecision(m.at(i)),',');
				printElement(out,m.at(i),findprecision(m.at(i)),',');
				printElement(out,y->at(i),findprecision(z->at(i)),',');
				printElement(out,z->at(i),findprecision(z->at(i)),',');
				out << std::endl;
			}
			errtemporaneo.clear();
			return std::vector<double>{a,b,delta,sigmay,sigmaa,sigmab, corr};
		} 
	} else {
		std::cout<<"I dati non sono stati forniti in coppia"<<std::endl;
		out.flush();
		exit(1);
	}
}

double pi = 3.14159265359;
double th_err = 0.000290888/sqrt(6);

int main(){
	std::ifstream campione1("campione1");
	std::vector<double> angoli_medi_1;
	std::vector<std::string> nomi_colori;
	std::vector<double> lambda = {643.8, 508.6, 480.0, 467.8, 441.6};
	double a1,a2,b1,b2;
	std::string temp;
	while( campione1 >> a1 >> a2 >> b1 >> b2 >> temp ){	
		//std::cerr << a1 << '\t' << a2 << '\t' << b1 << '\t' << b2 << '\t' << temp << std::endl;
		angoli_medi_1.push_back( ( (a1 + a2/60.0) + (b1-180 +b2/60) )*pi/360 );
		nomi_colori.push_back( temp );
	}
	campione1.close();
	std::ifstream campione2("campione2");
	std::vector<double> angoli_medi_2;
	while( campione2 >> a1 >> a2 >> b1 >> b2 >> temp ){
		angoli_medi_2.push_back( ( (a1 + a2/60.0) + (b1-180 +b2/60) )*pi/360 );
		temp.clear();
	}
	campione2.close();
	double alfa = pi/3;
	double alfa_err = sqrt(2)*th_err;
	std::vector<double> n1, n1_err;
	for (double i : angoli_medi_1){
		n1.push_back(sin((i+alfa)/2.0)/sin(alfa/2.0));
		n1_err.push_back(0.5*th_err*sqrt(2*pow(sin(i/2.0),2.0)/pow(sin(alfa/2.0),4.0) + pow(cos((i+alfa)/2.0),2.0)/pow(sin(alfa/2.0),2.0)));
	}
	std::vector<double> n2, n2_err;
	for (double i : angoli_medi_2){
		n2.push_back(sin((i+alfa)/2.0)/sin(alfa/2.0));
		n2_err.push_back(0.5*th_err*sqrt(2*pow(sin(i/2.0),2.0)/pow(sin(alfa/2.0),4.0) + pow(cos((i+alfa)/2.0),2.0)/pow(sin(alfa/2.0),2.0)));
	}
	std::ofstream out1("dispersione1");
	std::ofstream out1tex("grafico1.tex");	
	out1tex << "\\emph{Colore}&$\\lambda [nm] $&$n(\\lambda)$&$\\sigma_{n}$ \\\\ \\hline"<< std::endl;
	for(std::vector<double>::size_type t = 0; t < angoli_medi_1.size(); ++t){
		out1 << std::fixed << lambda[t] << '\t' << angoli_medi_1[t] << '\t' << n1[t] << '\t' << n1_err[t] << '\t' << nomi_colori[t] << std::endl;
		out1tex << '$' << nomi_colori[t] << "$&" << std::fixed << std::setprecision(1) << lambda[t] << '&' << std::setprecision(findprecision(n1_err[t])) << n1[t] << '&' << std::setprecision(findprecision(n1_err[t])) << n1_err[t] << "\\\\" << std::endl;
	}
	std::ofstream out2("dispersione2");
	std::ofstream out2tex("grafico2.tex");
	out2tex << "\\emph{Colore}&$\\lambda [nm] $&$n(\\lambda)$&$\\sigma_{n}$ \\\\ \\hline"<< std::endl;
	for(std::vector<double>::size_type t = 0; t < angoli_medi_2.size(); ++t){
		out2 << std::fixed << lambda[t] << '\t' << angoli_medi_2[t] << '\t' << n2[t] << '\t' << n2_err[t] << '\t' << nomi_colori[t] << std::endl;
		out2tex << '$' << nomi_colori[t] << "$&" << std::fixed << std::setprecision(1) << lambda[t] << '&' << std::setprecision(findprecision(n2_err[t])) << n2[t] << '&' << std::setprecision(findprecision(n2_err[t])) << n2_err[t] << "\\\\" << std::endl;
	}
	std::ofstream out3("medie_n");
	for(std::vector<double>::size_type t = 0; t < n1.size(); ++t){
		out3 << nomi_colori[t] << '\t' << (n1[t]/pow(n1_err[t],2.0)+n2[t]/pow(n2_err[t],2.0))/(1.0/pow(n1_err[t],2.0)+ 1.0/pow(n2_err[t],2.0)) << '\t' << 1.0/sqrt(1.0/pow(n1_err[t],2.0)+ 1.0/pow(n2_err[t],2.0)) << std::endl;
	}
}
