reset
set term postscript eps color
set fit errorvariables
set encoding iso_8859_1
set zeroaxis
unset colorbox
set ylabel "Indice di rifrazione" offset 2
#colori = "#0005FF #008EFF #00CCFF #00FF0C #FF0300"
attesi = "441.6 467.8 480.0 508.6 643.8"
set format x "%g"
set key at graph 0.5,0.9 reverse Left textcolor "black"
set palette maxcolors 5
set palette defined (0 '#CC0000', 1 '#00CC00', 2 '#00CCFF', 3 '#0000BB', 4 '#7F00FF' )
list = system("ls | grep -v 'eps' | grep -v 'fit' | grep -E 'dispersione[[:digit:]]?'")
do for [file in list]{
stats file u 1 nooutput
f(x) = a + b*x
#g(x) = c + d/x**2
set print file.".fit"
fit f(x) file u (1.0/($1)**2):3:4 via a ,b
k = sqrt(FIT_WSSR/FIT_NDF)
print sprintf("a = %f, a_err = %f, b = %f, b_err = %f, Chi = %d , NDF = %d , k = %f", a, a_err, b, b_err, FIT_WSSR, FIT_NDF,k)
#fit g(x) file u 1:3:4 via c,d
#print sprintf("c = %f, c_err = %f, d = %f, d_err = %f, Chi = %d , NDF = %d", c, c_err, d, d_err, FIT_WSSR, FIT_NDF)
print sprintf("a = %f, a_err = %f, b = %f, b_err = %f, Chi = %d , NDF = %d", a, a_err*k, b, b_err*k, FIT_NDF, FIT_NDF)
set out file."_hyp.eps"
set xlabel "Lunghezza d'onda [nm]"
plot [STATS_min-50:STATS_max+50] file u 1:3:($4*k):0 w yerror lc palette pt 1 ps 0.5 not, '+' w yerror lc 'black' pt 1 title "n({/Symbol l})", f(1/x**2) w l lt 0 lc 'black' title sprintf("%f + %f/{/Symbol l}^2",a,b)
set out file."_retta.eps"
stats file u (1.0/($1)**2) nooutput
set xlabel "Reciproco lunghezza d'onda al quadrato [nm^{-2}]"
plot [STATS_min-10**(-7):STATS_max+10**(-7)] file u (1.0/($1)**2):3:($4*k):0 w yerror lc palette pt 1 ps 0.5 not,'+' w yerror lc 'black' pt 1 title "n({/Symbol l})", f(x) w l lt 0 lc 'black' title sprintf("%f + %f*x",a,b)

}
set key default
set key top right
set out "kurwa.eps"
h(x) = 1.593524 + 9217.560840/x**2
set xlabel "Lunghezza d'onda [nm]"
plot "dispersione2" u 1:(h($1)):0 lc palette pt 1 not, for [i=0:4] '+' u (NaN):(NaN):(i) w p pt 1 ps 2 lc palette title sprintf("n({/Symbol l}=%s nm) = %.4f", word(attesi,5-i) , h(real(word(attesi,5-i))) ) , h(x) ls 0 lc rgb 'black' title "1.593524 + 9217.560840/x^2"
