#include <Riostream.h>
#include <stdlib.h>
#include <TROOT.h>
#include <TSystem.h>
#include "TFile.h"
#include "TTree.h"
#include "TNtuple.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TH1F.h"
#include "TF1.h"
#include "TFitResult.h"
#include "TRint.h"
#include "TLegend.h"
#include "TBranch.h"
#include "TStyle.h"
#include "TGaxis.h"
#include <iostream>
#include <fstream>

/*#include <string>
#include "AnalysisInfo.h"
#include "Getline.h"
#include <memory>
#include <iterator>
#include <algorithm>
#include <tuple>
#include <map>
#include <unordered_map>
#include <utility>
#include <iomanip>
#include <functional>
*/ 
struct slimport_data_t {
	ULong64_t	timetag; //time stamp
	UInt_t		baseline;
	UShort_t	qshort; //integration with shorter time
	UShort_t	qlong; //integration with longer time
	UShort_t	pur;
	UShort_t	samples[4096];
};


int main () {
	slimport_data_t indata;
	short int chan=1;
	int numBins=500;
	double minX=0.;
	double maxX=30000.;
	//std::string name_file=
	
	TFile *infile = new TFile("tag_na22_calib.root");
	TH1F *spettro = new TH1F("spettro","Total spectrum",numBins,minX,maxX);
	TTree *intree = (TTree*)infile->Get("acq_tree_0");
	TBranch *inbranch = intree->GetBranch(Form("acq_ch%d",chan)); // Numero del canale variabile
	inbranch->SetAddress(&indata.timetag);
	std::cout<<inbranch->GetEntry(30)<<std::endl;
	
 //histogram filling
	std::cout<<inbranch->GetEntry(56789)<<std::endl;
	
	for (int i=0; i<inbranch->GetEntries(); i++) {
		inbranch->GetEntry(i);
		spettro->Fill(indata.qlong); 
		}
	
	TFile *outfile = new TFile("test.root","RECREATE");
	spettro->Write();
	return 0;
}
