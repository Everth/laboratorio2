#include <iostream>
#include <cmath>
#include <vector>
#include <string>
#include <sstream>
#include <limits>
#include <map>
#include <utility>


enum class phys_quantity {VDC, VAC, IDC, IAC, FREQ, SECFREQ, OHM, CAPACITY};

constexpr long double operator "" _p(long double l) {
    return l / 1000.0 / 1000.0 / 1000.0 / 1000.0;
}

constexpr long double operator "" _n(long double l) {
    return l / 1000.0 / 1000.0 / 1000.0;
}

constexpr long double operator "" _u(long double l) {
    return l / 1000.0 / 1000.0;
}

constexpr long double operator "" _m(long double l) {
    return l / 1000.0;
}

constexpr long double operator "" _k(long double l) {
    return l * 1000;
}

constexpr long double operator "" _M(long double l) {
    return l * 1000 * 1000;
}

constexpr long double operator "" _G(long double l) {
    return l * 1000 * 1000 * 1000;
}

class InstrumentSpecs {
	public:
	static std::map<phys_quantity, std::map<double, std::pair<double,double>>> errs;
};

InstrumentSpecs MTX3293_specs;

MTX3293_specs.errs.insert( {phys_quantity::OHM, { {1.0_k, std::make_pair(100, 100)} , {1.0_M, std::make_pair(200,200)} } } );

struct misura {
	double value;
	double error;
	misura()=default;
	misura(double value, double error) : value(value), error(error) {};
};

	

class MTX3293 : public misura {
	double gain_err;
	double digit_err;
	double tot_err(int a, phys_quantity b,int c) { return 0;} ;
	double gainerr(){
			std::cerr << "Porcoddio" << std::endl;
			return std::numeric_limits<double>::quiet_NaN();		
		}
	double digiterr(){
			std::cerr << "Porcamadonna" << std::endl;
			return std::numeric_limits<double>::quiet_NaN();	
		}
	public:
	MTX3293()=default;
	MTX3293(double value, phys_quantity quantity, double fondoscala): misura(value,tot_err(value,quantity,fondoscala)) , gain_err(gainerr()), digit_err(digiterr()) {if (value > fondoscala) std::cerr << "Allora sei stronzo" << std::endl;}
	~MTX3293() = default;
};

int main() {
	//std::vector<std::map<double, std::pair<double,double>>> test1;
	//test1.push_back({ {1.0_k, std::make_pair(100, 100)} , {1.0_M, std::make_pair(200,200)} });
	MTX3293 test(9.897e3, phys_quantity::OHM, 100e3);
	MTX3293 test2(9.897e3,phys_quantity::OHM, 1e3);
	//std::map<phys_quantity, std::map<double, std::pair<double,double>>> test1;
	//test1[phys_quantity::OHM] = { {1.0_k, std::make_pair(100, 100)} , {1.0_M, std::make_pair(200,200)} };
}
	
