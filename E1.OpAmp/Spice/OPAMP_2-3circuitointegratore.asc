Version 4
SHEET 1 916 680
WIRE 352 -128 272 -128
WIRE 480 -128 416 -128
WIRE -320 -16 -416 -16
WIRE -32 -16 -240 -16
WIRE 80 -16 -32 -16
WIRE 272 -16 272 -128
WIRE 272 -16 144 -16
WIRE 336 -16 272 -16
WIRE 480 -16 480 -128
WIRE 480 -16 416 -16
WIRE -416 32 -416 -16
WIRE 704 32 688 32
WIRE 848 32 784 32
WIRE -32 48 -32 -16
WIRE 368 64 352 64
WIRE 848 96 848 32
WIRE 896 96 848 96
WIRE 352 112 352 64
WIRE 272 128 272 -16
WIRE 320 128 272 128
WIRE 480 144 480 -16
WIRE 480 144 384 144
WIRE 512 144 480 144
WIRE -416 160 -416 112
WIRE -32 160 -32 128
WIRE 320 160 272 160
WIRE 896 160 896 96
WIRE 704 192 688 192
WIRE 848 192 848 96
WIRE 848 192 784 192
WIRE 352 208 352 176
WIRE 384 208 352 208
WIRE 272 304 272 160
FLAG -32 160 0
FLAG 512 144 OUT
FLAG -416 160 0
FLAG 272 304 0
FLAG -32 -16 V1
FLAG 896 160 0
FLAG 688 32 Vcc
FLAG 688 192 Vee
FLAG 368 64 Vcc
FLAG 384 208 Vee
SYMBOL res 432 -32 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName Rf
SYMATTR Value 1.5Meg
SYMBOL res -48 32 R0
SYMATTR InstName R3
SYMATTR Value 50
SYMBOL voltage -416 16 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
WINDOW 3 -153 193 Left 2
SYMATTR Value PULSE(0 +2 3m 1n 1n 10m 20m)
SYMATTR InstName Vg
SYMBOL res -336 0 R270
WINDOW 0 32 56 VTop 2
WINDOW 3 0 56 VBottom 2
SYMATTR InstName Rg
SYMATTR Value 50
SYMBOL cap 144 -32 R90
WINDOW 0 0 32 VBottom 2
WINDOW 3 32 32 VTop 2
SYMATTR InstName C1
SYMATTR Value 200p
SYMBOL cap 416 -144 R90
WINDOW 0 0 32 VBottom 2
WINDOW 3 32 32 VTop 2
SYMATTR InstName C2
SYMATTR Value 500p
SYMBOL Opamps\\LTC1151 352 80 R0
WINDOW 3 269 390 Left 2
SYMATTR InstName U2
SYMBOL voltage 688 192 M90
WINDOW 0 -32 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName Vee
SYMATTR Value -15
SYMBOL voltage 688 32 R270
WINDOW 0 32 56 VTop 2
WINDOW 3 -32 56 VBottom 2
SYMATTR InstName Vcc
SYMATTR Value +15
TEXT -608 -120 Left 2 !.tran 90m
