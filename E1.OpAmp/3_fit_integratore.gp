reset
set term postscript eps enhanced
set out "3_fit_integratore.eps"
set xlabel "V_{in} [V]"
set ylabel "V_{out} [V]"
set fit error
f(x) = m*x + q
mt = -214.0/458
fit f(x) "3_OPAMP_integratore.txt" using 1:3 via m,q
plot "3_OPAMP_integratore.txt" using 1:3, f(x) t sprintf("%f*x + %f",m,q), NaN t sprintf("m_{teor}=%g ; {/Symbol l}= %f", mt, abs(mt-m)/m_err), NaN t sprintf("m = %f +- %f (%.1f\%)", m, m_err, abs(m_err/m*100)), NaN t sprintf("q = %f +- %f (%.1f\%)", q, q_err, abs(q_err/q*100))

#RESIDUI
set out "3_residui_integratore.eps"
set xzeroaxis
plot "3_OPAMP_integratore.txt" using 1:($3-f($1)):(FIT_STDFIT) w yerror
