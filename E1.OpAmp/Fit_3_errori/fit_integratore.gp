reset
set tics nomirror
set term postscript eps enhanced "Verdana, 14"
set encoding iso_8859_1
set out "3_fit_integratore.eps"
set fit error
set macros
text = "NaN w dots linecolor rgb 'white' title sprintf"
set xlabel ""
set ylabel ""
set key reverse
f(x) = m*x + q
set fit error
mt = -215.0/458
mt_err = mt*sqrt( (9.199/215)**2 + (10.895/458)**2 )

set bmargin 3
set lmargin 9
set rmargin 3
set multiplot layout 2,1
fit f(x) "3_OPAMP_integratore.csv" using 1:6:10 via m,q
m_old = m
fit f(x) "3_OPAMP_integratore.csv" using 1:6:(sqrt($10**2+m_old**2*$5**2)) via m,q
plot "3_OPAMP_integratore.csv" using 1:6:($10**2+m_old**2*$5**2) w yerror not, f(x) t sprintf("%f*x + %f",m,q), @text("m_{teor}=%g ; {/Symbol l}= %f", mt, abs(mt-m)/sqrt(m_err**2+mt**2)), @text("m = %f {\261} %f (%.1f\%)", m, m_err, abs(m_err/m*100)), @text("q = %f {\261} %f (%.1f\%)", q, q_err, abs(q_err/q*100)), @text("{/Symbol c^2}_r=%f",FIT_STDFIT**2)

#RESIDUI
stats "3_OPAMP_integratore.csv" using 1 prefix "X" noout
x_m = X_mean
var_x = X_ssd**2
new_y_err = "sqrt($10**2+m_old**2*$5**2)"
rho = - x_m / sqrt(var_x + x_m**2)

set xzeroaxis
set label 1 'V_1 [V]' at screen 0.5,0.02 center
set label 2 'V_{out] [V]' at screen 0.01, 0.5 center rotate by 90
plot "3_OPAMP_integratore.csv" using 1:($6-f($1)):(sqrt(@new_y_err**2+$1**2*m_err**2+q_err**2+2*rho*m_err*q_err)) w yerror not
unset multiplot
