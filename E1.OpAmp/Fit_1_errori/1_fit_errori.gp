reset
set term post eps enhanced "Verdana, 14"
set encoding iso_8859_1
set out "1_fit_invertente.eps"
set fit error
set macros
text = "NaN w dots linecolor rgb 'white' title sprintf"
f(x) = q + m*x
f1(x) = c + d*x
f2(x) = e + h*x
set xlabel "V_1 [V]"
set ylabel "V_{out} [V]"
fit f(x) "1_OPAMP_invertente.txt" using 1:3 via m,q
plot "1_OPAMP_invertente.txt" using 1:3, f(x) t sprintf("%f + %f*x", q, m), NaN t sprintf("m = %f +- %f (%.1f\%)", m, m_err, abs(m_err/m*100)), NaN t sprintf("q = %f +- %f (%.1f\%)", q, q_err, abs(q_err/q*100))


#Residui tutto insieme
set out "1_residui_invertente.eps"
set xzeroaxis
plot "1_OPAMP_invertente.txt" using 1:($3-f($1)):(FIT_STDFIT) w yerror, "1_OPAMP_invertente.txt" using 1:($3-f($1)):($4) w labels offset char 1,1 notitle

unset zeroaxis
#FIT separati
fit [0:] f1(x)  "1_OPAMP_invertente.txt" using 1:3 via c,d
fit [:0] f2(x)  "1_OPAMP_invertente.txt" using 1:3 via e,h

set out "1_fit_pos_invertente.eps"
plot [0:] "1_OPAMP_invertente.txt" using 1:3, f1(x) t sprintf("%f + %f*x", c, d)

set out "1_fit_neg_invertente.eps"
plot [:0] "1_OPAMP_invertente.txt" using 1:3, f2(x) t sprintf("%f + %f*x", e, h)

#Residui separati
set xzeroaxis
set out "1_residui_pos.eps"
plot [0:] "1_OPAMP_invertente.txt" using 1:($3-f1($1)), NaN t sprintf("%f + %f*x", c, d)

set out "1_residui_neg.eps"
plot [:0] "1_OPAMP_invertente.txt" using 1:($3-f2($1)), NaN t sprintf("%f + %f*x", e, h)

unset zeroaxis
#SCALA FINO A CINQUE
g1(x) = q1 + m1*x
g2(x) = q2 + m2*x
set out "1_fit_centrale.eps"
fit g1(x) "1_OPAMP_invertente.txt" using ( $4 == 5 ? NaN : $1):3 via q1,m1
plot "1_OPAMP_invertente.txt" using ( $4 == 5 ? NaN : $1):3, g1(x), @text("m1 = %f {\261} %f", m1, m1_err)

#SCALA CINQUE
set out "1_fit_lati.eps"
fit g2(x) "1_OPAMP_invertente.txt" using ( $4 == 5 ? $1 : NaN):3 via q2,m2
plot "1_OPAMP_invertente.txt" using ( $4 == 5 ? $1 : NaN):3, g2(x), @text("m1 = %f {\261} %f", m2, m2_err)


#RESIDUI CENTRO E LATI
set out "1_residui_centrale.eps"
set xzeroaxis
plot "1_OPAMP_invertente.txt" using ( $4 == 5 ? NaN : $1):($3-g1($1))

set out "1_residui_lati.eps"
plot "1_OPAMP_invertente.txt" using ( $4 == 5 ? $1 : NaN):($3-g2($1))
