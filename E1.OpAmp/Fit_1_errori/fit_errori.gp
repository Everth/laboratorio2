###CONFIG GENERALE###
reset
set tics nomirror
set term post eps enhanced "Verdana, 14"
set encoding iso_8859_1
set out "1_fit_invertente.eps"
set fit error
set macros
text = "NaN w dots linecolor rgb 'white' title sprintf"
set xlabel ""
set ylabel ""

###COSTANTI E FUNZIONI##
A_exp=-8.26230170758816
A_exp_err=0.003884048838619
f(x) = q + m*x
f1(x) = c + d*x
f2(x) = e + h*x
g1(x) = q1 + m1*x
g2(x) = q2 + m2*x

###FIT CON TUTTI I DATI###

fit f(x) "1_OPAMP_invertente.csv" using 1:6:10 yerror via m,q
#plot "1_OPAMP_invertente.csv" using 1:6:10 w yerror, f(x) t sprintf("%f*x %+f", m, q), @text("m = %f \261 %f (%.1f\%)", m, m_err, abs(m_err/m*100)), @text("q = %f \261 %f (%.1f\%)", q, q_err, abs(q_err/q*100)), @text("{/Symbol l} = %f",abs(m-A_exp)/sqrt(m_err**2+A_exp_err**2))

set bmargin 3
set lmargin 6
set multiplot layout 2,1
m_old=m
fit f(x) "1_OPAMP_invertente.csv" using 1:6:(sqrt($10**2+m_old**2*$5**2)) yerror via m,q
#set table "1.dat"
#splot "1_OPAMP_invertente.csv" using 1:6:(sqrt($10**2+m_old**2*$5**2))
#unset table
plot "1_OPAMP_invertente.csv" using 1:6:(sqrt($10**2+m_old**2*$5**2)) w yerror not, f(x) t sprintf("%f*x + %f", m, q), @text("m = %f \261 %f (%.1f\%)", m, m_err, abs(m_err/m*100)), @text("q = %f \261 %f (%.1f\%)", q, q_err, abs(q_err/q*100)), @text("{/Symbol l} = %f",abs(m-A_exp)/sqrt(m_err**2+A_exp_err**2)), @text("{/Symbol c^2}_r=%f",FIT_STDFIT**2), "1_OPAMP_saturazione.csv" u 1:6:(sqrt($10**2+m_old**2*$5**2)) w yerror lt 1 not

stats "1_OPAMP_invertente.csv" using 1 prefix "X" noout
x_m = X_mean
var_x = X_ssd**2
new_y_err = "sqrt($10**2+m_old**2*$5**2)"
rho = - x_m / sqrt(var_x + x_m**2)

#RESIDUI
set label 1 'V_1 [V]' at screen 0.5,0.02 center
set label 2 'V_{out] [V]' at screen 0.01, 0.5 center rotate by 90
set xzeroaxis
#plot "1_OPAMP_invertente.csv" using 1:($6-f($1)):(sqrt(@new_y_err**2+$1**2*m_err**2+q_err**2+2*rho*m_err*q_err)) w yerror not, "1_OPAMP_saturazione.csv" u 1:(NaN) not
plot "1_OPAMP_invertente.csv" using 1:($6-f($1)):(@new_y_err) w yerror not, "1_OPAMP_saturazione.csv" u 1:(NaN) not
unset zeroaxis
unset label 1
unset label 2
set bmargin
set lmargin
unset multiplot

###CORREZIONE CHI QUADRO###

set out "1_fit_invertente__K.eps"
set bmargin 3
set lmargin 6
set multiplot layout 2,1
fit f(x) "1_OPAMP_invertente.csv" using 1:6:(FIT_STDFIT*sqrt($10**2+m_old**2*$5**2)) yerror via m,q
plot "1_OPAMP_invertente.csv" using 1:6:(FIT_STDFIT*sqrt($10**2+m_old**2*$5**2)) w yerror not, f(x) t sprintf("%f*x + %f", m, q), @text("m = %f \261 %f (%.1f\%)", m, m_err, abs(m_err/m*100)), @text("q = %f \261 %f (%.1f\%)", q, q_err, abs(q_err/q*100)), @text("{/Symbol l} = %f",abs(m-A_exp)/sqrt(m_err**2+A_exp_err**2)), @text("{/Symbol c^2}_r=%f",FIT_STDFIT**2)

#RESIDUI
set label 1 'V_1 [V]' at screen 0.5,0.02 center
set label 2 'V_{out] [V]' at screen 0.01, 0.5 center rotate by 90
set xzeroaxis
#plot "1_OPAMP_invertente.csv" using 1:($6-f($1)):(sqrt((FIT_STDFIT*@new_y_err)**2+$1**2*m_err**2+q_err**2+2*rho*m_err*q_err)) w yerror not #NON SERVE PIù
plot "1_OPAMP_invertente.csv" using 1:($6-f($1)):(FIT_STDFIT*@new_y_err) w yerror not
unset zeroaxis
unset label 1
unset label 2
set bmargin
set lmargin
unset multiplot


###SPLIT POSITIVI E NEGATIVI###
set ylabel "V_{out} [V]"
set xlabel "V_1 [V]"
fit [0:] f1(x) "1_OPAMP_invertente.csv" using 1:6:10 yerror via d,c
fit [:0] f2(x) "1_OPAMP_invertente.csv" using 1:6:10 yerror via h,e
d_old=d
h_old=h
set out "1_fit_pos.eps"
fit [0:] f1(x) "1_OPAMP_invertente.csv" using 1:6:(sqrt($10**2+d_old**2*$5**2)) yerror via c,d
plot [0:] "1_OPAMP_invertente.csv" using 1:6:(sqrt($10**2+d_old**2*$5**2)) w yerror, f1(x) t sprintf("%f*x %+f", d, c), @text("m = %f \261 %f (%.1f\%)", d, d_err, abs(d_err/d*100)), @text("q = %f \261 %f (%.1f\%)", c, c_err, abs(c_err/c*100)), @text("{/Symbol l} = %f",abs(d-A_exp)/sqrt(d_err**2+A_exp_err**2))
set out "1_fit_neg.eps"
fit [:0] f2(x) "1_OPAMP_invertente.csv" using 1:6:(sqrt($10**2+h_old**2*$5**2)) yerror via e,h
plot [:0] "1_OPAMP_invertente.csv" using 1:6:(sqrt($10**2+h_old**2*$5**2)) w yerror, f2(x) t sprintf("%f*x %+f", h, e), @text("m = %f \261 %f (%.1f\%)", h, h_err, abs(h_err/h*100)), @text("q = %f \261 %f (%.1f\%)", e, e_err, abs(e_err/e*100)), @text("{/Symbol l} = %f",abs(h-A_exp)/sqrt(h_err**2+A_exp_err**2))
#RESIDUI
set xzeroaxis
set out "1_residui_pos.eps"
stats "1_OPAMP_invertente.csv" using ($1 > 0 ? $1 : NaN) prefix "pos" noout
new_y1_err = "sqrt($10**2+d_old**2*$5**2)"
rho1 = - pos_mean / sqrt(pos_ssd**2 + pos_mean**2)
#plot "1_OPAMP_invertente.csv" using ($1 > 0 ? $1 : 0):($6-f1($1)):(sqrt(@new_y1_err**2+$1**2*d_err**2+c_err**2+2*rho1*d_err*c_err)) w yerror
plot "1_OPAMP_invertente.csv" using ($1 > 0 ? $1 : 0):($6-f1($1)):(@new_y1_err) w yerror
set out "1_residui_neg.eps"
stats "1_OPAMP_invertente.csv" using ($1 > 0 ? NaN : $1) prefix "neg" noout
new_y2_err = "sqrt($10**2+h_old**2*$5**2)"
rho2 = - neg_mean / sqrt(neg_ssd**2 + neg_mean**2) 
#plot "1_OPAMP_invertente.csv" using ($1 > 0 ? NaN : $1):($6-f2($1)):(sqrt(@new_y2_err**2+$1**2*h_err**2+e_err**2+2*rho2*h_err*e_err)) w yerror
plot "1_OPAMP_invertente.csv" using ($1 > 0 ? NaN : $1):($6-f2($1)):(@new_y2_err) w yerror
unset xzeroaxis

##INSIEME POSITIVI E NEGATIVI##

set out "1_fit_l_r.eps"
set bmargin 3
set lmargin 6
set multiplot layout 2,2
set key font ",8" reverse
unset xlabel 
unset ylabel
set label 1 'V_1 [V]' at screen 0.5,0.02 center
set label 2 'V_{out} [V]' at screen 0.01, 0.5 center rotate by 90
plot [:0] "1_OPAMP_invertente.csv" using 1:6:(sqrt($10**2+h_old**2*$5**2)) w yerror not, f2(x) t sprintf("%f*x %+f", h, e), @text("m = %f \261 %f (%.1f\%)", h, h_err, abs(h_err/h*100)), @text("q = %f \261 %f (%.1f\%)", e, e_err, abs(e_err/e*100)), @text("{/Symbol l} = %f",abs(h-A_exp)/sqrt(h_err**2+A_exp_err**2))
unset label 1
unset label 2
plot [0:] "1_OPAMP_invertente.csv" using 1:6:(sqrt($10**2+d_old**2*$5**2)) w yerror not, f1(x) t sprintf("%f*x %+f", d, c), @text("m = %f \261 %f (%.1f\%)", d, d_err, abs(d_err/d*100)), @text("q = %f \261 %f (%.1f\%)", c, c_err, abs(c_err/c*100)), @text("{/Symbol l} = %f",abs(d-A_exp)/sqrt(d_err**2+A_exp_err**2))
set xzeroaxis
#plot "1_OPAMP_invertente.csv" using ($1 > 0 ? NaN : $1):($6-f2($1)):(sqrt(@new_y2_err**2+$1**2*h_err**2+e_err**2+2*rho2*h_err*e_err)) w yerror t "Residui"
plot "1_OPAMP_invertente.csv" using ($1 > 0 ? NaN : $1):($6-f2($1)):(@new_y2_err) w yerror t "Residui"
#plot "1_OPAMP_invertente.csv" using ($1 > 0 ? $1 : NaN):($6-f1($1)):(sqrt(@new_y1_err**2+$1**2*d_err**2+c_err**2+2*rho1*d_err*c_err)) w yerror t "Residui"
plot "1_OPAMP_invertente.csv" using ($1 > 0 ? $1 : NaN):($6-f1($1)):(@new_y1_err) w yerror t "Residui"
unset zeroaxis
unset multiplot

###SCALE DIVERSE###

##SCALA2 FINO A CINQUE
set out "1_fit_centrale.eps"
fit g1(x) "1_OPAMP_invertente.csv" using ( $7 == 5 ? NaN : $1):6:10 yerror via q1,m1
m1_old = m1
fit g1(x) "1_OPAMP_invertente.csv" using ( $7 == 5 ? NaN : $1):6:(sqrt($10**2+m1_old**2*$5**2)) via q1,m1
plot "1_OPAMP_invertente.csv" using ( $7 == 5 ? NaN : $1):6:(sqrt($10**2+m1_old**2*$5**2)) w yerror not, g1(x) t sprintf("%f*x %+f", m1, q1) , @text("m1 = %f {\261} %f", m1, m1_err), @text("{/Symbol l} = %f",abs(m1-A_exp)/sqrt(m1_err**2+A_exp_err**2))
CHI_mid = FIT_WSSR
NDF_mid = FIT_NDF

##SCALA2 CINQUE
set out "1_fit_lati.eps"
fit g2(x) "1_OPAMP_invertente.csv" using ( $7 == 5 ? $1 : NaN):6:10 via q2,m2
m2_old = m2
fit g2(x) "1_OPAMP_invertente.csv" using ( $7 == 5 ? $1 : NaN):6:(sqrt($10**2+m2_old**2*$5**2)) via q2,m2
plot "1_OPAMP_invertente.csv" using ( $7 == 5 ? $1 : NaN):6:(sqrt($10**2+m1_old**2*$5**2)) w yerror not, g2(x) t sprintf("%f*x %+f", m2, q2) , @text("m2 = %f {\261} %f", m2, m2_err), @text("{/Symbol l} = %f",abs(m2-A_exp)/sqrt(m2_err**2+A_exp_err**2))
CHI_side = FIT_WSSR
NDF_side = FIT_NDF

#RESIDUI
set xzeroaxis
set out "1_residui_centrale.eps"
stats "1_OPAMP_invertente.csv" using ( $7 == 5 ? NaN : $1) prefix "center" noout
new_y3_err = "sqrt($10**2+m1_old**2*$5**2)"
rho3 = - center_mean / sqrt(center_ssd**2 + center_mean**2)
#plot "1_OPAMP_invertente.csv" using ( $7 == 5 ? NaN : $1):($6-g1($1)):(sqrt(@new_y3_err**2+$1**2*m1_err**2+q1_err**2+2*rho3*m1_err*q1_err)) w yerror
plot "1_OPAMP_invertente.csv" using ( $7 == 5 ? NaN : $1):($6-g1($1)):(@new_y3_err) w yerror
set out "1_residui_lati.eps"
stats "1_OPAMP_invertente.csv" using ( $7 == 5 ? $1 : NaN) prefix "sides" noout
new_y4_err = "sqrt($10**2+m2_old**2*$5**2)"
rho4 = - sides_mean / sqrt(sides_ssd**2 + sides_mean**2)
#plot "1_OPAMP_invertente.csv" using ( $7 == 5 ? $1 : NaN):($6-g2($1)):(sqrt(@new_y4_err**2+$1**2*m2_err**2+q2_err**2+2*rho4*m2_err*q2_err)) w yerror
plot "1_OPAMP_invertente.csv" using ( $7 == 5 ? $1 : NaN):($6-g2($1)):(@new_y4_err) w yerror


###INSIEME CENTRO E LATI###

set out "1_fit_c_s.eps"
set bmargin 3
set lmargin 6

set multiplot layout 2,2
set key font ",8" reverse
unset xlabel 
unset ylabel
unset xzeroaxis
set label 1 'V_1 [V]' at screen 0.5,0.02 center
set label 2 'V_{out] [V]' at screen 0.01, 0.5 center rotate by 90
plot "1_OPAMP_invertente.csv" using ( $7 == 5 ? NaN : $1):6:(sqrt($10**2+m1_old**2*$5**2)) w yerror not, f1(x) t sprintf("%f*x %+f", m1, q1), @text("m = %f \261 %f (%.1f\%)", m1, m1_err, abs(m1_err/m1*100)), @text("q = %f \261 %f (%.1f\%)", q1, q1_err, abs(q1_err/q1*100)), @text("{/Symbol l} = %f",abs(m1-A_exp)/sqrt(m1_err**2+A_exp_err**2)), @text("{/Symbol c^2}_r=%f",CHI_mid/NDF_mid)
unset label 1
unset label 2
plot "1_OPAMP_invertente.csv" using ( $7 == 5 ? $1 : NaN):6:(sqrt($10**2+m2_old**2*$5**2)) w yerror not, f2(x) t sprintf("%f*x %+f", m2, q2), @text("m = %f \261 %f (%.1f\%)", m2, m2_err, abs(m2_err/m2*100)), @text("q = %f \261 %f (%.1f\%)", q2, q2_err, abs(q2_err/q2*100)), @text("{/Symbol l} = %f",abs(m2-A_exp)/sqrt(m2_err**2+A_exp_err**2)), @text("{/Symbol c^2}_r=%f",CHI_side/NDF_side)
set xzeroaxis
#plot "1_OPAMP_invertente.csv" using ( $7 == 5 ? NaN : $1):($6-g1($1)):(sqrt(@new_y3_err**2+$1**2*m1_err**2+q1_err**2+2*rho3*m1_err*q1_err)) w yerror t "Residui"
#plot "1_OPAMP_invertente.csv" using ( $7 == 5 ? $1 : NaN):($6-g2($1)):(sqrt(@new_y4_err**2+$1**2*m2_err**2+q2_err**2+2*rho4*m2_err*q2_err)) w yerror t "Residui"
plot "1_OPAMP_invertente.csv" using ( $7 == 5 ? NaN : $1):($6-g1($1)):(@new_y3_err) w yerror t "Residui"
plot "1_OPAMP_invertente.csv" using ( $7 == 5 ? $1 : NaN):($6-g2($1)):(@new_y4_err) w yerror t "Residui"
unset zeroaxis
unset multiplot

###INSIEME CENTRO E LATI CON WSSR CORRETTO###
fit g1(x) "1_OPAMP_invertente.csv" using ( $7 == 5 ? NaN : $1):6:(sqrt(CHI_mid/NDF_mid)*sqrt($10**2+m1_old**2*$5**2)) via q1,m1
NEW_CHI_mid = FIT_STDFIT**2
fit g2(x) "1_OPAMP_invertente.csv" using ( $7 == 5 ? $1 : NaN):6:(sqrt(CHI_side/NDF_side)*sqrt($10**2+m2_old**2*$5**2)) via q2,m2
NEW_CHI_side = FIT_STDFIT**2

set out "1_fit_c_s__K.eps"
set bmargin 3
set lmargin 6

set multiplot layout 2,2
set key font ",8" reverse
unset xlabel 
unset ylabel
unset xzeroaxis
set label 1 'V_1 [V]' at screen 0.5,0.02 center
set label 2 'V_{out] [V]' at screen 0.01, 0.5 center rotate by 90
plot "1_OPAMP_invertente.csv" using ( $7 == 5 ? NaN : $1):6:(sqrt(CHI_mid/NDF_mid)*sqrt($10**2+m1_old**2*$5**2)) w yerror not, f1(x) t sprintf("%f*x %+f", m1, q1), @text("m = %f \261 %f (%.1f\%)", m1, m1_err, abs(m1_err/m1*100)), @text("q = %f \261 %f (%.1f\%)", q1, q1_err, abs(q1_err/q1*100)), @text("{/Symbol l} = %f",abs(m1-A_exp)/sqrt(m1_err**2+A_exp_err**2)), @text("{/Symbol c^2}_r=%f",NEW_CHI_mid)
unset label 1
unset label 2
plot "1_OPAMP_invertente.csv" using ( $7 == 5 ? $1 : NaN):6:(sqrt(CHI_side/NDF_side)*sqrt($10**2+m2_old**2*$5**2)) w yerror not, f2(x) t sprintf("%f*x %+f", m2, q2), @text("m = %f \261 %f (%.1f\%)", m2, m2_err, abs(m2_err/m2*100)), @text("q = %f \261 %f (%.1f\%)", q2, q2_err, abs(q2_err/q2*100)), @text("{/Symbol l} = %f",abs(m2-A_exp)/sqrt(m2_err**2+A_exp_err**2)), @text("{/Symbol c^2}_r=%f",NEW_CHI_side)
set xzeroaxis
#plot "1_OPAMP_invertente.csv" using ( $7 == 5 ? NaN : $1):($6-g1($1)):(sqrt(CHI_mid/NDF_mid)*sqrt(@new_y3_err**2+$1**2*m1_err**2+q1_err**2+2*rho3*m1_err*q1_err)) w yerror t "Residui"
#plot "1_OPAMP_invertente.csv" using ( $7 == 5 ? $1 : NaN):($6-g2($1)):(sqrt(CHI_side/NDF_side)*sqrt(@new_y4_err**2+$1**2*m2_err**2+q2_err**2+2*rho4*m2_err*q2_err)) w yerror t "Residui"
plot "1_OPAMP_invertente.csv" using ( $7 == 5 ? NaN : $1):($6-g1($1)):(sqrt(CHI_mid/NDF_mid)*@new_y3_err) w yerror t "Residui"
plot "1_OPAMP_invertente.csv" using ( $7 == 5 ? $1 : NaN):($6-g2($1)):(sqrt(CHI_side/NDF_side)*@new_y4_err) w yerror t "Residui"
unset zeroaxis
unset multiplot
