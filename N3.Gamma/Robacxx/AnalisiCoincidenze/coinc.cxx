#include <Riostream.h>
#include <stdlib.h>
#include <TROOT.h>
#include <TSystem.h>
#include "TGeoManager.h"
#include "TNtuple.h"
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include "TView.h"
#include "TParticle.h"
#include "TTreeReader.h"
#include "TTreeReaderArray.h"
#include "TLine.h"
#include "TBenchmark.h"
#include <algorithm>
#include <vector>
#include <iterator>
#include "TStyle.h"


//c&r g++ -Wall -I. -I `root-config --cflags` -o coinc.out coinc.cxx `root-config --libs` && ./coinc.out Co-60_coinc_sgn 0 1 peaks_sigma

struct acqParams_t {
	UInt_t		acqTimestamp;
	UShort_t	dppVersion;
	UShort_t	nsPerSample;
	UShort_t	nsPerTimetag;
	UShort_t	numChannels;
	UShort_t	numSamples[32];
};

struct psdParams_t {
	UInt_t		channel;
	UInt_t		threshold;
	UInt_t		pretrigger;
	UInt_t		pregate;
	UInt_t		shortgate;
	UInt_t		longgate;
	UInt_t		numsamples;
};

struct acqEventPSD_t {
	ULong64_t	timetag;
	UInt_t		baseline;
	UShort_t	qshort;
	UShort_t	qlong;
	UShort_t	pur;
	UShort_t	samples[4096];
};

void ScaleXaxis(TH1D* histoch, TF1* conversionfunction, std::string convertedlabel){ //La funzione deve essere lineare
	TAxis *newaxis = histoch->GetXaxis(); //ty Pepe_Le_Pew
	if (newaxis->GetXbins()->GetSize()) { 
		//Axis with variable bins
		TArrayD X(*(newaxis->GetXbins()));
		for(Int_t i = 0; i < X.GetSize(); i++) X[i] = conversionfunction->Eval(X[i]);
		newaxis->Set((X.GetSize() - 1), X.GetArray()); //new Xbins
	} else {
		//Axis with fixed bins
		newaxis->Set( newaxis->GetNbins(),
		conversionfunction->Eval(newaxis->GetXmin()), //new Xmin
		conversionfunction->Eval(newaxis->GetXmax()) ); //new Xmax
	}
	histoch->ResetStats();
	newaxis->SetTitle(convertedlabel.c_str());
}

struct center_and_sigma : std::pair<double,double> {
    using std::pair<double,double>::pair;
};

std::istream& operator>>(std::istream& stream, center_and_sigma& in) {
    return stream >> in.first >> in.second;
}

bool condition(const double& var, const double& center, const double& sigma, double n_sigmas = 2){
	return ( (var > ( center - n_sigmas*sigma ) ) && (var < ( center + n_sigmas*sigma ) ) );
}

bool condition(const double& var,const center_and_sigma& center, double n_sigmas = 2){
	return condition(var, center.first, center.second, n_sigmas);
}



static constexpr int coinc_clock_limit = 200;
static constexpr int view_A = 11500, view_B = 7000;
static constexpr double qlongbinsize = 10;


// g++ -Wall -I. -I `root-config --cflags` -o coinc.out coinc.cxx `root-config --libs` && ./coinc.out Co-60_coinc_sgn 0 1 peaks_sigma

int main(int argc, char* argv[]){
	char filename[32];
	strncpy(filename,argv[1],32);
	int chA = std::atoi(argv[2]);
	int chB = std::atoi(argv[3]);
	
	//coincidenza temporale ed energetica
	std::string input_peaks(argv[4]);
	std::vector<center_and_sigma> peaks_sigma;
	std::ifstream fit_results(input_peaks);
	std::istream_iterator<center_and_sigma> beginf(fit_results),endf;
	std::copy(beginf, endf, std::back_inserter(peaks_sigma));
	TH1D* htime_lowA_highB = new TH1D("htime_lowA_highB","Time difference in ns; time [ns]; counts [a.u.]",::coinc_clock_limit,-::coinc_clock_limit/10,::coinc_clock_limit/10);
	TH1D* htime_lowB_highA = new TH1D("htime_lowB_highA","Time difference in ns; time [ns]; counts [a.u.]",::coinc_clock_limit,-::coinc_clock_limit/10,::coinc_clock_limit/10);
		
	
	TFile* outfile = new TFile(Form("anal_%s%s",filename,".root"),"RECREATE");
	TNtuple* nt = new TNtuple("nt","coincidences","eA:eB:qlongA:qlongB:time");
  TH1D* htime = new TH1D("htime","Time difference in ADC clocks; time [clocks]; counts [a.u.]",0.5*::coinc_clock_limit,-::coinc_clock_limit,::coinc_clock_limit); // mi stai dicendo che l'unica differenza è il binning?
  TH1D* htimeHD = new TH1D("htimeHD","Time difference in ns; time [ns]; counts [a.u.]",::coinc_clock_limit*5,-::coinc_clock_limit/10,::coinc_clock_limit/10);
  TH1D* hqlongA = new TH1D("hqlongA","Energy spectrum from detector A;Energy [u.a.];Counts",::view_A/::qlongbinsize,0,::view_A);
  TH1D* hqlongB = new TH1D("hqlongB","Energy spectrum from detector B;Energy [u.a.];Counts",::view_B/::qlongbinsize,0,::view_B);
	TMultiGraph* signalsA = new TMultiGraph();
	TMultiGraph* signalsB = new TMultiGraph();
	
	TBenchmark executiontime;
	executiontime.Start("analysis");
	
	TFile* infile = new TFile(filename,"READ"); // Form("%s.root",filename)
  TTree* inctree= (TTree*)infile->Get("acq_tree_0");
  TCanvas* multicanvas = new TCanvas("cfr_sgn_coinc", "Confronto segnali in coincidenza"); 
  
  acqEventPSD_t inc_data1, inc_data2; //salvare oggetti e non puntatori deve assicurare che non vengano  eliminati prima del TTree o della chiamata TTree::ResetBranchAddress()
  // aligned channels
  TBranch *incbranch_1, *incbranch_2;
  incbranch_1 = inctree->GetBranch(Form("acq_ch%i",chA));
  incbranch_1->SetAddress(&inc_data1);
  incbranch_2 = inctree->GetBranch(Form("acq_ch%i",chB));
  incbranch_2->SetAddress(&inc_data2);
  
  psdParams_t params;
  TBranch *config = inctree->GetBranch("psd_params");
  config->SetAddress(&params);
  config->GetEntry(0);
  int samplesA = static_cast<int>(params.numsamples);
  config->GetEntry(1);
  int samplesB = static_cast<int>(params.numsamples);
  
  int eA = 0, eB = 0; //overflow?
  int skipA = 0, skipB = 0, nGood = 0;
  int coinc_lowA = 0, coinc_lowB = 0, nocoincen = 0;
  Long64_t timediff = 0;
  
  int tot_eventsA = incbranch_1->GetEntries();
  int tot_eventsB = incbranch_2->GetEntries();
  
  
	while (eA<tot_eventsA && eB<tot_eventsB) {
	
	incbranch_1->GetEntry(eA);
	incbranch_2->GetEntry(eB);
	
	// differenza tempi nella scheda. Vale 0 con il FW STD...
	timediff = inc_data1.timetag - inc_data2.timetag;
	
	if (TMath::Abs(timediff)>::coinc_clock_limit) { // coincidenza a 400x4 ns
		// alignment
		if (timediff>0) {
			eB++; skipB++; 
		} else {
			eA++; skipA++; 
		}
	} 
	else {
		// c'e' la coincidenza...
		double qlongA = inc_data1.qlong;
		double qlongB = inc_data2.qlong;
		hqlongA->Fill(qlongA);
		hqlongB->Fill(qlongB);

		// imposta la baseline variabile al 50% energia
		
		UShort_t baselineA = inc_data1.baseline;
		UShort_t baselineB = inc_data2.baseline;
		UShort_t thresholdA = (baselineA + *(std::min_element(inc_data1.samples, inc_data1.samples + samplesA)))/2;
		UShort_t thresholdB = (baselineB + *(std::min_element(inc_data2.samples, inc_data2.samples + samplesB)))/2;
		
		if( (nGood < 5) && ( (condition(qlongA, peaks_sigma[0]) && condition(qlongB, peaks_sigma[3])) || (condition(qlongA, peaks_sigma[1]) && condition(qlongB, peaks_sigma[2])) ) ){
			TGraph* signalA = new TGraph(samplesA);
			signalA->SetName(Form("sgn_a_%i",eA));
			signalA->SetTitle(Form("sgn_a_%i;Clock counts [4ns];V [u.a.]",eA));
			signalA->SetLineColor(eA);
			for (int jA=0; jA<samplesA; ++jA) {
					signalA->SetPoint(jA,jA,inc_data1.samples[jA]);
      }
			signalsA->Add(signalA,"l");
			
			TGraph* signalB = new TGraph(samplesB);
			signalB->SetName(Form("sgn_a_%i",eB));
			signalB->SetTitle(Form("sgn_a_%i;Clock counts [4ns];V [u.a.]",eB));
			signalB->SetLineColor(eA); //metto lo stesso colore alle rette che si riferiscono all'evento in coincidenza
			for (int jB=0; jB<samplesB; ++jB) {
					signalB->SetPoint(jB,jB,inc_data2.samples[jB]);
      }
			signalsB->Add(signalB,"l");
		}
		// x_a = (timeAit inc_data1.samples -1), x_b = x_a + 1, y_b = *timeAit, 
		auto timeAit = std::find_if(inc_data1.samples, inc_data1.samples + samplesA, [&](const UShort_t& s) {return s < thresholdA;} );
		double timeA = (timeAit - inc_data1.samples) - static_cast<double>(*timeAit - thresholdA)/(*timeAit - *(timeAit-1));
		auto timeBit = std::find_if(inc_data2.samples, inc_data2.samples + samplesB, [&](const UShort_t& s) { return s < thresholdB; });
		double timeB = (timeBit - inc_data2.samples) - static_cast<double>(*timeBit - thresholdB)/(*timeBit - *(timeBit-1));
		
		float tempo_ns = timediff*4.0 + (timeA - timeB)*4.0; // delta_t in ns
		htime->Fill(tempo_ns);
		htimeHD->Fill(tempo_ns);
		//Low A, High B
		if ( condition(qlongA, peaks_sigma[0]) && condition(qlongB, peaks_sigma[3]) ){
			htime_lowA_highB->Fill(tempo_ns);
			++coinc_lowA;
		} else if ( condition(qlongA, peaks_sigma[1]) && condition(qlongB, peaks_sigma[2]) ) {
			htime_lowB_highA->Fill(tempo_ns);
			++coinc_lowB;
		} else {
			++nocoincen;
		}
		nt->Fill(eA, eB, inc_data1.qlong, inc_data2.qlong, tempo_ns);
		eA++; eB++; nGood++;
		}
	}
	executiontime.Show("analysis");
	// alignment report
	std::cout << "Channels " << chA << " and " << chB << " aligned with " << nGood << " coincident events" << std::endl;
	std::cout << "Unmatched entries: " << skipA*100./incbranch_1->GetEntries() << "% (chA), ";
	std::cout << skipB*100./incbranch_2->GetEntries() << "% (chB)" << std::endl;
	std::cout << "Coincidences 2 sigmas lowA highB: " << coinc_lowA << "\t Coincidences 2 sigmas lowB highA: " << coinc_lowB << std::endl;
	std::cout << "Time coincidences of events distant more than 2sigmas from opposite peaks: " << nocoincen << std::endl;

	infile->Close();

	outfile->cd();
	
	multicanvas->Divide(2);
	multicanvas->cd(1);
	signalsA->Draw("AP");
	signalsA->GetXaxis()->SetTitle("time [clocks]");
	multicanvas->cd(2);
	signalsB->Draw("AP");
	signalsB->GetXaxis()->SetTitle("time [clocks]");
	multicanvas->Modified();
	signalsA->Write("signalsA");
	signalsB->Write("signalsB");
	multicanvas->Print("coinc_waveforms.eps","eps");
	multicanvas->Write();
	
	TF1* pippo = new TF1("scalebins","x/4.0");
	ScaleXaxis(htime,pippo,"time [ns]");
	//fit dei due picchi
	TF1* fitsx=new TF1("fitsx","gaus",-10,8);
	TF1* fitdx=new TF1("fitdx","gaus",-10,8);
	gStyle->SetOptFit(1111);
	TCanvas* timediff_peaks = new TCanvas("timediff_peaks","timediff_peaks");
	timediff_peaks->Divide(2);
	timediff_peaks->cd(1);
	htime_lowA_highB->Fit("fitsx","S");
	htime_lowA_highB->Draw();
	
	timediff_peaks->cd(2);
	htime_lowB_highA->Fit("fitdx","	S");
	htime_lowB_highA->Draw();
	timediff_peaks->Modified();
	timediff_peaks->Print("time_resolution_peaks.eps","eps");
	timediff_peaks->Write();
	htime_lowA_highB->Write();
	htime_lowB_highA->Write();
	fitsx->Write();
	fitdx->Write();	
	//fit di tutto
	
	TCanvas* ctimediff = new TCanvas("ctimediff","Differenza di tempo con tutte le coincidenze");
	TF1* fitresol=new TF1("fitresol","gaus",-12,10);
	ctimediff->cd();
	htimeHD->Fit("fitresol","S");
	htimeHD->Draw();
	ctimediff->Print("time_resolution.eps","eps");
	ctimediff->Write();
	htimeHD->Write();
	fitresol->Write();	ctimediff->Write();
	
	TCanvas* chqA = new TCanvas("canvas_hqA","canvas_hqA");
	hqlongA->Draw();
	chqA->Print("coinc_hq0.eps","eps");
	TCanvas* chqB = new TCanvas("canvas_hqB","canvas_hqB");
	hqlongB->Draw();
	chqB->Print("coinc_hq1.eps","eps");	
	
	
	outfile->Write();
	outfile->Close();
}

