#include <Riostream.h>
#include <stdlib.h>
#include <TROOT.h>
#include <TSystem.h>
#include "TFile.h"
#include "TTree.h"
#include "TNtuple.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TF1.h"
#include "TFitResult.h"
#include "TRint.h"
#include "TString.h"
#include "AnalysisInfo.h"
#include "TGaxis.h"
#include "TStyle.h"
#include <iostream>
#include <fstream>
#include <string>
#include <memory>
#include <iterator>
#include <algorithm>
#include <tuple>
#include <unordered_map>
#include <map>
#include <utility>
#include <iomanip>

//compile g++ -Wall -I. -I `root-config --cflags` -o Fit_coinc.out Fit_coinc.cxx AnalysisInfo.cc `root-config --libs` && ./Fit_coinc.out fit ranges_coinc conversion calibrationparams_unw 

static constexpr int min_events = 10;
static constexpr double n_bins = 25;

struct FitInfoChannels{
	std::string isotope_name;
	int viewfrom;
	int viewto;
	int fitfrom;
	int fitto;
	std::string branch;
	void print(std::ostream& out=std::cerr){ out << isotope_name << "\t" << viewfrom << "\t" << viewto  << "\t" << fitfrom << "\t" << fitto  << "\t" << branch << std::endl;}
	friend std::istream& operator>>(std::istream &is, FitInfoChannels &fitinfo);
};

std::istream& operator>>(std::istream &is, FitInfoChannels &fitinfo){
	is >> fitinfo.isotope_name >> fitinfo.viewfrom >> fitinfo.viewto >> fitinfo.fitfrom >> fitinfo.fitto >> fitinfo.branch;
	return is; //si può compattare in return is >> ecc >> ecc
}

void ScaleXaxis(TH1D* histoch, TF1* conversionfunction, std::string convertedlabel){ //La funzione deve essere lineare
	TAxis *newaxis = histoch->GetXaxis(); //ty Pepe_Le_Pew
	if (newaxis->GetXbins()->GetSize()) { 
		//Axis with variable bins
		TArrayD X(*(newaxis->GetXbins()));
		for(Int_t i = 0; i < X.GetSize(); i++) X[i] = conversionfunction->Eval(X[i]);
		newaxis->Set((X.GetSize() - 1), X.GetArray()); //new Xbins
	} else {
		//Axis with fixed bins
		newaxis->Set( newaxis->GetNbins(),
		conversionfunction->Eval(newaxis->GetXmin()), //new Xmin
		conversionfunction->Eval(newaxis->GetXmax()) ); //new Xmax
	}
	histoch->ResetStats();
	newaxis->SetTitle(convertedlabel.c_str());
}

struct TableFormat {
    std::ostream& outs;
    int width;
    char fill;
    TableFormat(std::ostream& out=std::cerr,int width=15, char fill=' '): outs(out), width(width), fill(fill) {}
    template<typename T>
    TableFormat& operator<<(const T& data) {
        outs << std::setw(width) << data << std::setw(width) << std::setfill(fill);
        return *this;
    }
    TableFormat& operator<<(std::ostream&(*out)(std::ostream&)) {
        outs << out;
        return *this;
    }
};

// g++ -Wall -I. -I `root-config --cflags` -o Fit_coinc.out Fit_coinc.cxx AnalysisInfo.cc `root-config --libs` && ./Fit_coinc.out inputfile anal_Co-60_coinc_sgn.root fit ranges_coinc fitout fit_results_coinc

int main(int argc, char** argv){
	
	AnalysisInfo* info = new AnalysisInfo( argc, argv );
	//da dare fit ranges conversion conversionparams. Servono limiti sull'energia e/o sul binning
	if (!info->contains("inputfile")) {std::cerr << "Missing file to analyze! Aborting." << std::endl; return -1;}
	std::string inputfile = info->value("inputfile");// "anal_Co-60_coinc_sgn.root" o "Co-60_coinc.root" 
	std::string name = "Co-60_coinc_sgn";
	
	
	int fakeargc=1; char* fakeargv[fakeargc];
	TRint *theApp = new TRint("App", &fakeargc, fakeargv,0,0,1);
	
	TF1 *conversionfunction = nullptr;
	if(info->contains("conversion")){
		conversionfunction = new TF1("conversion", "[0]+[1]*x");
		std::ifstream userparamsfile(info->value("conversion")); //la funzione la so, mi servono solo i due parametri A e B (*x)
		std::istream_iterator<double> iit(userparamsfile),eos;
		double userparams[10]; // L'hanno definita con 10 parametri a prescindere quindi non posso sbagliare
		std::copy(iit,eos, userparams);
		conversionfunction->SetParameters(userparams);
	}
	
	
	
	if (info->contains("fit")){
		std::ifstream fitranges;
		fitranges.open(info->value("fit") ,std::ios::in); 
		if(!fitranges.is_open()){
			std::cerr << info->value("fit") << " not opened!" << std::endl;
			return -1;
		}
		
		std::vector<FitInfoChannels> ranges;
		std::istream_iterator<FitInfoChannels> start(fitranges), end;
		std::copy(start, end, std::back_inserter(ranges));
		for (auto i : ranges) i.print(); //overkill
		
		gStyle->SetOptStat(kTRUE);
		gStyle->SetOptFit(1111);
		
		std::vector<TFitResultPtr> results;
		
		TFile* infile = TFile::Open(inputfile.c_str(),"READ");
		TNtuple* datatuple = nullptr;
		infile->GetObject("nt",datatuple);
		if (!datatuple) {std::cerr << "Non hai accesso all'ntupla!" << std::endl; return -1;}
		std::for_each(ranges.begin(), ranges.end(), [&](const FitInfoChannels& limits){
			static std::unordered_map<std::string, unsigned> peak_dictionary;
			std::string peak_id = limits.isotope_name + "-" + limits.branch;
			++peak_dictionary[peak_id];
			std::string call_count(std::to_string(peak_dictionary[peak_id]));
			std::string condensed_name(peak_id + "_" + call_count);
			TString cname = Form("%s",condensed_name.c_str());
			TString hname = Form("histo_%s",cname.Data());
			std::string outfilename = Form("%s.root",cname.Data());
			std::string outimgname = Form("%s.eps",cname.Data());
			auto outfile = std::make_shared<TFile>( outfilename.c_str() ,"RECREATE"); //Output file
			TCanvas* fitcanvas = new TCanvas( Form("canvas_%s",cname.Data()));
			double binwidth = (limits.viewto-limits.viewfrom)/::n_bins;
			TH1D* fithisto = new TH1D( hname , hname, binwidth, limits.viewfrom, limits.viewto);
			datatuple->Draw(Form("%s >> %s",limits.branch.c_str(),hname.Data()) );
			outfile->cd();
			fithisto->SetLineColor(kBlue);
			fithisto->SetLineStyle(1);
			TF1 *func = new TF1(Form("func_%s",cname.Data()),"gaus", limits.fitfrom, limits.fitto); // fit sempre gaussiani
			gStyle->SetOptFit(1111);
			TFitResultPtr res = fithisto->Fit(func,"RS");
			fithisto->Draw(); //questa devo capire perché non me la faceva funzionare molto spesso. In ogni caso devo mettere la conversione da ste parti
			fitcanvas->Write();
			fithisto->Write();
			func->Write();
			res->Print("V");
			res->Write();
			fitcanvas->SaveAs(outimgname.c_str(),"eps");
			results.push_back(res);
			outfile->Close();
			delete fitcanvas;
		});
		
		
		if(info->contains("fitout")){ //è sempre uno fino a quando non correggo per avere conversioni diverse per i due canali
			std::ofstream fitresult(info->value("fitout"));
			//double interc =conversionfunction->GetParameter(0);
			//double pendenza=conversionfunction->GetParameter(1);
			//errore centroide convertito = pendenza*(TMath::Sqrt(cov(1,1)))
			TableFormat fitresulttable(fitresult,25,' ');
			fitresulttable << "Element name" << "Centroide [u.a]" << "Sigma [u.a]"  << "Errore centroide [u.a]" << "Semiampiezza [u.a.]" << std::endl;
			for (unsigned i = 0; i<ranges.size(); ++i){
				TMatrixDSym cov = results[i]->GetCovarianceMatrix();
				fitresulttable << ranges[i].isotope_name << results[i]->GetParams()[1] << results[i]->GetParams()[2] << TMath::Sqrt(cov(1,1)) << sqrt(2*log(2))*results[i]->GetParams()[2] << std::endl;
			}
		}
		
		//Plottare qlongA (picco 1.33Mev qlongB) e qlongB (picco 1.33Mev qlongA)
		
		double bin_num = 20;
		int start_A = 2000;
		int start_B = 1500;
		int view_A = 12000;
		int view_B = 7000;
		//LOW_A = 0, HIGH_A = 1, LOW_B = 2, HIGH_B = 3
		double high_A = results[1]->GetParams()[1];
		double high_B = results[3]->GetParams()[1];
		double sigma_high_A = results[1]->GetParams()[2];
		double sigma_high_B = results[3]->GetParams()[2];
		
		TFile* energy_coinc = new TFile("energy_coinc.root", "RECREATE");
		energy_coinc->cd();
		TCanvas* confronto = new TCanvas("confronto","Confronto spettri in coincidenza energetica");
		confronto->Divide(2);
		confronto->cd(1);
		TH1D* A_highB = new TH1D("qlongA_coinc",Form("qlongB > %f && qlongB < %f", high_B - 2*sigma_high_B, high_B + 2*sigma_high_B),view_A/bin_num,0,view_A);
		datatuple->Draw("qlongA>>qlongA_coinc",Form("qlongB > %f && qlongB < %f", high_B - 2*sigma_high_B, high_B + 2*sigma_high_B));
		confronto->cd(2);
		TH1D* B_highA = new TH1D("qlongB_coinc",Form("qlongA > %f && qlongA < %f", high_A - 2*sigma_high_A, high_A + 2*sigma_high_A),view_B/bin_num,0,view_B);
		datatuple->Draw("qlongB>>qlongB_coinc",Form("qlongA > %f && qlongA < %f", high_A - 2*sigma_high_A, high_A + 2*sigma_high_A));
		A_highB->GetXaxis()->SetTitle("Energy A [u.a.]");
		B_highA->GetXaxis()->SetTitle("Energy B [u.a.]");
		confronto->Modified();
		confronto->Write();
		A_highB->Write();
		B_highA->Write();
		confronto->Print("energy_coinc.eps","eps");
		
		
		TCanvas* energy2D = new TCanvas("energy2D", "Energia B vs Energia A [u.a.]" );
		datatuple->Draw("qlongB:qlongA",Form("qlongA > %i && qlongA < %i && qlongB > %i && qlongB < %i",start_A,view_A,start_B,view_B), "goff");
		//TH2D* henergy2D = new TH2D("henergy2D", "Energia B vs Energia A [u.a.]", (view_A-start_A), start_A, view_A, (view_B-start_B), start_B, view_B);
		TGraph *genergy2D = new TGraph(datatuple->GetSelectedRows(), datatuple->GetV2(), datatuple->GetV1());
		genergy2D->Draw("AP");
		genergy2D->SetTitle("");
		genergy2D->GetXaxis()->SetRangeUser(start_A,view_A);
		genergy2D->GetXaxis()->SetTitle("Energy A [u.a.]");
		genergy2D->GetYaxis()->SetRangeUser(start_B,view_B);
		genergy2D->GetYaxis()->SetTitle("Energy B [u.a.]");
		energy2D->Modified();
		energy2D->Write();
		genergy2D->Write();
		energy2D->Print("energy2D.eps","eps");
		
	}
	theApp->Run();
	return 0;
}


