#include <Riostream.h>
#include <stdlib.h>
#include <TROOT.h>
#include <TSystem.h>
#include "TFile.h"
#include "TTree.h"
#include "TNtuple.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TH1.h"
#include "TRint.h"
#include <iostream>
#include <fstream>
#include <string>
#include <memory>
#include <utility>

struct acqParams_t {
	UInt_t		acqTimestamp;
	UShort_t	dppVersion;
	UShort_t	nsPerSample;
	UShort_t	nsPerTimetag;
	UShort_t	numChannels;
	UShort_t	numSamples[32];
};

struct psdParams_t {
	UInt_t		channel;
	UInt_t		threshold;
	UInt_t		pretrigger;
	UInt_t		pregate;
	UInt_t		shortgate;
	UInt_t		longgate;
	UInt_t		numsamples;
};

struct acqEventPSD_t {
	ULong64_t	timetag;
	UInt_t		baseline;
	UShort_t	qshort;
	UShort_t	qlong;
	UShort_t	pur;
	UShort_t	samples[4096];
};

int main(int argc, char**argv){
	TRint *theApp = new TRint("App", &argc, argv,0,0,1);
	auto kcl_new = std::make_shared<TFile>("KCl_new.root","READ");
	TNtuple* ntcopy = (TNtuple*)(kcl_new->Get("nt"))->Clone();
	gDirectory->ls();
	psdParams_t* ch = nullptr;
	ntcopy->SetBranchAddress("ch",&ch);
	TFile* kcl_fixed = new TFile("KCl_2.root","RECREATE");
	kcl_fixed->cd();
	TTree* newtuple = ntcopy->CloneTree(0);
	//non posso cambiare il nome...
	int goodentries = (int)ntcopy->GetEntries("time<8000");
	std::cout << (int)ntcopy->GetEntries() << '\t' << (int)ntcopy->GetEntries("time<8000") << std::endl;
	for(int i = 0; i < goodentries; ++i){
		ntcopy->GetEntry(i);
		newtuple->Fill();
	}
	kcl_new->Close();
	//c'è solo un oggetto in memoria
	newtuple->Write("nt");
	//ntupla salvata su file
	TH1F* channel0= new TH1F("hq0","hq0",15000/25,0,15000);
	TH1F* channel1= new TH1F("hq1","hq1",15000/25,0,15000);
	//channel0->SetCanExtend(TH1::kAllAxes);
	newtuple->Draw("qlong>>hq0", "ch==0 && qlong < 15000");
	newtuple->Draw("qlong>>hq1", "ch==1 && qlong < 15000");
	channel0->Draw();
	channel0->Write();
	channel1->Write();
	theApp->Run(kFALSE);
	kcl_fixed->Close();
	return 0;
}
