#include <Riostream.h>
#include <stdlib.h>
#include <TROOT.h>
#include <TSystem.h>
#include "TFile.h"
#include "TTree.h"
#include "TNtuple.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TH1D.h"
#include "TF1.h"
#include "TFitResult.h"
#include "TRint.h"
#include "TLegend.h"
#include "TStyle.h"
#include "AnalysisInfo.h"
#include "TGaxis.h"
#include "Getline.h"
#include <iostream>
#include <fstream>
#include <string>
#include <memory>
#include <iterator>
#include <algorithm>
#include <tuple>
#include <map>
#include <unordered_map>
#include <utility>
#include <iomanip>
#include <functional>


//compile g++ -Wall -I. -I `root-config --cflags` -o Gamma.out Gamma.cxx AnalysisInfo.cc `root-config --libs`
//need inputfiles list = "Co-60 Na-22 (bg_K-40 KCl_K-40)", ecc
// if fit or calibrate ranges = "Co-60	7600	8700	7950	8500 1 1173", ecc
//run ./Gamma.out  inputfiles list visualize 
////-> manually write fit range parameters to files specifying wheter or not to use them as calibration and eventually their expected energy value and save img
//fits ./Gamma.out inputfiles list fit ranges (/calibrate)
//run ./Gamma.out inputfiles list conversion calibrationparams visualize
// element_name, viewfrom, viewto, fitfrom, fitto, calibrate, expected_value; per ogni fit
//////A questo punto vorrei usare un altro programma (part 2?) per normalizzare e sottrarre il fondo ambientale del K-40 al KCl. Serve inserire dati stechiometrici per efficienza fotopicco. In questo programma ci può essere anche la parte del cioccolato!

static constexpr int min_events = 10;
static constexpr double n_bins = 25;

struct Calibration{
	bool calibrate;
	double expected;
	double expected_err;
	friend struct FitInfo;
	friend std::istream& operator>>(std::istream&, Calibration&);
};

std::istream& operator>>(std::istream &is, Calibration &calibration){
	is >> calibration.calibrate >> calibration.expected >> calibration.expected_err;
	return is;
}

void ScaleXaxis(TH1D* histoch, TF1* conversionfunction, std::string convertedlabel){ //La funzione deve essere lineare
	TAxis *newaxis = histoch->GetXaxis(); //ty Pepe_Le_Pew
	if (newaxis->GetXbins()->GetSize()) { 
		//Axis with variable bins
		TArrayD X(*(newaxis->GetXbins()));
		for(Int_t i = 0; i < X.GetSize(); i++) X[i] = conversionfunction->Eval(X[i]);
		newaxis->Set((X.GetSize() - 1), X.GetArray()); //new Xbins
	} else {
		//Axis with fixed bins
		newaxis->Set( newaxis->GetNbins(),
		conversionfunction->Eval(newaxis->GetXmin()), //new Xmin
		conversionfunction->Eval(newaxis->GetXmax()) ); //new Xmax
	}
	histoch->ResetStats();
	newaxis->SetTitle(convertedlabel.c_str());
}

struct FitInfo{
	std::string isotope_name;
	int viewfrom;
	int viewto;
	int fitfrom;
	int fitto;
	Calibration compare;
	//Non printo compare perché mi siddia
	void print(std::ostream& out=std::cerr){ out << isotope_name << "\t" << viewfrom << "\t" << viewto  << "\t" << fitfrom << "\t" << fitto  << "\t" << std::boolalpha << compare.calibrate << "\t" << compare.expected << "\t" << compare.expected_err << std::endl;}
	//friend std::ostream& operator<<(std::ostream &os, FitInfo &fitinfo);
	friend std::istream& operator>>(std::istream &is, FitInfo &fitinfo);
};

std::istream& operator>>(std::istream &is, FitInfo &fitinfo){
	is >> fitinfo.isotope_name >> fitinfo.viewfrom >> fitinfo.viewto >> fitinfo.fitfrom >> fitinfo.fitto >> fitinfo.compare;
	return is; //si può compattare in return is >> ecc >> ecc
}

struct element_and_range : std::pair<std::string, std::string> {
    using std::pair<std::string,std::string>::pair;
};

std::istream& operator>>(std::istream& stream, element_and_range& in) {
    return stream >> in.first >> in.second;
}
 
struct element_and_boundaries : std::tuple<std::string,double,double,double> {
    using std::tuple<std::string,double, double,double>::tuple;
};

std::istream& operator>>(std::istream& stream, element_and_boundaries& in) {
	std::string a;
	double b,c,d;
	stream >> a >> b >> c >> d;
	in = std::make_tuple(a,b,c,d);
	//std::cerr << "Internal loop" << a << '\t' << b << '\t' << c << '\t' << d << std::endl; //DEBUG
	return stream;
}

struct TimeAreaInfo { //lo so, funzionalmente uguale a una tuple.
	double time;
	double bin_start;
	double bin_end;
	double integral;
	TimeAreaInfo(double t, double s, double e, double i) : time(t), bin_start(s), bin_end(e), integral(i) {};
};

struct TableFormat {
    std::ostream& outs;
    int width;
    char fill;
    TableFormat(std::ostream& out=std::cerr,int width=15, char fill=' '): outs(out), width(width), fill(fill) {}
    template<typename T>
    TableFormat& operator<<(const T& data) {
        outs << std::setw(width) << data << std::setw(width) << std::setfill(fill);
        return *this;
    }
    TableFormat& operator<<(std::ostream&(*out)(std::ostream&)) {
        outs << out;
        return *this;
    }
};

std::pair<TFitResultPtr,Calibration> fitmany(FitInfo limits, bool draw=true){
		static std::unordered_map<std::string, unsigned> peak_dictionary;
		std::string element_name = limits.isotope_name; //confonde un po' ma non so che isotopo è il cioccolato quindi jamme ja
		++peak_dictionary[element_name];
		std::string rootname = element_name + ".root";
		std::string call_count(std::to_string(peak_dictionary[element_name]));
		
		std::string condensed_name(element_name + "_" + call_count);
		
		std::string histo(std::string("histo_") + condensed_name), 
								canvas(std::string("canvas_") + condensed_name),
								fitfunc(std::string("fitfunc_") + condensed_name);
		std::string outfilename = condensed_name + ".root";
		std::string outimgname = condensed_name + ".eps"; //cambio in eps
		auto outfile = std::make_shared<TFile>(outfilename.c_str(),"RECREATE"); // qua creo il root file
		TCanvas* fitcanvas = new TCanvas(canvas.c_str(),condensed_name.c_str());
		//TH1D* fithisto = (TH1D*)(new TFile(rootname.c_str()))->Get("hq0")->Clone(); //Devo tener conto anche di altre key: so come fare
		TFile* infile = new TFile(rootname.c_str());
		TH1D* fithisto =nullptr;
		if (infile->GetListOfKeys()->Contains("hq0")){
			fithisto = (TH1D*)(infile->Get("hq0"))->Clone();
		} else if (infile->GetListOfKeys()->Contains("net_spectrum")) {
			fithisto = (TH1D*)(infile->Get("net_spectrum"))->Clone(); //bandaid fix
		} else {
			std::cerr << "I COULDN'T GET YOUR HISTOGRAM BABY" << std::endl;
			exit(1);
		}
		outfile->cd();
		fithisto->SetLineColor(kBlue);
		fithisto->SetLineStyle(1);
		fithisto->SetName(histo.c_str());
		fithisto->SetTitle(histo.c_str());
		fithisto->GetXaxis()->SetRangeUser(limits.viewfrom, limits.viewto); 
		TF1 *func = new TF1(fitfunc.c_str(),"gaus", limits.fitfrom, limits.fitto); // fit sempre gaussiani
		TFitResultPtr res = fithisto->Fit(func,"RS");
		gStyle->SetOptFit(1111);
		if (draw) fithisto->Draw(); //questa devo capire perché non me la faceva funzionare molto spesso. In ogni caso devo mettere la conversione da ste parti
		fitcanvas->Write();
		fithisto->Write();
		func->Write();
		res->Print("V");
		res->Write();
		fitcanvas->SaveAs(outimgname.c_str(),"eps");
		outfile->Close();
		return std::make_pair(res,limits.compare);
}

int main(int argc, char** argv){
	
	AnalysisInfo* info = new AnalysisInfo( argc, argv );
	//argc : inputfiles (Co-60, Na-22, (Kcl,cioccolato - fondo), altro) mode (visualize*, fit, calibrate, coincidence) units (default, energy (given calibration file))
	std::string inputfiles;
	std::vector<element_and_range> files; //coppie nome file - limite visual
	std::map<std::string, TimeAreaInfo> time_area_info; //NOTA: per esportare tipi complessi potrei usare JSON->binario.
	std::string &ifs = inputfiles;

	TF1 *conversionfunction = nullptr;
	if(info->contains("conversion")){
		conversionfunction = new TF1("conversion", "[0]+[1]*x");
		std::ifstream userparamsfile(info->value("conversion")); //la funzione la so, mi servono solo i due parametri A e B (*x)
		std::istream_iterator<double> iit(userparamsfile),eos;
		double userparams[10]; // L'hanno definita con 10 parametri a prescindere quindi non posso sbagliare
		std::copy(iit,eos, userparams);
		conversionfunction->SetParameters(userparams);
	}

	if (info->contains("bgfix")){ //sottrae il fondo e crea net_element.root con gli istogrammi bkg, element, net element
		//int fakeargc=0; char* fakeargv[fakeargc];
		//TRint *theApp = new TRint("App", &fakeargc, fakeargv,0,0,1); qua mi servono solo per fermare il ciclo
		std::vector<element_and_boundaries> bgfiles;//andava bene anche una lista anzi mi sarei confuso di meno
		std::string bgfix_fn = info->value("bgfix");
		std::ifstream bgfix;
		bgfix.open(bgfix_fn, std::ios::in);
		if(!bgfix.is_open()){
			std::cerr << "Input file name for data whose background has to be removed is wrong or it couldn't be opened" << std::endl;
			return -1;
		} else {
			std::istream_iterator<element_and_boundaries> start(bgfix), end;
			std::copy(start,end,std::back_inserter(bgfiles));
		}
		
		//Funziona in tutti e tre i modi...
		/*
		for (std::vector<element_and_boundaries>::size_type i = 0; i<bgfiles.size(); ++i){
			std::cerr << "Ciclo " << i << std::endl;
			std::cerr << std::get<0>(bgfiles[i]) << '\t' << std::get<1>(bgfiles[i]) << '\t' << std::get<2>(bgfiles[i]) << std::endl;
		}
		
		int ics =0;
		for (const auto &row : bgfiles){
			std::cerr << ++ics << std::endl;
			std::cerr << std::get<0>(row) << '\t' << std::get<1>(row) << '\t' << std::get<2>(row) << std::endl;
		}
		
		std::cerr << std::get<0>(bgfiles[0]) << '\t' << std::get<1>(bgfiles[0]) << '\t' << std::get<2>(bgfiles[0]) << std::endl;
		std::cerr << std::get<0>(bgfiles[1]) << '\t' << std::get<1>(bgfiles[1]) << '\t' << std::get<2>(bgfiles[1]) << std::endl;
		std::cerr << std::get<0>(bgfiles[2]) << '\t' << std::get<1>(bgfiles[2]) << '\t' << std::get<2>(bgfiles[2]) << std::endl;
		*/
		
		for (const auto row : bgfiles) { //top level o low level const? non mi ricordo....
			const std::string nomecampione = std::get<0>(row);
			std::cerr << nomecampione << std::endl;
			std::string output = std::string("net_") + nomecampione + ".root";
			std::string input = nomecampione + ".root";
			std::string timehisto = nomecampione + "_time_histo";
			std::string spectrum = nomecampione + "_spectrum";
			std::string title = std::string("Spettro ") + nomecampione + " al netto della sottrazione del fondo";
			std::string qlong = std::string("qlong>>") + spectrum;
			double view_to = std::get<3>(row);
			std::string condition = std::string("ch==0 && qlong<") + std::to_string(view_to);
			double nbins = view_to/::n_bins;
			
			TFile* bgfix = new TFile(output.c_str(),"RECREATE");
			auto bgcanvas = std::make_shared<TCanvas>("net",nomecampione.c_str());
			
			TFile* el = TFile::Open(input.c_str(),"READ"); //shared? weak?
			std::cerr << "Aperto TFile " << input << std::endl;
			auto el_nt = (TNtuple*)(el->Get("nt"));
			bgfix->cd();
			double el_max_time = el_nt->GetMaximum("time"); //funziona?????
			double el_min_time = el_nt->GetMinimum("time");
			double el_time = el_max_time-el_min_time;
			//auto el_spectrum = static_cast<TH1D*>(el->Get("hq0"));
			TH1D* el_spectrum = new TH1D(spectrum.c_str(), spectrum.c_str(),nbins,0,view_to); // I BIN E IL PARAMETRO 
			el_nt->Draw(qlong.c_str(),condition.c_str());
			el_spectrum->Write(spectrum.c_str());
			//Devo ancora capire perché se chiudo qua il file mi manda in segfault
			
			TFile* bkg = TFile::Open("bkg.root","READ"); //USO il metodo statico
			std::cerr << "Aperto TFile " << "bkg.root" << std::endl;
			auto bkg_nt = static_cast<TNtuple*>(bkg->Get("nt"));
			bgfix->cd();
			double bkg_max_time = bkg_nt->GetMaximum("time");
			double bkg_min_time = bkg_nt->GetMinimum("time");
			double bkg_time = bkg_max_time-bkg_min_time; //Potrei condensare il tutto, non mi servono le due variabili di sopra..
			//Sfrutto le informazioni potentissime a mia disposizione per avere gli stessi bin tra fondo e dati?
			TH1D* bkg_spectrum = new TH1D("bkg_spectrum","bkg_spectrum", nbins,0,view_to);
			bkg_nt->Draw("qlong>>bkg_spectrum", condition.c_str());
			bgcanvas->WaitPrimitive();
			bkg_spectrum->Scale(el_time/bkg_time); //da manuale cambia anche gli errori quando ridimensiona
			bgcanvas->Modified();
			bgfix->cd();
			bkg_spectrum->Write("bkg_spectrum");
			auto hnet = static_cast<TH1D*>(el_spectrum->Clone("net_spectrum"));
			hnet->SetTitle(title.c_str());
			hnet->Sumw2(1); // setta il calcolo automatico degli errori statistici. Vorrei fare TH1::Sumw2(kTrue) se è static func
			//clone->SetStats("irmen");
			hnet->SetLineColor(kBlue);
			hnet->SetTitle(title.c_str());
			hnet->Add(bkg_spectrum,-1.); // sottrae il background
			hnet->Write();
			hnet->Draw();
			
			double bin_start = hnet->GetXaxis()->FindBin(std::get<1>(row));
			double bin_end = hnet->GetXaxis()->FindBin(std::get<2>(row));
			double integral = hnet->Integral(bin_start,bin_end);
			//std::cerr << "Tempo acquisizione campione = "<<  el_time << '\t' << nomecampione << std::endl;
			//std::cerr << "Tempo acquisizione background = " << bkg_time <<std::endl;
			time_area_info.insert( std::pair<std::string, TimeAreaInfo>(nomecampione, TimeAreaInfo(el_time,bin_start,bin_end,integral)));
			time_area_info.insert( std::pair<std::string, TimeAreaInfo>("background", TimeAreaInfo(bkg_time,0.,0.,0.))); //non metto integrale fondo
			bgfix->Close(); // Chiude il file. Tanto ho già salvato quello che mi serviva.
			//theApp->Run(); qui dentro fa un giro solo
		}
	}
	/*PARTE DELL'EFFICIENZA*/
	
	
	if (info->contains("efficiency") ){ //calcola l'efficienza del rivelatore in base allo spettro del KCl e da qui la m_K nel cioccolato
		long double M_KCl1 = 10, //g
								M_KCl2 = 144.3, //g
								e_M = 0.03,
								m_K = 39.0983, //g/mol
								m_Cl = 35.45,  //g_mol
								N_KCl1 = M_KCl1 / (m_K+m_Cl),
								N_KCl2 = M_KCl2 / (m_K+m_Cl),
								e_N=e_M / (m_K+m_Cl),
								tau_K40 = 1.2504e9 * 365.256363051 * 24. * 60. * 60. / log(2.), //s
								e_tau_K40=0.003e9*365.256363051 * 24. * 60. * 60. / log(2.),
								fraz_K40 = 0.000117,
								e_fraz_K40 = 0.000001,
								canali_gamma = 0.1055,
								A1 = fraz_K40*canali_gamma*N_KCl1* 6.022140857e23/tau_K40, //dovrei tipo salvare le costanti importanti in una struct a parte
								e_A1= A1*sqrt( pow((e_fraz_K40/fraz_K40),2.)+pow((e_N/N_KCl1),2.)+pow((e_tau_K40/tau_K40),2.) ),
								A2 = fraz_K40*canali_gamma*N_KCl2* 6.022140857e23/tau_K40, //dovrei tipo salvare le costanti importanti in una struct a parte
								e_A2= A2*sqrt( pow((e_fraz_K40/fraz_K40),2.)+pow((e_N/N_KCl2),2.)+pow((e_tau_K40/tau_K40),2.) ),
								Atot1=A1/canali_gamma,
								e_Atot1=e_A1/canali_gamma,
								Atot2=A2/canali_gamma,
								e_Atot2=e_A2/canali_gamma;
		//HAI USATO GLI SPETTRI NETTI RINORMALIZZ COL TEMPO
		//>>mi servono solo bin start bin end e el time
		auto KCl_infos = time_area_info.find("KCl");
		if (KCl_infos == time_area_info.end()) {std::cerr << "C'è un problema con i dati da ricavare da net kcl" << std::endl; return -3;}
		auto bkg_infos = time_area_info.find("background");
		if (KCl_infos == time_area_info.end()) {std::cerr << "C'è un problema con i dati da ricavare da bkg" << std::endl; return -3;}
		auto KCl_2_infos = time_area_info.find("KCl_2");
		if (KCl_2_infos == time_area_info.end()) {std::cerr << "C'è un problema con i dati da ricavare da net kcl_2" << std::endl; return -3;}
		double time_bkg = bkg_infos->second.time; // COME SI OTTIENE IL TEMPO BKG?
		double time_kcl1 = KCl_infos->second.time;
		double time_kcl2= KCl_2_infos->second.time;
		double gamma_revealed1 = KCl_infos->second.integral;
		double e_gamma_revealed1=sqrt(KCl_infos->second.integral);
		double gamma_revealed2 = KCl_2_infos->second.integral;
		double e_gamma_revealed2=sqrt(KCl_2_infos->second.integral);
		double gamma_emitted1 = A1*time_kcl1;
		double e_gamma_emitted1=e_A1*time_kcl1;
		double gamma_emitted2 = A2*time_kcl2;
		double e_gamma_emitted2=e_A2*time_kcl2;
		double efficiency1 = gamma_revealed1/gamma_emitted1;
		double e_efficiency1 = efficiency1*sqrt(pow(e_gamma_revealed1/gamma_revealed1,2.)+pow(e_gamma_emitted1/gamma_emitted1,2.) );
		double efficiency2 = gamma_revealed2/gamma_emitted2; 
		double e_efficiency2 = efficiency2*sqrt(pow(e_gamma_revealed2/gamma_revealed2,2.)+pow(e_gamma_emitted2/gamma_emitted2,2.) );
		std::ofstream efficienza("efficienza");
		efficienza << "Tempo acquisizione KCl1 = "<<  time_kcl1 << " [s]" << std::endl;
		efficienza << "Tempo acquisizione KCl2 = "<<  time_kcl2 << " [s]" << std::endl;								
		efficienza << "Attivita gamma prevista 10g KCl1 = " << A1 <<" +- "<<e_A1<<" [bq]\n" 
							 << "Attivita totale prevista 10g KCl1 = " << Atot1 <<" +- "<<e_Atot1<<" [bq]\n" 
							 << "Stima numero gamma emessi1 = A*t = " << gamma_emitted1<<" +- "<<e_gamma_emitted1
							 << "\nStima numero gamma rivelati1 = area = " << gamma_revealed1<<" +- "<<e_gamma_revealed1
							 << "\nEfficienza totale = " << efficiency1 <<" +- "<<e_efficiency1<< std::endl;
		efficienza << "Attivita prevista gamma 144,3g KCl = " << A2 <<" +- "<<e_A2<<" [bq]\n" 
							 << "Attivita prevista totale 144,3g KCl = " << Atot2 <<" +- "<<e_Atot2<<" [bq]\n" 
							 << "Stima numero gamma emessi = A*t = " << gamma_emitted2<<" +- "<<e_gamma_emitted2 
							 << "\nStima numero gamma rivelati2 = area = " << gamma_revealed2<<" +- "<<e_gamma_revealed2
							 << "\nEfficienza totale = " << efficiency2 <<" +- "<<e_efficiency2<< std::endl;
		
		auto chocolate_infos = time_area_info.find("chocolate");
		if (chocolate_infos == time_area_info.end()) {std::cerr << "C'è un problema con i dati da ricavare da net chocolate" << std::endl; return -3;}
		double time_chocolate = chocolate_infos->second.time;
		double gamma_revealed_chocolate = chocolate_infos->second.integral;
		double e_gamma_revealed_chocolate=sqrt(chocolate_infos->second.integral);
		double gamma_emitted_chocolate = gamma_revealed_chocolate/efficiency1;
		double e_gamma_emitted_chocolate= gamma_emitted_chocolate*sqrt(pow(e_gamma_revealed_chocolate/gamma_revealed_chocolate,2.)+pow(e_efficiency1/efficiency1,2.) );
		double mass_K_chocolate = tau_K40 * gamma_emitted_chocolate *m_K / (canali_gamma* time_chocolate * 6.022140857e23*fraz_K40);
		
		double e_mass_K_chocolate= mass_K_chocolate*sqrt(pow(e_gamma_emitted_chocolate/gamma_emitted_chocolate,2.)+pow(e_tau_K40/tau_K40,2.)+pow(e_fraz_K40/fraz_K40,2.));
		efficienza << "Tempo acquisizione cioccolato = "<<  time_chocolate << " [s]" << std::endl;
		efficienza << "Gamma rivelati dal cioccolato = " << gamma_revealed_chocolate << " +- "<< sqrt(gamma_revealed_chocolate) << std::endl;
		efficienza << "Gamma emessi dal cioccolato = " << gamma_emitted_chocolate << " +- "<< sqrt(gamma_emitted_chocolate) << std::endl;
		efficienza << "Massa di potassio in 204.4g di cioccolata fondente = " << mass_K_chocolate << " +- "<<e_mass_K_chocolate<< "\nPercentuale di potassio nel cioccolato =" << mass_K_chocolate/204.4*100.0 << "%" << std::endl;
		efficienza <<"TEMPO ACQUISIZIONE DEL FONDO = "<< time_bkg<<"[s]"<<std::endl;
	}
	
	
	
	/* **************   */
	
	
	if (info->contains("inputfiles")) { //Legge il file che contiene nomi file root e range visualizzazione. Bin = view/::n_bins
		ifs = info->value("inputfiles");
		std::ifstream elements;
		elements.open(ifs, std::ios::in);
		if(!elements.is_open()){
			std::cerr << "Input file name is wrong or it couldn't be opened" << std::endl;
			return -1;
		} else {
			std::istream_iterator<element_and_range> start(elements), end;
			std::copy(start,end,std::back_inserter(files));
		}
	} else {
		std::cerr << "Input file is missing. No data will be analyzed." << std::endl;
		return -1;
	}
	
	if (info->contains("visualize")){ //Visualizza e stampa ad immagine gli istogrammi qlong (ch==0)
		int fakeargc=0; char* fakeargv[fakeargc];
		TRint *theApp = new TRint("App", &fakeargc, fakeargv,0,0,1);
		for(const auto &file : files) { //not sure, forse glielo do per copia
			std::string rootfilename = file.first + ".root";
			std::cerr << rootfilename << std::endl;
			//Qui è dove devo estrarre ciò che mi serve.. https://root.cern.ch/root/html534/guides/users-guide/InputOutput.html
			TFile* rootfile = TFile::Open(rootfilename.c_str(), "READ"); //non so se sono hq0 e hq1 in tutti i casi o ci sono 2 e 3: check?
			gStyle->SetOptStat(kTRUE);
			gStyle->SetOptFit(1111);
			double view_limit(std::stod(file.second));
			if (rootfile->IsZombie()) return -2; //il file c'è ma non è un root file
			std::string _hq0 = file.first + "_hq0";
			std::string _hq0img = _hq0 + ".eps"; //FORMATO IMMAGINE
			//std::string	_hq1 = file.first + "_hq1";
			std::string qlong0 = std::string("qlong>>") + _hq0;
			std::string ch0_condition = std::string("ch == 0 & qlong <") + std::to_string(view_limit);
			
			if( strncmp(file.first.c_str(),"net_",4) == 0) { //File a cui è stato sottratto il fondo
				TCanvas* netcanvas = new TCanvas("cnet", _hq0.c_str());
				TH1D* nethisto = nullptr;
				rootfile->GetObject("net_spectrum",nethisto);
				if(info->contains("conversion")){
					ScaleXaxis(nethisto, conversionfunction, "Energy [keV]");
				} else {
					nethisto->SetXTitle("Energy [a.u.]");
				}
				nethisto->Draw(">>cnet");
				nethisto->BufferEmpty();
				netcanvas->Modified();
				netcanvas->Update();
				netcanvas->Print(_hq0img.c_str(),"eps");
				delete netcanvas;
				continue; // c'era un modo migliore? credo di sì
			}
			TIter next(rootfile->GetListOfKeys()); //NOTA IMPORTANTE: DEVO FARE UNA FACTORY DI TRINT LO SO CHE L'HO SCRITTO 6 VOLTE
			TKey* key;
			while((key=(TKey*)next())){
				if( strncmp(key->GetClassName(),"TNtuple",7) == 0){ //Al posto di prendere il grafico che non so com'è fatto, stampo dalla ntupla
					//spostato definizioni stringhe
					TCanvas* hq0canvas = new TCanvas("c0",_hq0.c_str());
					TH1D* histoch = new TH1D( _hq0.c_str(), _hq0.c_str(), view_limit/25, 0, view_limit);
					histoch->SetDirectory(rootfile);
					((TNtuple*)(key->ReadObj()))->Draw(qlong0.c_str(), ch0_condition.c_str());
					//histoch->SetTitle(_hq0.c_str());
					histoch->SetYTitle("Counts");
					histoch->GetYaxis()->CenterTitle();
					//histoch->GetXaxis()->SetRangeUser(0,view_limit);
					if (info->contains("conversion")){
						TAxis *newaxis = histoch->GetXaxis(); //ty Pepe_Le_Pew
						if (newaxis->GetXbins()->GetSize()) { 
							//Axis with variable bins
							TArrayD X(*(newaxis->GetXbins()));
							for(Int_t i = 0; i < X.GetSize(); i++) X[i] = conversionfunction->Eval(X[i]);
							newaxis->Set((X.GetSize() - 1), X.GetArray()); //new Xbins
						} else {
							//Axis with fixed bins
							newaxis->Set( newaxis->GetNbins(),
							conversionfunction->Eval(newaxis->GetXmin()), //new Xmin
							conversionfunction->Eval(newaxis->GetXmax()) ); //new Xmax
						}
						histoch->ResetStats();
						newaxis->SetTitle("Energy [keV]");
					} else {
						histoch->SetXTitle("Energy [a.u.]");
					}
					histoch->Draw(">>c0");
					histoch->BufferEmpty();
					hq0canvas->Modified();
					hq0canvas->Update();
					hq0canvas->SaveAs(_hq0img.c_str());
					//Getline("Type <return> to exit: "); 
					delete hq0canvas;
				}	
			}
				/*if( strncmp(key->GetClassName(),"TH1",3) == 0 ){ //Printa tutti gli istogrammi dentro il root file. non desiderabile
					std::string histname = key->GetName();
					auto histoch = (TH1D*)(rootfile->Get(key->GetName()));
					histoch->SetDirectory(0);
					if( histoch->GetEntries() > ::min_events ){ //limite minimo di eventi per stampare/analizzare l'istogramma
						TCanvas *canvasch = new TCanvas( (std::string(histname + "canvas")).c_str(), histname.c_str());
						histoch->Draw();
						histoch->SetTitle( (file.first + "_" +  histname).c_str() );
						histoch->SetYTitle("Counts");
						histoch->GetYaxis()->CenterTitle();
						histoch->GetXaxis()->SetRangeUser(0,view_limit);
						if(info->contains("conversion")){
							//histoch->GetXaxis()->SetLabelSize(0);
							//histoch->GetXaxis()->SetTickLength(0);
							TAxis *newaxis = histoch->GetXaxis();
							if (newaxis->GetXbins()->GetSize()) { //Code snippet by Pepe_Le_Pew
								// an axis with variable bins
								// note: bins must remain in increasing order, hence the "Scale"
								// function must be strictly (monotonically) increasing
								TArrayD X(*(newaxis->GetXbins()));
								for(Int_t i = 0; i < X.GetSize(); i++) X[i] = conversionfunction->Eval(X[i]);
								newaxis->Set((X.GetSize() - 1), X.GetArray()); // new Xbins
								} else {
									// an axis with fix bins
									// note: we modify Xmin and Xmax only, hence the "Scale" function
									// must be linear (and Xmax must remain greater than Xmin)
									newaxis->Set( newaxis->GetNbins(), 
									conversionfunction->Eval(newaxis->GetXmin()), // new Xmin
									conversionfunction->Eval(newaxis->GetXmax()) ); // new Xmax
								}
								histoch->ResetStats();
								newaxis->SetTitle("Energy [keV]");
						} else { 
							histoch->SetXTitle("Energy [a.u.]");
						}
						histoch->BufferEmpty();
						canvasch->Modified();
						canvasch->Update();
						canvasch->SaveAs( std::string(file.first + "_" + histname + ".png").c_str() );
						//mi serve un modo facile per aggiungere e modificare le stringhe
					} else {
						std::cerr << "Histogram " << histname << " of file " << rootfilename << " has less than " << ::min_events << " events. Not drawn." << std::endl;
					}
				}
			}*/
		}
		theApp->Run(kTRUE);
	}

	if (info->contains("fit")){
		std::ifstream fitranges;
		fitranges.open(info->value("fit") ,std::ios::in); 
		if(!fitranges.is_open()){
			std::cerr << info->value("fit") << " not opened!" << std::endl;
			return -1;
		}
		
		std::vector<FitInfo> ranges;
		std::istream_iterator<FitInfo> start(fitranges), end;
		std::copy(start, end, std::back_inserter(ranges));
		for (auto i : ranges) i.print();


		//Solo una TRapp può essere aperta in ogni momento.. Come fare?
		int fakeargc=1; char* fakeargv[fakeargc];
		TRint *theApp = new TRint("App", &fakeargc, fakeargv,0,0,1);
		gStyle->SetOptStat(kTRUE);
		gStyle->SetOptFit(1111);
		
		std::vector<std::pair<TFitResultPtr, Calibration>> results;
		for (auto i : ranges) { results.push_back(fitmany(i, false)); } //Capire perché non funziona if (draw) ...
		
		
		
		if(info->contains("calibrate") && info->contains("fit")) {
			std::ofstream retta("calib_line");
			std::ofstream retta_unw("calib_line_unw");
			for(std::vector<std::pair<TFitResultPtr,Calibration>>::size_type i=0; i < results.size(); ++i){
				if(results[i].second.calibrate == true ) {
					TMatrixDSym cov = results[i].first->GetCovarianceMatrix();
					//semilarghezza results[i].first->GetParams()[2]*(sqrt(2.0*log(2.0)))
					// errore centroide TMath::Sqrt(cov(1,1))
					//sigma fit results[i].first->GetParams()[2]
					//std::double uniterr = 1;
					retta << results[i].first->GetParams()[1] <<  '\t' << results[i].second.expected << '\t' << TMath::Sqrt(cov(1,1)) << '\t' << results[i].second.expected_err <<  std::endl;
					retta_unw << results[i].first->GetParams()[1] << '\t' << results[i].second.expected << std::endl;
				}
			}
			
			gStyle->SetStatX(0.5); gStyle->SetStatY(0.9);	
			TFile *fcalib=new TFile("calib_line.root","RECREATE"); // output file for calibration
		
		
			TCanvas* canvascalibunw = new TCanvas("canvas_unw","Calibrazione (non pesata)");
			TGraph *plot1 = new TGraph("calib_line_unw");
			plot1->SetTitle("Calibrazione(non pesato)"); //Il titolo del plot serve sempre
			TF1* calibfuncunw = new TF1("linear_unw","[0]+[1]*x");
			calibfuncunw->SetParNames("A","B");
			TFitResultPtr fitresultunw = plot1->Fit("linear_unw","S");
			plot1->Draw("AP*"); //c'è solo un canvas aperto.
			canvascalibunw->Print("calib_line_unw.eps","eps");
			calibfuncunw->Write();
			fitresultunw->Write();
			plot1->Write();
			

			TCanvas* canvascalib = new TCanvas("canvas","Calibrazione");
			TGraphErrors *plot2 = new TGraphErrors("calib_line"); 
			plot2->SetTitle("Calibrazione");
			TF1* calibfunc = new TF1("linear","[0]+[1]*x");			
			calibfunc->SetParNames("C","D");
			TFitResultPtr fitresult = plot2->Fit("linear","S");
			plot2->Draw("AP*");		
			calibfunc->Write();
			fitresult->Write();
			plot2->Write();
			canvascalib->Print("calib_line.eps","eps");
			
			//SALVO I PARAMETRI DEL FIT IN DUE FILE! UNW è chiaramente quello unweighted
			std::ofstream conversionparametersunw("calibrationparams_unw");
			conversionparametersunw << calibfuncunw->GetParameter("A") << '\t' << calibfuncunw->GetParameter("B") << std::endl;
			conversionparametersunw.close();
			
			std::ofstream conversionparameters("calibrationparams");
			conversionparameters << calibfunc->GetParameter("C") << '\t' << calibfunc->GetParameter("D") << std::endl;
			conversionparameters.close();
			
			// GRAFICO DEI RESIDUI NONPESATI
			TCanvas *residualsunw = new TCanvas("Residui_unw","Residui fit non pesato");
			TGraphErrors *grunw = new TGraphErrors("calib_line_unw"); // parte da una copia del grafico originale
			for (int i=0; i<plot1->GetN(); i++) {
				double res = plot1->GetY()[i] - calibfunc->Eval(plot1->GetX()[i]); // residuo
				grunw->SetPoint(i,plot1->GetX()[i],res);
				double eres = TMath::Sqrt(calibfuncunw->GetChisquare()/calibfuncunw->GetNDF()); //sigma a posteriori se metto errori unitari
				grunw->SetPointError(i,0,eres);
			}
			grunw->SetName("Residui-fit-non pesato");
			grunw->SetTitle("Residui fit non pesato"); //?
			grunw->Draw("AP*");
			residualsunw->Update();
			TLine* lzero = new TLine(residualsunw->GetUxmin(), 0.0,residualsunw->GetUxmax(), 0.0);
			lzero->SetLineStyle(2);
			lzero->Draw();
			residualsunw->Update();
			grunw->Write();
			residualsunw->Write();
			residualsunw->Print("calib_line_residuals_unw.eps","eps");	
			
			// GRAFICO DEI RESIDUI PESATI
			TCanvas *residuals = new TCanvas("Residui","Residui fit pesato");
			TGraphErrors *gr = new TGraphErrors("calib_line"); // parte da una copia del grafico originale
			for (int i=0; i<plot2->GetN(); i++) {
				double res = plot2->GetY()[i] - calibfunc->Eval(plot2->GetX()[i]); // residuo
				gr->SetPoint(i,plot2->GetX()[i],res);
				double eresy = plot2->GetEY()[i]; // contributo delle Yi
				double eresx = calibfunc->Derivative(plot2->GetX()[i])*plot2->GetEX()[i]; // contrib. Xi (approx. 1 ordine)
				double eres = TMath::Sqrt(eresy*eresy+eresx*eresx);
				//double eres = TMath::Sqrt(calibfunc->GetChisquare()/calibfunc->GetNDF()); //sigma a posteriori se metto errori unitari
				gr->SetPointError(i,0,eres);
			}
			gr->SetName("Residui-fit-pesato");
			gr->SetTitle("Residui fit pesato");
			gr->Draw("AP*");
			residuals->Update();
			lzero = new TLine(residuals->GetUxmin(), 0.0,residuals->GetUxmax(), 0.0);
			lzero->SetLineStyle(2);
			lzero->Draw();
			residuals->Update();
			gr->Write();
			residuals->Write();
			residuals->Print("calib_line_residuals.eps","eps");	
			fcalib->Close();
		}
		
		if(info->contains("conversion")){
			std::ofstream fitresult("fit_results");
			//double interc =conversionfunction->GetParameter(0);
			double pendenza=conversionfunction->GetParameter(1);
			TableFormat fitresulttable(fitresult,25,' ');
			fitresulttable << "Element name" << "Centroide [u.a]" << "Centroide [keV]" << "Sigma [u.a]"<<"Sigma [keV]" << "Errore centroide [u.a]"<<"Errore centroide [keV]" << "Semiampiezza [u.a.]" <<"Semiampiezza [keV]"<< std::endl;
			for (unsigned i = 0; i<ranges.size(); ++i){
				TMatrixDSym cov = results[i].first->GetCovarianceMatrix();
				fitresulttable << ranges[i].isotope_name << results[i].first->GetParams()[1] << conversionfunction->Eval(results[i].first->GetParams()[1])<< results[i].first->GetParams()[2] << pendenza*results[i].first->GetParams()[2]<< TMath::Sqrt(cov(1,1))<< pendenza*(TMath::Sqrt(cov(1,1))) << sqrt(2*log(2))*results[i].first->GetParams()[2] << pendenza*sqrt(2*log(2))*results[i].first->GetParams()[2] << std::endl;
			}
		}
		
		
		
		
		
		theApp->Run();
		
		
	}
	return 0;
}















	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
