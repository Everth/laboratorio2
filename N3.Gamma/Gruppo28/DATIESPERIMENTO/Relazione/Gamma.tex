\documentclass[fontsize=11pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[italian]{babel}
\usepackage[a4paper]{geometry}
\geometry{top=20mm, left=20mm, right=20mm}
\usepackage{array}
\usepackage[justification=centering]{caption}
\usepackage{amsmath}
\usepackage{gensymb}
\usepackage{mathrsfs}
\usepackage{hyperref}
\usepackage{cancel}
\usepackage{graphicx}
\usepackage[T1]{fontenc}
\usepackage{caption}
\usepackage{tabularx}
\usepackage{catchfilebetweentags}
\DeclareGraphicsExtensions{.pdf,.png,.jpg}
\DeclareGraphicsRule{.png}{png}{.png.bb}{}
\fontencoding{T1}
\begin{document}
\title{\huge \textbf{Relazione di laboratorio} \\ \large {Misure di raggi gamma con rivelatori a scintillazione NaI(Tl)} \\ \large Gruppo 28}
\date{12 Maggio 2018}
\author{ Andrea Currò Dossi - 1122269 \quad Alessandro Lenoci - 1126038 \quad Samuele Albano - 1122661\\ }
\maketitle

\section{Obiettivi dell'esperienza}
Gli obiettivi dell'esperienza sono:
\begin{enumerate}\setlength{\itemsep}{0pt}
\item La calibrazione in energia del sistema di rivelazione di raggi gamma tramite sorgenti note di $^{60}$Co e $^{22}$Na.
\item Il riconoscimento delle energie dei raggi gamma emessi da una sorgente di $^{152}$Eu e di $^{137}$Cs.
\item La stima dell'efficienza di fotopicco da emissioni gamma di campioni contenenti $^{40}$K.
\item La stima della quantità di potassio presente in un campione di cioccolata fondente.
\item La stima della risoluzione temporale del rivelatore tramite misure di coincidenza con la sorgente di $^{60}$Co.
\end{enumerate}

\section{Analisi dati}

\subsection{Calibrazione in energia del rivelatore}
Per studiare gli spettri energetici della radiazione gamma emessa da sorgenti radioattive è necessario effettuare una calibrazione del rivelatore a scintillazione NaI attivato con tallio. Per farlo si acquisiscono gli spettri di sorgenti con emissioni gamma note quali il $^{60}$Co e $^{22}$Na, le cui energie sono riportate in tabella \ref{tab:CoNa}. 

\begin{table}[htbp]
\centering
\caption{Valori energetici gamma emessi dalle sorgenti $^{60}$Co e $^{22}$Na}
\begin{tabular}{cccccc}
\emph{Sorgente}&\emph{Nome gamma}&$E_\gamma^* \: \mathrm{[keV]}$&$\sigma_{E_\gamma^*} \: \mathrm{[keV]}$&$P$\footnotemark[1]&$\sigma_{P}$\\ \hline
$^{60}$Co&$\gamma_{3,1}$(Ni)&1173.228&0.003&99.85&0.03\\
&$\gamma_{1,0}$(Ni)&1332.492&0.004&99.9826&0.0006\\ \hline
$^{22}$Na&$\gamma^{\pm}$&510.998946&0.000001&180.7&0.2\\
&$\gamma_{1,0}$(Ne)&1274.537&0.007&99.94&0.13\\ \hline
\end{tabular}
\label{tab:CoNa}
\end{table}
\footnotetext[1]{Fotoni emessi per 100 disintegrazioni. }

Il $^{60}$Co è un isotopo radioattivo del cobalto prodotto artificialmente che decade $\beta ^{-}$ al $99,88\%$  al terzo stato eccitato del $^{60}$Ni emettendo poi due gamma in cascata $\gamma_{3,1}$(Ni) e $\gamma_{1,0}$(Ni). Il $^{22}$Na è invece un isotopo radioattivo del sodio con una vita media di circa 2.61 anni che decade per decadimento $\beta^+$ o per cattura elettronica al primo stato eccitato del $^{22}$Ne emettendo poi 
un fotone $\gamma_{1,0}$(Ne) di energia pari circa a 1.274 MeV. Inoltre il positrone emesso nel decadimento $\beta$ si annichila con un elettrone emettendo due fotoni $\gamma^{\pm}$ da 511 keV back-to-back: ci si aspetta in prima approssimazione una frequenza doppia di rivelazione di questi fotoni rispetto al
$\gamma_{1,0}$.



Si sono quindi acquisiti gli spettri energetici (conteggi in funzione dell'energia, in unità arbitrarie) del $^{60}$Co e $^{22}$Na, riportati in Fig.\ref{fig:Co} e Fig.\ref{fig:Na}. Si notano in questi grafici le tipiche strutture degli spettri in energia. Lo spettro del $^{60}$Co presenta due fotopicchi marcati associati ai fotoni $\gamma_{3,1}$(Ni) e $\gamma_{1,0}$(Ni), una distribuzione continua dovuta all'effetto Compton (\emph{Compton continuum}) a valori di energia inferiori, seguita dalla relativa spalla e da un tratto associato a eventi di Compton multiplo e infine a energie più alte un picco meno intenso relativo agli eventi per cui i fotoni $\gamma_{3,1}$(Ni) e $\gamma_{1,0}$(Ni) giungono insieme sul rivelatore (\emph{sum peak}). Lo spettro del $^{22}$Na
presenta un picco molto marcato associabile ai fotoni $\gamma^{\pm}$ da 511 keV e un picco energie intermedie relativo al fotone $\gamma_{1,0}$(Ne). Si nota che il rapporto tra le intensità dei due picchi è molto diverso dal valore atteso ($\simeq 2$): ciò è probabilmente dovuto all'alto rate di fotoni $\gamma^{\pm}$ che incidono sul rivelatore per cui, usando PMT non compensati, come nel caso dell'apparato a disposizione, il guadagno è alterato. Inoltre a basse energie si può notare un picco associato ad una raccolta parziale di carica (\emph{low energy peak}) che va trascurato, un effetto Compton poco visibile e, come nel caso precedente, un sum peak ad alte energie.
\\

\begin{minipage}[htbp]{\textwidth}
\begin{minipage}{0.5\textwidth}
\centering
\captionof{figure}{Spettro energetico $^{60}$Co }\label{fig:Co}
\includegraphics[width=0.95\linewidth]{../Co-60_hq0.eps}
\end{minipage}
\hfill
\begin{minipage}{0.5\textwidth}
\centering
\captionof{figure}{Spettro energetico $^{22}$Na}\label{fig:Na}
\includegraphics[width=0.95\linewidth]{../Na-22_hq0.eps}
\end{minipage}
\end{minipage}
Si è quindi proceduto ad individuare le posizioni $x_0$ dei centroidi dei vari picchi effettuando dei fit sui picchi energetici con una funzione gaussiana
\[f(x)=Ke^{-\frac{1}{2\sigma^2}(x-x_0)^2}\] 
Dal fit si ricavano le posizioni dei centroidi $x_0$, gli errori ad essi associati $\sigma_{x_0}$ e il parametro $\sigma$ da cui si ricava la FWHM$=\sqrt{8\ln 2}\sigma$. A titolo di esempio si riportano i fit gaussiani per i picchi corrispondenti ai fotoni $\gamma_{1,0}$(Ni) del $^{60}$Co e $\gamma_{1,0}$(Ne) del $^{22}$Na in Fig.\ref{fig:Cofit} e Fig.\ref{fig:Nafit}. Si è scelto di interpolare tutti i picchi a disposizione, compresi i sum peaks dei due elementi, al fine di avere più punti per la calibrazione del detector. Si riportano in tabella \ref{tab:calib} i risultati dei fit gaussiani. 
\\

\begin{minipage}[htbp]{\textwidth}
\begin{minipage}{0.5\textwidth}
\centering
\captionof{figure}{Fit gaussiano picco $\gamma_{1,0}$(Ni) del  $^{60}$Co }\label{fig:Cofit}
\includegraphics[width=0.95\linewidth]{../Co-60_2.eps}
\end{minipage}
\hfill
\begin{minipage}{0.5\textwidth}
\centering
\captionof{figure}{Fit gaussiano picco $\gamma_{1,0}$(Ne) del $^{22}$Na}\label{fig:Nafit}
\includegraphics[width=0.95\linewidth]{../Na-22_2.eps}
\end{minipage}
\end{minipage}
\begin{table}[htbp]
\centering
\caption{Valori fit lineare per la calibrazione}
\begin{tabular}{cccccc}
\emph{Sorgente}&\emph{Nome gamma}&$x_{0} \: [u.a]$&$\sigma_{x_{0}} \: [u.a]$&$E_\gamma^*$ [keV]&$\sigma_{E_\gamma^*}$ [keV]\\ \hline
$^{60}$Co&$\gamma_{3,1}$(Ni)&8217.0&0.7&1173.228&0.003\\
&$\gamma_{1,0}$(Ni)&9358.5&0.8&1332.492&0.004\\ \hline
$^{22}$Na&$\gamma^{\pm}$&3535.1&0.3&510.998946&0.000001\\
&$\gamma_{1,0}$(Ne)&8874&1&1274.537&0.007\\
&$\gamma^{\pm}+\gamma_{1,0}$(Ne)&12540&2&1785.537&0.007\\ \hline
\end{tabular}
\label{tab:calib}
\end{table}
Si è quindi proceduto alla calibrazione del detector disponendo i valori dei centroidi $x_0$ e i corrispondenti valori noti di energia $E_\gamma^*$ in un grafico $(x_0,E_\gamma^*)$ ed effettuando un'interpolazione lineare con una retta di equazione $E_\gamma^* =A+Bx_0$. Si suppone infatti che in buona approssimazione la risposta del rivelatore per i fotoni con energie nell'intervallo preso in esame sia lineare: questa ipotesi sarà da verificare a posteriori. Si riportano nelle Fig.\ref{fig:calibline} e Fig.\ref{fig:calibres} i grafico del fit lineare e il relativo grafico dei residui. Si sono forniti al fit, eseguito con ROOT, gli errori sui centroidi $\sigma_{x_0}$ e sulle energie note $\sigma_{E_\gamma^*}$ e per gli errori sui residui si è usata la formula
\[\sigma_\delta=\sqrt{\sigma_{E_\gamma^*}^2+B^2\sigma_{x_0}^2}\]
\\

\begin{minipage}[htbp]{\textwidth}
\begin{minipage}{0.5\textwidth}
\centering
\captionof{figure}{Fit lineare di calibrazione }\label{fig:calibline}
\includegraphics[width=0.95\linewidth]{../calib_line.eps}
\end{minipage}
\hfill
\begin{minipage}{0.5\textwidth}
\centering
\captionof{figure}{Grafico dei residui fit calibrazione}\label{fig:calibres}
\includegraphics[width=0.95\linewidth]{../calib_line_residuals.eps}
\end{minipage}
\end{minipage}

Si nota dal grafico dei residui in Fig.\ref{fig:calibres} che l'errore associato ai centroidi è decisamente sottostimato e determina un valore del $\chi^2$
ridotto troppo distante dall'unità. Ciò significa che l'andamento è solo in prima approssimazione lineare ma non compatibile con la retta: la risposta del detector non è propriamente lineare. Si è deciso quindi di effettuare un fit non pesato al fine di attribuire alle posizioni dei centroidi l'errore a posteriori: in tal modo questo errore risulterà indicativo della non perfetta linearità della risposta del rivelatore. Si riportano quindi nelle figure
Fig.\ref{fig:caliblineunw} e Fig.\ref{fig:calibresunw} il nuovo fit lineare non pesato e il corrispondente grafico dei residui con errore a posteriori. L'errore a posteriori ottenuto è $\sigma^*=5$ keV.
\\

\begin{minipage}[htbp]{\textwidth}
\begin{minipage}{0.5\textwidth}
\centering
\captionof{figure}{Fit lineare di calibrazione non pesato}\label{fig:caliblineunw}
\includegraphics[width=0.95\linewidth]{../calib_line_unw.eps}
\end{minipage}
\hfill
\begin{minipage}{0.5\textwidth}
\centering
\captionof{figure}{residui fit di calibrazione con errore a posteriori}\label{fig:calibresunw}
\includegraphics[width=0.95\linewidth]{../calib_line_residuals_unw.eps}
\end{minipage}
\end{minipage}

Si ottengono dunque i parametri di calibrazione
\[A= 11 \pm 7 \: \mathrm{keV} \hspace{4cm} B= 0.1416 \pm 0.0007\: \mathrm{keV/u.a.}\]

\subsection{Riconoscimento emissioni $\gamma$ del $^{137}$Cs e $^{152}$Eu}
Ottenuta la formula di conversione in energia del rivelatore si può verificare la bontà della calibrazione effettuata andando a rivelare le emissioni gamma da altre sorgenti radioattive, in particolare dal $^{137}$Cs e $^{152}$Eu. Nelle Fig.\ref{fig:Cs} e Fig.\ref{fig:Eu} sono riportati gli spettri energetici acquisiti. Si riportano nella tabella \ref{tab:CsEu} le emissioni gamma di maggiore probabilità delle sorgenti considerate che si sono associate ai picchi presenti negli spettri: ci sono infatti alcune emissioni gamma, in particolare per $^{152}$Eu, che pur avendo probabilità comparabili con le altre non sono ben riconoscibili negli spettri. 

\begin{minipage}[htbp]{\textwidth}
\begin{minipage}{0.5\textwidth}
\centering
\captionof{figure}{Spettro energetico $^{137}$Cs}\label{fig:Cs}
\includegraphics[width=0.95\linewidth]{../Cs-137_hq0.eps}
\end{minipage}
\hfill
\begin{minipage}{0.5\textwidth}
\centering
\captionof{figure}{Spettro energetico $^{152}$Eu}\label{fig:Eu}
\includegraphics[width=0.95\linewidth]{../Eu-152_hq0.eps}
\end{minipage}
\end{minipage}

\begin{table}[htbp]
\centering
\caption{Valori energetici gamma emessi dalle sorgenti $^{137}$Cs e $^{152}$Eu}
\begin{tabular}{cccccc}
\emph{Sorgente}&\emph{Nome gamma}&$E_\gamma^* \: \mathrm{[keV]}$&$\sigma_{E_\gamma^*} \: \mathrm{[keV]}$&$P$&$\sigma_{P}$\\ \hline
$^{137}$Cs&$\gamma_{2,0}$(Ba)&661.657&0.003&85.0&0.2\\ \hline
$^{152}$Eu&$\gamma_{1,0}$(Gd)&344.279&0.001&26.6&0.1\\
&$\gamma_{7,1}$(Gd)&778.907&0.002&12.97&0.06\\
&$\gamma_{9,1}$(Sm)&964.08&0.02&14.50&0.06\\
&$\gamma_{10,1}$(Sm)&1112.080&0.003&13.41&0.06\\
&$\gamma_{13,1}$(Sm)&1408.013&0.003&20.85&0.08\\ \hline
\end{tabular}
\label{tab:CsEu}
\end{table}

Si notano nello spettro del $^{152}$Cs le strutture già evidenziate nello spettro del $^{60}$Co (Compton continuum, Compton multiplo); poiché il fotopicco è molto intenso come nel caso del fotopicco $\gamma^\pm$ del $^{22}$Na presenta anch'esso i problemi derivanti dalla non compensazione dei fotomoltiplicatori per cui si avranno forma e posizione falsate rispetto a quelle attese. Per quanto riguarda lo spettro del $^{152}$Eu si nota un picco molto intenso a basse energie che si è associato al $\gamma_{7,1}$(Gd): bisogna precisare però che dato l'alto numero di transizioni gamma per l'elemento in esame e la bassa energia del fotone, al picco contribuiscono altri eventi che si associano a raccoglimenti parziali di carica o a transizioni non individuabili a causa della risoluzione limitata dell'apparato. Sono presenti inoltre numerosi picchi ad energia superiore, che emergono in modo non molto netto dalla distribuzione continua: anche in questo caso contribuiranno fenomeni di effetto Compton non associabili ad un gamma caratteristico e/o sum peaks dei fotopicchi ad energie inferiori. Si nota difatti che le intensità relative dei picchi ottenuti non corrispondono ai valori attesi dalle probabilità riportate in tabella \ref{tab:CsEu}.

 Si effettuano in ogni caso dei fit gaussiani sui picchi individuati, in modo analogo a quanto effettuato nella sezione precedente. Una volta ottenuti i valori dei centroidi dei picchi, grazie alla calibrazione effettuata si possono convertire queste misure in energia (keV) e associare ad esse l'errore $\sigma^*$ a posteriori precedentemente ottenuto. Si vuole quindi calcolare la compatibilità dei valori delle energie dei gamma caratteristici $E_\gamma$ così ottenute con i valori noti in letteratura $E_\gamma^*$
\[\lambda=\frac{|E_\gamma-E_\gamma^*|}{\sqrt{\sigma_{E_\gamma^*}^2+{\sigma^*}^2}}\]
Si riportano nella tabella \ref{tab:CsEucomp} le posizioni dei centroidi individuati con relativi errori dati dal fit, le energie convertite $E_\gamma$, le energie tabulate $E_\gamma^*$ con relativi errori e le compatibilità calcolate.





\begin{table}[htbp]
\centering
\caption{Valori energetici gamma emessi dalle sorgenti $^{137}$Cs e $^{152}$Eu ottenuti dalla calibrazione e compatibilità con i valori tabulati}
\begin{tabular}{ccccccccc}
\emph{Sorgente}&\emph{Nome gamma}&$x_{0} \: [u.a]$&$\sigma_{x_{0}} \: [u.a]$&$E_{\gamma} ^*$ [keV]&$\sigma^*$ [keV]&$E_{\gamma}$ [keV]&$\sigma_{E_{\gamma}}$ [keV]&$\lambda$\\ \hline
$^{137}$Cs&$\gamma_{2,0}$(Ba)&4762.2&0.4&661.657&0.003&685&5&4.7\\ \hline
$^{152}$Eu&$\gamma_{1,0}$(Gd)&2433.7&0.6&344.279&0.001&355&5&2\\
&$\gamma_{7,1}$(Gd)&5502&2&778.907&0.002&789&5&2\\
&$\gamma_{9,1}$(Sm)&6857&4&964.08&0.02&981&5&3.4\\
&$\gamma_{10,1}$(Sm)&7851&3&1112.080&0.003&1122&5&2\\
&$\gamma_{13,1}$(Sm)&10110&3&1408.013&0.003&1442&5&6.8\\ \hline
\end{tabular}
\label{tab:CsEucomp}
\end{table}

Le compatibilità ottenute non sono ottimali: ciò è dovuto soprattutto alla non aderenza della risposta in energia del detector ad un modello lineare ma anche agli effetti discussi sopra riguardo gli spettri in energia.
\newpage
\subsection{Stima dell'efficienza di fotopicco}
Si vuole ora stimare l'efficienza di fotopicco del sistema di misura tramite l'acquisizione di emissioni gamma relative al $^{40}$K contenuto in campioni di KCl. Il $^{40}$K è un isotopo naturale del potassio con abbondanza isotopica $\jmath=0.0117 \pm 0.0001 \%$ che possiede due canali di decadimento principali come si mostra in Fig.\ref{fig:K40}(DDEP): $\beta^-$ al $^{40}$Ca ($\simeq 90\%$) e cattura elettronica in due modalità: al primo livello eccitato del $^{40}$Ar con successiva emissione di un $\gamma_{1,0}$(Ar) da 1461 keV ($p=10.55\%$) e  allo stato fondamentale ($0.2\%$). Per la rivelazione del gamma bisognerà quindi considerare la probabilità $p$ del canale di decadimento relativo al primo stato eccitato del $^{40}$Ar. Si sono considerati due campioni di KCl (d'ora in avanti indicati come campione 1 e 2) di massa differente riportati con le informazioni rilevanti sul $^{40}$K nella tabella \ref{tab:K}. Ai valori misurati delle masse si associa l'errore dato dalla più piccola tacca di lettura fratto $\sqrt{12}$ per misure su strumenti digitali: $\sigma_M =0.1/\sqrt{12}$ g.
\\

\begin{minipage}[htbp]{\textwidth}
\begin{minipage}{0.4\textwidth}
\centering
\captionof{table}{Valori utili per lo studio del $^{40}$K e del KCl}\label{tab:K}
\begin{tabular}{c|c}
$E_{\gamma_{7,1}}\:\mathrm{[keV]}$&1460.882$\pm$0.006\\
$\jmath$&(0.0117$\pm$0.0001)$10^{-2}$\\
$\tau_{^{40}K}\:\mathrm{[s]}$&(5.69$\pm$0.01)$10^{16}$\\
$m_K\:\mathrm{[g/mol]}$&39.0983\\
$m_{Cl}\:\mathrm{[g/mol]}$&35.45\\
$M_{KCl}^{(1)}\:\mathrm{[g]}$&10.00$\pm$0.03\\
$M_{KCl}^{(2)}\:\mathrm{[g]}$&144.30$\pm$0.03\\
$N_A\:\mathrm{[mol^{-1}]}$&$6.022140857\cdot10^{23}$\\
$\Delta T_{KCl}^{(1)}\:\mathrm{[s]}$&3014.4\\
$\Delta T_{KCl}^{(2)}\:\mathrm{[s]}$&2526.2\\
$\Delta T_{bkg}\:\mathrm{[s]}$&73202.5\\
\end{tabular}
\end{minipage}
\hfill
\begin{minipage}{0.5\textwidth}
\centering
\captionof{figure}{Schema di decadimento del $^{40}$K}\label{fig:K40}
\includegraphics[width=\linewidth]{../K-40_decay.pdf}
\end{minipage}
\end{minipage}
\\
\\

Si sono dunque acquisiti gli spettri in energia dei due campioni di KCl e uno spettro del fondo ambientale (schermato dal pozzetto del rivelatore). La misura del fondo è stata effettuata a cavallo dei due giorni tra le misure dei campioni di KCl con un tempo di acquisizione di circa 20 ore al fine di avere una statistica sufficiente all'analisi dati. Si riporta lo spettro del fondo acquisito in Fig.\ref{fig:bkg}. Si nota distintamente il fotopicco riconducibile all'emissione gamma $\gamma_{1,0}$(Ar) e un numero molto grande di eventi a basse energie. Quindi si è provveduto a sottrarre il fondo agli spettri in energia dei campioni di KCl tenendo conto  dei diversi tempi di acquisizione, riportati in tabella \ref{tab:K}, con una rinormalizzazione. Si mostrano gli spettri dei due campioni al netto della sottrazione del fondo nelle Fig.\ref{fig:KCl1} e Fig.\ref{fig:KCl2}. Si notano in modo evidente in entrambi gli spettri i picchi energetici corrispondenti alle emissioni gamma $\gamma_{1,0}$(Ar) del $^{40}$K e un low energy peak. Si è quindi individuata la posizione dei centroidi e si è provveduto a convertire queste posizioni in energie (keV) basandosi sulla calibrazione precedentemente effettuata: si suppone infatti che la regione in cui il rivelatore risponde in modo lineare si estenda fino all'energia del fotone $\gamma_{1,0}$(Ar): si riportano i risultati in tabella \ref{tab:net}. Alle energie si è associato, come fatto in precedenza, l'errore a posteriori $\sigma^*$. Essendo i tempi di acquisizione molto lunghi si considerano del tutto trascurabili gli errori su questi tempi. Le compatibilità con il valore $E_{\gamma_{7,1}}$ sono buone per il fotopicco del fondo e del primo campione ma peggiore per il secondo campione: si pensa che ciò sia dovuto al minor tempo di acquisizione.

\begin{minipage}[htbp]{\textwidth}
\begin{minipage}{0.45\textwidth}
\centering
\captionof{figure}{Spettro energetico fondo ambientale}\label{fig:bkg}
\includegraphics[width=0.85\linewidth]{../bkg_hq0.eps}
\end{minipage}
\begin{minipage}{0.55\textwidth}
\centering
\captionof{table}{Posizioni centroidi spettro di fondo e KCl}
\begin{tabular}{cccccc}
\emph{Sorgente}&$x_{0} \: [u.a]$&$\sigma_{x_{0}} \: [u.a]$&$E_{\gamma}$ [keV]&$\sigma^*$ [keV]&$\lambda$\\ \hline
Fondo&10198&3&1454&5&1\\
KCl$^{(1)}$&10171&9&1450&5&1.4\\
KCl$^{(2)}$&10101&4&1441&5&4\\
\end{tabular}
\label{tab:net}
\end{minipage}
\end{minipage}


\begin{minipage}[htbp]{\textwidth}
\begin{minipage}{0.5\textwidth}
\centering
\captionof{figure}{Spettro netto del KCl$^{(1)}$}\label{fig:KCl1}
\includegraphics[width=0.85\linewidth]{../net_KCl_hq0.eps}
\end{minipage}
\hfill
\begin{minipage}{0.5\textwidth}
\centering
\captionof{figure}{Spettro netto del KCl$^{(2)}$}\label{fig:KCl2}
\includegraphics[width=0.85\linewidth]{../net_KCl_2_hq0.eps}
\end{minipage}
\end{minipage}
Si procede ora al calcolo delle efficienze totali di fotopicco nel caso dei due campioni. Esse sono date dal rapporto tra il numero di fotoni rivelati dal sistema e il numero di fotoni emessi dalla sorgente in riferimento a quelli di una specifica energia, nel caso in esame  
\[ \varepsilon ^{(1,2)} = \frac{n_{\gamma, r} ^{(1,2)}}{n_{\gamma, e} ^{(1,2)}}  \hspace{2cm} \sigma_{\varepsilon ^{(1,2)}} \simeq \varepsilon ^{(1,2)} \sqrt{\left(\frac{\sigma{n_{\gamma, r} ^{(1,2)}}}{n_{\gamma, r} ^{(1,2)}}\right)^2+\left(\frac{\sigma{n_{\gamma, e} ^{(1,2)}}}{n_{\gamma, e} ^{(1,2)}}\right)^2}\]
Occorre quindi stimare i fotoni emessi e rivelati. Il numero di fotoni emessi si ricava dal prodotto dell'attività del campione per il tempo di acquisizione.
\[ n_{\gamma, e} ^{(1,2)} = \alpha^{(1,2)}\Delta T_{KCl}^{(1,2)} \hspace{2cm} \sigma_{n_{\gamma, e} ^{(1,2)}} \simeq n_{\gamma, e} ^{(1,2)} \sqrt{\left(\frac{\sigma_{\alpha^{(1,2)}}}{\alpha^{(1,2)}}\right)^2 + \left(\frac{\sigma_{\Delta T_{KCl}^{(1,2)}}}{\Delta T_{KCl}^{(1,2)}}\right)^2}\]
L'attività $\alpha$ a sua volta è nota data la massa dei campioni, da cui il numero di moli
\[ \alpha^{(1,2)} = \frac{\jmath N_A N_{KCl}^{(1,2)}p}{\tau_{^{40}K}} \hspace{2cm} \sigma_{\alpha^{(1,2)}} \simeq \alpha^{(1,2)} \sqrt{\left(\frac{\sigma_{\jmath}}{\jmath}\right)^2+\left(\frac{\sigma_{N_{KCl}^{(1,2)}}}{N_{KCl}^{(1,2)}}\right)^2+\left(\frac{\sigma_{\tau_{^{40}K}}}{\tau_{^{40}K}}\right)^2}\]


\begin{minipage}[htbp]{\textwidth}
\begin{minipage}[htbp]{0.4\textwidth}
\centering
\captionof{table}{Risultati efficienza dei due campioni di KCl}
\begin{tabular}{c|c}
$\alpha^{(1)}\:\mathrm{[Bq]}$&17.5$\pm$0.2\\
$\alpha^{(2)}\:\mathrm{[Bq]}$&252$\pm$2\\
$\alpha_\mathit{tot} ^{(1)}\:\mathrm{[Bq]}$&166$\pm$2\\
$\alpha_\mathit{tot} ^{(2)}\:\mathrm{[Bq]}$&2400$\pm$20\\
$n_{\gamma, e} ^{(1)}$&52800$\pm$500\\
$n_{\gamma, e} ^{(2)}$&638000$\pm$6000\\
$n_{\gamma, r} ^{(1)}$&1550$\pm$40\\
$n_{\gamma, r} ^{(2)}$&7480$\pm$90\\
$\varepsilon ^{(1)}$&0.0293$\pm$0.0008\\
$\varepsilon ^{(2)}$&0.0117$\pm$0.0002\\
\end{tabular}
\label{tab:eff}
\end{minipage}
\begin{minipage}[htbp]{0.6\textwidth}
Il numero di fotoni rivelati associabili ai fotoni $\gamma_{1,0}$(Ar) si stima andando a calcolare l'area sottesa al fotopicco dei due spettri netti e si associa a questa stima l'errore 
\[\sigma_{n_{\gamma, r} ^{(1,2)}}=\sqrt{n_{\gamma, r} ^{(1,2)}}\] 
dato dalla statistica di Poisson. Si riportano nella tabella \ref{tab:eff} le stime ottenute. Si nota che i due valori di efficienza stimati non sono tra loro compatibili. Si suppone che ciò sia dovuto alla diversa posizione dei due campioni che determina un diverso angolo solido accettato dal rivelatore e dunque una variazione significativa sul numero di conteggi: infatti il campione 1 era stato addossato al rivelatore mentre il campione 2, molto più pesante è stato posto leggermente più in basso, staccato dal rivelatore.
\end{minipage}
\end{minipage}
\subsection{Stima della quantità di potassio nella cioccolata fondente}
Si vuole ora dare una stima della quantità di potassio presente in un campione di circa 200 g di cioccolata fondente da una misura di emissioni gamma  $\gamma_{1,0}$(Ar) relative al $^{40}$K ivi presente. Per farlo si effettua un'acquisizione dello spettro energetico gamma del campione di cioccolata e si procede alla sottrazione del fondo ambientale precedentemente acquisito. Si mostra in Fig.\ref{fig:choco} lo spettro della cioccolata al netto della sottrazione del fondo.  Si nota purtroppo che, essendo l'attività della cioccolata alquanto ridotta, si hanno pochi conteggi a disposizione e il picco netto risulta poco pulito: un risultato migliore si sarebbe ottenuto con un'acquisizione più lunga. Inoltre risulta non compatibile il valore in energia del centroide con la $E_\gamma$ per il $\gamma_{1,0}$(Ar), si pensa per lo stesso motivo appena discusso.

\begin{minipage}[htbp]{\textwidth}
\begin{minipage}{0.5\textwidth}
\centering
\captionof{figure}{Spettro energetico netto cioccolata }\label{fig:choco}
\includegraphics[width=0.95\linewidth]{../net_chocolate_hq0.eps}
\end{minipage}
\begin{minipage}{0.5\textwidth}
\centering
\captionof{table}{Risultati campione di cioccolata}
\begin{tabular}{c|c}
$M_\mathit{choco}\:\mathrm{[g]}$&200.60$\pm$0.03\\
$x_0\:\mathrm{[u.a.]}$&10030$\pm$20\\
$E_{\gamma}\:\mathrm{[keV]}$&1430$\pm$5\\
$\lambda$&6\\
$\Delta T_\mathit{choco}\:\mathrm{[s]}$&5028.2\\
$n_{\gamma, r}$&380$\pm$20\\
$n_{\gamma, e}$&13100$\pm$100\\
$M_\mathit{choco}^K\:\mathrm{[g]}$&0.78$\pm$0.05\\
\end{tabular}
\label{tab:choco}
\end{minipage}
\end{minipage}

La quantità di potassio presente nella cioccolata si ricava a partire dal numero di fotoni $\gamma_{1,0}$(Ar) emessi dalla sorgente che dà un'informazione sulla quantità di $^{40}$K e quindi sulla quantità totale di potassio presente nel campione. Il numero di fotoni emessi si ricava dal numero di quelli rivelati dal sistema di misura e dall'efficienza totale di fotopicco precedentemente stimata. Si è scelto di considerare l'efficienza $\varepsilon^{(1)}$ in quanto la cioccolata è stata disposta sul rivelatore in modo del tutto analogo al primo campione di KCl e quindi in condizioni geometriche in buona approssimazione equivalenti.
\[n_{\gamma, e} = \frac{n_{\gamma, r}}{\varepsilon ^{(1)}} \hspace{2cm} \sigma_{n_{\gamma, e}} \simeq n_{\gamma, e} \sqrt{\left(\frac{\sigma_{n_{\gamma, r}}}{n_{\gamma, r}}\right)^2+\left(\frac{\sigma_{\varepsilon ^{(1)}}}{\varepsilon ^{(1)}}\right)^2}\]
Si ricava quindi la massa di potassio contenuta nel campione in modo inverso a quanto effettuato nella sezione precedente con il KCl. 
\[M_{choco}^K = \frac{\tau_{^{40}K}n_{\gamma, e}m_K}{\jmath\Delta T_\mathit{choco}N_A p} \hspace{2cm} \sigma_{M_{choco}^K} \simeq M_{choco}^K \sqrt{\left(\frac{\sigma_{\tau_{^{40}K}}}{\tau_{^{40}K}}\right)^2+\left(\frac{\sigma_{\jmath}}{\jmath}\right)^2+\left(\frac{\sigma_{n_{\gamma, e}}}{n_{\gamma, e}}\right)^2}\]
La stima della percentuale di potassio presente nel capione di cioccolata è quindi data da
\[\mathcal{P} = \frac{M_{choco}^K}{M_{choco}} \hspace{3cm} \sigma_{\mathcal{P}}\simeq\mathcal{P}\sqrt{\left(\frac{\sigma_{M_{choco}^K}}{M_{choco}^K}\right)^2+\left(\frac{\sigma_{M_{choco}}}{M_{choco}}\right)^2}\]
Si ottiene il valore
\[\mathcal{P}= 0.38\pm 0.02 \:\%\]
che risulta coerente con i valori noti di potassio nella cioccolata fondente.
\\
\subsection{Misure in coincidenza con $^{60}$Co e stima della risoluzione temporale}
I fotoni gamma caratteristici  $\gamma_{3,1}$(Ni) e $\gamma_{1,0}$(Ni) da 1173 keV e 1332 keV del decadimento del $^{60}$Co sono emessi praticamente in simultanea e in direzioni casuali essendo la vita media del terzo e del primo stato eccitato del $^{60}$Ni rispettivamente di 0.3 ps e 0.7 ps. Utilizzando due rivelatori, denominati A (quello già utilizzato in precedenza) e B, per studiare i decadimenti della sorgente, si può in alcuni casi osservare due fotoni che giungono uno nel primo rivelatore e l'altro nel secondo: queste misure si dicono \emph{in coincidenza}. Attraverso un'opportuna macro di analisi di ROOT si potranno distinguere gli eventi in coincidenza temporale ovvero tali che i fotoni si possano considerare rivelati simultaneamente; con una discriminazione in energia poi si possono riconoscere gli eventi in cui un fotone caratteristico è rivelato da A e l'altro da B in coincidenza e che quindi provengono in ottima approssimazione dal decadimento dello stesso nucleo di $^{60}$Co. É importante notare che la sorgente è stata posta a ridosso del rivelatore A per cui, pur essendo i rivelatori identici per costruzione (rivelatori NaI(Tl) da $3'' \times 3''$), la situazione sarà asimmetrica riguardo l'acquisizione temporale dei segnali. 
Si mostrano in Fig.\ref{fig:coinc0} e Fig.\ref{fig:coinc1}  gli spettri ottenuti dai due rivelatori. Si notano delle differenze importanti dovute alla diversa scala in energia tra il rivelatore A e B: per questo motivo si ha un comportamento diverso alle basse energie per il rivelatore B che non presenta un Compton continuum uniforme ma solamente la spalla. Invece si può dire che la differenza di accettanza per i due rivelatori non influisce significativamente sulla forma degli spettri.
\\

\begin{minipage}[htbp]{\textwidth}
\begin{minipage}{0.5\textwidth}
\centering
\captionof{figure}{Spettro energetico misure in coincidenza rivelatore A}\label{fig:coinc0}
\includegraphics[width=0.95\linewidth]{/home/ale/Laboratorio/laboratorio2/N3.Gamma/Robacxx/AnalisiCoincidenze/coinc_hq0.eps}
\end{minipage}
\hfill
\begin{minipage}{0.5\textwidth}
\centering
\captionof{figure}{Spettro energetico misure in coincidenza rivelatore B}\label{fig:coinc1}
\includegraphics[width=0.95\linewidth]{/home/ale/Laboratorio/laboratorio2/N3.Gamma/Robacxx/AnalisiCoincidenze/coinc_hq1.eps}
\end{minipage}
\end{minipage}

Si acquisiscono quindi i campioni dei singoli segnali generati dai PMT relativi ai due rivelatori (su due canali distinti) successivamente inviati al computer dal digitizer. I segnali, mostrati in Fig.\ref{fig:sgn}, hanno approssimativamente la forma $V\simeq -V_0 t^3 e^{-t/\tau}$ per $t>0$ con $V_0$ proporzionale all'energia del fotone rivelato: si stima un tempo di salita del segnale di $ \simeq 280 $ ns ovvero 70 clock e un tempo di rilassamento $\tau\simeq 400 $ ns corrispondente a 100 clock.
La scheda del digitizer ha un trigger comune ai due canali determinato dalla presenza in coincidenza dei due gamma entro un certo intervallo di tempo in unità di clock, ovvero 4 ns, e quindi i segnali dei due rivelatori hanno lo stesso tempo di riferimento (\emph{timetag}) determinato però dal trigger di uno dei due segnali. Si escludono preliminarmente i segnali in cui non c'è corrispondenza tra i timetag dei due canali: questi eventi non sono in coincidenza. 
\\

\begin{minipage}{\textwidth}
\centering
\captionof{figure}{Segnali nei canali A e B coi colori corrispondenti allo stesso evento in coincidenza}\label{fig:sgn}
\includegraphics[width=0.5\linewidth]{/home/ale/Laboratorio/laboratorio2/N3.Gamma/Robacxx/AnalisiCoincidenze/coinc_waveforms.eps}
\end{minipage}

Si sono poi graficati gli eventi di coincidenza in un istogramma bidimensionale $\left(E_\gamma^A,E_\gamma^B\right)$ come riportato in Fig.\ref{fig:bidim}. Si riconoscono le seguenti strutture: le coppie di fasce rettilinee con energie comprese circa in $\left(4500<E_\gamma^A<5500\right)$ u.a. e $\left(8000<E_\gamma^A<9500\right)$ u.a. corrispondono alle coincidenze dei due gamma caratteristici di un rivelatore con l'effetto Compton dell'altro, la zona a bassa energia per entrambi i rivelatori corrisponde alle coincidenze di low energy peak e Compton continuum di entrambi i rivelatori e infine le zone approssimativamente circolari per le energie contemporaneamente di circa $\left(4500<E_\gamma^A<5500\right)$ u.a. e $\left(8000<E_\gamma^A<9500\right)$ u.a. corrispondono ai fotoni caratteristici del decadimento di uno stesso nucleo. Si notano anche, seppur debolmente, le spalle Compton ($E_\gamma^A \simeq 7000$ u.a. e $E_\gamma^B \simeq 3700$ u.a.). Si nota infine una zona con pochi eventi in coincidenza a bassa energia per il rivelatore B e in corrispondenza dei picchi per il rivelatore A.

Inoltre si sono graficati in Fig.\ref{fig:coincpicchi} gli istogrammi degli spettri energetici del rivelatore A in coincidenza con il fotopicco corrispondente al fotone di energia 1332 keV del rivelatore B e viceversa: si nota anche in questo caso l'effetto della diversa scala in energia del rivelatore B (calibrato diversamente rispetto al rivelatore A) e lo scarso numero di eventi a basse energie in corrispondenza del fotopicco di A. In entrambi i grafici sono presenti un fotopicco relativo ai fotoni  $\gamma_{3,1}$(Ni) di energia 1173 keV provenienti dallo stesso nucleo di quelli $\gamma_{1,0}$(Ni) da 1332 keV rivelati dall'altro detector e nel primo, ad energie superiori, un picco meno intenso che corrisponde alla rivelazione di due fotoni $\gamma_{1,0}$(Ni), prodotti quindi da due nuclei diversi.
\\

\begin{minipage}[htbp]{\textwidth}
\begin{minipage}{0.5\textwidth}
\centering
\captionof{figure}{Spettri energetici A e B in coincidenza con il fotopicco a 1332 keV}\label{fig:coincpicchi}
\includegraphics[width=\linewidth]{/home/ale/Laboratorio/laboratorio2/N3.Gamma/Robacxx/AnalisiCoincidenze/energy_coinc.eps}
\end{minipage}
\hfill
\begin{minipage}{0.5\textwidth}
\centering
\captionof{figure}{Bidimensionale $E_\gamma^A$ vs $E_\gamma^B$}\label{fig:bidim}
\includegraphics[width=0.95\linewidth]{/home/ale/Laboratorio/laboratorio2/N3.Gamma/Robacxx/AnalisiCoincidenze/energy2D.eps}
\end{minipage}
\end{minipage}

Si vuole infine effettuare un'analisi precisa dei tempi dei diversi segnali per determinare la risoluzione temporale dell'apparato.
Si cerca di definire l'istante di inizio del segnale in modo più preciso possibile rispetto alla variabilità in ampiezza del segnale che è proporzionale all'energia depositata dal fotone nel rivelatore. Si vuole quindi determinare con precisione questo istante per i due segnali provenienti dai due rivelatori A e B e per farlo si procede con due accorgimenti:
\begin{enumerate}\setlength{\itemsep}{0pt}
\item Si definisce un valore di soglia $V^*$ per il segnale in tensione (ADC counts) variabile e proporzionale all'energia: si imposta una soglia relativa al $50\%$ del massimo del segnale che è definito in negativo rispetto ad un valore di baseline. Questo accorgimento dovrebbe ridurre il cosiddetto \emph{time jitter} ovvero la deviazione del segnale da un andamento periodico in riferimento al clock, che è un effetto indesiderato delle linee di trasmissione.
\item Si determina l'istante di inizio dei segnali come l'istante di superamento $t^*$ della soglia $V(t)=V^*$ sopra definita: si traccia un segmento che congiunge i due punti $(t_1,V_1)$ e $(t_2,V_2)$ corrispondenti rispettivamente al clock successivo e precedente al superamento del valore $V^*$. Si trova quindi 
\[t^* = t_1 + \left(\frac{V^* - V_1}{V_2 - V_1}\right)\left(t_2 - t_1\right)\]


\end{enumerate}
L'algoritmo è stato quindi implementato sulla macro di ROOT e si è calcolata la differenza temporale $\Delta t = t_A ^* - t_B ^*$ dal quale si va a stimare 
la risoluzione temporale dell'apparato come la larghezza a metà altezza del picco di coincidenza dell'istogramma dei conteggi delle differenze di tempo. Si nota un'evidente asimmetria dell'istogramma verso valori negativi di $\Delta t$ il che vuol dire che in media $t_B^*$ è maggiore di $t_A^*$ e quindi i segnali del rivelatore B  presentano un offset rispetto a quelli del rivelatore A: si suppone che ciò sia dovuto alla posizione del campione a ridosso del rivelatore A il che implica un diverso tempo di volo del fotone verso il rivelatore. Un calcolo veloce infatti mostra che  una differenza di cammino di circa 20 cm implica un ritardo di impatto del fotone di circa 0.7 ns. Nonostante questa asimmetria si è provveduto a fittare l'istogramma con una gaussiana al fine di stimare la risoluzione temporale $\Delta t^*$dalla larghezza a metà altezza $\mathrm{FWHM=\sqrt{8\ln2}}\,\sigma$. Si è ottenuta quindi la stima
\[\Delta t^*=  (8.36 \pm 0.02)  \:\mathrm{ns}\]
Questa stima è stata ricavata da tutti gli eventi in coincidenza che però non sono associati necessariamente al decadimento dello stesso nucleo e quindi all'emissione simultanea dei fotoni. Una migliore definizione della risoluzione temporale si può quindi ricavare considerando solo gli eventi propriamente in coincidenza in cui i fotoni caratteristici giungono uno su un rivelatore e uno sull'altro ovvero gli eventi attorno ai fotopicchi incrociati visibili in Fig.\ref{fig:bidim}. Si è deciso quindi di considerare solo gli eventi compresi in un range energetico di $2\sigma$ attorno ai centroidi dei fotopicchi degli spettri. Si nota, in Fig.\ref{fig:diffsing}, asimmetrie meno marcate rispetto all'istogramma in Fig.\ref{fig:difftot}. Si sono ottenute le altre due stime 
\[\Delta t^*_{A,B} = (5.83 \pm 0.04) \:\mathrm{ns} \hspace{3cm}\Delta t^*_{B,A}= (5.94 \pm 0.04) \:\mathrm{ns}\]
Essendo queste due stime discretamente compatibili ($\lambda=1.9$), si è deciso di calcolarne la media pesata e di offrire quest'ultima come stima della risoluzione temporale delle misure in coincidenza
\[\Delta \tilde{t} = (5.88 \pm 0.03)\:\mathrm{ns}\]

\begin{minipage}[htbp]{\textwidth}
\begin{minipage}{0.45\textwidth}
\centering
\captionof{figure}{Istogramma differenze di tempo $\Delta t$ di tutti gli eventi in coincidenza}\label{fig:difftot}
\includegraphics[width=\linewidth]{/home/ale/Laboratorio/laboratorio2/N3.Gamma/Robacxx/AnalisiCoincidenze/time_resolution.eps}
\end{minipage}
\hfill
\begin{minipage}{0.55\textwidth}
\centering
\captionof{figure}{Istogramma differenze di tempo $\Delta t$ relativi ai gamma caratteristici dello stesso nucleo : a sinistra i fotoni $\gamma_{3,1}$(Ni) in A e $\gamma_{1,0}$(Ni) in B, a destra il viceversa}\label{fig:diffsing}
\includegraphics[width=0.95\linewidth]{/home/ale/Laboratorio/laboratorio2/N3.Gamma/Robacxx/AnalisiCoincidenze/time_resolution_peaks.eps}
\end{minipage}
\end{minipage}
\section{Conclusioni}
\begin{enumerate}
\item Per quanto riguarda la calibrazione in energia del sistema di rivelazione di raggi gamma tramite sorgenti note di $^{60}$Co e $^{22}$Na si sono ottenuti, tramite un fit lineare non pesato, i parametri $A= 11 \pm 7 \: \mathrm{keV}$ e $B= 0.1416 \pm 0.0007\: \mathrm{keV/u.a.}$.
\item Tramite un confronto con le energie note in letteratura dei raggi gamma emessi da una sorgente di $^{152}$Eu e di $^{137}$Cs si è verificata la bontà della calibrazione, che si è rivelata non ottimale ma sufficiente ad individuare le energie dei picchi date le posizioni dei centroidi.
\item Tramite la rivelazione di emissioni gamma di campioni contenenti $^{40}$K si sono ottenute due stime per l'efficienza totale di fotopicco che variano in modo significativo a causa della diversa posizione del campione nel pozzetto che determina una diversa accettanza: $\varepsilon ^{(1)}=0.0293\pm0.0008$ e
$\varepsilon ^{(2)}=0.0117\pm0.0002$.
\item Da una analisi dello spettro della radiazione gamma emessa da un campione di cioccolata fondente si è stimata la percentuale di potassio in esso presente che è risultata coerente con il valore noto in letteratura: $\mathcal{P}= 0.38\pm 0.02 \:\%$.
\item Per quanto riguarda la stima della risoluzione temporale del rivelatore tramite misure di coincidenza con la sorgente di $^{60}$Co si è ricavato il valore di $\Delta \tilde{t} = (5.88 \pm 0.03)\:\mathrm{ns}$ relativo agli eventi corrispondenti alla rivelazione di un gamma caratteristico nel rivelatore A e l'altro nel rivelatore B e quindi provenienti dallo stesso nucleo.
\end{enumerate}
\end{document}





\item La calibrazione in energia del sistema di rivelazione di raggi gamma tramite sorgenti note di $^{60}$Co e $^{22}$Na.
\item Il riconoscimento delle energie dei raggi gamma emessi da una sorgente di $^{152}$Eu e di $^{137}$Cs.
\item La stima dell'efficienza di fotopicco da emissioni gamma di campioni contenenti $^{40}$K.
\item La stima della quantità di potassio presente in un campione di cioccolata fondente.
\item La stima della risoluzione temporale del rivelatore tramite misure di coincidenza con la sorgente di $^{60}$Co.









MINITODO
Spiegare l'algoritmo della macro      FATTO
Commentare l'istogramma ottenuto con l'algoritmo      1)
Commentare gli spettri totali e in coincidenza con le loro diversità in forma      2)
Commentare il bidimensionale    FATTO
Commentare gli spettri in coincidenza coi picchi di 1.17 e 1.33 MeV   FATTO
Commentare e fittare l istogramma temporale con condizione in energia nelle due macchiette

Stima finale risoluzione temporale


