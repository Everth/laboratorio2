#include <Riostream.h>
#include <stdlib.h>
#include <TROOT.h>
#include <TSystem.h>
#include "TFile.h"
#include "TTree.h"
#include "TNtuple.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TH1D.h"
#include "TF1.h"
#include "TFitResult.h"
#include "TRint.h"
#include "TString.h"
#include "AnalysisInfo.h"
#include "TGaxis.h"
#include "TStyle.h"
#include <iostream>
#include <fstream>
#include <string>
#include <memory>
#include <iterator>
#include <algorithm>
#include <tuple>
#include <unordered_map>
#include <map>
#include <utility>
#include <iomanip>

//compile g++ -Wall -I. -I `root-config --cflags` -o Fit_coinc.out Fit_coinc.cxx AnalysisInfo.cc `root-config --libs` && ./Fit_coinc.out fit ranges_coinc conversion calibrationparams_unw 

static constexpr int min_events = 10;
static constexpr double n_bins = 25;

struct FitInfoChannels{
	std::string isotope_name;
	int viewfrom;
	int viewto;
	int fitfrom;
	int fitto;
	std::string branch;
	void print(std::ostream& out=std::cerr){ out << isotope_name << "\t" << viewfrom << "\t" << viewto  << "\t" << fitfrom << "\t" << fitto  << "\t" << branch << std::endl;}
	friend std::istream& operator>>(std::istream &is, FitInfoChannels &fitinfo);
};

std::istream& operator>>(std::istream &is, FitInfoChannels &fitinfo){
	is >> fitinfo.isotope_name >> fitinfo.viewfrom >> fitinfo.viewto >> fitinfo.fitfrom >> fitinfo.fitto >> fitinfo.branch;
	return is; //si può compattare in return is >> ecc >> ecc
}

void ScaleXaxis(TH1D* histoch, TF1* conversionfunction, std::string convertedlabel){ //La funzione deve essere lineare
	TAxis *newaxis = histoch->GetXaxis(); //ty Pepe_Le_Pew
	if (newaxis->GetXbins()->GetSize()) { 
		//Axis with variable bins
		TArrayD X(*(newaxis->GetXbins()));
		for(Int_t i = 0; i < X.GetSize(); i++) X[i] = conversionfunction->Eval(X[i]);
		newaxis->Set((X.GetSize() - 1), X.GetArray()); //new Xbins
	} else {
		//Axis with fixed bins
		newaxis->Set( newaxis->GetNbins(),
		conversionfunction->Eval(newaxis->GetXmin()), //new Xmin
		conversionfunction->Eval(newaxis->GetXmax()) ); //new Xmax
	}
	histoch->ResetStats();
	newaxis->SetTitle(convertedlabel.c_str());
}

struct TableFormat {
    std::ostream& outs;
    int width;
    char fill;
    TableFormat(std::ostream& out=std::cerr,int width=15, char fill=' '): outs(out), width(width), fill(fill) {}
    template<typename T>
    TableFormat& operator<<(const T& data) {
        outs << std::setw(width) << data << std::setw(width) << std::setfill(fill);
        return *this;
    }
    TableFormat& operator<<(std::ostream&(*out)(std::ostream&)) {
        outs << out;
        return *this;
    }
};

int main(int argc, char** argv){
	
	AnalysisInfo* info = new AnalysisInfo( argc, argv );
	//da dare fit ranges conversion conversionparams. Servono limiti sull'energia e/o sul binning?
	std::string inputfile = "anal_Co-60_coinc_sgn.root";
	std::string name = "Co-60_coinc_sgn";
	
	int fakeargc=1; char* fakeargv[fakeargc];
	TRint *theApp = new TRint("App", &fakeargc, fakeargv,0,0,1);
	
	TF1 *conversionfunction = nullptr;
	if(info->contains("conversion")){
		conversionfunction = new TF1("conversion", "[0]+[1]*x");
		std::ifstream userparamsfile(info->value("conversion")); //la funzione la so, mi servono solo i due parametri A e B (*x)
		std::istream_iterator<double> iit(userparamsfile),eos;
		double userparams[10]; // L'hanno definita con 10 parametri a prescindere quindi non posso sbagliare
		std::copy(iit,eos, userparams);
		conversionfunction->SetParameters(userparams);
	}
	
	if (info->contains("fit")){
		std::ifstream fitranges;
		fitranges.open(info->value("fit") ,std::ios::in); 
		if(!fitranges.is_open()){
			std::cerr << info->value("fit") << " not opened!" << std::endl;
			return -1;
		}
		
		std::vector<FitInfoChannels> ranges;
		std::istream_iterator<FitInfoChannels> start(fitranges), end;
		std::copy(start, end, std::back_inserter(ranges));
		for (auto i : ranges) i.print(); //overkill
		
		gStyle->SetOptStat(kTRUE);
		gStyle->SetOptFit(1111);
		
		std::vector<TFitResultPtr> results;
		
		TFile* infile = TFile::Open(inputfile.c_str(),"READ");
		TNtuple* datatuple = nullptr;
		infile->GetObject("nt",datatuple);
		if (!datatuple) {std::cerr << "Non hai accesso all'ntupla!" << std::endl; return -1;}
		std::for_each(ranges.begin(), ranges.end(), [&](const FitInfoChannels& limits){
			static std::unordered_map<std::string, unsigned> peak_dictionary;
			std::string peak_id = limits.isotope_name + "-" + limits.branch;
			++peak_dictionary[peak_id];
			std::string call_count(std::to_string(peak_dictionary[peak_id]));
			std::string condensed_name(peak_id + "_" + call_count);
			TString cname = Form("%s",condensed_name.c_str());
			TString hname = Form("histo_%s",cname.Data());
			std::string outfilename = Form("%s.root",cname.Data());
			std::string outimgname = Form("%s.eps",cname.Data());
			auto outfile = std::make_shared<TFile>( outfilename.c_str() ,"RECREATE"); //Output file
			TCanvas* fitcanvas = new TCanvas( Form("canvas_%s",cname.Data()));
			double binwidth = (limits.viewto-limits.viewfrom)/::n_bins;
			TH1D* fithisto = new TH1D( hname , hname, binwidth, limits.viewfrom, limits.viewto);
			std::cerr << "SOOOPRA IL FILO" << std::endl;
			datatuple->Draw(Form("%s >> %s",limits.branch.c_str(),hname.Data()) );
			outfile->cd();
			fithisto->SetLineColor(kBlue);
			fithisto->SetLineStyle(1);
			std::cerr << "DI UNA RAGNATELA" << std::endl;
			TF1 *func = new TF1(Form("func_%s",cname.Data()),"gaus", limits.fitfrom, limits.fitto); // fit sempre gaussiani
			std::cerr << "EI RITENENDO IL GIOCO INTERESSANTE" << limits.fitfrom << '\t' << limits.fitto << std::endl;
			gStyle->SetOptFit(1111);
			TFitResultPtr res = fithisto->Fit(func,"RS");
			fithisto->Draw(); //questa devo capire perché non me la faceva funzionare molto spesso. In ogni caso devo mettere la conversione da ste parti
			fitcanvas->Write();
			fithisto->Write();
			func->Write();
			res->Print("V");
			res->Write();
			fitcanvas->SaveAs(outimgname.c_str(),"eps");
			results.push_back(res);
			outfile->Close();
			delete fitcanvas;
		});
		
		if(info->contains("conversion")){
			std::ofstream fitresult("fit_results_coinc");
			//double interc =conversionfunction->GetParameter(0);
			double pendenza=conversionfunction->GetParameter(1);
			TableFormat fitresulttable(fitresult,25,' ');
			fitresulttable << "Element name" << "Centroide [u.a]" << "Centroide [keV]" << "Sigma [u.a]"<<"Sigma [keV]" << "Errore centroide [u.a]"<<"Errore centroide [keV]" << "Semiampiezza [u.a.]" <<"Semiampiezza [keV]"<< std::endl;
			for (unsigned i = 0; i<ranges.size(); ++i){
				TMatrixDSym cov = results[i]->GetCovarianceMatrix();
				fitresulttable << ranges[i].isotope_name << results[i]->GetParams()[1] << conversionfunction->Eval(results[i]->GetParams()[1])<< results[i]->GetParams()[2] << pendenza*results[i]->GetParams()[2]<< TMath::Sqrt(cov(1,1))<< pendenza*(TMath::Sqrt(cov(1,1))) << sqrt(2*log(2))*results[i]->GetParams()[2] << pendenza*sqrt(2*log(2))*results[i]->GetParams()[2] << std::endl;
			}
		}
	}
	theApp->Run();
	return 0;
}


