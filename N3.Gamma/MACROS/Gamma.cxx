#include <Riostream.h>
#include <stdlib.h>
#include <TROOT.h>
#include <TSystem.h>
#include "TFile.h"
#include "TTree.h"
#include "TNtuple.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TH1D.h"
#include "TF1.h"
#include "TFitResult.h"
#include "TRint.h"
#include "TLegend.h"
#include "TStyle.h"
#include "AnalysisInfo.h"
#include "TGaxis.h"
#include <iostream>
#include <fstream>
#include <string>
#include <memory>
#include <iterator>
#include <algorithm>
#include <unordered_map>
#include <utility>

//compile g++ -Wall -I. -I `root-config --cflags` -o Gamma.out Gamma.cxx AnalysisInfo.cc `root-config --libs`
//need inputfiles list = "Co-60 Na-22 (bg_K-40 KCl_K-40)", ecc
// if fit or calibrate ranges = "Co-60	7600	8700	7950	8500 1 1173", ecc
//run ./Gamma.out  inputfiles list  visualize 
////-> manually write fit range parameters to files specifying wheter or not to use them as calibration and eventually their expected energy value and save img
//run ./Gamma.out inputfiles test conversion calibrationparams visualize
// element_name, viewfrom, viewto, fitfrom, fitto, calibrate, expected_value; per ogni fit
//////A questo punto vorrei usare un altro programma (part 2?) per normalizzare e sottrarre il fondo ambientale del K-40 al KCl. Serve inserire dati stechiometrici per efficienza fotopicco. In questo programma ci può essere anche la parte del cioccolato!

static constexpr int min_events = 10;

struct Calibration{
	bool calibrate;
	double expected;
	double expected_err;
	friend struct FitInfo;
	friend std::istream& operator>>(std::istream&, Calibration&);
};

std::istream& operator>>(std::istream &is, Calibration &calibration){
	is >> calibration.calibrate >> calibration.expected >> calibration.expected_err;
	return is;
}
	

struct FitInfo{
	std::string isotope_name;
	int viewfrom;
	int viewto;
	int fitfrom;
	int fitto;
	Calibration compare;
	//Non printo compare perché mi siddia
	void print(std::ostream& out=std::cerr){ out << isotope_name << "\t" << viewfrom << "\t" << viewto  << "\t" << fitfrom << "\t" << fitto << std::endl;}
	//friend std::ostream& operator<<(std::ostream &os, FitInfo &fitinfo);
	friend std::istream& operator>>(std::istream &is, FitInfo &fitinfo);
};

std::istream& operator>>(std::istream &is, FitInfo &fitinfo){
	is >> fitinfo.isotope_name >> fitinfo.viewfrom >> fitinfo.viewto >> fitinfo.fitfrom >> fitinfo.fitto >> fitinfo.compare;
	return is;
}



std::pair<TFitResultPtr,Calibration> fitmany(FitInfo limits, bool draw=true){
		static std::unordered_map<std::string, unsigned> peak_dictionary;
		std::string element_name = limits.isotope_name; //confonde un po' ma non so che isotopo è il cioccolato quindi jamme ja
		++peak_dictionary[element_name];
		std::string rootname = element_name + ".root";
		std::string call_count(std::to_string(peak_dictionary[element_name]));
		
		std::string condensed_name(element_name + "_" + call_count);
		
		std::string histo(std::string("histo_") + condensed_name), 
								canvas(std::string("canvas_") + condensed_name),
								fitfunc(std::string("fitfunc_") + condensed_name);
		std::string outfilename = condensed_name + ".root";
		std::string outimgname = condensed_name + ".png";
		auto outfile = std::make_shared<TFile>(outfilename.c_str(),"RECREATE"); // qua creo il root file
		TCanvas* fitcanvas = new TCanvas(canvas.c_str());		
		TH1D* fithisto = (TH1D*)(new TFile(rootname.c_str()))->Get("hq0")->Clone(); //ATTENZIONE HQ0 DOVREBBE ESSERE ENTRAMBI I CANALI!! Fare come sotto
		outfile->cd(); //Menomale che li apro in sola scrittura i file.. credo.. lo faccio?
		fithisto->SetLineColor(kBlue);
		fithisto->SetLineStyle(1);
		fithisto->SetName(histo.c_str());
		fithisto->SetTitle(histo.c_str());
		fithisto->GetXaxis()->SetRangeUser(limits.viewfrom, limits.viewto); 
		TF1 *func = new TF1(fitfunc.c_str(),"gaus", limits.fitfrom, limits.fitto); // fit sempre gaussiani
		TFitResultPtr res = fithisto->Fit(func,"S");
		if (draw) fithisto->Draw(); //questa devo capire perché non me la faceva funzionare molto spesso. In ogni caso devo mettere la conversione da ste parti
		fitcanvas->Write();
		fithisto->Write();
		func->Write();
		res->Print("V");
		res->Write();
		fitcanvas->SaveAs(outimgname.c_str());
		outfile->Close();
		return std::make_pair(res,limits.compare);
}

int main(int argc, char** argv){
	
	AnalysisInfo* info = new AnalysisInfo( argc, argv );
	//argc : inputfiles (Co-60, Na-22, (Kcl,cioccolato - fondo), altro) mode (visualize*, fit, calibrate, coincidence) units (default, energy (given calibration file))
	std::string inputfiles;
	std::vector<std::string> files; //SE CI AGGIUNGO .ROOT ALLA FINE.....
	std::string &ifs = inputfiles;
	
	if (info->contains("bgfix")){
		
		int fakeargc=0; char* fakeargv[fakeargc];
		TRint *theApp = new TRint("App", &fakeargc, fakeargv,0,0,1);
		std::cerr << "un elefante "<< std::endl;
		TFile* bgfix = new TFile("net_K-40.root","RECREATE");
		bgfix->cd();
		
		std::cerr << "si dondolava" << std::endl;
		
		auto kcl = std::make_shared<TFile>("KCl.root","READ");
		auto kcl_nt = static_cast<TNtuple*>(kcl->Get("nt"));
		std::cerr << "sopra il filo" << std::endl;
		kcl_nt->Draw("time>>temphist1"); TH1F *kcl_time_histo = (TH1F*)gDirectory->Get("temphist1");
    double kcl_time = kcl_time_histo->Integral();
    std::cerr << kcl_time << std::endl;
    std::cerr << "di una ragnatela" << std::endl;
    bgfix->cd();
    auto kcl_spectrum = static_cast<TH1D*>(kcl->Get("hq0"));
    kcl_spectrum->Write();
    //kcl->Close();
    
    std::cerr << "trovando la cosa" << std::endl;
    auto bkg = std::make_shared<TFile>("bkg.root","READ");
		auto bkg_nt = static_cast<TNtuple*>(bkg->Get("nt"));
		std::cerr << "molto interessante" << std::endl;
		bkg_nt->Draw("time>>temphist2"); TH1F *bkg_time_histo = (TH1F*)gDirectory->Get("temphist2");
    double bkg_time = bkg_time_histo->Integral();
    std::cerr << "andò a chiamare" << std::endl;
    auto bkg_spectrum = static_cast<TH1D*>(bkg->Get("hq0"));
    bgfix->cd();
    std::cerr << "un altro elefante..." << std::endl;
    bkg_spectrum->Scale(kcl_time/bkg_time); //da manuale cambia anche gli errori quando ridimensiona
    std::cerr << "Due elefanti" << std::endl;
    bkg_spectrum->Write();
    //bkg->Close();
    
		bgfix->cd();
		std::cerr << "si dondolavano.." << std::endl;
		auto hnet = static_cast<TH1D*>(kcl_spectrum->Clone());
		std::cerr << "tanto qua non ci arriva" << std::endl;
		hnet->Sumw2(1); // setta il calcolo automatico degli errori statistici. Vorrei fare TH1::Sumw2(kTrue) se è static func
		//clone->SetStats("irmen");
		hnet->SetLineColor(kBlue);
		hnet->SetTitle("Spettro KCl al netto della sottrazione del fondo");
		hnet->Add(bkg_spectrum,-1.); // sottrae il background
		hnet->Draw();
		hnet->Write();
		//bgfix->SaveAs("nonhocapito.png");
		bgfix->Write(); // salva il root file
		theApp->Run();
	}
	
	if (info->contains("inputfiles")) {
		ifs = info->value("inputfiles");
		std::ifstream elements;
		elements.open(ifs, std::ios::in);
		if(!elements.is_open()){
			std::cerr << "Input file name is wrong or it couldn't be opened" << std::endl;
			return -1;
		} else {
			std::istream_iterator<std::string> start(elements), end;
			std::copy(start,end,std::back_inserter(files));
		}
	} else {
		std::cerr << "Input file is missing. No data will be analyzed." << std::endl;
		return -1;
	}
	
	TF1 *conversionfunction = nullptr;
	if(info->contains("conversion")){
		conversionfunction = new TF1("conversion", "[0]+[1]*x");
		std::ifstream userparamsfile(info->value("conversion")); //la funzione la so, mi servono solo i due parametri A e B (*x)
		std::istream_iterator<double> iit(userparamsfile),eos;
		double userparams[10]; // L'hanno definita con 10 parametri a prescindere quindi non posso sbagliare
		std::copy(iit,eos, userparams);
		conversionfunction->SetParameters(userparams);
	}
	
	
	if (info->contains("visualize")){
		for(const auto &file : files) { //not sure, forse glielo do per copia
			int fakeargc=0; char* fakeargv[fakeargc];
			TRint *theApp = new TRint("App", &fakeargc, fakeargv,0,0,1);
			std::string rootfilename = file + ".root";
			//Qui è dove devo estrarre ciò che mi serve.. https://root.cern.ch/root/html534/guides/users-guide/InputOutput.html
			TFile* rootfile = TFile::Open(rootfilename.c_str(), "READ"); //non so se sono hq0 e hq1 in tutti i casi o ci sono 2 e 3: check?
			gStyle->SetOptStat(kTRUE);
			gStyle->SetOptFit(1111);
			if (rootfile->IsZombie()) return -2; //il file c'è ma non è un root file
			
			TIter next(rootfile->GetListOfKeys()); //NOTA IMPORTANTE: DEVO FARE UNA FACTORY DI TRINT LO SO CHE L'HO SCRITTO 6 VOLTE
			TKey* key;
			while((key=(TKey*)next())){
				if( strncmp(key->GetClassName(),"TH1",3) == 0 ){ //o th1f 4
					std::string histname = key->GetName();
					auto histoch = (TH1D*)(rootfile->Get(key->GetName()));
					histoch->SetDirectory(0);
					if( histoch->GetEntries() > ::min_events ){ //limite minimo di eventi per stampare/analizzare l'istogramma
						TCanvas *canvasch = new TCanvas( (std::string(histname + "canvas")).c_str(), histname.c_str());
						histoch->Draw();
						if(info->contains("conversion")){
							//histoch->GetXaxis()->SetLabelSize(0);
							//histoch->GetXaxis()->SetTickLength(0);
							TAxis *newaxis = histoch->GetXaxis();
							if (newaxis->GetXbins()->GetSize()) { //Code snippet by Pepe_Le_Pew
								// an axis with variable bins
								// note: bins must remain in increasing order, hence the "Scale"
								// function must be strictly (monotonically) increasing
								TArrayD X(*(newaxis->GetXbins()));
								for(Int_t i = 0; i < X.GetSize(); i++) X[i] = conversionfunction->Eval(X[i]);
								newaxis->Set((X.GetSize() - 1), X.GetArray()); // new Xbins
								} else {
									// an axis with fix bins
									// note: we modify Xmin and Xmax only, hence the "Scale" function
									// must be linear (and Xmax must remain greater than Xmin)
									newaxis->Set( newaxis->GetNbins(), 
									conversionfunction->Eval(newaxis->GetXmin()), // new Xmin
									conversionfunction->Eval(newaxis->GetXmax()) ); // new Xmax
								}
								histoch->ResetStats();
								newaxis->SetTitle("Energy [MeV]");
						} else { 
							histoch->SetXTitle("Energy [a.u.]");
						}
						histoch->SetYTitle("Counts");
						histoch->BufferEmpty();
						canvasch->Modified();
						canvasch->Update();
						canvasch->SaveAs( std::string(file + "_" + histname + ".png").c_str() );
					} else {
						std::cerr << "Histogram " << histname << " of file " << rootfilename << " has less than " << ::min_events << " events. Not drawn." << std::endl;
					}
				}
			}
			
			/* QUESTO è QUELLO CHE C'è SOPRA PERò MEGLIO
			auto channel0 = (TH1D*)(rootfile->Get("hq0")); //ATTENZIONE QUI AI NOMI DEI CANALI
			TCanvas *c0 = new TCanvas("c0","Channel 0"); //default size
			channel0->Draw();
			channel0->SetXTitle("Energy [a.u.]"); // oppure convertita in keV se c'è l'opzione..
			channel0->SetYTitle("Counts");
			c0->Modified();
			c0->SaveAs( std::string(file + "_ch0" + ".png").c_str() );
			
			
			auto channel1 = (TH1D*)(rootfile->Get("hq1")); //ANCHE QUI
			TCanvas *c1 = new TCanvas("c1","Channel 1");
			channel1->Draw();
			channel1->SetXTitle("Energy [a.u.]");
			channel1->SetYTitle("Counts");
			c1->Modified();
			c1->SaveAs( std::string(file + "_ch1" + ".png").c_str() );
			*/
			theApp->Run(kTRUE);
		}
	}
	
	//per fit e calibrate, mi serve view prima
	if ((info->contains("fit")) or (info->contains("calibrate")) ){
		std::ifstream fitranges;
		fitranges.open(info->value("fit") ,std::ios::in); 
		if(!fitranges.is_open()){
			std::cerr << info->value("fit") << " not opened!" << std::endl;
			return -1;
		}
		
		std::vector<FitInfo> ranges;
		std::istream_iterator<FitInfo> start(fitranges), end;
		std::copy(start, end, std::back_inserter(ranges));
		for (auto i : ranges) i.print();


		//MI SERVE UNA FACTORY DI TRINT VI PREGO
		int fakeargc=1; char* fakeargv[fakeargc];
		TRint *theApp = new TRint("App", &fakeargc, fakeargv,0,0,1);
		gStyle->SetOptStat(kTRUE);
		gStyle->SetOptFit(1111);
		
		std::vector<std::pair<TFitResultPtr, Calibration>> results;
		for (auto i : ranges) { results.push_back(fitmany(i, false)); } //Capire perché non funziona if (draw) ...
		
		std::cerr << results.size() << '\t' << std::boolalpha << results[0].second.calibrate << std::endl;
		
		if(info->contains("calibrate")) {
			std::ofstream retta("calib_line");
			for(std::vector<std::pair<TFitResultPtr,Calibration>>::size_type i=0; i < results.size(); ++i){
				if(results[i].second.calibrate == true ) {
					//STO PRENDENDO RADICE DI DUE LOG DUE PER SIGMA COME ERRORE! SONO UN CANE
					retta << results[i].first->GetParams()[1] <<  '\t' << results[i].second.expected << '\t' << results[i].first->GetParams()[2]*(sqrt(2.0*log(2.0)))<< '\t' << results[i].second.expected_err <<  std::endl;
				}
			}
				
			TFile *fcalib=new TFile("calib_line.root","RECREATE"); // output file for calibration
			fcalib->cd();
			TCanvas* canvascalib = new TCanvas;
			TGraphErrors *plot2 = new TGraphErrors("calib_line"); 
			plot2->SetTitle("Calibrazione");
			gStyle->SetStatX(0.5); gStyle->SetStatY(0.9);
			TF1* calibfunc = new TF1("linear","[0]+[1]*x");
			TFitResultPtr fitresult, &fitres = fitresult;
			calibfunc->SetParNames("A","B");
			fitres = plot2->Fit("linear","S");
			calibfunc->Write();
			fitres->Write();
			plot2->Draw("AL*");
			plot2->Write();
			canvascalib->SaveAs("calib_line.png");
			fcalib->Write();
			
			//SALVO I PARAMETRI DEL FIT IN UN FILE
			std::ofstream conversionparameters("calibrationparams");
			conversionparameters << calibfunc->GetParameter("A") << '\t' << calibfunc->GetParameter("B") << std::endl;
			conversionparameters.close();
			
			// GRAFICO DEI RESIDUI
			TCanvas *residuals = new TCanvas("Residui");
			TGraphErrors *gr = new TGraphErrors("calib_line"); // parte da una copia del grafico originale
			for (int i=0; i<plot2->GetN(); i++) {
				double res = plot2->GetY()[i] - calibfunc->Eval(plot2->GetX()[i]); // residuo
				gr->SetPoint(i,plot2->GetX()[i],res);
				double eresy = plot2->GetEY()[i]; // contributo delle Yi
				double eresx = calibfunc->Derivative(plot2->GetX()[i])*plot2->GetEX()[i]; // contrib. Xi (approx. 1 ordine)
				double eres = TMath::Sqrt(eresy*eresy+eresx*eresx);
				gr->SetPointError(i,0,eres);
			}
			gr->SetName("Grafico dei residui");
			gr->SetTitle("Grafico dei residui");
			gr->Draw("Ap*");
			gr->Write();
			residuals->Write();
			residuals->SaveAs("calib_line_residuals.png");	
		}
		
		
		theApp->Run();
		
		
	}
	return 0;
}















	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
