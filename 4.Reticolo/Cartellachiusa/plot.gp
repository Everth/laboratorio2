reset
set term postscript eps color
set fit errorvariables
set encoding iso_8859_1
set zeroaxis
d = 12.65e-6
d_err = 0.05e-6
#colori = "#9400D3 #0000CD #0000FF #008000 #FF0000"
#colori lambda = "#0005FF #008EFF #00CCFF #00FF0C #FF0300"
colori = "#7F00FF #0000BB #00CCFF #00CC00 #CC0000"
nomi = "Violetto Blu2 Blu1 Verde Rosso"
attesi = "441.6 467.8 480.0 508.6 643.8"
offset = real( system("awk 'NR==1' offset | grep -E -o '= -?[[:digit:]]+(.[[:digit:]]+)?' | sed 's/= //' ") )
offset_err = real( system("awk 'NR==3' offset | grep -E -o '= -?[[:digit:]]+(.[[:digit:]]+)?' | sed 's/= //' ") )
th_err = 0.000290888/sqrt(6)
sinth_err(x) = abs(cos(x)*th_err)
thmean(a,b,c,d) = ((315.0-(a+b/60.0))*pi/180 + (135.0+1.0/6-(c+d/60.0))*pi/180)/2
set xlabel "Ordine" 
set ylabel "Sin {/Symbol q}" offset 3
f(x) = A + B*x
set print "tabella_fit_old.tex"
print sprintf("\\emph{Colore}&$A$&$\\sigma_{A}$&$B$&$\\sigma_{B}$&$\\chi^2$&$\\emph{NDF}$&$\\emph{Compatibilità con l'origine}$&$K$ \\\\ \\hline")
do for [i=1:words(nomi)]{
set out word(nomi,i).".eps"
fit f(x) "center1" every :::(i-1)::(i-1) using 5:(sin(thmean(($1),($2),($3),($4)))):(sinth_err(thmean(($1),($2),($3),($4)))) via A,B
comp_zero_old = abs(A)/sqrt(A_err**2+th_err**2)
k = sqrt(FIT_WSSR/FIT_NDF)
stats "center1" using 5 every :::(i-1)::(i-1) nooutput
A_old = A
A_err_old = A_err
B_old = B
B_err_old = B_err
WSSR_old = FIT_WSSR
NDF_old = FIT_NDF
print sprintf("\\emph{%s}&%f&%f&%f&%f&%f&%d&%f&%f \\\\",word(nomi,i), A_old, A_err_old, B_old, B_err_old, WSSR_old, NDF_old, comp_zero_old,k)
fit f(x) "center1" every :::(i-1)::(i-1) using 5:(sin(thmean(($1),($2),($3),($4)))):(sinth_err(thmean(($1),($2),($3),($4)))*k) via A,B
plot [STATS_min-0.5:STATS_max+0.5] "center1" every :::(i-1)::(i-1) using 5:(sin(thmean(($1),($2),($3),($4)))):((sinth_err(thmean(($1),($2),($3),($4))))*k) w yerror pt 0 ps 1 lc rgb word(colori,i) title word(nomi,i) , f(x) lc rgb word(colori,i) title sprintf("%f + %f*m",A,B)
comp_zero_k = abs(A)/sqrt((k*A_err)**2+th_err**2)
print sprintf("\\emph{%s con K}&%f&%f&%f&%f&%f&%d&%f& \\\\",word(nomi,i), A, k*A_err, B, k*B_err, FIT_WSSR, FIT_NDF, comp_zero_k)
#set table word(nomi,i).".dat"
#plot "center1" every :::(i-1)::(i-1) using 5:(thmean(($1),($2),($3),($4))):(sin(thmean(($1),($2),($3),($4)))):(sinth_err(thmean(($1),($2),($3),($4)))):(1) w xyerror
#unset table
#save variables word(nomi,i).".fit"
}
#set print "tabella_fit_J.tex"
#print sprintf("\\emph{Colore}&$A$&$\\sigma_{A}$&$B$&$\\sigma_{B}$&$\\chi^2$&$\\emph{NDF}$&$\\emph{S}$&$K$&$\\lambda [nm]$&$\\sigma_{\\lambda} [nm]$ \\\\ \\hline")
set print "lambda_J.tex"
print sprintf("\\emph{Colore con \\tilde{k}}&$\\lambda [nm]$&$\\sigma_{\\lambda} [nm]$&\\emph{Compatibilità} \\\\ \\hline")
th_err = sqrt(0.000290888**2/6+offset_err**2)
do for [i=1:words(nomi)]{
set out word(nomi,i)."_corr.eps"
stats "center1" using 5 every :::(i-1)::(i-1) nooutput
fit f(x) "center1" every :::(i-1)::(i-1) using 5:(sin(thmean(($1),($2),($3),($4))-offset)):(sinth_err(thmean(($1),($2),($3),($4))-offset)) via A,B
comp_zero_j = abs(A)/sqrt(A_err**2+0.000290888**2/6)
lambda = d*B
lambda_err = lambda*sqrt((d_err/d)**2+(B_err/B)**2)
#print sprintf("\\emph{%s con \\jmath}&%f&%f&%f&%f&%f&%f&%f&%f&%f&%f \\\\",word(nomi,i), A, A_err, B, B_err, FIT_WSSR, FIT_NDF, comp_zero_j,k,lambda*10**9,lambda_err*10**9)
k = sqrt(FIT_WSSR/FIT_NDF)
comp_zero_j_k = abs(A)/sqrt((k*A_err)**2+0.000290888**2/6)
lambda_err_k = lambda*sqrt((d_err/d)**2+(k*B_err/B)**2)
#print sprintf("\\emph{%s con \\jmath}&%f&%f&%f&%f&%f&%f&%f&&&%f \\\\",word(nomi,i), A, k*A_err, B, k*B_err, FIT_NDF, FIT_NDF, comp_zero_j_k,lambda_err_k*10**9)
set ylabel "Sin {/Symbol q\052}" offset 3
print sprintf("\\emph{%s}&%f&%f&%f \\\\",word(nomi,i),lambda*10**9,lambda_err_k*10**9,abs(lambda*10**9-word(attesi,i))/(lambda_err_k*10**9))
plot [STATS_min-0.5:STATS_max+0.5] "center1" every :::(i-1)::(i-1) using 5:(sin(thmean(($1),($2),($3),($4))-offset)):(sinth_err(thmean(($1),($2),($3),($4))-offset)*k) w yerror pt 0 ps 1 lc rgb word(colori,i) title word(nomi,i) , f(x) lc rgb word(colori,i) title sprintf("%f + %f*m",A,B)
#set table word(nomi,i)."_J.dat"
#plot "center1" every :::(i-1)::(i-1) using 5:(sin(thmean(($1),($2),($3),($4))-offset)):(sinth_err(thmean(($1),($2),($3),($4))-offset)):(1) w yerror
#unset table
}
# [STATS_min-0.5:STATS_max+0.5]"center1" every :::(i-1)::(i-1) using 5:(sin(thmean(($1),($2),($3),($4))-offset)):(sprintf("%f",(sinth_err(thmean(($1),($2),($3),($4)))))) w labels notitle
