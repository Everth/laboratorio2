#include <iostream>
#include <dirent.h>
#include <cmath>
#include <vector>
#include <string>
#include <fstream>
#include <iterator>
#include <iomanip>
#include <regex>
#include <sstream>
#include <typeinfo>

double valormedio(std::vector<double> a){
	double sum=0;
	int campione=a.size();
	for(double i:a){
	sum+=i;
		}
	return (sum+0.0)/campione;
};

double stdev(std::vector<double> a){
	double variance;
	double vv=valormedio(a);
	for(double i:a){
		variance+= pow((i-vv),2); }
		//Uso size-1 perché è la stima più precisa
		return sqrt(variance/(a.size()-1));
}

std::vector<std::string> read_directory( const std::string& path, std::string reg = ".*") {
	std::vector <std::string> result;
	std::regex reg1(reg.c_str());
	struct dirent* de;
	DIR* dp;
	dp = opendir( path.empty() ? "." : path.c_str() );
	if (dp) {
		while ((de = readdir(dp))) {
			if (de == nullptr) break;
			if (de->d_name[0]=='.' )  {
				std::cerr << de->d_name << " skipped" << std::endl;
				continue;
				} else {
					if(std::regex_match(de->d_name,reg1)){
						result.push_back( std::string(de->d_name) );
					} else {
						std::cerr << de->d_name << " not maching"<< std::endl;
					}
			}
		}
	closedir( dp );
	std::sort( result.begin(), result.end() );
	} else {
	std::cerr<< "Directory non aperta" << std::endl;
	}
	return result;
}

struct decpoint : std::numpunct<char> {
    char do_decimal_point()   const { return '.'; }
};

double mean_stdev(std::vector<double> a){
	double variance;
	double vv=valormedio(a);
	for(double i:a){
		variance+= pow((i-vv),2.0); }
		return (sqrt(variance/(a.size()-1)))/sqrt(a.size());
};

struct offset_colore{
	std::vector<double> offset;
	std::string color_name;
};

main(){
	std::string namefmt= ".*\\.dat";
	std::regex rgx(namefmt);
	std::string _DIR_;
	std::cerr << _DIR_.empty() << std::endl;
	std::vector<std::string> nome_files = read_directory(_DIR_,namefmt);
	std::vector<double> offset_singoli;
	std::vector<offset_colore> offset_gruppi;
	for(int i = 0; i < nome_files.size(); ++i){
		std::ifstream input(_DIR_ + nome_files.at(i));
		if(!input.is_open()) {
			std::cerr << "Impossibile aprire il file " << nome_files.at(i) << std::endl;
			return 1;
		}
		//Prendo il numero delle fenditure
		std::smatch match;
		std::string colore;
		std::string &COL = colore;
		offset_colore temporaneo;
		if(std::regex_search(nome_files.at(i), match, rgx)){
			std::regex_match(nome_files.at(i), match, rgx);
			COL = match[0];
			std::cerr << "COLORE = " << COL << std::endl;
		}
		temporaneo.color_name = colore;
		//Riempio i vettori con i dati
		int ncol = 4;
		std::istream_iterator<double> start(input), end;
		std::vector<double> numbers(start, end);
		std::vector<std::vector<double>> temp(ncol);
		for (std::vector<double>::size_type i=0 ; i< numbers.size(); ++i){
			temp[i%ncol].push_back(numbers.at(i));
		}
		for(std::vector<double>::size_type t = 0; t < temp[1].size()/2; ++t){
			temporaneo.offset.push_back((temp[1][t]+temp[1][temp[1].size()-t-1])/2);
			std::cerr << temp[0][t] << '\t' << temp[0][temp[1].size()-t-1] << std::endl;
			offset_singoli.push_back((temp[1][t]+temp[1][temp[1].size()-t-1])/2);
		}
		offset_gruppi.push_back(temporaneo);
	}
	std::ofstream out("offset");
	out << std::fixed << "Media spostamenti = " << valormedio(offset_singoli) << "\nDeviazione standard = " << stdev(offset_singoli) << "\nErrore della media = " << mean_stdev(offset_singoli) << std::endl;
	return 0;
}































